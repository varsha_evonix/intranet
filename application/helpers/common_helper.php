<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function uploadFile($img,$elementName='file')
    {

		$CI =& get_instance();

		$dt = '';

       //$user_id = $this->session->userdata('user_id'); 
			//$CI->session->userdata('is_logged_in');
       $user_id = $CI->session->userdata('user_id');;

      $ext = pathinfo($img, PATHINFO_EXTENSION);

		$dt = time().uniqid();

        $filename = $dt.'.'.$ext;

        $tileFileName = 'tile_'.$dt.'.'.$ext;

		$fn = $elementName;		

       

        if (!is_dir('uploads'))

        {

            mkdir('./uploads', 0777, TRUE);

        }

       

        $user_folder = 'excelfile'; // User folder name

        if (!is_dir('uploads/'.$user_folder))

        {

            mkdir('./uploads/'.$user_folder, 0777, TRUE);

        }

        $destination_image_path = './uploads/'.$user_folder.'/';

		//$destination_image_path = './uploads/';
       

        $config['upload_path'] = $destination_image_path;

        $config['allowed_types'] = 'csv';

        $config['max_size']    = '1000000';

        $config['max_width']  = '10024';

        $config['max_height']  = '7068';

        $config['file_name'] = $filename;

        $CI->load->library('upload', $config);

        $CI->upload->initialize($config);



        if ( ! $CI->upload->do_upload($fn))

        {

           $error = array('error' => $CI->upload->display_errors());

			
          
            //echo '<pre>';
			/*print_r($error); 
            exit; */

            //echo 502; exit;

        }

        else

        {

            $inputFile = $_SERVER['DOCUMENT_ROOT'].'/'.$CI->config->item('appname').'uploads/'.$filename;

			$outputFile = $_SERVER['DOCUMENT_ROOT'].'/'.$CI->config->item('appname').'uploads/'.$tileFileName;

			//if(imgResize($inputFile,$outputFile))

				return $filename;

        }

    }
 ?>   
