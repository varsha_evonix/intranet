<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('fetch_model');
        $this->load->model('insert_model');
        $this->load->model('update_model');
        $this->load->model('delete_model');
    }

    public function check_session($role = '') {
        if (!empty($this->session->userdata('role'))) {
            if ($this->session->userdata('role') == $role) {
                return true;
            } else {
                redirect('welcome/noauthority');
            }
        } else {
            redirect('welcome/login');
        }
    }

    public function main_checkAuthority($ptid) {
        return $this->fetch_model->ListAuthority($ptid);
    }

    public function main_checkDegree($degreename) {
        return $this->fetch_model->CheckDegreeName($degreename);
    }

    public function main_checkQualificationYear($sid, $s_selectedQualification) {
        return $this->fetch_model->checkQualificationYear($sid, $s_selectedQualification);
    }

    public function main_addEducationName($eduName) {
        return $this->insert_model->addEducationName($eduName);
    }

    public function main_addDomain($data) {
        return $this->insert_model->addDomain($data);
    }

    public function batch_list_pt($ptid) {
        return $this->fetch_model->batch_list_pt($ptid);
    }

    public function get_all_batch_list() {
        return $this->fetch_model->get_all_batch_list();
    }

    public function get_batch_list_pt($ptid) {
        return $this->fetch_model->get_batch_list_pt($ptid);
    }

    //-------------------------SANJAY ----------------------------------------------------
// new CODE //
    // admin information

    public function admin_information_main($cid) {
        return $this->fetch_model->admin_information($cid);
    }

    public function changePlacementTeamStatus_main($data, $ptid) {
        return $this->update_model->changePlacementTeamStatus($data, $ptid);
    }

//
// ADMIN
    // Start Manage Groups
    public function creategroup_function($info) { //forword to admin dashboard
        if ($this->insert_model->insertGroup($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function managegroups_function() {
        $groupalldata = $this->fetch_model->getallgroups();
        return $groupalldata;
    }

    public function managegroups_function1() {
        $groupalldata = $this->fetch_model->getallgroups1();
        return $groupalldata;
    }

    public function group_function($groupid) {
        $groupdata = $this->fetch_model->getgroup($groupid);
        return $groupdata;
    }

    public function updategroup_function($info) { //forword to admin dashboard
        if ($this->update_model->updateGroup($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function deletegroup_main($groupid) {
        return ($this->delete_model->deletegroup($groupid));
    }

    // End Manage Groups
    // Start Manage Teams

    public function createteam_function($info) {
        //echo "createteam_function"; exit;
        if ($this->insert_model->insertTeam($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function createteam_function_new($info) {
        //echo "createteam_function"; exit;
        if ($this->insert_model->insertTeam_new($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function createteam_admin_function($info) {
//echo "createteam_function"; exit;
        if ($this->insert_model->insertTeam_admin_new($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function studentbatchdata_function() {

        $studentbatchdata = $this->fetch_model->getallstudentbatch();
        return $studentbatchdata;
    }

    public function studentdata_function() {

        $studentdata = $this->fetch_model->getallstudent();
        return $studentdata;
    }

    public function facultydata_function() {

        $facultydata = $this->fetch_model->getallfaculty();
        return $facultydata;
    }

    public function guestuserdata_function() {

        $guestuserdata = $this->fetch_model->getallguestuser();
        return $guestuserdata;
    }

    public function manageplteam_function() {
        $plteamdata = $this->fetch_model->getallplteam();
        return $plteamdata;
    }

    public function plteam_function($plteamid) {
        //echo "Asd"; exit;
        $plteam = $this->fetch_model->getplteam($plteamid);
        return $plteam;
    }

    public function updateteam_function($info) { //forword to admin dashboard
        if ($this->update_model->updateTeam($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function plteamgroups_function() {
        $groupalldata = $this->fetch_model->getallplteamgroups();
        return $groupalldata;
    }

    public function assignauthority_function($info) {
        if ($this->insert_model->insertAuthority($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function assignStudents_main($info) {
        if ($this->insert_model->assignStudents($info)) {
            return true;
        } else {
            return false;
        }
    }

    // End Manage Teams
    //-------------------------SANJAY ----------------------------------------------------
    //------------------------------------------------------------------ placement_team


    /* SANJAY */


    /* SANJAY */
    /* Start Manage Job */

    public function addjob_function($info) { //forword to admin dashboard
        $job_id = $this->insert_model->insertJob($info);
        return $job_id;
    }

    public function addselectionprocess_main($sp) {
        if ($this->insert_model->insertselectionprocess($sp)) {
            return true;
        } else {
            return false;
        }
    }

    public function addjobbatch_main($data) {
        if ($this->insert_model->insertjobbatch($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function addjobcriteria_main($c) {
        if ($this->insert_model->insertjobcriteria($c)) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchorgname_function() {
        //echo "asd"; exit;
        $orgname = $this->fetch_model->getorgname();
        return $orgname;
    }

    public function master_selectionprocess_main() {
        return($this->fetch_model->master_selectionprocess());
    }

    public function master_selectionprocess_main1() {
        return($this->fetch_model->master_selectionprocess1());
    }

    public function managejobs_function() {
        $joballdata = $this->fetch_model->getalljobs();
        return $joballdata;
    }

    public function job_function($jobid) {
        return $jobdata = $this->fetch_model->getjob($jobid);
    }

    public function fetchselectionprocess($jobid) {
        $sp = $this->fetch_model->getselectionprocess($jobid);
        if ($sp) {
            return $sp;
        }
    }

    public function jobcriteria1_main($jobid) {
        $jb = $this->fetch_model->jobcriteria1($jobid);
        if ($jb) {
            return $jb;
        }
    }

    public function jobcriteria2_main($jobid) {
        $jb = $this->fetch_model->jobcriteria2($jobid);
        if ($jb) {
            return $jb;
        }
    }

    public function jobcriteria3_main($jobid) {
        $jb = $this->fetch_model->jobcriteria3($jobid);
        if ($jb) {
            return $jb;
        }
    }

    public function jobcriteria4_main($jobid) {
        $jb = $this->fetch_model->jobcriteria4($jobid);
        if ($jb) {
            return $jb;
        }
    }

    public function jobcriteria5_main($jobid) {
        $jb = $this->fetch_model->jobcriteria5($jobid);
        if ($jb) {
            return $jb;
        }
    }

    public function get_job_batch_main($jobid) {
        return $this->fetch_model->get_job_batch($jobid);
    }

    public function updatejob_functionadmin($info) {
        if ($this->update_model->updateJobAdmin($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function updatejob_function($info) {
        if ($this->update_model->updateJob($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function deletejob_main($jobid) {
        return ($this->delete_model->deletejob($jobid));
    }

    public function deleteselectionprocess_main($jobid) {
        return ($this->delete_model->deleteSelectionProcess($jobid));
    }

    public function deletejobcriteria_main($jobid) {
        return ($this->delete_model->deleteJobCritera($jobid));
    }

    public function deletebatchjob_main($jobid) {
        return ($this->delete_model->deleteBatchJob($jobid));
    }

    public function viewjobs_function($sid) {
        $joballdata = $this->fetch_model->getjobs($sid);
        return $joballdata;
    }

    public function applyjob_function($data) {
        if ($this->insert_model->applyjob($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function getApplications_main($jobid) {
        $jobdata1 = $this->fetch_model->getApplications($jobid);
        return $jobdata1;
    }

    public function getApplications_function() {
        $jobdata1 = $this->fetch_model->getallApplications();
        return $jobdata1;
    }

    public function fetchjobselectionprocess($jobid) {
        $data = $this->fetch_model->fetchjobselection($jobid);
        return $data;
    }

    public function fetchjobsel() {
        $data = $this->fetch_model->fetchjobselection1();
        return $data;
    }

    public function savestatus_main($status, $sid, $jobid) {
        if ($this->update_model->saveStatus($status, $sid, $jobid)) {
            return true;
        } else {
            return false;
        }
    }

    public function changeJobStaus_main($info, $jobid) {
        return $this->update_model->changeJobStaus($info, $jobid);
    }

    public function master_jobtype_main() {
        return($this->fetch_model->master_jobtype());
    }

    /* End Manage Job */
    /* Start Manage Event */

    public function fetcheventname_function() {
        //echo "asd"; exit;
        $orgname = $this->fetch_model->geteventname();
        return $orgname;
    }

    /* End Manage Event */

    /* SANJAY */

//-------------------------VIMAL ----------------------------------------------------
// ------manage student ------------

    public function main_changeStudentStatus($sid) {
        return($this->update_model->changeStudentStatus($sid));
    }

//---------- end of manage student
//---------- start form ------------------
    public function formsList_main($fid) {
        return($this->fetch_model->formsList($fid));
    }

    public function UpdateFormsData_main($data, $formId) {
        return($this->update_model->UpdateFormsData($data, $formId));
    }

//---------- end form ------------------
//---------start change password-----------------
    // change password from student profile
    public function checkOldPwd_main($pwd, $sid) {
        return($this->fetch_model->checkOldPwd($pwd, $sid));
    }

    public function changePassword_main($data, $cid) {
        return($this->update_model->changePassword($data, $cid));
    }

//---------end change password-----------------	
//------------------------------start company -----------------------------


    public function main_checkVerticle($verticleName) {
        return $this->fetch_model->checkVerticle($verticleName);
    }

    public function createcompany_main($data) { // insert company
        if ($this->insert_model->create_company($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function displaycompany_main() { // display all companies
        $data = $this->fetch_model->displaycompany();
        if ($data) {
            return $data;
        }
    }

    public function displaycompanycontact_main() { // display all companies
        $contactdata = $this->fetch_model->displaycompanycontact();
        if ($contactdata) {
            return $contactdata;
        }
    }

    public function displaycipdata_main($tabname) { // display all companies
        $contactdata = $this->fetch_model->displaycipdata($tabname);
        if ($contactdata) {
            return $contactdata;
        }
    }

    public function displaycipdetails_main() { // display all companies
        $contactdata = $this->fetch_model->displaycipdetails();
        if ($contactdata) {
            return $contactdata;
        }
    }

    public function fatchcompany_main($cid) { // display $cid company
        $data = $this->fetch_model->fatchcompany($cid);
        if ($data) {
            return $data;
        }
    }

    public function companyaddress_main($cid) { //display $cid's address
        $data = $this->fetch_model->companyaddress($cid);
        if ($data) {
            return $data;
        }
    }

    public function companycontact_main($cid) { //display $cid's contacts
        $data = $this->fetch_model->companycontacts($cid);
        if ($data) {
            return $data;
        }
    }

    public function addcompanyinfo_main($data) {
        $comid = $this->insert_model->addcompanyinfo($data);
        if ($comid) {
            return $comid;
        } else {
            return false;
        }
    }

    public function addcompanyaddress_main($data) {
        $comid = $this->insert_model->addcompanyaddress_main($data);
        if ($comid) {
            return $comid;
        } else {
            return false;
        }
    }

    public function addcompanycontacts_main($data) {
        $comid = $this->insert_model->addcompanycontacts_main($data);
        if ($comid) {
            return $comid;
        } else {
            return false;
        }
    }

    public function editcompanyinfo_main($data, $cid) {
        return ($this->update_model->editCompanyInfo($data, $cid));
    }

    public function deletecompanyaddress_main($cid) {
        return ($this->delete_model->deleteCompanyAddress($cid));
    }

    public function deletecompanycontacts_main($cid) {
        return ($this->delete_model->deleteCompanyContacts($cid));
    }

    public function deletecompanyinfo_main($cid) {
        return ($this->delete_model->deleteCompanyInfo($cid));
    }

//------------------------------end company -----------------------------
//--------------------------------------start STUDENT------------------------------------------


    public function forms_main($sid) {
        return($this->fetch_model->forms($sid));
    }

    public function student_information_main($sid) {
        return($this->fetch_model->basicInformation($sid));
    }

    public function student_addLocal_main($sid) {
        return($this->fetch_model->localAddress($sid));
    }

    public function student_addPerm_main($sid) {
        return($this->fetch_model->permanentAddress($sid));
    }

    public function student_education_main($sid) {
        return($this->fetch_model->education($sid));
    }

    public function delete_address_main($cid) {
        return($this->delete_model->address($cid));
    }

    public function insert_address_main($data) {
        return($this->insert_model->address($data));
    }

    public function master_education_main($sid) {
        return($this->fetch_model->master_education($sid));
    }

    public function master_domain_main() {
        return($this->fetch_model->master_domain());
    }

    public function addEducation_main($data) {
        return($this->insert_model->addStudEducation($data));
    }

    public function student_professional_main($sid) {
        return($this->fetch_model->studentExperience($sid));
    }

    public function addExperience_main($data) {
        return($this->insert_model->addStudExperience($data));
    }

    public function student_specialization_main($sid) {
        return($this->fetch_model->studentSpecialization($sid));
    }

    public function addSpecialization_main($data) {
        return($this->insert_model->addStudSpecialization($data));
    }

    public function addCerification_main($data) {
        return($this->insert_model->addStudCertification($data));
    }

    public function student_additionalCourse_main($sid) {
        return($this->fetch_model->studentAdditionalCourse($sid));
    }

    public function addAdditionalCourse_main($data) {
        return($this->insert_model->addAdditionalCourse($data));
    }

    public function student_projects_main($sid) {
        return($this->fetch_model->studentProjects($sid));
    }

    public function addProjects_main($data) {
        return($this->insert_model->addProjects($data));
    }

    public function student_resposiblity_main($sid) {
        return($this->fetch_model->studentResponsiblity($sid));
    }

    public function addResponsiblity_main($data) {
        return($this->insert_model->addResponsiblity($data));
    }

    public function student_awards_main($sid) {
        return($this->fetch_model->studentAwards($sid));
    }

    public function addAwards_main($data) {
        return($this->insert_model->addAwards($data));
    }

    public function student_skills_main($sid) {
        return($this->fetch_model->studentSkills($sid));
    }

    public function student_hobbies_main($sid) {
        return($this->fetch_model->studentHobbies($sid));
    }

    public function student_extraActivity_main($sid) {
        return($this->fetch_model->student_extraActivity($sid));
    }

    public function addSkills_main($data) {
        return($this->insert_model->addSkills($data));
    }

    public function addHobbies_main($data) {
        return($this->insert_model->addHobbies($data));
    }

    public function addExtraActivity_main($data) {
        return($this->insert_model->addExtraActivity($data));
    }

//========== PLACEMENT TEAM + STUDENT (CHANGE STATUS & DELETE from profile )================

    public function updateSinfo_main($data, $sid) {
        return($this->update_model->updateStudentInfo($data, $sid));
    }

    // original
    /* public function pt_changeStatus_main($sid,$eid,$data,$page)
      {
      //print_r($eid); exit;
      //print_r($data); exit;
      //echo $page; exit;
      return($this->update_model->pt_changeStatus($sid,$eid,$data,$page));
      } */

    public function pt_changeStatus_main($sid, $eid, $data, $page) {
        //echo $sid; exit;
        //print_r($eid); exit;
        //print_r($data); exit;
        //echo $page; exit;
        return($this->update_model->pt_changeStatus($sid, $eid, $data, $page));
    }

    public function pt_deleteStudProfileInfo_main($eid, $page) {
        return($this->delete_model->pt_deleteStudProfileInfo($eid, $page));
    }

    public function std_changeStatus_main($eid, $data, $page) {
        return($this->update_model->std_changeStatus($eid, $data, $page));
    }

    public function batch_specialization_main($sid) {
        return($this->fetch_model->batch_specializationStudent($sid));
    }

    public function fatch_detail_main($page, $id) {
        return($this->fetch_model->fatch_detail($page, $id));
    }

//--------------------------------------end STUDENT------------------------------------------
//-------------------------VIMAL ----------------------------------------------------
//======================================= MANASI ============================================================
    public function batch($cname, $terms, $enabled, $bname, $info) {
        $this->load->model('insert_model');
        if ($this->insert_model->course_insert($cname, $terms, $enabled, $bname, $info)) {
            return true;
        } else {
            return false;
        }
    }

    public function view_Batch() {

        $this->load->model('fetch_model');
        return ($this->fetch_model->displayBatch());


        // $this->batch_list($data);
        //  return $data;
    }

    public function delete_batch($batch_course_id) {
        $this->load->model('fetch_model');
        if ($this->fetch_model->DeleteBatch($batch_course_id)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_Batches($batch_course_id) {

        $check = $this->delete_model->deletebatches1($batch_course_id);
        if ($check) {
            return true;
        } else {
            return false;
        }
    }

    public function displaystudents($bcid) {
        $this->load->model('fetch_model');
        return $this->fetch_model->studlist1($bcid);
    }

    public function pt_allocated_studentInfo_main($bcid, $ptid) {
        return $this->fetch_model->pt_allocated_studentInfo($bcid, $ptid);
    }

    public function allocated_studentInfo_main($bcid) {
        return $this->fetch_model->allocated_studentInfo($bcid);
    }

    public function get_placementstatus() {
        return($this->fetch_model->get_placementstatus());
    }

    public function saveplacementstatus($optId, $studid, $reason) {
        if ($this->insert_model->savestatus($optId, $studid, $reason)) {
            return true;
        } else {
            return false;
        }
    }

    public function guestuser_allocated_studentInfo_main($bcid, $guestuserid) {
        return $this->fetch_model->guestuser_allocated_studentInfo($bcid, $guestuserid);
    }

    public function studs_delete($ids) {
        return $this->delete_model->deleteStud1($ids);
    }

    public function Change_batch($bcnew, $bcid) {
        $new = $this->update_model->change_batches($bcnew, $bcid);
        if ($new == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public function edit_Batch($batch_id, $course_id) {
        $this->load->model('fetch_model');
        $batchdata = $this->fetch_model->getbatch($batch_id, $course_id);

        return $batchdata;
    }

    public function changecourse() {
        $this->load->model('fetch_model');
        $batchdata = $this->fetch_model->getcourselist();

        return $batchdata;
    }

    public function batch_List() {
        $this->load->model('fetch_model');
        $batchdata = $this->fetch_model->getbatchlist();
        return $batchdata;
    }

    public function updatebatch_function($data, $info, $courseid) {
        $this->load->model('update_model');
        if ($this->update_model->updatebatch($data, $info, $courseid)) {
            return true;
        } else {
            return false;
        }
    }

    public function insert_student_main($credentials) {
        $this->load->model('insert_model');
        $this->insert_model->insert_student($credentials);
        $credid = $this->db->insert_id();
        $this->insert_student1($credid);
    }

    public function insert_student1_main($data, $bname1) {
        $this->load->model('insert_model');
        if ($this->insert_model->studentinfo($data, $bname1)) {
            return true;
        } else {
            return false;
        }
    }

    /////----FACULTY-------///
    public function insert_faculty_main($credentials) {
        $this->load->model('insert_model');
        $this->insert_model->insert_faculty($credentials);
        $credid = $this->db->insert_id();
        $this->insert_faculty1($credid);
    }

    public function insert_faculty1_main($data) {
        $this->load->model('insert_model');
        if ($this->insert_model->facultyinfo($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function Get_faculty() {
        return $this->fetch_model->get_faculty();
    }

    public function edit_faculty($fid) {
        $batchdata = $this->fetch_model->editfaculty($fid);
        return $batchdata;
    }

    public function updatefaculty_function($data) {
        if ($this->update_model->updatefac($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function fac_delete($fid) {
        return ($this->delete_model->deleteFac($fid));
    }

    ///-------FACULTY----//
    ///----------BULK FACULTY------------//
    public function Insert_faculty($insert_data1) {
        return $this->insert_model->insert_faculty_bulk($insert_data1);
    }

    //////-BULK FACULTY---------------------------------------------------------

    /* public function viewstudent()
      {
      $this->load->model('fetch_model');
      return($this->fetch_model->studlist());
      } */

    /////----GUEST_USER-------///

    public function guest_information_main($cid) {
        return $this->fetch_model->guest_information($cid);
    }

    public function insert_guestuser_main($credentials) {
        $this->load->model('insert_model');
        $this->insert_model->insert_guestuser($credentials);
        $credid = $this->db->insert_id();
        $this->insert_guestuser1($credid);
    }

    public function insert_guestuser1_main($data) {
        $this->load->model('insert_model');
        if ($guid = $this->insert_model->guestuserinfo($data)) {
            return $guid;
        } else {
            return false;
        }
    }

    public function Get_guestuser() {
        $guestuser = $this->fetch_model->get_guestuser();
        return $guestuser;
    }

    public function edit_guestuser($fid) {
        $batchdata = $this->fetch_model->editguestuser($fid);
        return $batchdata;
    }

    public function updateguestuser_function($data) {
        if ($this->update_model->updateguestuser($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function guestuser_delete($fid) {
        return ($this->delete_model->deleteguestuser($fid));
    }

    public function ppmreport_delete($sid) {
        return ($this->delete_model->deleteppmreport($sid));
    }

    public function assignguestuserStudents_main($info) {

        if ($this->insert_model->assignguestuserStudents($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function getassignedstudentlist_function($guest_user_id) {
        $joballdata = $this->fetch_model->getassignedstudentlist($guest_user_id);
        return $joballdata;
    }

    public function studentreport_function($info) { //forword to admin dashboard
        if ($this->insert_model->insertstudentreport($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function guest_student_report1($stud_id, $guest_user_id) {
        $alldata = $this->fetch_model->get_guest_student_report1($stud_id, $guest_user_id);
        return $alldata;
    }

    public function guest_student_report2($stud_id, $guest_user_id) {
        $alldata = $this->fetch_model->get_guest_student_report2($stud_id, $guest_user_id);
        return $alldata;
    }

    public function student_report1($stud_id) {
        $alldata = $this->fetch_model->get_student_report1($stud_id);
        return $alldata;
    }

    public function student_report2($stud_id) {
        $alldata = $this->fetch_model->get_student_report2($stud_id);
        return $alldata;
    }

    /////----GUEST_USER-------///


    public function create_course($data) {
        $this->load->model('insert_model');
        if ($this->insert_model->insert_course($data)) {
            return true;
        } else {
            return false;
        }
    }

    //madhuri 22/4/16
    public function create_news($data) {
        $this->load->model('insert_model');
        if ($this->insert_model->insert_news($data)) {
            return true;
        } else {
            return false;
        }
    }

    //madhuri 25/4/16
    public function viewnews() {
        $this->load->model('fetch_model');
        return($this->fetch_model->newslist());
    }

    public function edit_news($news_id) {
        $this->load->model('fetch_model');
        $coursedata = $this->fetch_model->getnews($news_id);

        return $coursedata;
    }

    /// madhuri end 
    public function viewcourse() {
        $this->load->model('fetch_model');
        return($this->fetch_model->courselist());
    }

    public function delete_course($course_id) {


        $this->load->model('delete_model');
        if ($this->delete_model->deleteCourse($course_id)) {
            return true;
        } else {
            return false;
        }
    }

    public function edit_course($course_id) {
        $this->load->model('fetch_model');
        $coursedata = $this->fetch_model->getcourse($course_id);

        return $coursedata;
    }

    public function updatecourse_function($data) {

        $this->load->model('update_model');
        if ($this->update_model->updatecourse($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function specialization($sname, $cname) {
        $this->load->model('insert_model');
        if ($this->insert_model->insert_specialization($sname, $cname)) {
            return true;
        } else {
            return false;
        }
    }

    public function viewspl() {
        $this->load->model('fetch_model');
        return($this->fetch_model->spllist());
    }

    public function edit_spl($specialization_id) {
        $this->load->model('fetch_model');
        $batchdata = $this->fetch_model->getspl($specialization_id);

        return $batchdata;
    }

    //by kaushal (20/05/2015)
    public function course_spl($specialization_id) {
        $this->load->model('fetch_model');
        $coursespldata = $this->fetch_model->getcoursespl($specialization_id);

        return $coursespldata;
    }

    public function changebatch_course($batch_id, $course_id) {
        $this->load->model('fetch_model');
        $coursespldata = $this->fetch_model->getbatchcourse($batch_id, $course_id);

        return $coursespldata;
    }

    //end of kaushal
    public function updatespl_function($data) {
        $this->load->model('update_model');
        if ($this->update_model->updatespl($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete_spl($specialization_id) {
        $this->load->model('delete_model');
        if ($this->delete_model->deleteSpl($specialization_id)) {
            return true;
        } else {
            return false;
        }
    }

    public function viewstudents_main($bname1) {
        $this->load->model('fetch_model');
        return $this->fetch_model->studlist($bname1);
    }

    public function displaystudent($bname1) {
        return $this->fetch_model->studlist($bname1);
    }

    public function batchcourse_List() {

        $batchdata = $this->fetch_model->batchcourselist();
        return $batchdata;
    }

    /* public function get_batch_List()
      {

      $batchdata=$this->fetch_model->fetch_get_batch_List();
      return $batchdata;

      } */

    public function edit_stud($sid) {
        $this->load->model('fetch_model');
        $batchdata = $this->fetch_model->editstud($sid);
        return $batchdata;
    }

    public function updatestud_function($data) {
        $this->load->model('update_model');
        if ($this->update_model->updatestud($data)) {
            return true;
        } else {
            return false;
        }
    }

    public function stud_delete($sid) {
        $this->load->model('delete_model');
        return ($this->delete_model->deleteStud($sid));
    }

//======================================= MANASI ============================================================
//==========================================HIMANI==============================================================


    public function Insert_credential($insert_data) {

        return $this->insert_model->insert_credential($insert_data);
    }

    public function Insert_student($insert_data1) {
        return $this->insert_model->insert_student_bulk($insert_data1);
    }

    public function Get_student() {
        return $this->fetch_model->get_student();
    }

    public function Get_batch_course_list() {
        return $this->fetch_model->get_batch_course_list();
    }

    public function Get_course_list() {
        return $this->fetch_model->get_course_list();
    }

    public function Fetch_batch_course($bname1) {
        return $this->fetch_model->fetch_batch_course($bname1);
    }

    public function Insert_batch_student($batch_student) {
        $this->insert_model->insert_batch_student($batch_student);
    }

//============================================================HIMANI=====================================================
// by kaushal 08/06/2015 

    function get_guestuser_purpose() {
        return $this->fetch_model->get_guestuser_purpose();
    }

    function guestuser_purpose($guserid) {
        return $this->fetch_model->guestuser_purpose($guserid);
    }

    public function Get_ppmguestuser() {
        $guestuser = $this->fetch_model->get_ppmguestuser();
        return $guestuser;
    }

    function checkEmailExist($email, $role) {
        return $this->fetch_model->checkEmailExist($email, $role);
    }

    function get_deletedstudent() {
        return $this->fetch_model->get_deletedStudents();
    }

    function stud_add($sid, $abatchid) {
        $this->load->model('update_model');
        return ($this->update_model->addStud($sid, $abatchid));
    }

    function viewdeletedstudents_main($bname1) {
        $this->load->model('fetch_model');
        return $this->fetch_model->deletedstudlist($bname1);
    }

    public function Get_studppmreport() {
        $guestuser = $this->fetch_model->get_studppmreport();
        return $guestuser;
    }

    public function ppm1report_function($info) { //forword to admin dashboard
        if ($this->update_model->updateppm1report($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function ppm2report_function($info) { //forword to admin dashboard
        if ($this->update_model->updateppm2report($info)) {
            return true;
        } else {
            return false;
        }
    }

    public function submitplacementdetails_function($stud_id, $job_id, $package, $location, $joiningdate) { //forword to admin dashboard
        if ($this->insert_model->insertplacementdetails($stud_id, $job_id, $package, $location, $joiningdate)) {
            return true;
        } else {
            return false;
        }
    }

    public function getstudinfo($sid) {
        return $this->fetch_model->getstudinfo($sid);
    }

    public function getjobinfo($sid) {
        return $this->fetch_model->getjobinfo($sid);
    }

}
