<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fetch_model extends CI_Model {

    function ListAuthority($ptid) {

        $query = $this->db->query
                ("SELECT ma.authority_name
							FROM `tbl_authority_group` a 
							join master_authority ma
							on a.`authority_id` = ma.authority_id
							WHERE `placement_team_id` = $ptid");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    function batch($sid) {
        $sid;
        $query = $this->db->query("SELECT DISTINCT (tbc.batch_course_id), mb.batch_name, mc.course_name,mb.out_year

										FROM tbl_batch_course tbc
                                        
										INNER JOIN master_batch mb ON mb.batch_id = tbc.batch_id
                                        
										INNER JOIN master_course mc ON mc.course_id = tbc.course_id
                                        
                              INNER JOIN tbl_batch_student tbs ON tbs.batch_course_id = tbc.batch_course_id
                                        
										WHERE mb.isdelete = 0 and tbs.student_id ='$sid'");

        return $query->result();
    }

    function batch_list_pt($ptid) {
        $query = $this->db->get_where('tbl_pt_students', array('placement_team_id' => $ptid));
        //echo $query->num_rows(); exit;

        if ($query->num_rows() > 0) {

            $query = $this->db->query("SELECT DISTINCT (tbc.batch_course_id), mb.batch_name, mc.course_name
										FROM tbl_batch_course tbc
										INNER JOIN master_batch mb ON mb.batch_id = tbc.batch_id
										INNER JOIN master_course mc ON mc.course_id = tbc.course_id
										INNER JOIN tbl_pt_students tps ON tps.batch_id = tbc.batch_course_id
										WHERE mb.isdelete = 0 and tps.placement_team_id =$ptid");

            return $query->result();
        } else {
            return false;
        }
    }

    function get_all_batch_list() {
        $query = $this->db->query("SELECT DISTINCT (tbc.batch_course_id), mb.batch_name, mc.course_name
										FROM tbl_batch_course tbc
										INNER JOIN master_batch mb ON mb.batch_id = tbc.batch_id
										INNER JOIN master_course mc ON mc.course_id = tbc.course_id
										INNER JOIN tbl_pt_students tps ON tps.batch_id = tbc.batch_course_id
										WHERE mb.isdelete = 0");

        return $query->result();
    }

    function get_batch_list_pt() {
        $query = $this->db->query("SELECT DISTINCT (tbc.batch_course_id), mb.batch_name, mc.course_name
										FROM tbl_batch_course tbc
										INNER JOIN master_batch mb ON mb.batch_id = tbc.batch_id
										INNER JOIN master_course mc ON mc.course_id = tbc.course_id
										INNER JOIN tbl_pt_students tps ON tps.batch_id = tbc.batch_course_id
										WHERE mb.isdelete = 0");

        return $query->result();
    }

    function pt_allocated_studentInfo($batchid, $ptid) {
        /* echo "SELECT s.PRN, s.student_id, s.credentials_id, s.first_name,s.DOB, s.middle_name, s.last_name, s.gender, s.primary_email, c.status, b.batch_course_id, tbc.batch_id, tbc.course_id, mb.batch_name, mc.course_name
          FROM tbl_student_information s, tbl_batch_student b,tbl_credentials c, master_course mc, master_batch mb, tbl_batch_course tbc, tbl_pt_students tps
          WHERE b.batch_course_id = $batchid
          AND s.student_id = b.student_id
          AND c.credentials_id = s.credentials_id
          AND tbc.batch_id = mb.batch_id
          AND tbc.course_id = mc.course_id
          AND b.batch_course_id = tbc.batch_course_id
          and s.isdelete=0
          and c.isdelete=0
          and tps.student_id = s.student_id
          and mc.isdelete=0 and mb.isdelete=0
          and tps.placement_team_id = ".$ptid;
          exit; */
        $q1 = $this->db->query("SELECT s.PRN, s.student_id, s.credentials_id, s.first_name,s.DOB, s.middle_name, s.last_name, s.gender, s.primary_email, c.status, b.batch_course_id, tbc.batch_id, tbc.course_id, mb.batch_name, mc.course_name
						FROM tbl_student_information s, tbl_batch_student b,tbl_credentials c, master_course mc, master_batch mb, tbl_batch_course tbc, tbl_pt_students tps
						WHERE b.batch_course_id = $batchid
						AND s.student_id = b.student_id
						AND c.credentials_id = s.credentials_id
						AND tbc.batch_id = mb.batch_id
						AND tbc.course_id = mc.course_id
						AND b.batch_course_id = tbc.batch_course_id 
						and s.isdelete=0 
						and c.isdelete=0
						and tps.student_id = s.student_id
						and mc.isdelete=0 and mb.isdelete=0
						and tps.placement_team_id = " . $ptid
        );

        return $q1->result();
    }

    function allocated_studentInfo($batchid) {
        $q1 = $this->db->query("select s.PRN, s.student_id, s.first_name, s.middle_name, s.last_name, fs.pl_status, fs.reason
from tbl_student_information s inner join tbl_batch_student bs
on s.student_id = bs.student_id left outer join tbl_finalplacementstatus fs on s.student_id = fs.student_id
where s.isdelete = 0 and bs.batch_course_id = $batchid order by s.PRN");

        return $q1->result();
    }

    function get_placementstatus() {
        $dom = $this->db->get('master_placementstatus');
        if ($dom->num_rows() > 0) {
            return $dom->result();
        } else {
            return null;
        }
    }

    function guestuser_allocated_studentInfo($batchid, $guestuserid) {
        $q1 = $this->db->query("select tgs.*, tsi.PRN, tsi.first_name, tsi.middle_name, tsi.last_name from tbl_guestuser_students tgs inner join tbl_student_information tsi on tgs.student_id = tsi.student_id where tgs.guest_user_id=$guestuserid and batch_id=$batchid");
        return $q1->result();
    }

    function admin_information($cid) {

        $query = $this->db->get_where('tbl_faculty', array('credentials_id' => $cid));

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function CheckDegreeName($degreeName) {


        $query = $this->db->get_where('master_education', array('edcutation_name' => $degreeName));

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function checkQualificationYear($sid, $s_selectedQualification) {

        $query = $this->db->query
                ("SELECT MAX(passing_year) as passing_year ,degree_name
							FROM  tbl_student_educations 
							WHERE  student_id =$sid
							and degree_name < $s_selectedQualification
							GROUP BY  degree_name
							ORDER BY degree_name ASC ");


        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

//.................................SANJAY..................


    function getallgroups1() {
        // $qallgroups = $this->db->get('master_groups');
        // return $qallgroups->result();

        $qallgroups = $this->db->query("SELECT * FROM  master_groups where group_name !='Placement Admin'");
        return $qallgroups->result();
    }

    function getallgroups() {
        // $qallgroups = $this->db->get('master_groups');
        // return $qallgroups->result();

        $qallgroups = $this->db->query("SELECT * FROM  master_groups where group_name !='Placement Admin' and status != 0");
        return $qallgroups->result();
    }

    function getgroup($groupid) {
        $qgroup = $this->db->get_where('master_groups', array('group_id' => $groupid));
        return $qgroup->result();
    }

    function getallstudentbatch() {
        //SELECT master_batch.*, tbl_batch_course.*, master_course.* from master_batch inner join tbl_batch_course on master_batch.batch_id = tbl_batch_course.batch_id inner join master_course on tbl_batch_course.course_id = master_course.course_id
        //SELECT mb.*, tbc.batch_course_id, tbc.course_id, mc.course_name from master_batch mb inner join tbl_batch_course tbc on mb.batch_id = tbc.batch_id inner join master_course mc on tbc.course_id = mc.course_id
        $qallstudentbatch = $this->db->query("SELECT mb.*, tbc.batch_course_id, tbc.course_id, mc.course_name from master_batch mb inner join tbl_batch_course tbc on mb.batch_id = tbc.batch_id inner join master_course mc on tbc.course_id = mc.course_id");
        return $qallstudentbatch->result();
    }

    function getallstudent() {
        $qallfaculty = $this->db->get('tbl_student_information');
        return $qallfaculty->result();
    }

    function getallfaculty() {
        $qallfaculty = $this->db->get('tbl_faculty');
        return $qallfaculty->result();
    }

    function getallguestuser() {
        $qguestuser = $this->db->get('tbl_guest_user');
        return $qguestuser->result();
    }

    function getallplteam() {
        $qallplteam = $this->db->group_by('group_id');
        $qallplteam = $this->db->get('tbl_placement_team');
        return $qallplteam->result();
    }

    function getplteam($plteamid) {
        //echo "asd"; exit;
        $qplteam = $this->db->get_where('tbl_placement_team', array('placement_team_id' => $plteamid));
        //$qplteam = $this->db->query("select * from tbl_placement_team where placement_team_id = ".$plteamid);
        return $qplteam->row();
    }

    function getallplteamgroups() {
//		$qallgroups = $this->db->query('select tpt.placement_team_id, tpt.group_id, tpt.credentials_id, tpt.user_type, mg.group_name from tbl_placement_team tpt inner join master_groups mg on tpt.group_id = mg.group_id where mg.group_name !="Placement Admin" group by tpt.group_id');
        $qallgroups = $this->db->query('SELECT tpt.placement_team_id, tpt.group_id, tpt.credentials_id, tpt.user_type, mg.group_name
								FROM tbl_placement_team tpt
								INNER JOIN master_groups mg ON tpt.group_id = mg.group_id
								WHERE mg.group_name !=  "Placement Admin"
								GROUP BY tpt.user_type, tpt.group_id');

        return $qallgroups->result();
    }

    /* Start Manage Job */

    function getorgname() {
        //echo "xzc"; exit;
        $qorgname = $this->db->get('tbl_company');
        return $qorgname->result();
    }

    function prn($rollno) {

        $query = $this->db->query("select PRN from tbl_student_information where PRN ='$rollno'");

        if ($query->num_rows() > 0) {
            echo 1;
        } else {

            echo 0;
        }
    }

    function getalljobs() {
        // original query  
        $qalljobs = $this->db->query('select tj.job_id,tj.organization_id,tj.offered_profile,tj.last_date_to_applied,tj.status,tc.organization_name FROM tbl_job tj inner join tbl_company tc on tj.organization_id = tc.organization_id where tj.isdelete=0 order by tj.job_id desc');

        //$qalljobs = $this->db->query('select tj.*, tc.organization_name, count(tja.applied_id) as count FROM tbl_job tj inner join tbl_company tc on tj.organization_id = tc.organization_id left outer join tbl_job_applied tja on tj.job_id=tja.job_id where tj.isdelete=0 group by tja.job_id order by tj.job_id desc');
        return $qalljobs->result();
    }

    function getjobs($sid) {
        $jobs = $this->db->query("select tj.*,tjt.*, tc.organization_name,tb.batch_course_id from tbl_job tj,tbl_company tc,tbl_batch_job tb,tbl_batch_student ts,master_job_type tjt where tj.organization_id = tc.organization_id and ts.student_id= $sid and ts.batch_course_id=tb.batch_course_id and tb.job_id=tj.job_id and tj.status=1 and tj.isdelete=0 and tjt.job_type_id = tj.job_type_id group by job_id order by tj.job_id desc");

        return $jobs->result();

        /*
          $jobs = $this->db->query("
          SELECT *
          FROM `tbl_job`tj
          inner join tbl_company tc on tc.organization_id = tj.organization_id
          inner join tbl_batch_job tbj on tbj.batch_course_id = (select batch_course_id from tbl_batch_student where student_id = 42)
          inner join master_job_type tjt on tjt.job_type_id = tj.job_type_id
          where

          tj.status=1 and tj.isdelete=0

          ");
          return $jobs->result(); */
    }

    function get_job_batch($id) {
        $jobs = $this->db->query("select tb.batch_course_id from tbl_batch_job tb where tb.job_id=$id");
        return $jobs->result();
    }

    function getjob($jobid) {


        //$qjob = $this->db->get_where('tbl_job', array('job_id' => $jobid));
        // $qjob = $this->db->query("select tj.*,tb.*,tc.organization_name FROM tbl_job tj ,tbl_company tc,tbl_batch_job tb where tj.organization_id = tc.organization_id and tb.job_id=tj.job_id and tj.job_id = ".$jobid."");
        // return $qjob->result();

        $qjob = $this->db->query("SELECT tj. * , tc.organization_name
					 					FROM tbl_job tj, tbl_company tc
					 					WHERE tj.organization_id = tc.organization_id
					 					AND tj.job_id =$jobid");

        return $qjob->result();
    }

    function master_selectionprocess() {
        $dom = $this->db->query('select * from master_selection_process where selection_id > 3');
        if ($dom->num_rows() > 0) {
            return $dom->result();
        } else {
            return null;
        }
    }

    function master_selectionprocess1() {
        $dom = $this->db->query('select * from master_selection_process');
        if ($dom->num_rows() > 0) {
            return $dom->result();
        } else {
            return null;
        }
    }

    function getselectionprocess($jobid) {
        //$query = $this->db->get_where('tbl_job_selection_process', array('job_id' => $jobid));

        $query = $this->db->query("select * from tbl_job_selection_process where job_id=$jobid and selection_id not in (2,3)");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    function fetchjobselection($jobid) {
        $query = $this->db->query("select tj.*,ts.* from tbl_job_selection_process tj,master_selection_process ts where tj.job_id = '$jobid' and tj.selection_id=ts.selection_id");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    function fetchjobselection1() {
        $query = $this->db->query("select tj.*,ts.* from tbl_job_selection_process tj,master_selection_process ts,tbl_job_applied tba where tj.selection_id=ts.selection_id and tba.status=ts.selection_process and tba.job_id=tj.job_id");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    function jobcriteria1($jobid) {

        //$query = $this->db->get_where('tbl_job_criteria', array('job_id' => $jobid));
        $query = $this->db->query("select * from tbl_job_criteria where job_id = '$jobid' and criteria_id = 1");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    function jobcriteria2($jobid) {
        //$query = $this->db->get_where('tbl_job_criteria', array('job_id' => $jobid));
        $query = $this->db->query("select * from tbl_job_criteria where job_id = '$jobid' and criteria_id = 2");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    function jobcriteria3($jobid) {
        //$query = $this->db->get_where('tbl_job_criteria', array('job_id' => $jobid));
        $query = $this->db->query("select * from tbl_job_criteria where job_id = '$jobid' and criteria_id = 3");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    function jobcriteria4($jobid) {
        //$query = $this->db->get_where('tbl_job_criteria', array('job_id' => $jobid));
        $query = $this->db->query("select * from tbl_job_criteria where job_id = '$jobid' and criteria_id = 4");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    function jobcriteria5($jobid) {
        //$query = $this->db->get_where('tbl_job_criteria', array('job_id' => $jobid));
        $query = $this->db->query("select * from tbl_job_criteria where job_id = '$jobid' and criteria_id = 5");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    function getApplications($jobid) {
        $jobs = $this->db->query("select s.PRN,s.student_id,s.first_name,s.middle_name,s.last_name, mb.batch_name,tba.job_id,tba.status,mc.course_name from tbl_batch_job tb,tbl_job tj,tbl_job_applied tba,master_batch mb,master_course mc,tbl_batch_course tbc,tbl_student_information s,tbl_batch_student tbs where s.student_id=tba.student_id and tba.job_id = '$jobid' and tb.batch_course_id=tbc.batch_course_id and tbc.batch_id=mb.batch_id and tbc.course_id= mc.course_id and tba.job_id=tj.job_id and tbs.student_id=s.student_id and tbc.batch_course_id=tbs.batch_course_id and tb.job_id=tba.job_id group by s.PRN");
        return $jobs->result();
    }

    function getallApplications() {
        $jobs = $this->db->query("select s.PRN,s.student_id,s.first_name,s.middle_name,s.last_name, mb.batch_name,tba.job_id,tba.status,mc.course_name, tj.offered_profile, tc.organization_name from tbl_batch_job tb,tbl_job tj,tbl_job_applied tba,master_batch mb,master_course mc,tbl_batch_course tbc,tbl_student_information s,tbl_batch_student tbs, tbl_company tc where s.student_id=tba.student_id and tb.batch_course_id=tbc.batch_course_id and tbc.batch_id=mb.batch_id and tbc.course_id= mc.course_id and tba.job_id=tj.job_id and tbs.student_id=s.student_id and tbc.batch_course_id=tbs.batch_course_id and tb.job_id=tba.job_id and tc.organization_id=tba.job_id group by s.PRN");
        return $jobs->result();
    }

    function master_jobtype() {
        $dom = $this->db->get('master_job_type');
        if ($dom->num_rows() > 0) {
            return $dom->result();
        } else {
            return null;
        }
    }

    // function getApplications($jobid)
    // {
    //     $jobs= $this->db->query("select s.student_id,s.first_name,s.middle_name,s.last_name, mb.batch_name,tba.job_id,mc.course_name from tbl_batch_job tb,tbl_job tj,tbl_job_applied tba,master_batch mb,master_course mc,tbl_batch_course tbc,tbl_student_information s,tbl_batch_student tbs where s.student_id=tba.student_id and tba.job_id = '$jobid' and tb.batch_course_id=tbc.batch_course_id and tbc.batch_id=mb.batch_id and tbc.course_id= mc.course_id and tba.job_id=tj.job_id and tbs.student_id=s.student_id and tbc.batch_course_id=tbs.batch_course_id and tb.job_id=tba.job_id");
    //     return $jobs->result();
    // }

    /* End Manage Job */

    /* Start Manage Event */

    function geteventname() {
        $qeventname = $this->db->get('master_event');
        return $qeventname->result();
    }

    /* End Manage Event */

    //........................SANJAY..............................................





    public function login($uid, $pwd) { // for login
        $query = $this->db->get_where('tbl_credentials', array('username' => $uid, 'password' => $pwd));
        $query->result();
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function checkPlacementTeam($cid) {
        $query = $this->db->get_where('tbl_placement_team', array('credentials_id' => $cid, 'isdeleted' => 0));
        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    // public function checkPlacementTeam_for_faculty($cid)
    // {
    // 	$query = $this->db->get_where('tbl_placement_team', array('credentials_id' => $cid,'isdeleted'=>0));
    // 	 if($query->num_rows() == 1)
    // 	 {
    // 	 	return $query->result();	 
    // 	 }
    // 	 else
    // 	 {
    // 	 	return false;
    // 	 }
    // }	

    public function model_check_PT_group_status($cid) {
        $query = $this->db->query
                ("SELECT PT.status AS PT_status, G.status AS G_status
								FROM tbl_placement_team PT, master_groups G
								WHERE PT.group_id = G.group_id
								AND PT.credentials_id =$cid");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $r) {
                if (($r->PT_status) && ($r->G_status)) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function allStudent() {
        // $this->db->select('s.fname AS First_name, s.mname AS Middle_name,s.lname AS Last_name, s.batch AS Batch,s.course AS Course,u.status AS Status, u.id AS id');
        // $this->db->from('student_info s');
        // $this->db->join('user u', 'u.id=s.email', 'left outer');
        // $this->db->where('r.client_id', 1);
        // $this->db->where('r.prog_id', 3);
        // $query = $this->db->get();
        // //SELECT user.status, student.f_name, user.uid
        // //FROM tbl_credentials user
        // //RIGHT JOIN student_details student
        // //ON user.uid=student.uid

        $query = $this->db->query
                ("SELECT user.status, student.f_name,student.m_name ,student.l_name,user.uid, student.course, student.batch
						   FROM tbl_credentials user
						   RIGHT JOIN student_details student ON user.uid = student.uid");

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function studentData($uid) {
        $query = $this->db->get_where('tbl_credentials', array('uid' => $uid));
        $query->result();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }

    public function checkOldPwd($pwd, $cid) {
        $query = $this->db->get_where('tbl_credentials', array('password' => md5($pwd), 'credentials_id' => $cid));
        $query->result();
        if ($query->num_rows() == 1) {
            //return $query->result();
            return true;
        } else {
            return false;
        }
    }

//--------- -----------------------------------------------VIMAL------------------------------------
    // ---------------ORGANIZATION--------------------- 




    public function checkVerticle($verticleName) {


        $query = $this->db->get_where('master_domain', array('domain_name' => $verticleName));

        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function displaycompany() {

        $query = $this->db->query("	SELECT com . * , sinfo.first_name, sinfo.middle_name, sinfo.last_name, md.*
											FROM tbl_company com
											INNER JOIN master_domain md ON md.domain_id = com.organization_type 
											INNER JOIN tbl_placement_team pt ON pt.placement_team_id = com. added_by_PT
											INNER JOIN tbl_student_information sinfo ON sinfo.credentials_id = pt.credentials_id"
        );

        if ($query->num_rows() > 0) {
            // print_r( $query->result());
            // exit;
            return $query->result();
        } else {
            return false;
        }

        // $query = $this->db->get('tbl_company');
        // if ($query->num_rows() > 0)
        // {
        // 	$info = $query->result();
        // 	foreach ($query->result() as $row) // fatch placement_team id
        // 	{
        // 	 	    $pt_id= $row->added_by_PT;
        // 	 	    // fatch credential_id if from tbl_placement_team
        // 	 		$query_credential = $this->db->query
        // 	 					(" SELECT credentials_id
        //    			 		   	   FROM tbl_placement_team
        //              	 		   WHERE placement_team_id = $pt_id ");
        // 	 		if ($query_credential->num_rows() > 0)
        // 	 		{			
        // 	 			foreach ($query_credential->result() as $row) // fatch placement_team id
        // 				{
        // 					$credential_id = $row->credentials_id;
        // 					$query_role = $this->db->query
        // 	 					(" SELECT role
        //    			 		   	   FROM tbl_credentials
        //              	 		   WHERE credentials_id = $credential_id ");
        // 	 					if ($query_role->num_rows() > 0)
        // 	 					{
        // 	 						foreach ($query_role->result() as $row) // fatch placement_team id
        // 							{
        // 								$query_name = $this->db->query
        // 			 					(
        // 			 						" SELECT first_name, last_name
        // 			   			 		   	  FROM tbl_student_information
        // 		             	 		   WHERE credentials_id = $credential_id ");
        // 			 					if ($query_name->num_rows() > 0)
        // 	 							{
        // 	 								foreach ($query_name->result() as $row) // fatch placement_team id
        // 									{ 
        // 										$name[] = $row->first_name;
        // 									}
        // 								}
        // 							}
        //  					}
        // 				}
        // 				}
        // 		}
        // 		  $i=-1;
        // 		  foreach ($info as $e) {
        // 		  		$i++;
        //  		  		$data []= array(
        // 							"organization_id" => $e->organization_id,
        // 							"organization_name" =>  $e->organization_name,
        // 							"organization_type" =>  $e->organization_type,
        // 							"pt_name" =>  $name[$i],
        // 							"added_date" =>  $e->added_date
        // 						);
        // 	 		  }
        // 			 return($data);
        // }
        // else
        // {
        // 	return null;
        // }
    }

    public function displaycompanycontact() {

        $query = $this->db->query("SELECT com . * , sinfo.first_name, sinfo.middle_name, sinfo.last_name, md.*, cc.contact_name, cc.contact_number1, cc.contact_number2, cc.primary_email, cc.location FROM tbl_company com INNER JOIN master_domain md ON md.domain_id = com.organization_type INNER JOIN tbl_placement_team pt ON pt.placement_team_id = com. added_by_PT INNER JOIN tbl_student_information sinfo ON sinfo.credentials_id = pt.credentials_id INNER JOIN tbl_company_contact cc ON cc.organization_id = com.organization_id");

        if ($query->num_rows() > 0) {
            // print_r( $query->result());
            // exit;
            return $query->result();
        } else {
            return false;
        }
    }

    public function displaycipdata($tabname) {

        $query = $this->db->query("select * from tbl_cipdata where tabname='" . $tabname . "' order by id asc");

        if ($query->num_rows() > 0) {
            // print_r( $query->result());
            // exit;
            return $query->result();
        } else {
            return false;
        }
    }

    public function displaycipdetails() {

        $query = $this->db->query("select * from tbl_cipdetails order by id asc");

        if ($query->num_rows() > 0) {
            // print_r( $query->result());
            // exit;
            return $query->result();
        } else {
            return false;
        }
    }

    public function fatchcompany($cid) {
        $query = $this->db->get_where('tbl_company', array('organization_id' => $cid));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function companyaddress($cid) {

        $query = $this->db->get_where('tbl_company_address', array('organization_id' => $cid));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }


        /*

          $query = $this->db->query("SELECT cinfo.* , cadd.*, cdetail.*
          FROM `tbl_company` cinfo
          Join tbl_company_address cadd on cinfo.`organization_id` = cadd.`organization_id`
          Join tbl_company_contact cdetail on cinfo.`organization_id` = cdetail.`organization_id`
          where cinfo.`organization_id` = $cid");
          if ($query->num_rows() > 0)
          {
          return $query->result();
          }
          else
          {
          return false;
          }s

         */
    }

    public function companycontacts($cid) {
        $query = $this->db->get_where('tbl_company_contact', array('organization_id' => $cid));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

//--------- ---------------ORGANIZATION--------------------- 
// manage forms


    public function formsList($sid) {
        $query = $this->db->query("	SELECT mf.form_name,dmf.m_f_id,dmf.status,dmf.batch_course_id ,dmf.start_date, dmf.end_date
					FROM tbl_display_manage_forms dmf
					RIGHT JOIN master_forms mf ON dmf.form_id = mf.form_id
					WHERE dmf.m_f_id = $sid"
        );


        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

//manage forms
//--------- ---------------STUDENT PROFILE--------------------- 


    public function batch_specializationStudent($sid) {
        // original query
        /* $query = $this->db->query("	SELECT s.specialization_name, s.specialization_id
          FROM tbl_specialization s
          RIGHT JOIN batch_course_specialization bcs ON bcs.spl_id = s.specialization_id
          WHERE bcs.batch_course_id = (
          SELECT batch_course_id
          FROM tbl_batch_student
          WHERE student_id =$sid )
          AND s.specialization_id NOT
          IN (
          SELECT  `specialization_name`
          FROM tbl_student_specializations
          WHERE student_id =$sid
          )"); */

        $query = $this->db->query("SELECT s.specialization_name, s.specialization_id FROM tbl_specialization s RIGHT JOIN batch_course_specialization bcs ON bcs.spl_id = s.specialization_id WHERE bcs.batch_course_id = (SELECT batch_course_id FROM tbl_batch_student WHERE student_id =$sid )");

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function forms($sid) {
        $query = $this->db->query("	SELECT mf.form_id, mf.form_name,dmf.status ,dmf.start_date, dmf.end_date
					FROM tbl_display_manage_forms dmf
					RIGHT JOIN master_forms mf ON dmf.form_id = mf.form_id
					WHERE dmf.batch_course_id = ( 
								SELECT batch_course_id
										FROM tbl_batch_student
										WHERE student_id =$sid )  "
        );

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function basicInformation($sid) {
        $query = $this->db->get_where('tbl_student_information', array('student_id' => $sid));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function localAddress($cid) {
        $query = $this->db->get_where('tbl_address', array('credentials_id' => $cid, 'address_type' => 'L'));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function permanentAddress($cid) {

        $query = $this->db->get_where('tbl_address', array('credentials_id' => $cid, 'address_type' => 'P'));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function education($sid) {

        $query = $this->db->query(" SELECT sedu.*, edu.edcutation_name
								FROM  tbl_student_educations sedu
								RIGHT JOIN master_education edu 
								ON sedu.education_id = edu.education_id 
								WHERE  student_id =$sid order by sedu.degree_name");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    function master_education($sid) {

        $query = $this->db->query(" SELECT * 
										FROM  `master_education` 
										WHERE  `education_id` NOT 
										IN ( SELECT  `education_id` 
										FROM tbl_student_educations
										WHERE student_id =$sid
										)
										ORDER BY  `edcutation_name` ");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }


        $edu = $this->db->get('master_education');
        if ($edu->num_rows() > 0) {
            return $edu->result();
        } else {
            return null;
        }
    }

    function master_domain() {
        $dom = $this->db->get('master_domain');
        if ($dom->num_rows() > 0) {
            return $dom->result();
        } else {
            return null;
        }
    }

    public function studentExperience($sid) {
        $query = $this->db->query("SELECT *
										FROM  tbl_student_experiences
										WHERE student_id =$sid");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function studentSpecialization($sid) {
        //$query = $this->db->get_where('tbl_student_specializations', array('student_id' => $sid));

        $query = $this->db->query("SELECT  se.* , sp.specialization_name as sp_name
											FROM  tbl_student_specializations se
											RIGHT JOIN tbl_specialization sp
											ON se.specialization_name = sp.specialization_id
											WHERE se.student_id =$sid");

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return null;
        }
    }

    public function studentAdditionalCourse($sid) {
        //original query
        /* $query = $this->db->query("	SELECT sc . * , md.domain_name AS domain
          FROM tbl_student_additional_courses sc
          RIGHT JOIN master_domain md ON sc.course_domain = md.domain_id
          WHERE sc.student_id =$sid"); */

        /* $query = $this->db->query("SELECT sc . * , md.domain_name AS domain FROM tbl_student_additional_courses sc RIGHT JOIN master_domain md ON sc.course_domain = md.domain_id or sc.course_domain != md.domain_id WHERE sc.student_id = $sid group by a_c_id"); */

        $query = $this->db->query("SELECT * from tbl_student_additional_courses WHERE student_id = $sid");

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function studentProjects($sid) {
        //original query
        /* $query = $this->db->query("	SELECT sp . * , md.domain_name as domain
          FROM tbl_student_projects sp, master_domain md
          WHERE sp.student_id = $sid AND sp.`project_domain` = md.domain_id order by project_id desc"); */

        $query = $this->db->query("SELECT * from tbl_student_projects WHERE student_id = $sid");


        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function studentResponsiblity($sid) {
        // original query
        /* $query = $this->db->query("SELECT spr.* , edu.edcutation_name, se.university_board_name
          FROM  tbl_student_possition_responsibility spr,  tbl_student_educations se,  master_education edu
          WHERE spr.student_id = se.student_id
          AND spr.p_r_education = se.edu_id
          AND se.education_id = edu.education_id
          AND spr.student_id =$sid"); */
        $query = $this->db->query("SELECT spr.* FROM tbl_student_possition_responsibility spr WHERE spr.student_id =$sid");

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function studentAwards($sid) {
        $query = $this->db->get_where('tbl_student_achivements', array('student_id' => $sid));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function studentSkills($sid) {
        $query = $this->db->get_where('tbl_student_skills', array('student_id' => $sid));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function studentHobbies($sid) {
        $query = $this->db->get_where('tbl_student_hobbies', array('student_id' => $sid));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function student_extraActivity($sid) {
        $query = $this->db->get_where('tbl_student_extra_activity', array('student_id' => $sid));
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    public function fatch_detail($page, $id) {
        if ($page == "page_experience") {
            $this->db->select('responsibilities');
            $query = $this->db->get_where('`tbl_student_experiences', array('experience_id' => $id));
            if ($query->num_rows() > 0) {
                return $query->result();
            } else {
                return null;
            }
        }
    }

//--------- ---------------STUDENT PROFILE--------------------- 
//--------- -----------------------------------------------VIMAL------------------------------------	
// --------------------------------- MANASI -------------------------------------

    public function displayBatch() {

        $batch = $this->db->query("SELECT tbc.enabled,c.course_id,mb.batch_id,batch_name, batch_course_id,in_year, out_year, course_name
                                         FROM master_batch mb, tbl_batch_course tbc, master_course c
                                         WHERE mb.batch_id = tbc.batch_id
                                          AND tbc.course_id = c.course_id
										 and mb.isdelete=0");

        return $batch->result();
    }

    public function get_dropdown_list() {

        $query = $this->db->query('select course_id,course_name  from master_course where isdelete=0 and status=1 order by course_name');
        return $query->result();
    }

    public function DeleteBatch($batch_course_id) {

        $query = $this->db->query("SELECT s.PRN, s.student_id, s.credentials_id, s.first_name,s.DOB,
							s.middle_name, s.last_name, s.gender, s.primary_email, c.status, b.batch_course_id,
							tbc.batch_id, tbc.course_id, mb.batch_name, mc.course_name
							FROM tbl_student_information s, tbl_batch_student b, 
							tbl_credentials c, master_course mc, master_batch mb, tbl_batch_course tbc
							WHERE b.batch_course_id ='$batch_course_id'
							AND s.student_id = b.student_id
							AND c.credentials_id = s.credentials_id
							AND tbc.batch_id = mb.batch_id
							AND tbc.course_id = mc.course_id
							AND b.batch_course_id = tbc.batch_course_id and s.isdelete=0 and c.isdelete=0
							and mc.isdelete=0 and mb.isdelete=0 and b.isdelete=0");
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function studlist1($bcid) {

        $q1 = $this->db->query("SELECT s.PRN, s.student_id, s.credentials_id, s.first_name,s.DOB,
							s.middle_name, s.last_name, s.gender, s.primary_email, c.status, b.batch_course_id,
							tbc.batch_id, tbc.course_id, mb.batch_name, mc.course_name
							FROM tbl_student_information s, tbl_batch_student b, 
							tbl_credentials c, master_course mc, master_batch mb, tbl_batch_course tbc
							WHERE b.batch_course_id ='$bcid'
							AND s.student_id = b.student_id
							AND c.credentials_id = s.credentials_id
							AND tbc.batch_id = mb.batch_id
							AND tbc.course_id = mc.course_id
							AND b.batch_course_id = tbc.batch_course_id and s.isdelete=0 and c.isdelete=0
							and mc.isdelete=0 and mb.isdelete=0"
        );

        return $q1->result();
    }

    /* public function DeleteBatch($batch_id)
      {
      $isdelete= array('isdelete'=>'1');
      $this->db->where("batch_id",$batch_id);
      if($this->db->update('master_batch', $isdelete))
      {
      return true;

      }
      else
      {
      return false;
      }

      //$this->db->query("delete from tbl_batch_course where batch_course_id=$batchid");

      } */

    public function getbatch($batch_id, $course_id) {//change batch
        $course_query = $this->db->query("select course_id,course_name from master_course c where course_id='$course_id' and isdelete=1");
        if ($course_query->num_rows > 0) {
            $this->session->set_flashdata("errormsg", "The course of this batch has been deleted!You cannot edit this Batch");
            redirect('superadmin/managebatches');
        } else {
            $qbatch = $this->db->query("SELECT tbc.enabled,tbc.batch_course_id, mb.batch_id,mb.batch_name,mb.in_year,mb.out_year,c.course_id, course_name,tbc.no_of_terms
                                         FROM master_batch mb, tbl_batch_course tbc, master_course c
                                         WHERE mb.batch_id='$batch_id' AND mb.batch_id = tbc.batch_id
                                          AND tbc.course_id = c.course_id and mb.isdelete=0 and c.isdelete=0");

            return $qbatch->result();
        }
    }

    public function getcourselist() {
        $query = $this->db->query('select * from master_course where isdelete=0 and status=1 order by course_id');
        return $query->result();
    }

    public function getbatchlist() {
        $query = $this->db->query('select * from master_batch where isdelete=0 order by batch_id');
        return $query->result();
    }

    //query by kaushal (20/05/2015)
    public function getcoursespl($specialization_id) {
        $query = $this->db->query('select DISTINCT c.course_id,c.course_name from master_course c left outer join tbl_course_specialization cs on cs.course_id = c.course_id where c.status="1" and c.isdelete="0" or cs.specialization_id=' . $specialization_id . ' or c.course_id =(select c.course_id from master_course c join tbl_course_specialization cs on cs.course_id = c.course_id where c.status="1" and cs.specialization_id=' . $specialization_id . ' and c.isdelete="0" )');

        return $query->result();
    }

    public function getbatchcourse($batch_id, $course_id) {
        $query = $this->db->query('select DISTINCT c.course_id,c.course_name from master_course c left outer join tbl_batch_course bc on bc.course_id = c.course_id where c.status="1" and c.isdelete="0" or bc.batch_id=' . $batch_id . ' or c.course_id =(select c.course_id from master_course c join tbl_batch_course bc on bc.course_id = c.course_id where c.status="1" and bc.batch_id=' . $batch_id . ' and c.isdelete="0" )');

        return $query->result();
    }

    // end of kaushal
    public function studlist($bname1) {

        $q1 = $this->db->query("SELECT s.PRN, s.student_id, s.credentials_id, s.first_name,
							s.middle_name, s.last_name, s.gender, s.primary_email, c.status, b.batch_course_id,
							tbc.batch_id, tbc.course_id, mb.batch_name, mc.course_name
							FROM tbl_student_information s, tbl_batch_student b, 
							tbl_credentials c, master_course mc, master_batch mb, tbl_batch_course tbc
							WHERE b.batch_course_id ='$bname1'
							AND s.student_id = b.student_id
							AND c.credentials_id = s.credentials_id
							AND tbc.batch_id = mb.batch_id
							AND tbc.course_id = mc.course_id
							AND b.batch_course_id = tbc.batch_course_id and s.isdelete=0 and c.isdelete=0
							and mb.isdelete=0");


        /* $q1=$this->db->query("select s.PRN,s.student_id,s.credentials_id,s.first_name,s.middle_name,s.last_name,gender,email_id,status,b.batch_course_id
          from tbl_student_information s,tbl_batch_student b,tbl_credentials c
          where b.batch_course_id='$bname1' and s.student_id=b.student_id and c.credential_id=s.credentials_id"); */

        return $q1->result();
    }

    ///-------------FACULTY------------------------------------///

    /* function get_faculty() {     
      //$query = $this->db->query('select tb.*,tc.* from tbl_faculty tb,tbl_credentials tc where tb.credentials_id=tc.credentials_id and tb.isdelete=0 and tc.isdelete=0');

      $query = $this->db->query('select tb.*, tc.isdelete from tbl_faculty tb,tbl_credentials tc where tb.credentials_id=tc.credentials_id and tb.isdelete=0 and tc.isdelete=0');

      if ($query->num_rows() > 0) {
      return $query->result();
      } else {
      return FALSE;
      }
      } */

    public function get_faculty() {
        $query = $this->db->query("select tb.*, tc.isdelete from tbl_faculty tb,tbl_credentials tc where tb.credentials_id=tc.credentials_id and tb.isdelete=0 and tc.isdelete=0");
        return $query->result();
    }

    public function editfaculty($fid) {
        $query = $this->db->query("select f.faculty_id,f.first_name,f.middle_name,f.last_name,f.gender,f.primary_email,f.contact_no_1,f.DOB,c.status,c.username,c.password,f.credentials_id from tbl_faculty f,tbl_credentials c
			  where f.credentials_id=$fid and c.credentials_id=f.credentials_id ");
        return $query->result();
    }

    ///---------------FACULTY------------------------------------///
    ///-------------GUEST USER------------------------------------///

    /* function get_guestuser() {
      $query = $this->db->query('select tg.*,tc.* from tbl_guest_user tg,tbl_credentials tc where tg.credentials_id=tc.credentials_id and tg.isdelete=0 and tc.isdelete=0');
      if ($query->num_rows() > 0) {
      return $query->result();
      } else {
      return FALSE;
      }
      } */

    function get_guestuser() {

        $query = $this->db->query("select tg.*,tc.* from tbl_guest_user tg,tbl_credentials tc where tg.credentials_id=tc.credentials_id and tg.isdelete=0 and tc.isdelete=0");
        return $query->result();
    }

    public function editguestuser($fid) {
        $query = $this->db->query("select tg.*,c.status,c.username,c.password,tg.credentials_id from tbl_guest_user tg,tbl_credentials c
		where tg.credentials_id=$fid and c.credentials_id=tg.credentials_id ");

        //$query = $this->db->query("select * from tbl_guest_user where credentials_id=$fid and isdelete=0");
        return $query->result();
    }

    function getassignedstudentlist($guest_user_id) {
        $qalljobs = $this->db->query("select tgs.*, tsi.PRN, tsi.first_name, tsi.middle_name, tsi.last_name from tbl_guestuser_students tgs inner join tbl_student_information tsi on tgs.student_id = tsi.student_id where tgs.guest_user_id=$guest_user_id");
        return $qalljobs->result();
    }

    function guest_information($cid) {

        $query = $this->db->get_where('tbl_guest_user', array('credentials_id' => $cid));

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function get_guest_student_report1($stud_id, $guest_user_id) {
        //echo $guest_user_id;exit;
        // org query
        /* $qalldata = $this->db->query("select * from tbl_ppm where stud_id = ".$stud_id." and guestuser_id = ".$guest_user_id." and ppm_type = 1"); */
        $qalldata = $this->db->query("select * from tbl_ppm where stud_id = " . $stud_id . " and ppm_type = 1");
        return $qalldata->row();
    }

    function get_guest_student_report2($stud_id, $guest_user_id) {
        //echo $guest_user_id;exit;
        // org query
        /* $qalldata = $this->db->query("select * from tbl_ppm where stud_id = ".$stud_id." and guestuser_id = ".$guest_user_id." and ppm_type = 2"); */
        $qalldata = $this->db->query("select * from tbl_ppm where stud_id = " . $stud_id . " and ppm_type = 2");
        return $qalldata->row();
    }

    function get_student_report1($stud_id) {
        //echo $guest_user_id;exit;
        $qalldata = $this->db->query("select * from tbl_ppm where stud_id = " . $stud_id . " and ppm_type = 1");
        return $qalldata->row();
    }

    function get_student_report2($stud_id) {
        //echo $guest_user_id;exit;
        $qalldata = $this->db->query("select * from tbl_ppm where stud_id = " . $stud_id . " and ppm_type = 2");
        return $qalldata->row();
    }

    ///-------------GUEST USER------------------------------------///

    public function courselist() {
        $query = $this->db->query("select * from master_course where isdelete=0 order by course_id");
        return $query->result();
    }

    //madhuri24/4/16
    public function newslist() {
        $query = $this->db->query("select * from intra_news  order by ID");
        return $query->result();
    }

    public function getnews($news_id) {
        $query = $this->db->query("select * from intra_news where ID='$news_id'");
        return $query->result();
    }

    public function getcourse($course_id) {
        $query = $this->db->query("select * from master_course where course_id='$course_id' and isdelete=0");
        return $query->result();
    }

    public function spllist() {
        $query = $this->db->query("select ts.specialization_id,specialization_name,ts.status,course_name from tbl_specialization ts,tbl_course_specialization cs,master_course c where ts.specialization_id=cs.specialization_id and c.course_id=cs.course_id and ts.isdelete=0 and c.isdelete=0");
        return ($query->result());
    }

    public function getspl($specialization_id) {
        $qbatch = $this->db->query("SELECT s.status,s.specialization_id,s.specialization_name,c.course_id,c.course_name
                                         FROM master_course c,tbl_course_specialization tcb,tbl_specialization s
                                         WHERE s.specialization_id='$specialization_id' AND s.specialization_id = tcb.specialization_id
                                          AND tcb.course_id = c.course_id and c.isdelete=0");

        return $qbatch->result();
    }

    public function subjectlist() {
        $q = $this->db->query("select s.subject_id,subject_name,course_id,course_name from master_subject s,master_course c,tbl_course_subject cs
		                     where s.subject_id=cs.subject_id and c.course_id=cs.courseid and s.isdelete=0 and c.isdelete=0
		                     order by subject_id");
        return $q->result();
    }

    public function getsub($subject_id) {
        $qbatch = $this->db->query("SELECT s.subject_id,s.subject_name,c.course_id,c.course_name
                                         FROM master_course c,tbl_course_subject tcs,master_subject s
                                         WHERE s.subject_id='$subject_id' AND s.subject_id = tcs.subject_id
                                          AND tcs.courseid = c.course_id and s.isdelete=0 and c.isdelete=0");

        return $qbatch->result();
    }

    public function batchcourselist() {

        $query = $this->db->query("select tbc.batch_course_id,batch_name,course_name from master_batch b,master_course c,tbl_batch_course tbc        
                                     where b.batch_id=tbc.batch_id and c.course_id=tbc.course_id
                                     and b.isdelete=0 and tbc.enabled=1 									 
									 order by b.batch_id");
        return $query->result();
    }

    /* public function fetch_get_batch_List()
      {

      $query = $this->db->query("select tbc.batch_course_id,batch_name,course_name from master_batch b,master_course c,tbl_batch_course tbc
      where b.batch_id=tbc.batch_id and c.course_id=tbc.course_id
      and b.isdelete=0 and tbc.enabled=1
      order by b.batch_id");
      return $query->result();

      } */

    public function editstud($sid) {
        $query = $this->db->query("select s.PRN,s.student_id,s.first_name,s.middle_name,s.last_name,s.gender,s.primary_email,s.DOB,status,c.username,c.password,s.credentials_id,b.batch_course_id
			  from tbl_student_information s,tbl_batch_student b,tbl_credentials c
			  where s.credentials_id=$sid and s.student_id=b.student_id and c.credentials_id=s.credentials_id ");
        return $query->result();
    }

    public function viewsubjectList($bname) {
        $query = $this->db->query("SELECT tbc.course_id, ms.subject_id, ms.subject_name
								FROM master_subject ms, tbl_batch_course tbc, tbl_course_subject cs
								WHERE tbc.course_id = cs.courseid
								AND tbc.batch_course_id =110
								AND cs.subject_id = ms.subject_id");
        print_r($query->result());
    }

//-------------------------------------MANASI------------------------------------------
//-------------------------------------HIMANI------------------------------------------	

    function get_student() {
        $query = $this->db->get('tbl_student_information');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }

    function fetch_batch_course($bname1) {
        $query2 = $this->db->query("select batch_course_id from tbl_batch_course where batch_id=$batch_id and course_id=$course_id");
        if ($query2->num_rows == 1) {
            $row2 = $query2->row();
            return $row2->batch_course_id;
        }
    }

    function get_batch_course_list() {
        $query = $this->db->query("select tbc.batch_course_id,batch_name,course_name from master_batch b,master_course c,tbl_batch_course tbc        
                                     where b.batch_id=tbc.batch_id and c.course_id=tbc.course_id and b.isdelete=0 and tbc.enabled=1	
									 order by b.batch_id");
        return $query->result();
    }

//-------------------------------------HIMANI------------------------------------------	
// by kaushal 08/06/2015 

    function get_guestuser_purpose() {
        $query = $this->db->query("select * from master_guestuser_purpose order by id");
        return $query->result();
    }

    function guestuser_purpose($guserId) {
        $query = $this->db->query("select * from tbl_guestuser_purpose where guestuser_id = $guserId");
        return $query->result();
    }

    function model_check_guestuser_authority($cid) {
        $query = $this->db->query("SELECT tgu.guest_user_id, tgu.first_name,tgp.purpose FROM tbl_guest_user as tgu INNER JOIN tbl_guestuser_purpose as tgp on tgp.guestuser_id = tgu.guest_user_id WHERE tgp.purpose=2 AND tgu.credentials_id=$cid");

        if ($query->num_rows() > 0) {
            return $query->result();
            //return true;
        } else {
            return false;
        }
    }

    function get_ppmguestuser() {

        $query = $this->db->query("select tg.*,tc.*,gp.* from tbl_guest_user tg,tbl_credentials tc, tbl_guestuser_purpose gp where tg.credentials_id=tc.credentials_id and tg.guest_user_id = gp.guestuser_id and gp.purpose=2 and tg.isdelete=0 and tc.isdelete=0 order by tg.guest_user_id");
        return $query->result();
    }

    function checkEmailExist($email, $role) {
        $this->db->select("username");

        $this->db->where("username", strtolower($email));
        $this->db->where("role", $role);

        $query = $this->db->get('tbl_credentials');

        if ($query->num_rows() > 0)
            return false;
        else
            return true;
    }

    function get_deletedStudents() {
        $query = $this->db->query("select ts.*,tc.status from tbl_student_information ts, tbl_credentials tc where ts.credentials_id = tc.credentials_id and tc.role='STUDENT' and ts.isdelete= 1 and tc.isdelete= 1");
        return $query->result();
    }

    function deletedstudlist($bname1) {

        $q1 = $this->db->query("SELECT s.PRN, s.student_id, s.credentials_id, s.first_name,
							s.middle_name, s.last_name, s.gender, s.primary_email, c.status, b.batch_course_id,
							tbc.batch_id, tbc.course_id, mb.batch_name, mc.course_name
							FROM tbl_student_information s, tbl_batch_student b, 
							tbl_credentials c, master_course mc, master_batch mb, tbl_batch_course tbc
							WHERE b.batch_course_id ='$bname1'
							AND s.student_id = b.student_id
							AND c.credentials_id = s.credentials_id
							AND tbc.batch_id = mb.batch_id
							AND tbc.course_id = mc.course_id
							AND b.batch_course_id = tbc.batch_course_id and s.isdelete=1 and c.isdelete=1
							and mb.isdelete=0");

        return $q1->result();
    }

    function get_studppmreport() {

        $query = $this->db->query("select tg.*,tc.* from tbl_guest_user tg,tbl_credentials tc where tg.credentials_id=tc.credentials_id and tg.isdelete=0 and tc.isdelete=0");
        return $query->result();
    }

    function getstudinfo($sid) {

        $query = $this->db->query("select s.student_id, s.PRN, s.first_name, s.middle_name, s.last_name, ifnull(p.job_id,0) job_id, ifnull(p.package,0) package, ifnull(p.joiningdate,'') joiningdate, ifnull(p.location,'') location from tbl_student_information s left outer join tbl_ppoplacedstatus p on s.student_id = p.stud_id where s.student_id = $sid");
        return $query->result();
    }

    function getjobinfo($sid) {

        $query = $this->db->query("select tj.job_id, tj.offered_profile, tc.organization_name from tbl_job tj inner join tbl_job_applied tja on tj.job_id = tja.job_id inner join tbl_company tc on tc.organization_id = tj.organization_id where tja.student_id = $sid");
        return $query->result();
    }

}
