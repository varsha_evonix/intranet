<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Delete_model extends CI_Model {


//-----------------------------vimal--------------------------------
	//-----------------------------ORGANIZATION--------------------------------

	public function deleteCompanyInfo($cid)
	{


		if($this->db->delete('tbl_company', "organization_id = $cid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}


	public function deleteCompanyAddress($cid)
	{


		if($this->db->delete('tbl_company_address', "organization_id = $cid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}


	public function deleteCompanyContacts($cid)
	{


		if($this->db->delete('tbl_company_contact', "organization_id = $cid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}





	//-----------------------------ORGANIZATION--------------------------------




//-------------------------------------------STUDENT------------------------------
	
//-------------------------------------------STUDENT------------------------------
	public function address($cid)
	{

		if($this->db->delete('tbl_address', "credentials_id = $cid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	public function pt_deleteStudProfileInfo($eid,$page)
	{
	if($page =="page_edu")
	{	
		if($this->db->delete('tbl_student_possition_responsibility', "p_r_education = $eid"))
		{
			if($this->db->delete('tbl_student_educations', "edu_id = $eid"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}		
	}
	if($page =="page_experience")
	{
		if($this->db->delete('tbl_student_experiences', "experience_id = $eid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	if($page =="page_specialization")
	{
		if($this->db->delete('tbl_student_specializations', "specialization_id = $eid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}


	if($page =="page_additional_course")
	{
		if($this->db->delete('tbl_student_additional_courses', "a_c_id = $eid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	if($page =="page_project")
	{
		if($this->db->delete('tbl_student_projects', "project_id = $eid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	
	if($page =="page_responsiblities")
	{
		if($this->db->delete('tbl_student_possition_responsibility', "p_r_id = $eid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}	

	if($page =="page_awards")
	{
		if($this->db->delete('tbl_student_achivements', "achivement_id = $eid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}	

	if($page =="page_skills")
	{
		if($this->db->delete('tbl_student_skills', "skills_id = $eid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}		

	if($page =="page_hobbies")
	{
		if($this->db->delete('tbl_student_hobbies', "hobby_id = $eid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}		

	if($page =="page_extraActivity")
	{
		if($this->db->delete('tbl_student_extra_activity', "activity_id = $eid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}		

}



//-------------------------------------------STUDENT------------------------------






//-------------------------------------------STUDENT------------------------------

//-----------------------------vimal--------------------------------



//-------------------------------------------SANJAY------------------------------

//-------------------------------------------MANAGE GROUP------------------------------

	public function deletegroup($groupid)
	{
		if($this->db->delete('master_groups', "group_id = $groupid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}

//-------------------------------------------MANAGE GROUP------------------------------

//-------------------------------------------SANJAY------------------------------



//------------------------------------------MANASI---------------------------------
	public function deleteCourse($course_id)
   {
		   
		 
		$isdelete= array('isdelete'=>'1');
	  $this->db->where("course_id",$course_id);
	  if($this->db->update('master_course', $isdelete))
	   { 
	      return true;
		  
	   }
	   else
	   {
	      return false;
	   }
	   
   }
   public function deleteSpl($specialization_id)
   {
      $isdelete= array('isdelete'=>'1');
	  $this->db->where('specialization_id',$specialization_id);
	  if($this->db->update('tbl_specialization', $isdelete))
	   { 
	      return true;
		  
	   }
	   else
	   {
	      return false;
	   }
	   
	   
 }
 
   public function deleteStud($sid)
   {
      
	   $isdelete= array('isdelete'=>'1');
		$this->db->where('credentials_id',$sid);
		$this->db->update('tbl_credentials', $isdelete);
		$this->db->where('credentials_id',$sid);
	
	  if($this->db->update('tbl_student_information', $isdelete))
	   { 
	      return true;
		  
	   }
	   else
	   {
	      return false;
	   }
	   
	
   }  
		public function deleteSubject($sid)
	{
		
		$isdelete= array('isdelete'=>'1');
		$this->db->where('subject_id',$sid);
	  if($this->db->update('master_subject', $isdelete))
	   { 
	      return true;
		  
	   }
	   else
	   {
	      return false;
	   }
	   
	  
   }  

  public function deleteStud1($ids)
   {
       $ids = $ids;
	   
	    foreach ($ids as $id){
			  $did = intval($id).'<br>';
	   $isdelete= array('isdelete'=>'1');
		$this->db->where('student_id',$did);
		$this->db->update('tbl_student_information', $isdelete);
			$this->db->where('student_id',$did);	
	  $this->db->update('tbl_batch_student', $isdelete);
		
	  $this->db->select('credentials_id')->from('tbl_student_information')->where('student_id',$did);

     $query = $this->db->get();

     if ($query->num_rows() > 0) {
         $credentials_id=$query->row()->credentials_id;
			$this->db->where('credentials_id',$credentials_id);	
	  $this->db->update('tbl_credentials', $isdelete);
	  
	/* $this->db->select('batch_course_id')->from('tbl_batch_student')->where('student_id',$student_id);
     $query1 = $this->db->get();
	 if ($query1->num_rows() > 0) {
           $batch_course_id=$query1->row()->batch_course_id;
		    $this->db->select('batch_id')->from('tbl_batch_course')->where('batch_course_id',$batch_course_id);
			$query2 = $this->db->get();
			 if ($query2->num_rows() > 0) {
				  $batch_id=$query2->row()->batch_id;
		   $this->db->where('batch_id',$batch_id);	
	  $this->db->update('master_batch', $isdelete);
	 }	*/
		}
	   
	 
	 else
	 {
		 return false;
	 }
		 
	
   }  
        
		
}

public function deletebatches1($batch_course_id)
{
	  $isdelete= array('isdelete'=>'1');
		$this->db->where('batch_course_id',$batch_course_id);
		$this->db->update('tbl_batch_course', $isdelete);
		$this->db->select('batch_id')->from('tbl_batch_course')->where('batch_course_id',$batch_course_id);
     $query = $this->db->get();
	  if ($query->num_rows() > 0) {
           $batch_id=$query->row()->batch_id;
		   	$this->db->where('batch_id',$batch_id);	
	      $data = $this->db->update('master_batch', $isdelete);
	        if($data)
			{
			return true;	
			}
			else
			{
			return false;
			}
				
			
	    
	  
	  }
	  
}   
///-----------FACULTY-----------------------------------------------------
  public function deleteFac($fid)
   {
      
	   $isdelete= array('isdelete'=>'1');
		$this->db->where('credentials_id',$fid);
		$this->db->update('tbl_credentials', $isdelete);
		$this->db->where('credentials_id',$fid);
	
	  if($this->db->update('tbl_faculty', $isdelete))
	   { 
	      return true;
		  
	   }
	   else
	   {
	      return false;
	   }
	   
	
   }  
////////------------FACULTY-------------------------------------------------   

// SANJAY //

///-----------GUEST USER-----------------------------------------------------

  public function deleteguestuser($fid)
   {
      
	   $isdelete= array('isdelete'=>'1');
		$this->db->where('credentials_id',$fid);
		$this->db->update('tbl_credentials', $isdelete);
		$this->db->where('credentials_id',$fid);
	
	  if($this->db->update('tbl_guest_user', $isdelete))
	   { 
	      return true;
		  
	   }
	   else
	   {
	      return false;
	   }
	   
	
   }
   
   public function deleteppmreport($sid)
   {
	
	  if($this->db->delete('tbl_ppm', array('stud_id' => $sid)))
	   {
	      return true;
		  
	   }
	   else
	   {
	      return false;
	   }
   }
   
////////------------GUEST USER-------------------------------------------------

// SANJAY //
			 //-------------JOB------------------------------------
public function deleteSelectionProcess($jobid)
	{


		if($this->db->delete('tbl_job_selection_process', "job_id = $jobid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	
	public function deleteJobCritera($jobid)
	{


		if($this->db->delete('tbl_job_criteria', "job_id = $jobid"))
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	
public function deleteBatchJob($jobid)
{
	if($this->db->delete('tbl_batch_job',"job_id = $jobid"))
	{
		return true;
	}
	else
	{
		return false;
	}
}

public function deletejob($jobid)
	{
		 $isdelete= array('isdelete'=>'1');
		$this->db->where('job_id',$jobid);
		 $result= $this->db->update('tbl_job', $isdelete);
		if($result)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
//------------------------------------JOB--------------------------------------
		
}
