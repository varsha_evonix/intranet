<?php
 
class Csv_model extends CI_Model {
 
    function __construct() {
        parent::__construct();
 
    }
	function get_course_list()
      {
     $this->db->from('master_course');
     $this->db->order_by('course_name');
     $result = $this->db->get();
     $return = array();
     if($result->num_rows() > 0) {
     foreach($result->result_array() as $row) {
      $return[$row['course_name']] = $row['course_name'];
     }
    }
     return $return;

}

     function get_batch_list()
   {
    $this->db->from('master_batch');
     $this->db->order_by('batch_name');
   $result = $this->db->get();
   $return = array();
        if($result->num_rows() > 0) {
            foreach($result->result_array() as $row) {
         $return[$row['batch_name']] = $row['batch_name'];
   }
  }
     return $return;

}


 
  
    function get_student() {     
        $query = $this->db->get('tbl_student_information');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }
	
	
      
    function insert_credential($data) {
		 $insert_query1= $this->db->insert_string('tbl_credentials', $data);
		$insert_query1= str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query1);
		 $this->db->query($insert_query1);
		   
    }
	 
	
    function insert_student($data1) {
        $insert_query= $this->db->insert_string('tbl_student_information', $data1);
		$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
		 $this->db->query($insert_query);
		 
    }

	
	
	function fetch_batch($batch_name)
	{
		$this->db->select('batch_id');
		    $this->db->where('batch_name',$batch_name);
		    $query = $this->db->get("master_batch"); 
		    if($query->num_rows == 1)
             {
				  $row = $query->row();
               return $row->batch_id;
               
				//echo $cid;
		        
              }
	}
	 function fetch_course($course_name)
	 {
	$this->db->select('course_id');
		    $this->db->where('course_name',$course_name);
		    $query1 = $this->db->get("master_course"); 
		    if($query1->num_rows == 1)
             {
                $row1 = $query1->row();
			    return $row1->course_id;
				//echo $cid;
		        
              }
	 }
	 
	 function fetch_batch_course($batch_id,$course_id)
	 {
		 $query2= $this->db->query("select batch_course_id from tbl_batch_course where batch_id=$batch_id and course_id=$course_id");
		    if($query2->num_rows == 1)
             {
                $row2=$query2->row();
			   return $row2->batch_course_id;
		      
              }
	 }
	 
	 function insert_batch_student($batch_student)
	 {
		 if($this->db->insert('tbl_batch_student',$batch_student))
		{
			    return true;
		}
		else
		{
		   return false;
		} 
			
	 }
	
	
		
    
}
?>