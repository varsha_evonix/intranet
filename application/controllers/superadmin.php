<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();

class Superadmin extends MY_Controller {

    public function __construct() {
        parent::__construct();


        $this->load->library(array('session', 'csvimport', 'upload'));
        $this->load->helper(array('form', 'url', 'download', 'common_helper'));
        $this->check_session('SUPER_ADMIN');
    }

//---------------------------------------- REQUIRED FUNCTION FOR VALIDATION -------------------
    public function createnews() { // check user role
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/createnews_view');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function insertnews() {
        //echo "<pre>";
        //print_r($_FILES);
        if ($this->input->post()) {
            $data = array('title' => $this->input->post('nname'),
                'url' => $this->input->post('urlcheck'),
                'urlLink' => $this->input->post('url'),
                'attachment' => $this->input->post('file'),
                'message' => $this->input->post('message'),
                'Enabled' => $this->input->post('isenable')
            );

            $config['upload_path'] = './uploads/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '100';
            $config['max_width'] = '1024';
            $config['max_height'] = '768';
            $this->upload->initialize($config);
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                //echo "<pre>";
                //print_r($error);
                //exit();
                //$this->load->view('upload_form', $error);
                $this->session->set_flashdata('msg', $error);
                redirect('superadmin/createnews');
            } else {
                if ($this->create_news($data)) {
                    if ($this->checkkUser() == "SUPER_ADMIN") {
                        $this->session->set_flashdata("message", "News has been created successfully!");
                        redirect('superadmin/createnews');
                    } else {
                        redirect('welcome/unAuthority');
                    }
                } else {
                    if ($this->checkkUser() == "SUPER_ADMIN") {
                        $this->session->set_flashdata("message", "Error in creating Course!");
                        redirect('superadmin/createnews');
                    } else {
                        redirect('welcome/unAuthority');
                    }
                }
            }
        }
    }

    public function managenews() {
        $data['news'] = $this->viewnews();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/managenews_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function editnews($newsid) {
        $newsedata['data'] = $this->edit_news($newsid);
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view("SUPER_ADMIN/editnews_view", $newsedata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function checkkUser() { // check user role
        return $this->session->userdata("role");
    }

//---------------------------------------- REQUIRED FUNCTION FOR VALIDATION -------------------	


    public function home() {
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/admin_home');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function createbatch() {
        $this->load->model('fetch_model');
        $data['cname'] = $this->fetch_model->get_dropdown_list();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/createbatch_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function insertbatch() {
        $info['specialization_id'] = $this->input->post('spec');
        $cname = $this->input->post('cname');
        $terms = $this->input->post('terms');
        $enabled = $this->input->post('enabled');
        $bname = array('batch_name' => $this->input->post('bname'),
            'in_year' => $this->input->post('inyear'),
            'out_year' => $this->input->post('outyear'),
            'isdelete' => $this->input->post('0'));


        if ($this->batch($cname, $terms, $enabled, $bname, $info)) {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Batch has been created successfully!");
                redirect('superadmin/createbatch');
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Error in creating Batch");
                redirect('superadmin/createbatch');
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

///-----------------Manage Batch---------------------------------------------//  
    public function managebatches() {
        $data['info'] = $this->view_Batch();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/managebatches_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function checkbatch($batch_course_id) {
        //$batchid=$this->input->post('id');
        if ($this->delete_batch($batch_course_id)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function deletestudents($bcid) {
        $batchdata['info'] = $this->displaystudents($bcid);
        $this->load->view("SUPER_ADMIN/deletestudents_view", $batchdata);
    }

    public function deleteallstudents() {
        $ids = ( explode(',', $this->input->get_post('ids')));
        if ($this->studs_delete($ids)) {
            echo "true";
            /* $this->session->set_flashdata("message","All students have been deleted successfully!");
              redirect('superadmin/deletestudents'); */
        } else {
            echo "false";
            /* $this->session->set_flashdata("errormsg","error!");
              redirect('superadmin/deletestudents'); */
        }
    }

    public function deletebatches() {
        $batch_course_id = $this->input->post('id');

        $check = $this->delete_Batches($batch_course_id);
        if ($check == 1) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function deletebatch() {
        $batch_course_id = $_POST['bcid2'];
        $check = $this->delete_Batches($batch_course_id);
        if ($check) {
            echo "true";
        } else {
            echo "false";
        }
    }

    /* public function deletebatch($batch_id)
      {
      //$batchid=$this->input->post('id');
      if($this->delete_batch($batch_id))
      {
      echo "true";


      }
      else
      {
      echo "false";
      }



      } */

    public function fetchbatch() {

        $bcid = $_POST['bcid'];

        $query3 = "select tbc.batch_course_id,batch_name,course_name from master_batch b,master_course c,tbl_batch_course tbc        
                                     where tbc.batch_course_id!='$bcid'and b.batch_id=tbc.batch_id and c.course_id=tbc.course_id and b.isdelete=0 and c.isdelete=0 and tbc.enabled=1
	 order by b.batch_id";
        $result = mysql_query($query3);
        $results = array();
        while ($row = mysql_fetch_array($result)) {

            $results[] = array(
                'batch_course_id' => $row['batch_course_id'],
                'batch_name' => $row['batch_name'],
                'course_name' => $row['course_name']
            );
        }

        echo $json = json_encode($results);
        //$this->load->view("SUPER_ADMIN/deletestudents_view",$batchdata);
    }

    public function changebatch() {
        $bcnew = $_POST['studvalue'];
        $bcid = $_POST['bcid1'];
        if ($this->Change_batch($bcnew, $bcid)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function createprn() {

        //$rollno = $this->input->post('rollno');  

        if ($this->checkkUser() == "SUPER_ADMIN") {

            $rollno = $this->input->post('rollno');

            $this->fetch_model->prn($rollno);
        }
    }

    public function editBatch($batch_id, $course_id) {
        $batchdata['data'] = $this->edit_batch($batch_id, $course_id);
        $batchdata['courseid'] = $course_id;
        //$batchdata['cname']=$this->changecourse();
        $batchdata['cname'] = $this->changebatch_course($batch_id, $course_id);
        //$courseid=$course_id;


        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view("SUPER_ADMIN/editbatch_view", $batchdata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    /////////-------Manage Batch------------------------------------------------
    public function dashboard() {
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/admin_home');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function updatebatch() {

        $courseid = $this->input->post('cname');
        $info['specialization_id'] = $this->input->post('spec');
        $data['batch_name'] = $this->input->post('bname');
        $data['batch_id'] = $this->input->post('batch_id');
        $data['in_year'] = $this->input->post('inyear');
        $data['out_year'] = $this->input->post('outyear');
        $data['no_of_terms'] = $this->input->post('terms');
        $data['course_id'] = $this->input->post('cname');
        $status = $this->input->post('enabled');

        if ($status == "on") {
            $data['enabled'] = 1;
        } else {
            $data['enabled'] = 0;
        }

        if ($this->updatebatch_function($data, $info, $courseid)) {

            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Batch has been updated successfully!");
                redirect('superadmin/editBatch/' . $this->input->post('batch_id') . '/' . $courseid);
            } else {
                $this->session->set_flashdata("errormsg", "You can not update specialization of this batch!");
                redirect('superadmin/editBatch/' . $this->input->post('batch_id') . '/' . $courseid);
            }
        } else {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Error in updating Batch!");
                redirect('superadmin/editBatch/' . $this->input->post('batch_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function createstudent() {
        $batchdata['bname'] = $this->batchcourse_List();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view("SUPER_ADMIN/createstudent_view", $batchdata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function insertstudent() {
        $status1 = $this->input->post('status');
        if ($status1 == "on") {
            $status = 1;
        } else {
            $status = 0;
        }

        $password = $this->input->post('prn');
        $student = 'STUDENT';
        $credentials = array('username' => $this->input->post('email'),
            'password' => md5($password),
            'role' => $student,
            'status' => $status);


        $this->insert_student_main($credentials);
    }

    public function insert_student1($credid) {
        $bname1 = $this->input->post('bname1');
        $fname = ucfirst($this->input->post('fname'));
        $mname = ucfirst($this->input->post('mname'));
        $lname = ucfirst($this->input->post('lname'));
        $dob = date('Y-m-d', strtotime($this->input->post('dob')));
        $data = array(
            'PRN' => $this->input->post('prn'),
            'first_name' => $fname,
            'middle_name' => $mname,
            'last_name' => $lname,
            'DOB' => $dob,
            'gender' => $this->input->post('gender'),
            //'primary_email'=>$this->input->post('email1'), 
            'allocated_email' => $this->input->post('email'),
            //'contact_no_1'=>$this->input->post('contact'),
            'credentials_id' => $credid
        );


        if ($this->insert_student1_main($data, $bname1)) {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Student has been added successfully!");
                redirect('superadmin/createstudent');
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Error in adding Student!");
                redirect('superadmin/createstudent');
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function view_student() {
        $data['info'] = $this->viewstudent();
        //$this->load->view('viewStudent',$data);
    }

    public function view_students() { //forword to super_Admin dashboard
        $bid = $_POST['bid'];
        $info = $this->viewstudents_main($bid);
        echo $json = json_encode($info);
    }

    public function createcourse() {
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/createcourse_view');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function insertcourse() {

        $data = array('course_name' => $this->input->post('cname'),
            'status' => $this->input->post('status'));


        if ($this->create_course($data)) {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Course has been created successfully!");
                redirect('superadmin/createcourse');
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Error in creating Course!");
                redirect('superadmin/createcourse');
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function managecourses() {
        $data['course'] = $this->viewcourse();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/managecourses_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function deletecourse($course_id) {
        if ($this->delete_course($course_id)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function editcourse($courseid) {
        $coursedata['data'] = $this->edit_course($courseid);
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view("SUPER_ADMIN/editcourse_view", $coursedata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function updatecourse() {
        $data['course_name'] = $this->input->post('cname');
        $data['course_id'] = $this->input->post('course_id');

        $status = $this->input->post('status');
        if ($status == "on") {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        if ($this->updatecourse_function($data)) {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Course has been updated successfully!");
                redirect('superadmin/editcourse/' . $this->input->post('course_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Error in updating course!");
                redirect('superadmin/editcourse/' . $this->input->post('course_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function createspecialization() {
        $batchdata['cname'] = $this->changecourse();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/createspecialization_view', $batchdata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function insertspecialization() {
        $cname = $this->input->post('cname');

        $sname = array('specialization_name' => $this->input->post('sname'),
            'status' => $this->input->post('status'));
        if ($this->specialization($sname, $cname)) {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "specialization has been created successfully!");
                redirect('superadmin/createspecialization');
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Error in updating course!");
                redirect('superadmin/createspecialization');
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function managespecializations() {
        $data['info'] = $this->viewspl();

        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/managespecializations_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function editspecialization($specialization_id) {

        $batchdata['data'] = $this->edit_spl($specialization_id);
        //$batchdata['cname']=$this->changecourse();
        $batchdata['cname'] = $this->course_spl($specialization_id);
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view("SUPER_ADMIN/editspecialization_view", $batchdata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function updatespecialization() {
        $data['specialization_name'] = $this->input->post('sname');
        $data['specialization_id'] = $this->input->post('specialization_id');
        $data['course_id'] = $this->input->post('cname');
        $status = $this->input->post('enabled');
        if ($status == "on") {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }
        if ($this->updatespl_function($data)) {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Specialization has been updated successfully!");
                redirect('superadmin/editspecialization/' . $this->input->post('specialization_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("errormsg", "Error in updating specialization!");
                redirect('superadmin/editspecialization/' . $this->input->post('specialization_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function deletespecialization($specialization_id) {

        if ($this->delete_spl($specialization_id)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function studentlist() {
        //$cname=$this->input->post('cname');
        $bname1 = $this->input->post('bname1');
        $batchdata['sel'] = $bname1;
        $batchdata['info'] = $this->displaystudent($bname1);
        $batchdata['bname'] = $this->batchcourse_List();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/managestudents_view', $batchdata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function managestudents() {
        $batchdata['bname'] = $this->batchcourse_List();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view("SUPER_ADMIN/managestudents_view", $batchdata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function editstudent($sid) {

        $batchdata['bname'] = $this->batchcourse_List();
        $batchdata['stud'] = $this->edit_stud($sid);
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view("SUPER_ADMIN/editstudent_view", $batchdata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function updatestudent() {

        $data['credentials_id'] = $this->input->post('credentials_id');
        $data['PRN'] = $this->input->post('prnno');

        $data['bid'] = $this->input->post('bname1');

        $data['student_id'] = $this->input->post('student_id');
        $data['first_name'] = $this->input->post('fname');
        $data['middle_name'] = $this->input->post('mname');
        $data['last_name'] = $this->input->post('lname');
        $data['username'] = $this->input->post('uname');
        $data['DOB'] = date('Y-m-d', strtotime($this->input->post('dob')));
        $data['gender'] = $this->input->post('gender');


        echo $status = $this->input->post('status');
        if ($status == "on") {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        if ($this->updatestud_function($data)) {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Student has been updated successfully!");
                redirect('superadmin/editstudent/' . $this->input->post('credentials_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("errormsg", "Error in updating Student!");
                redirect('superadmin/editstudent/' . $this->input->post('credentials_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function deletestudent() {


        $batchid = $this->input->post('batchid');
        $sid = $this->input->post('id');
        $this->session->set_userdata('studentid', $batchid);


        if ($this->stud_delete($sid)) {
            $this->session->set_userdata('studentid', $batchid);

            echo "true";
        } else {
            //  $this->session->set_flashdata("errormsg","Error in updating Student!");
            // redirect('superadmin/managestudents');
            echo "false";
        }
    }

    // code on 10/06/2015 by kaushal

    public function deletemultistudent() {
        $batchid = $this->input->post('batchid');
        $sid = $this->input->post('id');
        //$this->session->set_userdata('studentid',$batchid);

        $statusarr = explode(',', $sid);

        for ($i = 0; $i < (count($statusarr) - 1); $i++) {
            //echo $statusarr[$i];
            $this->stud_delete($statusarr[$i]);
        }
        echo "true";
    }

    ////----------------------------FACULTY MANUALLY-----------------///
    public function createfaculty() {
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view("SUPER_ADMIN/createfaculty_view");
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function insertfaculty() {
        $status1 = $this->input->post('status');
        if ($status1 == "on") {
            $status = 1;
        } else {
            $status = 0;
        }

        $password = $this->input->post('contact');
        $faculty = 'FACULTY';
        $credentials = array('username' => $this->input->post('email'),
            'password' => md5($password),
            'role' => $faculty,
            'status' => $status);


        $this->insert_faculty_main($credentials);
    }

    public function insert_faculty1($credid) {
        $status1 = $this->input->post('status');
        if ($status1 == "on") {
            $status = 1;
        } else {
            $status = 0;
        }

        $fname = ucfirst($this->input->post('fname'));
        $mname = ucfirst($this->input->post('mname'));
        $lname = ucfirst($this->input->post('lname'));
        $dob = date('Y-m-d', strtotime($this->input->post('dob')));
        $data = array(
            'first_name' => $fname,
            'middle_name' => $mname,
            'last_name' => $lname,
            'DOB' => $dob,
            'gender' => $this->input->post('gender'),
            'primary_email' => $this->input->post('email1'),
            'allocated_email' => $this->input->post('email'),
            'contact_no_1' => $this->input->post('contact'),
            'credentials_id' => $credid,
            'status' => $status
        );


        if ($this->insert_faculty1_main($data)) {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Faculty has been added successfully!");
                redirect('superadmin/createfaculty');
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Error in adding Faculty!");
                redirect('superadmin/createfaculty');
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function managefaculty() {
        $data['faculty'] = $this->Get_faculty();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/managefaculty_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function editfaculty($fid) {

        $data['fac'] = $this->edit_faculty($fid);
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view("SUPER_ADMIN/editfaculty_view", $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function updatefaculty() {

        $data['credentials_id'] = $this->input->post('credentials_id');
        $data['faculty_id'] = $this->input->post('faculty_id');
        $data['first_name'] = $this->input->post('fname');
        $data['middle_name'] = $this->input->post('mname');
        $data['last_name'] = $this->input->post('lname');
        $data['username'] = $this->input->post('uname') . domain_name;
        $data['DOB'] = date('Y-m-d', strtotime($this->input->post('dob')));
        $data['gender'] = $this->input->post('gender');
        $data['contact_no_1'] = $this->input->post('contact');


        $status = $this->input->post('status');
        if ($status == "on") {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        if ($this->updatefaculty_function($data)) {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("message", "Faculty has been updated successfully!");
                redirect('superadmin/editfaculty/' . $this->input->post('credentials_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "SUPER_ADMIN") {
                $this->session->set_flashdata("errormsg", "Error in updating Faculty!");
                redirect('superadmin/editfaculty/' . $this->input->post('credentials_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function deletefaculty($fid) {
        if ($this->fac_delete($fid)) {

            echo "true";
        } else {

            echo "false";
        }
    }

    /////---------------------------FACULTY MANUALLY-------------------//
    public function fetchspeclist() {

        $cname = $_POST['cname'];

        $qry = "SELECT s.specialization_name, s.specialization_id 
	 FROM tbl_specialization s, tbl_course_specialization tcs, master_course c
	 where s.specialization_id = tcs.specialization_id and
 	 s.isdelete = 0 and c.isdelete = 0 and s.status = 1 and s.isdelete = 0
	 AND c.course_id = tcs.course_id AND  tcs.course_id='" . $cname . "'";
        $result = mysql_query($qry);

        $results = array();
        while ($row = mysql_fetch_array($result)) {
            $results[] = array(
                'specialization_id' => $row['specialization_id'],
                'specialization_name' => $row['specialization_name']
            );
        }
        echo $json = json_encode($results);
    }

    public function fetchspeclist1() {

        $cname = $_POST['cname'];
        $batchid = $_POST['bid'];
        /* echo "SELECT batch_course_id from tbl_batch_course where batch_id='$batchid'and course_id='$cname'";
          $qry2= mysql_query("SELECT batch_course_id from tbl_batch_course where batch_id='$batchid'and course_id='$cname'");
          $res=mysql_fetch_array($qry2);
          if(mysql_num_rows($qry2) == 1)
          {
          //$row = $qry2->row();

          echo $bcid=$res['batch_course_id'];
          //echo $cid;

          } */
        $numqry = "SELECT s.specialization_name, s.specialization_id 
	 FROM tbl_specialization s, tbl_course_specialization tcs, master_course c
	 where s.specialization_id = tcs.specialization_id
	and s.isdelete = 0 
	 AND c.course_id = tcs.course_id AND  tcs.course_id='" . $cname . "'";

        $result = mysql_query($numqry);

        $results = array();

        while ($row = mysql_fetch_array($result)) {


            $qrychk = "select * from batch_course_specialization where spl_id = $row[specialization_id] and batch_course_id = '$batchid'";
            $resultchk = mysql_query($qrychk);
            $y = mysql_num_rows($resultchk);

            $results[] = array(
                'specialization_id' => $row['specialization_id'],
                'specialization_name' => $row['specialization_name'],
                'numqry' => 1,
                'y' => $y
            );
        }
        echo $json = json_encode($results);
    }

    public function batch_subject() {
        $batchdata['bname'] = $this->batchcourse_List();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('batch_subject', $batchdata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function fetchterm() {

        $bname = $_POST['bname'];

        $qry = "SELECT no_of_terms
	  FROM tbl_batch_course
	  WHERE batch_course_id ='" . $bname . "'";
        $result = mysql_query($qry);

        $results = array();
        while ($row = mysql_fetch_array($result)) {
            $results[] = array(
                'no_of_terms' => $row['no_of_terms']);
        }
        echo $json = json_encode($results);
    }

    public function fetchsubjlist() {
        $bname = $_POST['bname'];
        $qry = "SELECT tbc.course_id, ms.subject_id, ms.subject_name,ms.subject_id
	FROM master_subject ms, tbl_batch_course tbc, tbl_course_subject cs
	WHERE tbc.course_id = cs.courseid
	AND cs.subject_id = ms.subject_id and ms.isdelete=0 and tbc.batch_course_id='" . $bname . "'";
        $result = mysql_query($qry);

        $results = array();
        while ($row = mysql_fetch_array($result)) {

            $results[] = array(
                'subject_name' => $row['subject_name']);
        }

        echo $json = json_encode($results);
    }

    public function viewbatchspecialization() {
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $batchdata['bname'] = $this->batchcourse_List();

            $this->load->view("SUPER_ADMIN/managebatchspl_view", $batchdata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function viewbatchspl() {
        $bname = $_POST['bname'];
        $qry = "SELECT specialization_id, specialization_name
	 FROM tbl_specialization s, batch_course_specialization bcs
                 WHERE bcs.spl_id = s.specialization_id
                 AND bcs.batch_course_id ='" . $bname . "'";
        $result = mysql_query($qry);

        $results = array();
        while ($row = mysql_fetch_array($result)) {

            $results[] = array(
                'splecialization_name' => $row['specialization_name']);
        }

        echo $json = json_encode($results);
    }

//================================= HIMANI ==================================================	

    public function addbulkstudent() {
        $data['student'] = $this->Get_student();
        //$data['student'] = $this->csv_model->get_student();
        $data1['bname'] = $this->Get_batch_course_list();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/addbulkstudent_view', array_merge($data, $data1));
        } else {
            redirect('welcome/unAuthority');
        }
    }

    /* function download(){
      $data3 = file_get_contents(base_url()."samples/sample.csv"); // Read the file's contents
      $name = 'sample.csv';
      force_download($name, $data3);
      } */

    /* function importcsv1() {
      $data['student'] = $this->Get_student();

      if($_FILES['userfile']['name']!="")
      {
      $file = uploadFile($_FILES["userfile"]["name"],'userfile');
      }

      $ext=end((explode(".", $file)));


      if ($ext!='csv') {
      $this->session->set_flashdata('errormsg',"Please Upload Csv file Only");
      redirect(base_url().'superadmin/addbulkstudent');
      }

      else {
      //$file_data = $this->upload->data();
      //$file=$file_data['file_name'];
      $file_path =  './uploads/excelfile/'.$file;



      if ($this->csvimport->get_array($file_path)) {
      $csv_array = $this->csvimport->get_array($file_path);

      $i=0;
      $d=array();
      $mul=array();
      foreach ($csv_array as $row)
      {           $status=1;
      $student='STUDENT';
      $insert_data = array(
      'username'=>$row['allocated_email'],
      'password'=>md5($row['contact_no']),
      'role'=>$student,
      'status'=>$status
      );

      $check_data= array(
      'PRN'=>$row['prn_no'],
      'first_name'=>$row['first_name'],
      'middle_name'=>$row['middle_name'],
      'last_name'=>$row['last_name'],
      'gender'=>$row['gender(m/f)'],
      'DOB'=>$row['dob(YYYY-MM-DD)'],
      'primary_email'=>$row['primary_email'],
      'allocated_email'=>$row['allocated_email']
      );
      $sql1 = "SELECT * FROM tbl_student_information WHERE allocated_email='" . $row['allocated_email'] . "' or PRN='" . $row['prn_no']. "'";
      $already_exists1 = $this->db->query($sql1);
      if (($already_exists1->num_rows() == 0) && (($row['primary_email']!=NULL) && ($row['allocated_email']!=NULL) && ($row['contact_no']!=NULL) && ($row['prn_no']!=NULL) && ($row['first_name']!=NULL) && ($row['middle_name']!=NULL) && ($row['last_name']!=NULL) && ($row['dob(YYYY-MM-DD)']!=NULL) &&($row['gender(m/f)']!=NULL)) && ((preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $row['primary_email']))  && (preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $row['allocated_email'])) &&  (preg_match("/^[0-9]{10}$/", $row['contact_no']))  &&  (preg_match('/^[a-z]+$/i',$row['first_name']))  && (preg_match('/^[a-z]+$/i',$row['last_name'])) && (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$row['dob(YYYY-MM-DD)']))
      && (preg_match('/^[a-z0-9]{1,20}$/i',$row['prn_no']))&& (preg_match('/^[m,f]+$/i',$row['gender(m/f)'])) && (preg_match('/^[a-z]+$/i',$row['middle_name']))))

      {


      $this->Insert_credential($insert_data);
      $fname=ucfirst($row['first_name']);
      $mname=ucfirst($row['middle_name']);
      $lname=ucfirst($row['last_name']);
      $gender=strtoupper($row['gender(m/f)']);
      $crd_id=$this->db->insert_id();
      $insert_data1 = array(
      'PRN'=>$row['prn_no'],
      'first_name'=> $fname,
      'middle_name'=>$mname,
      'last_name'=>$lname,
      'DOB'=>$row['dob(YYYY-MM-DD)'],
      'gender'=>$gender,
      'primary_email'=>$row['primary_email'],
      'allocated_email'=>$row['allocated_email'],
      'contact_no_1'=>$row['contact_no'],
      'credentials_id'=>$crd_id
      );
      $this->Insert_student($insert_data1);
      $student_id=$this->db->insert_id();
      $bname1= $this->input->post('bname1');
      $batch_student = array(
      'batch_course_id'=>$bname1,
      'student_id'=>$student_id
      );
      $this->Insert_batch_student($batch_student);


      $i++;

      }
      else
      {

      $d = array(
      'PRN'=>$row['prn_no'],
      'first_name'=>$row['first_name'],
      'middle_name'=>$row['middle_name'],
      'last_name'=>$row['last_name'],
      'DOB'=>$row['dob(YYYY-MM-DD)'],
      'gender'=>$row['gender(m/f)'],
      'primary_email'=>$row['primary_email'],
      'contact_no_1'=>$row['contact_no']

      );
      $mul[]=$d;
      }

      }
      // for loop end
      if($i==0)
      {
      $data['error']=$mul;
      $data1['bname'] = $this->Get_batch_course_list();
      if($this->checkkUser() == "SUPER_ADMIN")
      {
      $this->load->view('SUPER_ADMIN/addbulkstudent_view',array_merge($data,$data1));
      }
      else
      {
      redirect('welcome/unAuthority');
      }
      }
      else
      {
      if($this->checkkUser() == "SUPER_ADMIN")
      {
      $this->session->set_flashdata('message', $i." ".'records have been imported successfully');
      redirect(base_url().'superadmin/addbulkstudent');
      }
      else
      {
      redirect('welcome/unAuthority');
      }
      }

      }
      }

      } */

    function importcsv1() {
        $data['student'] = $this->Get_student();

        if ($_FILES['userfile']['name'] != "") {
            $file = uploadFile($_FILES["userfile"]["name"], 'userfile');
        }

        $ext = end((explode(".", $file)));

        if ($ext != 'csv') {
            $this->session->set_flashdata('errormsg', "Please Upload Csv file Only");
            redirect(base_url() . 'superadmin/addbulkstudent');
        } else {
            //$file_data = $this->upload->data();
            //$file=$file_data['file_name']; 
            $file_path = './uploads/excelfile/' . $file;



            if ($this->csvimport->get_array($file_path)) {
                $csv_array = $this->csvimport->get_array($file_path);

                $i = 0;
                $count = 0;
                $d = array();
                $mul = array();
                foreach ($csv_array as $row) {
                    $status = 1;
                    $student = 'STUDENT';
                    $insert_data = array(
                        'username' => $row['allocated_email'],
                        'password' => md5($row['prn_no']),
                        'role' => $student,
                        'status' => $status
                    );
                    $dob = date('Y-m-d', strtotime($row['dob(DD-MM-YYYY)']));
                    $check_data = array(
                        'PRN' => $row['prn_no'],
                        'first_name' => $row['first_name'],
                        'middle_name' => $row['middle_name'],
                        'last_name' => $row['last_name'],
                        'gender' => $row['gender(m/f)'],
                        'DOB' => $dob,
                        //'primary_email'=>$row['personal_email'],
                        'allocated_email' => $row['allocated_email']
                    );
                    $sql1 = "SELECT * FROM tbl_student_information WHERE allocated_email='" . $row['allocated_email'] . "' or PRN='" . $row['prn_no'] . "'";
                    $already_exists1 = $this->db->query($sql1);
                    //print_r($already_exists1->num_rows());exit;
                    /* if (($already_exists1->num_rows() == 0) && (($row['allocated_email']!=NULL) && ($row['prn_no']!=NULL) && ($row['first_name']!=NULL)  && ($row['dob(YYYY-MM-DD)']!=NULL) &&($row['gender(m/f)']!=NULL)) && ((preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $row['allocated_email'])) && (preg_match('/^[a-z]+$/i',$row['first_name'])) && (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$row['dob(YYYY-MM-DD)']))
                      && (preg_match('/^[a-z0-9]{1,20}$/i',$row['prn_no'])) && (preg_match('/^[m,f]+$/i',$row['gender(m/f)']))) || ((preg_match('/^[a-z]+$/i',$row['middle_name'])) && (preg_match('/^[a-z]+$/i',$row['last_name'])))) */

                    if (($already_exists1->num_rows() == 0) && (($row['allocated_email'] != NULL) && ($row['prn_no'] != NULL) && ($row['first_name'] != NULL) && ($row['last_name'] != NULL) && ($row['dob(DD-MM-YYYY)'] != NULL) && ($row['gender(m/f)'] != NULL)) && ((preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $row['allocated_email'])) && (preg_match('/^[a-z]+$/i', $row['first_name'])) && (preg_match('/^[a-z]+$/i', $row['last_name'])) && (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/", $row['dob(DD-MM-YYYY)'])) && (preg_match('/^[a-z0-9]{1,20}$/i', $row['prn_no'])) && (preg_match('/^[m,f]+$/i', $row['gender(m/f)']))) && ((preg_match('/^[a-z]+$/i', $row['middle_name'])) || ($row['middle_name'] == NULL))) {
                        //echo 'sdfds';exit;

                        $this->Insert_credential($insert_data);
                        $fname = ucfirst($row['first_name']);
                        $mname = ucfirst($row['middle_name']);
                        $lname = ucfirst($row['last_name']);
                        $gender = strtoupper($row['gender(m/f)']);
                        $crd_id = $this->db->insert_id();
                        $insert_data1 = array(
                            'PRN' => $row['prn_no'],
                            'first_name' => $fname,
                            'middle_name' => $mname,
                            'last_name' => $lname,
                            'DOB' => $dob,
                            'gender' => $gender,
                            //'primary_email'=>$row['personal_email'],
                            'allocated_email' => $row['allocated_email'],
                            //'contact_no_1'=>$row['contact_no'],	
                            'credentials_id' => $crd_id
                        );
                        $this->Insert_student($insert_data1);
                        $student_id = $this->db->insert_id();
                        $bname1 = $this->input->post('bname1');
                        $batch_student = array(
                            'batch_course_id' => $bname1,
                            'student_id' => $student_id
                        );
                        $this->Insert_batch_student($batch_student);


                        $i++;
                    } else {
                        // echo 'neww';exit;

                        $d = array(
                            'PRN' => $row['prn_no'],
                            'first_name' => $row['first_name'],
                            'middle_name' => $row['middle_name'],
                            'last_name' => $row['last_name'],
                            'DOB' => $row['dob(DD-MM-YYYY)'],
                            'gender' => $row['gender(m/f)'],
                            'allocated_email' => $row['allocated_email'],
                                //'contact_no_1'=>$row['contact_no']	
                        );
                        $mul[] = $d;
                        //print_r($mul);exit;
                    }
                    $count++;
                }
                // for loop end
                // echo $i;
                //echo $count;
                //print_r($mul); exit;
                if (($i == 0) || ($i < $count)) {
                    //echo $i;
                    $importedcout = $i;
                    //echo $count; exit;
                    $data['error'] = $mul;
                    $data1['bname'] = $this->Get_batch_course_list();
                    if ($this->checkkUser() == "SUPER_ADMIN") {
                        //$this->session->set_flashdata('message', $importedcout." ".'records have been imported successfully');
                        $data['message'] = $importedcout . " " . 'records have been imported successfully';
                        $this->load->view('SUPER_ADMIN/addbulkstudent_view', array_merge($data, $data1));
                    } else {
                        redirect('welcome/unAuthority');
                    }
                } else {
                    //echo 'sdfds';exit;
                    if ($this->checkkUser() == "SUPER_ADMIN") {
                        $this->session->set_flashdata('message', $i . " " . 'records have been imported successfully');
                        redirect(base_url() . 'superadmin/addbulkstudent');
                    } else {
                        redirect('welcome/unAuthority');
                    }
                }
            }
        }
    }

    //////--------BULK FACULTY-------------------------------------------
    public function addbulkfaculties() {
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/addbulkfaculty_view');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    /* function download(){
      $data3 = file_get_contents(base_url()."samples/sample.csv"); // Read the file's contents
      $name = 'sample.csv';
      force_download($name, $data3);
      } */

    function importcsv2() {
        if ($_FILES['userfile']['name'] != "") {
            $file = uploadFile($_FILES["userfile"]["name"], 'userfile');
        }

        $ext = end((explode(".", $file)));

        if ($ext != 'csv') {
            $this->session->set_flashdata('errormsg', "Please Upload Csv file Only");
            redirect(base_url() . 'superadmin/addbulkfaculties');
        } else {

            $file_path = './uploads/excelfile/' . $file;

            if ($this->csvimport->get_array($file_path)) {

                $csv_array = $this->csvimport->get_array($file_path);
                $count = 0;
                $i = 0;
                $d = array();
                $mul = array();
                foreach ($csv_array as $row) {
                    $status = 1;
                    $faculty = 'FACULTY';
                    $insert_data = array(
                        'username' => @($row['allocated_email']),
                        'password' => @(md5($row['contact_no'])),
                        'role' => $faculty,
                        'status' => $status
                    );
                    $dob = date('Y-m-d', strtotime($row['dob(DD-MM-YYYY)']));
                    $check_data = array(
                        'first_name' => $row['first_name'],
                        'middle_name' => $row['middle_name'],
                        'last_name' => $row['last_name'],
                        'gender' => $row['gender(m/f)'],
                        'DOB' => $dob,
                        //'primary_email'=>$row['primary_email'],
                        'allocated_email' => $row['allocated_email']
                    );
                    $sql1 = "SELECT * FROM tbl_faculty WHERE allocated_email='" . $row['allocated_email'] . "'";
                    $already_exists1 = $this->db->query($sql1);
                    /* if (($already_exists1->num_rows() == 0) && (($row['primary_email']!=NULL) && (@($row['allocated_email'])!=NULL) && ($row['contact_no']!=NULL) && ($row['first_name']!=NULL) && ($row['middle_name']!=NULL) && ($row['last_name']!=NULL) && ($row['dob(YYYY-MM-DD)']!=NULL) &&($row['gender(m/f)']!=NULL)) && ((preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $row['primary_email']))  && (preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $row['allocated_email'])) &&  (preg_match("/^[0-9]{10}$/", $row['contact_no']))  &&  (preg_match('/^[a-z]+$/i',$row['first_name']))  && (preg_match('/^[a-z]+$/i',$row['last_name'])) && (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$row['dob(YYYY-MM-DD)'])) && (preg_match('/^[m,f]+$/i',$row['gender(m/f)'])) && (preg_match('/^[a-z]+$/i',$row['middle_name'])))) */

                    if (($already_exists1->num_rows() == 0) && (($row['allocated_email'] != NULL) && ($row['contact_no'] != NULL) && ($row['first_name'] != NULL) && ($row['last_name'] != NULL) && ($row['dob(DD-MM-YYYY)'] != NULL) && ($row['gender(m/f)'] != NULL)) && ((preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $row['allocated_email'])) && (preg_match('/^[a-z]+$/i', $row['first_name'])) && (preg_match('/^[a-z]+$/i', $row['last_name'])) && (preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/", $row['dob(DD-MM-YYYY)'])) && (preg_match('/^[0-9]{10}$/i', $row['contact_no'])) && (preg_match('/^[m,f]+$/i', $row['gender(m/f)']))) && ((preg_match('/^[a-z]+$/i', $row['middle_name'])) || ($row['middle_name'] == NULL))) {
                        $this->Insert_credential($insert_data);
                        $fname = ucfirst($row['first_name']);
                        $mname = ucfirst($row['middle_name']);
                        $lname = ucfirst($row['last_name']);
                        $gender = strtoupper($row['gender(m/f)']);
                        $crd_id = $this->db->insert_id();


                        $insert_data1 = array(
                            'first_name' => $fname,
                            'middle_name' => $mname,
                            'last_name' => $lname,
                            'DOB' => $dob,
                            'gender' => $gender,
                            //'primary_email'=>$row['primary_email'],
                            'allocated_email' => $row['allocated_email'],
                            'status' => $status,
                            'contact_no_1' => $row['contact_no'],
                            'credentials_id' => $crd_id
                        );
                        $this->Insert_faculty($insert_data1);
                        $i++;
                    } else {

                        $d = array(
                            'first_name' => $row['first_name'],
                            'middle_name' => $row['middle_name'],
                            'last_name' => $row['last_name'],
                            'DOB' => $row['dob(DD-MM-YYYY)'],
                            'gender' => $row['gender(m/f)'],
                            'allocated_email' => $row['allocated_email'],
                            'contact_no_1' => @($row['contact_no'])
                        );
                        $mul[] = $d;
                    }
                    $count++;
                }
                // for loop end
                if (($i == 0) || ($i < $count)) {
                    $data['error'] = $mul;
                    if ($this->checkkUser() == "SUPER_ADMIN") {
                        $data['message'] = $i . " " . 'records have been imported successfully';
                        $this->load->view('SUPER_ADMIN/addbulkfaculty_view', $data);
                    } else {
                        redirect('welcome/unAuthority');
                    }
                } else {
                    if ($this->checkkUser() == "SUPER_ADMIN") {
                        $this->session->set_flashdata('message', $i . " " . 'records have been imported successfully');
                        redirect(base_url() . 'superadmin/addbulkfaculties');
                    } else {
                        redirect('welcome/unAuthority');
                    }
                }
            }
        }
    }

    ////-----------BULK FACULTY---------------------------------------
// create placement admin

    public function createPlacementAdmin() { //forword to admin dashboard
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/createteam_view.php');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function fetchusertypelist() {
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $usertype = $_POST['usertype'];
            if ($usertype == 2) {
                $groupname = $_POST['groupname'];
                //echo "select * from tbl_placement_team where user_type = 'Faculty' and group_id = '$groupname'";
                $numfaculty = mysql_query("select * from tbl_placement_team where user_type = 'Faculty' and group_id = '$groupname'");

                if (mysql_num_rows($numfaculty) > 0) {
                    //$qry = "select tf.*, tpt.credentials_id as chkcredid from tbl_faculty tf left outer join tbl_placement_team tpt on tf.credentials_id = tpt.credentials_id and tpt.group_id = '$groupname' and tpt.isdeleted = 0 where tf.isdelete = 0 order by chkcredid desc";

                    $qry = "select tf.*, tpt.credentials_id as chkcredid from tbl_faculty tf left outer join tbl_placement_team tpt on tf.credentials_id = tpt.credentials_id and tpt.group_id = '$groupname' and tpt.isdeleted = 0 where tf.status = 1 and tf.isdelete = 0 order by chkcredid desc";

                    $result = mysql_query($qry);
                    $results = array();
                    while ($row = mysql_fetch_array($result)) {
                        if ($row['chkcredid'] != '') {
                            $facultyy = 1;
                        } else {
                            $facultyy = 0;
                        }
                        $results[] = array(
                            'credentials_id' => $row['credentials_id'],
                            'first_name' => $row['first_name'],
                            'last_name' => $row['last_name'],
                            'chkcredid' => $row['chkcredid'],
                            'numfaculty' => 1,
                            'facultyy' => $facultyy
                        );
                    }
                    echo $json = json_encode($results);
                } else {
                    $qry = "select * from tbl_faculty";
                    $result = mysql_query($qry);

                    $results = array();
                    while ($row = mysql_fetch_array($result)) {
                        $results[] = array(
                            'credentials_id' => $row['credentials_id'],
                            'first_name' => $row['first_name'],
                            'last_name' => $row['last_name'],
                            'numfaculty' => 0
                        );
                    }
                    echo $json = json_encode($results);
                }
            } else {
                redirect('welcome/noauthority');
            }
        }
    }

    public function createteam_main() {
        if ($this->checkkUser() == "SUPER_ADMIN") {
            // $info['user_type'] = $this->input->post('usertype');
            // $this->session->set_userdata("selectedGroup", $info['user_type']);
            // 	if($info['user_type'] == 2)
            // 	{
            $info['user_type'] = 'Faculty';
            $info['credentials_id'] = $this->input->post('Faculty');
            // }

            $info['group_id'] = 1;
            $info['created_by'] = $this->session->userdata("credentials_id"); // dynamic session login value
            // $info['StudentBatch'] = $this->input->post('StudentBatch');

            /* $status= $this->input->post('enabled');
              if($status == "on")
              {
              $info['status'] = 1;
              }
              else
              {
              $info['status'] = 0;
              } */
            $info['status'] = 1;

            //print_r($info);exit;

            if ($this->createteam_admin_function($info)) {
                $this->session->set_flashdata("successmessage", "Placement admin has been added successfully!");
                redirect('superadmin/createPlacementAdmin');
            } else {
                $this->session->set_flashdata("errormessage", "Error while add placement admin");
                redirect('superadmin/createPlacementAdmin');
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

    function changepassword() {
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/change_password_view.php');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function changepass_main($cid) {
        //echo $this->input->post('new_pwd');exit;
        //ini_set('memory_limit', '-1');
        //ini_set('memory_limit', '256M');
        //ob_flush();
        $data['password'] = md5($this->input->post('new_pwd'));

        if ($this->changePassword_main($data, $cid)) {
            $this->session->set_flashdata("sucess", "password has been successfully changed ");
        } else {
            $this->session->set_flashdata("error", "error while changing password");
        }
        redirect('superadmin/changepassword');
    }

    function checkOldPwd() {
        $cid = $_POST['cid'];
        //echo $cid.''.$_POST['oldpwd'];exit;
        if ($this->checkOldPwd_main($_POST['oldpwd'], $cid)) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function deletedstudents() {
        $batchdata['bname'] = $this->batchcourse_List();
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view("SUPER_ADMIN/deletedstudents_view", $batchdata);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    function view_deletedstudents() { //forword to super_Admin dashboard
        $bid = $_POST['bid'];
        $info = $this->viewdeletedstudents_main($bid);
        echo $json = json_encode($info);
    }

    function addmultistudent() {
        $abatchid = $this->input->post('abatchid');
        $sid = $this->input->post('id');
        //$this->session->set_userdata('studentid',$batchid);

        $statusarr = explode(',', $sid);

        for ($i = 0; $i < (count($statusarr) - 1); $i++) {
            //echo $statusarr[$i];
            $this->stud_add($statusarr[$i], $abatchid);
        }
        echo "true";
    }

}

?>