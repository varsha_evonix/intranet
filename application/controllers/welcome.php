<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();

class Welcome extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('fetch_model');
        $this->load->helper(array('url', 'form'));
        $this->load->library(array('session', 'form_validation'));
    }

    public function index() { // index function 
        $this->checkLogin();
    }

    public function login($cnf = false) { // load login view
        $value['cnf'] = $cnf;
        $this->load->view('login_view', $value);
    }

    public function superadmin() { //load super_admin/dashboad view
        if ($this->checkkUser() == "SUPER_ADMIN") {
            redirect('superadmin/home');
//			$this->load->view('SUPER_ADMIN/dashboard_view');
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function admin() { //load admin/dashboad view
        if ($this->checkkUser() == "FACULTY") {
            redirect('admin/index');

            //	$this->load->view('ADMIN/dashboard_view');
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function student() { //load student/dashboad view
        if ($this->checkkUser() == "STUDENT") {
            redirect('student/index');

            //$this->load->view('STUDENT/dashboard_view');
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function placementteam() { //load placementteam/dashboad view
        if ($this->checkkUser() == "PLACEMENT_TEAM") {
            redirect('placementteam/index');

            //$this->load->view('PLACEMENT_TEAM/dashboard_view');
        } else {
            redirect('welcome/noauthority');
        }
    }

    // added for guest user login
    public function guestuser() { //load guestuser/dashboad view
        if ($this->checkkUser() == "GUEST_USER") {
            //echo "asd"; exit;
            redirect('guestuser/assignedstudents');

            //	$this->load->view('GUEST_USER/dashboard_view');
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function block() {
        $this->load->view('bolckbyadmin_view');
    }

    // public function noAuthority()
    // {
    // 	$this->load->view('noauthority_view');
    // }


//    public function askpage() {
//        $this->load->view('askpage_view');
//    }

    public function blockbyadmin() {
        $this->load->view('blockbyadmin_view');
    }

    public function noauthority() { // for chacking authority of user if unauthorised then go to 500 view
        if ($this->checkkUser() == "SUPER_ADMIN") {
            $this->load->view('SUPER_ADMIN/500');
        } elseif ($this->checkkUser() == "FACULTY") {
            $data ['info'] = $this->admin_information_main($this->session->userdata("credentials_id")); // student inforamtion			
            if ($this->checkPT()) {
                $this->load->view('ADMIN/500', $data); //redirect to admin's 500 view
            } else {
                $this->session->set_flashdata("error", "you dont have any authority allocated by super admin");
                redirect('welcome/login');
            }
        } else if ($this->checkkUser() == "PLACEMENT_TEAM") {
            $data ['info'] = $this->student_information_main($this->session->userdata("student_id")); // student inforamtion		 	
            $this->load->view('PLACEMENT_TEAM/500', $data);
        } else if ($this->checkkUser() == "STUDENT") {
            $data ['info'] = $this->student_information_main($this->session->userdata("student_id")); // student inforamtion		 	
            redirect('student/noauthority', $data);
        }
        // added for guest user login
        else if ($this->checkkUser() == "GUEST_USER") {
            $this->load->view('GUEST_USER/500');
        } else {
            redirect('welcome/login'); //redirect to login	
        }
    }

    public function checkkUser() { // check user role
        return $this->session->userdata("role");
    }

    public function checkLogin() { // check if user is alredy loged id or not

        if ($this->session->userdata("role")) {
            if ($this->session->userdata("role") == "SUPER_ADMIN") {
                redirect('welcome/superadmin'); //redirect to admin's dashboard
            } else if ($this->session->userdata("role") == "FACULTY") {
                if ($this->checkPT()) {
                    redirect('welcome/admin'); //redirect to Placement team's dashboard
                } else {
                    redirect('welcome/noauthority');
                }
            } else if ($this->session->userdata("role") == "PLACEMENT_TEAM") {
                redirect('welcome/placementteam'); //redirect to student's dashboard
            }
            // added for guest user login
            else if ($this->session->userdata("role") == "GUEST_USER") {
                redirect('welcome/guestuser'); //redirect to admin's dashboard
            } else {
                redirect('welcome/student'); //redirect to student's dashboard
            }
        } else {
            redirect('welcome/login'); //redirect to login
        }
    }

    public function switchUser() {
        $role = $this->session->userdata("role");

        if ($role == "STUDENT") {
            if ($this->checkPT()) {
                $this->session->set_userdata("role", "PLACEMENT_TEAM");
                redirect('welcome/placementteam');
            } else {
                $this->noauthority();
            }

            /* $cid = $this->session->userdata("credentials_id");
              if($this->check_PT_Status($cid))
              {
              if($this->checkPT())
              {
              $this->session->set_userdata("role", "PLACEMENT_TEAM");
              redirect('welcome/placementteam');
              }
              else
              {
              $this->noauthority();
              }

              } */
        } else if ($role == "PLACEMENT_TEAM") {
            $this->ask_student();
        } else {
            $this->noauthority();
        }
    }

    public function logout() {
        $this->session->unset_userdata("user_id");
        $this->session->unset_userdata("role");
        $this->session->unset_userdata("credentials_id");
        $this->session->unset_userdata("student_id");
        $this->session->unset_userdata("stud_id");
        $this->session->unset_userdata("user_data");

        redirect('welcome/login');
    }

    public function login1() {
        $loginid = $this->input->post('loginid');
        $password = md5($this->input->post('password'));

        $data['loginInfo'] = $this->fetch_model->login($loginid, $password);

        if ($data['loginInfo']) {
            foreach ($data['loginInfo'] as $r) {

                if (($r->status) && (!$r->isdelete)) {
                    $this->session->set_userdata("credentials_id", $r->credentials_id);

                    if ($r->role == "SUPER_ADMIN") {
                   
                        $this->session->set_userdata("role", "SUPER_ADMIN");
                        redirect('welcome/superadmin');
                    } else if ($r->role == "FACULTY") {
                    
                        $this->session->set_userdata("role", "FACULTY");
                        if ($this->checkPT()) {
                            if ($this->check_PT_Status()) {
                                redirect('welcome/admin');
                            } else {
                                $this->session->set_flashdata("error", "you dont have any authority allocated by super admin");
                                redirect('welcome/blockbyadmin');
                            }
                        } else {
                            $this->session->set_flashdata("error", "you dont have any authority allocated by super admin");
                            redirect('welcome/noauthority');
                        }
                    }
                    // added for guest user login
                    else if ($r->role == "GUEST_USER") {
//                        echo '<pre>';print_r('elss');exit;
                        $this->session->set_userdata("role", "GUEST_USER");
//                        if ($this->chk_purpose()) {
//                            redirect('welcome/guestuser');
//                        } else {
                            $this->session->set_flashdata("error", "you dont have any authority allocated by super admin");
                            redirect('welcome/login');
//                        }
                    } else {
                        if ($this->checkPT()) {
                            
                            redirect('welcome/ask_student');
//                            redirect('welcome/askpage');
                        } else {
                            $this->ask_student();
                        }
                    }
                } else {
               
                    if (($r->status == 0) && ($r->isdelete == 1)) {
                        $this->session->set_flashdata("error", "You are deleted & block by admin");
                        redirect('welcome/blockbyadmin');
                    } else if ($r->status == 0) {
                        $this->session->set_flashdata("error", "You are block by admin");
                        redirect('welcome/blockbyadmin');
                    } else if ($r->isdelete == 1) {
                        $this->session->set_flashdata("error", "You are deleted by admin");
                        redirect('welcome/blockbyadmin');
                    }
                }
            }
        } else {
            $this->session->set_flashdata("error", "worng credentials");
            redirect('welcome/login');
        }
    }

    public function ask_placementteam() {
        $cid = $this->session->userdata("credentials_id");
        if ($this->check_PT_Status($cid)) {
            $this->session->set_userdata("role", "PLACEMENT_TEAM");
            $selectemail = "select student_id from tbl_student_information where credentials_id = '" . $this->session->userdata("credentials_id") . "'";
            $result = mysql_query($selectemail);
            $r = mysql_fetch_array($result);
            $num_rows = mysql_num_rows($result);

            if ($num_rows > 0) {
                $this->session->set_userdata("student_id", $r['student_id']);
            }


            redirect('welcome/placementteam');
        } else {
            $this->session->set_flashdata("error", "You are block by admin.");
            redirect('welcome/askpage');
        }
    }

    public function ask_student() {
        $this->session->set_userdata("role", "STUDENT");

        $selectemail = "select student_id from tbl_student_information where credentials_id = '" . $this->session->userdata("credentials_id") . "'";
        $result = mysql_query($selectemail);
        $r = mysql_fetch_array($result);
        $num_rows = mysql_num_rows($result);

        //original
        if ($num_rows > 0) {
            $this->session->set_userdata("student_id", $r['student_id']);
        }

        /* redirect('welcome/student'); */

        /* $this->session->set_userdata("student_id",$r['student_id']);

          redirect('welcome/student'); */

        $query = mysql_query("SELECT * FROM tbl_credentials WHERE agree = 0 and credentials_id = " . $this->session->userdata("credentials_id"));
        if (mysql_num_rows($query) == 0) {
            redirect('welcome/student');
        } else {
            redirect('welcome/login/cnf');
        }
    }

    public function approval() {
        $query = mysql_query("update tbl_credentials set agree = 1 where role = 'STUDENT' and credentials_id = " . $this->session->userdata("credentials_id"));
        redirect('welcome/student');
    }

    public function checkPT() {
        $cid = $this->session->userdata("credentials_id");
        $info = $this->fetch_model->checkPlacementTeam($cid);

        foreach ($info as $r) {
            if (($r->status == 1 ) && ($r->isdeleted == 0)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function check_PT_Status() {
        $cid = $this->session->userdata("credentials_id");
        $info = $this->fetch_model->model_check_PT_group_status($cid);
        if ($info) {
            return true;
        } else {
            return false;
        }
    }

    // by kaushal 09/06/2015

    function chk_purpose() {
        $cid = $this->session->userdata("credentials_id");
        $info = $this->fetch_model->model_check_guestuser_authority($cid);
        if ($info) {
            return true;
        } else {
            return false;
        }
    }

    function forgotpassword() {
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|callback_email_check');
        if ($this->form_validation->run() == TRUE) {
            $email = $this->input->post('email');
            $role = $this->input->post('role');
            $newpassword = $this->generateRandomString(8);
            $data = array('password' => md5($newpassword));
            $this->db->where('username', $email);
            $this->db->where('role', $role);
            $this->db->update('tbl_credentials', $data);


            include 'Massmailer/class.phpmailer.php';
            include_once 'Massmailer/language/phpmailer.lang-en.php';
            require 'Massmailer/class.smtp.php';

            $url = base_url();
            $message = "";
            $to = $email;
            $subject = "New password";
            /* $headers = "From:info@sitm.ac.in \r\n";
              $headers .= 'MIME-Version: 1.0' . "\r\n";
              $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";


              $message = "Your new password is as below<br />Password = <strong>".$newpassword."</strong><br />
              <a href='".$url."welcome/login' >Click here to login</a><br /><br /> <br />
              Regards, <br />
              Placement Team.";
              mail($to,$subject,$message,$headers); */

            $message = "Your new password is as below<br />Password = <strong>" . $newpassword . "</strong><br />
			<a href='" . $url . "welcome/login' >Click here to login</a><br /><br /> <br />
					 Regards, <br />
                     Placement Team.";


            $mail = new PHPMailer;
            $mail->Mailer = "mail";
            $mail->IsSMTP();                                      // Set mailer to use SMTP , Comment for server and uncomment for local server
            $mail->Host = 'smtp.mandrillapp.com';                 // Specify main and backup server
            $mail->Port = 587;                                    // Set the SMTP port
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'sitmplacementportal@gmail.com';                // SMTP username
            $mail->Password = '9pc0dAeM5JgIVeS_Cv4OUA';


            $mail->From = "info@sitm.ac.in";
            $mail->FromName = "SITM";
            //$mail->AddAddress('roykaushal89@gmail.com', 'Kaushal');  // Add a recipient
            $mail->AddAddress($to);               // Name is optional
            //$mail->WordWrap = 50;   
            $mail->IsHTML(true);                                  // Set email format to HTML

            $mail->Subject = $subject;
            $mail->Body = $message;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';		
            if ($mail->Send()) {
                //echo 'Message sent';exit;
            } else {
                //echo 'Message not sent';
                //echo "Mailer Error: " . $mail->ErrorInfo;exit;
            }

            $this->session->set_flashdata('succmsgpass', 'Your new password has been sent to your mail id.');
            redirect('welcome/forgotpassword', 'refresh');
        }
        $this->load->view('forgotpassword_view');
    }

    function email_check($str) {
        $role = $this->input->post('role');
        if ($this->checkEmailExist($str, $role)) {
            //echo $str.'sdfds';exit;
            $this->form_validation->set_message('email_check', 'Email address not exists.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function generateRandomString($length) {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

}
