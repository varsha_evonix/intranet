<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();

class Student extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

//---------------------------------------- REQUIRED FUNCTION FOR VALIDATION -------------------

    public function checkkUser() { // check user role
        return $this->session->userdata("role");
    }

    public function checkDegree() { // check user role
        if ($this->main_checkDegree($_POST['degreeName'])) {
            echo "true";
        } else {
            echo "flase";
        }
    }

//---------------------------------------- REQUIRED FUNCTION FOR VALIDATION -------------------	
    public function index() { //forword to admin dashboard
//        echo '<pre>';print_r($this->session->all_userdata());exit;
        if ($this->checkkUser() == "STUDENT") {
            $data ['info'] = $this->student_information_main($this->session->userdata("student_id")); // student inforamtion
            $this->load->view('STUDENT/dashboard_view', $data);
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function noauthority() { //forword to admin dashboard
        if ($this->checkkUser() == "STUDENT") {
            $data ['info'] = $this->student_information_main($this->session->userdata("student_id")); // student inforamtion
            $this->load->view('STUDENT/500.php', $data);
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function profile($sid) { //profile tab
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {
                    $this->profile_student($sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                $this->profile_student($sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function checkOldPwd() {
        $cid = $this->studCredentialId($_POST['sid']);
        if ($this->checkOldPwd_main($_POST['oldpwd'], $cid)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function changePassword($sid, $page) {

        $cid = $this->studCredentialId($sid);
        $data['password'] = md5($this->input->post('new_pwd'));

        if ($this->changePassword_main($data, $cid)) {
            $this->session->set_flashdata("sucess", "password has been successfully changed ");
        } else {
            $this->session->set_flashdata("error", "error while changing password");
        }
        $this->session->set_userdata("page_name", $page);
        redirect('student/profile/' . $sid);
    }

// change status of all forms 


    public function std_changeStudentInfoStatus($sid, $page, $id) {
        $this->session->set_userdata("page_name", $page);

        $data['verify_by_student'] = 1;
        $data['verify_PT_date'] = date('Y-m-d h:m:s');


        if ($this->std_changeStatus_main($id, $data, $page)) { // main controller
            $this->session->set_flashdata("sucess", "status has been sucesfully changed ");
        } else {
            $this->session->set_flashdata("error", "error while changing status");
        }
        redirect('student/profile/' . $sid);
    }

//std_changeStudentInfoStatus

    public function std_changeStudent_project_Status($sid, $page, $subPage, $id) {
        $this->session->set_userdata("page_name", $page);
        $this->session->set_userdata("subpage_name", $subPage);

        $data['verify_by_student'] = 1;
        $data['verify_PT_date'] = date('Y-m-d h:m:s');

        if ($this->std_changeStatus_main($id, $data, $page)) { // main controller
            $this->session->set_flashdata("sucess", "Status has been changed successfully ");
        } else {
            $this->session->set_flashdata("error", "Error while changing status");
        }
        redirect('student/profile/' . $sid);
    }

//changeStudentInfoStatus
    // function for profile loading
    public function profile_student($sid) {//profile information
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {
                    // student side 
                    $this->profile_data($sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                // placement team side
                $this->profile_data($sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//profile_student

    public function updateinfo($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) { // correct student
                    // student side..........
                    $sinfo['verify_by_student'] = 1;
                    $sinfo["verify_student_date"] = date('Y-m-d h:m:s');
                    $sinfo['verify_by_PT'] = 2;
                    $sinfo['verify_PT_id'] = null;
                    $sinfo['verify_PT_date'] = null;
                    $sinfo['first_name'] = $this->input->post('fname');
                    $sinfo['middle_name'] = $this->input->post('mname');
                    $sinfo['last_name'] = $this->input->post('lname');
                    $sinfo['DOB'] = date('Y-m-d', strtotime($this->input->post('bdate')));
                    $sinfo['gender'] = $this->input->post('gender');
                    $sinfo['contact_no_1'] = $this->input->post('con1');
                    $sinfo['contact_no_2'] = $this->input->post('con2');
                    $sinfo['primary_email'] = $this->input->post('p_email');
                    $sinfo['social_linkedin_id'] = $this->input->post('social_linkedin');

                    $sinfo["doc_passport"] = $_FILES['doc_passport']['name'];
                    $sinfo["doc_voter_id"] = $_FILES['doc_voter_id']['name'];
                    $sinfo["profile_picture"] = $_FILES['profile_picture']['name'];
                    $sinfo["objactives"] = $this->input->post('stud_objactives');

                    if ($sinfo["profile_picture"]) {

                        $ext = end((explode(".", $sinfo["profile_picture"])));
                        $uploaddirasd = 'uploaddocs/students_personal_documents/';
                        $fileasd = $uploaddirasd . basename($sid . "_" . "profile_picture" . '.' . $ext);

                        if (file_exists($uploaddirasd . basename($sid . "_" . "profile_picture.jpg"))) {
                            @unlink($uploaddirasd . basename($sid . "_" . "profile_picture.jpg"));
                        }

                        if (file_exists($uploaddirasd . basename($sid . "_" . "profile_picture.png"))) {
                            @unlink($uploaddirasd . basename($sid . "_" . "profile_picture.png"));
                        }


                        if (file_exists($uploaddirasd . basename($sid . "_" . "profile_picture.gif"))) {
                            @unlink($uploaddirasd . basename($sid . "_" . "profile_picture.gif"));
                        }


                        if (move_uploaded_file($_FILES['profile_picture']['tmp_name'], $fileasd)) {
                            $sinfo["profile_picture"] = $fileasd;
                        } else {
                            $sinfo["profile_picture"] = NULL;
                        }
                    } else {
                        /* $qry = "SELECT profile_picture, doc_passport, doc_voter_id
                          FROM  `tbl_student_information`
                          WHERE  student_id =$sid"; */

                        $qry = "SELECT profile_picture FROM `tbl_student_information` 
										WHERE  student_id =$sid";

                        $stud_document = mysql_query($qry);
                        // no change in profile picture...
                        while ($row = mysql_fetch_array($stud_document)) {
                            $sinfo["profile_picture"] = $row['profile_picture'];
                        }
                    }


                    if ($sinfo["doc_passport"]) {

                        $ext = end((explode(".", $sinfo["doc_passport"])));
                        $uploaddirasd = 'uploaddocs/students_personal_documents/';
                        $fileasd = $uploaddirasd . basename($sid . "_" . "doc_passport" . '.' . $ext);

                        if (file_exists($uploaddirasd . basename($sid . "_" . "doc_passport.jpg"))) {
                            @unlink($uploaddirasd . basename($sid . "_" . "doc_passport.jpg"));
                        }

                        if (file_exists($uploaddirasd . basename($sid . "_" . "doc_passport.png"))) {
                            @unlink($uploaddirasd . basename($sid . "_" . "doc_passport.png"));
                        }


                        if (file_exists($uploaddirasd . basename($sid . "_" . "doc_passport.gif"))) {
                            @unlink($uploaddirasd . basename($sid . "_" . "doc_passport.gif"));
                        }


                        if (move_uploaded_file($_FILES['doc_passport']['tmp_name'], $fileasd)) {
                            $sinfo["doc_passport"] = $fileasd;
                        } else {
                            $sinfo["doc_passport"] = NULL;
                        }
                    } else {
                        $qry = "SELECT doc_passport
										FROM  `tbl_student_information` 
										WHERE  student_id =$sid";

                        $stud_document = mysql_query($qry);
                        // no change in profile picture...
                        while ($row = mysql_fetch_array($stud_document)) {
                            $sinfo["doc_passport"] = $row['doc_passport'];
                        }
                    }

                    if ($sinfo["doc_voter_id"]) {

                        $ext = end((explode(".", $sinfo["doc_voter_id"])));
                        $uploaddirasd = 'uploaddocs/students_personal_documents/';
                        $fileasd = $uploaddirasd . basename($sid . "_" . "doc_voter_id" . '.' . $ext);

                        if (file_exists($uploaddirasd . basename($sid . "_" . "doc_voter_id.jpg"))) {
                            @unlink($uploaddirasd . basename($sid . "_" . "doc_voter_id.jpg"));
                        }

                        if (file_exists($uploaddirasd . basename($sid . "_" . "doc_voter_id.png"))) {
                            @unlink($uploaddirasd . basename($sid . "_" . "doc_voter_id.png"));
                        }


                        if (file_exists($uploaddirasd . basename($sid . "_" . "doc_voter_id.gif"))) {
                            @unlink($uploaddirasd . basename($sid . "_" . "doc_voter_id.gif"));
                        }


                        if (move_uploaded_file($_FILES['doc_voter_id']['tmp_name'], $fileasd)) {
                            $sinfo["doc_voter_id"] = $fileasd;
                        } else {
                            $sinfo["doc_voter_id"] = NULL;
                        }
                    } else {
                        $qry = "SELECT doc_voter_id
										FROM  `tbl_student_information` 
										WHERE  student_id =$sid";

                        $stud_document = mysql_query($qry);
                        // no change in profile picture...
                        while ($row = mysql_fetch_array($stud_document)) {
                            $sinfo["doc_voter_id"] = $row['doc_voter_id'];
                        }
                    }


                    if ($this->updateSinfo_main($sinfo, $sid)) { // main controller 
                        $this->session->set_flashdata("sucess", "information has been successfully updated");
                    } else {
                        $this->session->set_flashdata("error", "Error while updating student information");
                    }

                    $this->session->set_userdata("page_name", $page);
                    redirect(base_url() . 'student/profile/' . $sid);
                } else { // incorrect student
                    $this->load->view("STUDENT/500");
                }
            } else { // PLACEMENT TEAM
                // PLACEMENT TEAM side..........
                $sinfo['first_name'] = $this->input->post('fname');
                $sinfo['middle_name'] = $this->input->post('mname');
                $sinfo['last_name'] = $this->input->post('lname');
                $sinfo['DOB'] = date('Y-m-d', strtotime($this->input->post('bdate')));
                $sinfo['gender'] = $this->input->post('gender');
                $sinfo['contact_no_1'] = $this->input->post('con1');
                $sinfo['contact_no_2'] = $this->input->post('con2');
                $sinfo['primary_email'] = $this->input->post('p_email');
                $sinfo['social_linkedin_id'] = $this->input->post('social_linkedin');
                $sinfo['verify_by_student'] = 0;
                $sinfo["verify_student_date"] = null;
                $sinfo['verify_by_PT'] = 1;
                $sinfo['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $sinfo['verify_PT_date'] = date('Y-m-d h:m:s');
                $sinfo["objactives"] = $this->input->post('stud_objactives');


                $sinfo["profile_picture"] = $_FILES['profile_picture']['name'];
                $sinfo["doc_passport"] = $_FILES['doc_passport']['name'];
                $sinfo["doc_voter_id"] = $_FILES['doc_voter_id']['name'];



                if ($sinfo["profile_picture"]) {

                    $ext = end((explode(".", $sinfo["profile_picture"])));
                    $uploaddirasd = 'uploaddocs/students_personal_documents/';
                    $fileasd = $uploaddirasd . basename($sid . "_" . "profile_picture" . '.' . $ext);

                    if (file_exists($uploaddirasd . basename($sid . "_" . "profile_picture.jpg"))) {
                        @unlink($uploaddirasd . basename($sid . "_" . "profile_picture.jpg"));
                    }

                    if (file_exists($uploaddirasd . basename($sid . "_" . "profile_picture.png"))) {
                        @unlink($uploaddirasd . basename($sid . "_" . "profile_picture.png"));
                    }


                    if (file_exists($uploaddirasd . basename($sid . "_" . "profile_picture.gif"))) {
                        @unlink($uploaddirasd . basename($sid . "_" . "profile_picture.gif"));
                    }


                    if (move_uploaded_file($_FILES['profile_picture']['tmp_name'], $fileasd)) {
                        $sinfo["profile_picture"] = $fileasd;
                    } else {
                        $sinfo["profile_picture"] = NULL;
                    }
                } else {
                    /* $qry = "SELECT profile_picture, doc_passport, doc_voter_id
                      FROM  `tbl_student_information`
                      WHERE  student_id =$sid"; */

                    $qry = "SELECT profile_picture FROM  `tbl_student_information` 
										WHERE  student_id =$sid";

                    $stud_document = mysql_query($qry);
                    // no change in profile picture...
                    while ($row = mysql_fetch_array($stud_document)) {
                        $sinfo["profile_picture"] = $row['profile_picture'];
                    }
                }


                if ($sinfo["doc_passport"]) {

                    $ext = end((explode(".", $sinfo["doc_passport"])));
                    $uploaddirasd = 'uploaddocs/students_personal_documents/';
                    $fileasd = $uploaddirasd . basename($sid . "_" . "doc_passport" . '.' . $ext);

                    if (file_exists($uploaddirasd . basename($sid . "_" . "doc_passport.jpg"))) {
                        @unlink($uploaddirasd . basename($sid . "_" . "doc_passport.jpg"));
                    }

                    if (file_exists($uploaddirasd . basename($sid . "_" . "doc_passport.png"))) {
                        @unlink($uploaddirasd . basename($sid . "_" . "doc_passport.png"));
                    }


                    if (file_exists($uploaddirasd . basename($sid . "_" . "doc_passport.gif"))) {
                        @unlink($uploaddirasd . basename($sid . "_" . "doc_passport.gif"));
                    }


                    if (move_uploaded_file($_FILES['doc_passport']['tmp_name'], $fileasd)) {
                        $sinfo["doc_passport"] = $fileasd;
                    } else {
                        $sinfo["doc_passport"] = NULL;
                    }
                } else {
                    $qry = "SELECT doc_passport
										FROM  `tbl_student_information` 
										WHERE  student_id =$sid";

                    $stud_document = mysql_query($qry);
                    // no change in profile picture...
                    while ($row = mysql_fetch_array($stud_document)) {
                        $sinfo["doc_passport"] = $row['doc_passport'];
                    }
                }

                if ($sinfo["doc_voter_id"]) {

                    $ext = end((explode(".", $sinfo["doc_voter_id"])));
                    $uploaddirasd = 'uploaddocs/students_personal_documents/';
                    $fileasd = $uploaddirasd . basename($sid . "_" . "doc_voter_id" . '.' . $ext);

                    if (file_exists($uploaddirasd . basename($sid . "_" . "doc_voter_id.jpg"))) {
                        @unlink($uploaddirasd . basename($sid . "_" . "doc_voter_id.jpg"));
                    }

                    if (file_exists($uploaddirasd . basename($sid . "_" . "doc_voter_id.png"))) {
                        @unlink($uploaddirasd . basename($sid . "_" . "doc_voter_id.png"));
                    }


                    if (file_exists($uploaddirasd . basename($sid . "_" . "doc_voter_id.gif"))) {
                        @unlink($uploaddirasd . basename($sid . "_" . "doc_voter_id.gif"));
                    }


                    if (move_uploaded_file($_FILES['doc_voter_id']['tmp_name'], $fileasd)) {
                        $sinfo["doc_voter_id"] = $fileasd;
                    } else {
                        $sinfo["doc_voter_id"] = NULL;
                    }
                } else {
                    $qry = "SELECT doc_voter_id
										FROM  `tbl_student_information` 
										WHERE  student_id =$sid";

                    $stud_document = mysql_query($qry);
                    // no change in profile picture...
                    while ($row = mysql_fetch_array($stud_document)) {
                        $sinfo["doc_voter_id"] = $row['doc_voter_id'];
                    }
                }


                if ($this->updateSinfo_main($sinfo, $sid)) { // main controller 
                    $this->session->set_flashdata("sucess", "information has been successfully updated");
                } else {
                    $this->session->set_flashdata("error", "Error while updating student information");
                }

                $this->session->set_userdata("page_name", $page);
                redirect(base_url() . 'student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//updateinfo

    public function updateAddress($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) { // correct student
                    // student side..........
                    $sinfo['verify_by_student'] = 1;
                    $sinfo["verify_student_date"] = date('Y-m-d h:m:s');
                    $sinfo['verify_by_PT'] = 2;
                    $sinfo['verify_PT_id'] = null;
                    $sinfo['verify_PT_date'] = null;

                    // address information 

                    $local["address_type"] = ("L");
                    $local["credentials_id"] = $this->studCredentialId($sid);
                    $local["address_detail"] = $this->input->post('l_dadd');
                    $local["pincode"] = $this->input->post('l_zip');
                    $local["city"] = $this->input->post('l_city');
                    $local["state"] = $this->input->post('l_state');
                    $local["country"] = $this->input->post('l_country');
                    $local['verify_by_student'] = 1;
                    $local["verify_student_date"] = date('Y-m-d h:m:s');
                    $local['verify_by_PT'] = 2;
                    $local['verify_PT_id'] = null;
                    $local['verify_PT_date'] = null;

                    $perm["address_type"] = ("P");
                    $perm["credentials_id"] = $this->studCredentialId($sid);
                    $perm["address_detail"] = $this->input->post('p_dadd');
                    $perm["pincode"] = $this->input->post('p_zip');
                    $perm["city"] = $this->input->post('p_city');
                    $perm["state"] = $this->input->post('p_state');
                    $perm["country"] = $this->input->post('p_country');
                    $perm['verify_by_student'] = 1;
                    $perm["verify_student_date"] = date('Y-m-d h:m:s');
                    $perm['verify_by_PT'] = 2;
                    $perm['verify_PT_id'] = null;
                    $perm['verify_PT_date'] = null;

                    $addlocal = $this->student_addLocal_main($this->studCredentialId($sid)); // fatch all wxisting data

                    if ($addlocal) {
                        if ($this->delete_address_main($this->studCredentialId($sid))) { // delete existing address
                            if ($this->insert_address_main($local)) { // insert local address
                                $this->session->set_flashdata("sucess", "Address has been successfully updated");
                            } else {
                                $this->session->set_flashdata("error", "Error while updating local address");
                            }

                            if ($this->insert_address_main($perm)) { //insert permanent address
                                $this->session->set_flashdata("sucess", "Address has been successfully updated");
                            } else {
                                $this->session->set_flashdata("error", "Error while updating permanent address");
                            }
                        } else {
                            $this->session->set_flashdata("error", "Can't update address");
                        }
                    } else {
                        if ($this->insert_address_main($local)) { // insert local address
                            $this->session->set_flashdata("sucess", "Address has been successfully updated");
                        } else {
                            $this->session->set_flashdata("error", "Error while updating local address");
                        }
                        if ($this->insert_address_main($perm)) { //insert permanent address
                            $this->session->set_flashdata("sucess", "Address has been successfully updated");
                        } else {
                            $this->session->set_flashdata("error", "Error while updating permanent address");
                        }
                    }
                    $this->session->set_userdata("page_name", $page);

                    redirect('student/profile/' . $sid);
                } else { // incorrect student
                    $this->load->view("STUDENT/500");
                }
            } else { // PLACEMENT TEAM
                // PLACEMENT TEAM side..........
                // address information............
                $local["address_type"] = ("L");
                $local["credentials_id"] = $this->studCredentialId($sid);
                $local["address_detail"] = $this->input->post('l_dadd');
                $local["pincode"] = $this->input->post('l_zip');
                $local["city"] = $this->input->post('l_city');
                $local["state"] = $this->input->post('l_state');
                $local["country"] = $this->input->post('l_country');
                $local['verify_by_student'] = 0;
                $local["verify_student_date"] = null;
                $local['verify_by_PT'] = 1;
                $local['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $local['verify_PT_date'] = date('Y-m-d h:m:s');

                $perm["address_type"] = ("P");
                $perm["credentials_id"] = $this->studCredentialId($sid);
                $perm["address_detail"] = $this->input->post('p_dadd');
                $perm["pincode"] = $this->input->post('p_zip');
                $perm["city"] = $this->input->post('p_city');
                $perm["state"] = $this->input->post('p_state');
                $perm["country"] = $this->input->post('p_country');
                $perm['verify_by_student'] = 0;
                $perm["verify_student_date"] = null;
                $perm['verify_by_PT'] = 1;
                $perm['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $perm['verify_PT_date'] = date('Y-m-d h:m:s');

                $addlocal = $this->student_addLocal_main($this->studCredentialId($sid));

                if ($addlocal) {
                    if ($this->delete_address_main($this->studCredentialId($sid))) { // delete existing address
                        if ($this->insert_address_main($local)) { // insert local address
                            $this->session->set_flashdata("sucess", "Address has been successfully updated");
                        } else {
                            $this->session->set_flashdata("error", "Error while updating local address");
                        }

                        if ($this->insert_address_main($perm)) { //insert permanent address
                            $this->session->set_flashdata("sucess", "Address has been successfully updated");
                        } else {
                            $this->session->set_flashdata("error", "Error while updating permanent address");
                        }
                    } else {
                        $this->session->set_flashdata("error", "Can't update address");
                    }
                } else {

                    if ($this->insert_address_main($local)) { // insert local address
                        $this->session->set_flashdata("sucess", "Address has been successfully updated");
                    } else {
                        $this->session->set_flashdata("error", "Error while updating local address");
                    }

                    if ($this->insert_address_main($perm)) { //insert permanent address
                        $this->session->set_flashdata("sucess", "Address has been successfully updated");
                    } else {
                        $this->session->set_flashdata("error", "Error while updating permanent address");
                    }
                }

                $this->session->set_userdata("page_name", $page);
                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//updateAddress

    public function checkEducationYear() {
        $sid = $_POST['studentId'];
        $s_selectedQualification = $_POST['qualificationName'];
        $s_passingYear = $_POST['passingYear'];

        $data = $this->main_checkQualificationYear($sid, $s_selectedQualification);
//print_r($data); exit;
        if ($data) {
            foreach ($data as $r) {


                if ($s_selectedQualification > $r->degree_name) {
                    if ($s_passingYear > $r->passing_year) {
                        echo "true";
                    } else {
                        echo "false";
                    }
                }
            }
        } else {
            echo "true";
        }
    }

    public function addEducation($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {
                    // student side
                    $edu["student_id"] = $sid;
                    $edu['verify_by_student'] = 1;
                    $edu["verify_student_date"] = date('Y-m-d h:m:s');
                    $edu['verify_by_PT'] = 2;
                    $edu['verify_PT_id'] = null;
                    $edu['verify_PT_date'] = null;

                    $temp = $this->input->post('education');
                    foreach ($temp as $key => $value) {
                        $edu["degree_name"] = $value['edegree'];
                        $edu["education_id"] = $value['ename'];

                        if ($edu["education_id"] == "other") {
                            $eid = 0;
                            $data["edcutation_name"] = $this->input->post('other_degree');

                            $eid = $this->main_addEducationName($data);
                            if ($eid) {
                                $edu["education_id"] = $eid;
                            }
                        }


                        $edu["university_board_name"] = $value['bordUni'];
                        $edu["percentage_cgpa"] = $value['perc_cgpa'];
                        $edu["division"] = $value['division'];
                        $edu["passing_year"] = $value['pass_year'];


                        $edu["document_link"] = $_FILES['education']['name'][$key]['doc'];
                        $edu["document_link2"] = $_FILES['education2']['name'][$key]['doc'];
                        $edu["document_link3"] = $_FILES['education3']['name'][$key]['doc'];
                        $edu["document_link4"] = $_FILES['education4']['name'][$key]['doc'];
                        $edu["document_link5"] = $_FILES['education5']['name'][$key]['doc'];
                        $edu["document_link6"] = $_FILES['education6']['name'][$key]['doc'];
                        $edu["document_link7"] = $_FILES['education7']['name'][$key]['doc'];
                        $edu["document_link8"] = $_FILES['education8']['name'][$key]['doc'];
                        $edu["document_link9"] = $_FILES['education9']['name'][$key]['doc'];
                        $edu["document_link10"] = $_FILES['education10']['name'][$key]['doc'];
                        $ext = end((explode(".", $edu["document_link"])));
                        $ext2 = end((explode(".", $edu["document_link2"])));
                        $ext3 = end((explode(".", $edu["document_link3"])));
                        $ext4 = end((explode(".", $edu["document_link4"])));
                        $ext5 = end((explode(".", $edu["document_link5"])));
                        $ext6 = end((explode(".", $edu["document_link6"])));
                        $ext7 = end((explode(".", $edu["document_link7"])));
                        $ext8 = end((explode(".", $edu["document_link8"])));
                        $ext9 = end((explode(".", $edu["document_link9"])));
                        $ext10 = end((explode(".", $edu["document_link10"])));
                        $uploaddirasd = 'uploaddocs/education_documents/';

                        $fileasd = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC1" . "_" . $edu["degree_name"] . '.' . $ext);
                        $fileasd2 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC2" . "_" . $edu["degree_name"] . '.' . $ext2);
                        $fileasd3 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC3" . "_" . $edu["degree_name"] . '.' . $ext3);
                        $fileasd4 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC4" . "_" . $edu["degree_name"] . '.' . $ext4);
                        $fileasd5 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC5" . "_" . $edu["degree_name"] . '.' . $ext5);
                        $fileasd6 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC6" . "_" . $edu["degree_name"] . '.' . $ext6);
                        $fileasd7 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC7" . "_" . $edu["degree_name"] . '.' . $ext7);
                        $fileasd8 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC8" . "_" . $edu["degree_name"] . '.' . $ext8);
                        $fileasd9 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC9" . "_" . $edu["degree_name"] . '.' . $ext9);
                        $fileasd10 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC10" . "_" . $edu["degree_name"] . '.' . $ext10);

                        if (move_uploaded_file($_FILES['education']['tmp_name'][$key]['doc'], $fileasd)) {
                            $edu["document_link"] = $fileasd;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['education2']['tmp_name'][$key]['doc'], $fileasd2)) {
                            $edu["document_link2"] = $fileasd2;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['education3']['tmp_name'][$key]['doc'], $fileasd3)) {
                            $edu["document_link3"] = $fileasd3;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['education4']['tmp_name'][$key]['doc'], $fileasd4)) {
                            $edu["document_link4"] = $fileasd4;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['education5']['tmp_name'][$key]['doc'], $fileasd5)) {
                            $edu["document_link5"] = $fileasd5;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['education6']['tmp_name'][$key]['doc'], $fileasd6)) {
                            $edu["document_link6"] = $fileasd6;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['education7']['tmp_name'][$key]['doc'], $fileasd7)) {
                            $edu["document_link7"] = $fileasd7;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['education8']['tmp_name'][$key]['doc'], $fileasd8)) {
                            $edu["document_link8"] = $fileasd8;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['education9']['tmp_name'][$key]['doc'], $fileasd9)) {
                            $edu["document_link9"] = $fileasd9;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['education10']['tmp_name'][$key]['doc'], $fileasd10)) {
                            $edu["document_link10"] = $fileasd10;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }


                        if ($this->addEducation_main($edu)) {

                            $this->session->set_flashdata("sucess", " Academic qualification information has been added successfully ");
                        } else {
                            $this->session->set_flashdata("error", "Error while adding academic qualification information");
                        }
                    }//foreach

                    $this->session->set_userdata("page_name", $page);
                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                // placement team side
                $edu["student_id"] = $sid;
                $edu['verify_by_student'] = 2;
                $edu["verify_student_date"] = null;
                $edu['verify_by_PT'] = 1;
                $edu['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $edu['verify_PT_date'] = date('Y-m-d h:m:s');

                $temp = $this->input->post('education');
                foreach ($temp as $key => $value) {
                    $edu["degree_name"] = $value['edegree'];
                    $edu["education_id"] = $value['ename'];


                    if ($edu["education_id"] == "other") {
                        $eid = 0;
                        $data["edcutation_name"] = $this->input->post('other_degree');

                        $eid = $this->main_addEducationName($data);
                        if ($eid) {
                            $edu["education_id"] = $eid;
                        }
                    }

                    $edu["university_board_name"] = $value['bordUni'];
                    $edu["percentage_cgpa"] = $value['perc_cgpa'];
                    $edu["division"] = $value['division'];
                    $edu["passing_year"] = $value['pass_year'];

                    $edu["document_link"] = $_FILES['education']['name'][$key]['doc'];
                    $edu["document_link2"] = $_FILES['education2']['name'][$key]['doc'];
                    $edu["document_link3"] = $_FILES['education3']['name'][$key]['doc'];
                    $edu["document_link4"] = $_FILES['education4']['name'][$key]['doc'];
                    $edu["document_link5"] = $_FILES['education5']['name'][$key]['doc'];
                    $edu["document_link6"] = $_FILES['education6']['name'][$key]['doc'];
                    $edu["document_link7"] = $_FILES['education7']['name'][$key]['doc'];
                    $edu["document_link8"] = $_FILES['education8']['name'][$key]['doc'];
                    $edu["document_link9"] = $_FILES['education9']['name'][$key]['doc'];
                    $edu["document_link10"] = $_FILES['education10']['name'][$key]['doc'];
                    $ext = end((explode(".", $edu["document_link"])));
                    $ext2 = end((explode(".", $edu["document_link2"])));
                    $ext3 = end((explode(".", $edu["document_link3"])));
                    $ext4 = end((explode(".", $edu["document_link4"])));
                    $ext5 = end((explode(".", $edu["document_link5"])));
                    $ext6 = end((explode(".", $edu["document_link6"])));
                    $ext7 = end((explode(".", $edu["document_link7"])));
                    $ext8 = end((explode(".", $edu["document_link8"])));
                    $ext9 = end((explode(".", $edu["document_link9"])));
                    $ext10 = end((explode(".", $edu["document_link10"])));
                    $uploaddirasd = 'uploaddocs/education_documents/';

                    $fileasd = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC1" . "_" . $edu["degree_name"] . '.' . $ext);
                    $fileasd2 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC2" . "_" . $edu["degree_name"] . '.' . $ext2);
                    $fileasd3 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC3" . "_" . $edu["degree_name"] . '.' . $ext3);
                    $fileasd4 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC4" . "_" . $edu["degree_name"] . '.' . $ext4);
                    $fileasd5 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC5" . "_" . $edu["degree_name"] . '.' . $ext5);
                    $fileasd6 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC6" . "_" . $edu["degree_name"] . '.' . $ext6);
                    $fileasd7 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC7" . "_" . $edu["degree_name"] . '.' . $ext7);
                    $fileasd8 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC8" . "_" . $edu["degree_name"] . '.' . $ext8);
                    $fileasd9 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC9" . "_" . $edu["degree_name"] . '.' . $ext9);
                    $fileasd10 = $uploaddirasd . basename($edu["student_id"] . "_" . "DOC10" . "_" . $edu["degree_name"] . '.' . $ext10);

                    if (move_uploaded_file($_FILES['education']['tmp_name'][$key]['doc'], $fileasd)) {
                        $edu["document_link"] = $fileasd;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['education2']['tmp_name'][$key]['doc'], $fileasd2)) {
                        $edu["document_link2"] = $fileasd2;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['education3']['tmp_name'][$key]['doc'], $fileasd3)) {
                        $edu["document_link3"] = $fileasd3;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['education4']['tmp_name'][$key]['doc'], $fileasd4)) {
                        $edu["document_link4"] = $fileasd4;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['education5']['tmp_name'][$key]['doc'], $fileasd5)) {
                        $edu["document_link5"] = $fileasd5;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['education6']['tmp_name'][$key]['doc'], $fileasd6)) {
                        $edu["document_link6"] = $fileasd6;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['education7']['tmp_name'][$key]['doc'], $fileasd7)) {
                        $edu["document_link7"] = $fileasd7;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['education8']['tmp_name'][$key]['doc'], $fileasd8)) {
                        $edu["document_link8"] = $fileasd8;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['education9']['tmp_name'][$key]['doc'], $fileasd9)) {
                        $edu["document_link9"] = $fileasd9;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['education10']['tmp_name'][$key]['doc'], $fileasd10)) {
                        $edu["document_link10"] = $fileasd10;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }


                    if ($this->addEducation_main($edu)) {

                        $this->session->set_flashdata("sucess", " Academic qualification information has been added successfully ");
                    } else {
                        $this->session->set_flashdata("error", "Error while adding academic qualification information");
                    }
                }//foreach
                $this->session->set_userdata("page_name", $page);
                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addEducation

    public function addExperience($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {
                    $exp["student_id"] = $sid;
                    $exp['verify_by_student'] = 1;
                    $exp["verify_student_date"] = date('Y-m-d h:m:s');
                    $exp['verify_by_PT'] = 2;
                    $exp['verify_PT_id'] = null;
                    $exp['verify_PT_date'] = null;
                    $temp = $this->input->post('org');

                    foreach ($temp as $key => $value) {
                        $exp["company_name"] = $value['name'];
                        $exp["city_name"] = $value['city'];
                        $exp["business_unit"] = $value['domain'];
                        $exp["job_profile"] = $value['org_post'];
                        $exp["date_from"] = date('Y-m-d', strtotime($value['date_in']));
                        $exp["date_to"] = date('Y-m-d', strtotime($value['date_out']));
                        $exp["responsibilities"] = $value['respo'];

                        $exp["document_link"] = $_FILES['org']['name'][$key]['doc'];
                        $exp["document_link2"] = $_FILES['org2']['name'][$key]['doc'];
                        $exp["document_link3"] = $_FILES['org3']['name'][$key]['doc'];
                        $ext = end((explode(".", $exp["document_link"])));
                        $ext2 = end((explode(".", $exp["document_link2"])));
                        $ext3 = end((explode(".", $exp["document_link3"])));
                        $uploaddirasd = 'uploaddocs/professional_experience_documents/';

                        $fileasd = $uploaddirasd . basename($exp["student_id"] . "_" . "DOC1" . "_" . $exp["company_name"] . "_" . $exp["date_from"] . '.' . $ext);
                        $fileasd2 = $uploaddirasd . basename($exp["student_id"] . "_" . "DOC2" . "_" . $exp["company_name"] . "_" . $exp["date_from"] . '.' . $ext2);
                        $fileasd3 = $uploaddirasd . basename($exp["student_id"] . "_" . "DOC3" . "_" . $exp["company_name"] . "_" . $exp["date_from"] . '.' . $ext3);

                        if (move_uploaded_file($_FILES['org']['tmp_name'][$key]['doc'], $fileasd)) {
                            $exp["document_link"] = $fileasd;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['org2']['tmp_name'][$key]['doc'], $fileasd2)) {
                            $exp["document_link2"] = $fileasd2;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if (move_uploaded_file($_FILES['org3']['tmp_name'][$key]['doc'], $fileasd3)) {
                            $exp["document_link3"] = $fileasd3;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if ($this->addExperience_main($exp)) {
                            $this->session->set_flashdata("sucess", " Professional Experience Information  has been added successfully");
                        } else {
                            $this->session->set_flashdata("error", "Error while adding Professional Experience Information ");
                        }
                    }//foreach

                    $this->session->set_userdata("page_name", $page);
                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                //placement team code
                $exp["student_id"] = $sid;
                $exp['verify_by_student'] = 2;
                $exp["verify_student_date"] = null;
                $exp['verify_by_PT'] = 1;
                $exp['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $exp['verify_PT_date'] = date('Y-m-d h:m:s');

                $temp = $this->input->post('org');
                foreach ($temp as $key => $value) {
                    $exp["company_name"] = $value['name'];
                    $exp["city_name"] = $value['city'];
                    $exp["business_unit"] = $value['domain'];
                    $exp["job_profile"] = $value['org_post'];
                    $exp["date_from"] = date('Y-m-d', strtotime($value['date_in']));
                    $exp["date_to"] = date('Y-m-d', strtotime($value['date_out']));
                    $exp["responsibilities"] = $value['respo'];

                    $exp["document_link"] = $_FILES['org']['name'][$key]['doc'];
                    $exp["document_link2"] = $_FILES['org2']['name'][$key]['doc'];
                    $exp["document_link3"] = $_FILES['org3']['name'][$key]['doc'];
                    $ext = end((explode(".", $exp["document_link"])));
                    $ext2 = end((explode(".", $exp["document_link2"])));
                    $ext3 = end((explode(".", $exp["document_link3"])));
                    $uploaddirasd = 'uploaddocs/professional_experience_documents/';

                    $fileasd = $uploaddirasd . basename($exp["student_id"] . "_" . "DOC1" . "_" . $exp["company_name"] . "_" . $exp["date_from"] . '.' . $ext);
                    $fileasd2 = $uploaddirasd . basename($exp["student_id"] . "_" . "DOC2" . "_" . $exp["company_name"] . "_" . $exp["date_from"] . '.' . $ext2);
                    $fileasd3 = $uploaddirasd . basename($exp["student_id"] . "_" . "DOC3" . "_" . $exp["company_name"] . "_" . $exp["date_from"] . '.' . $ext3);

                    if (move_uploaded_file($_FILES['org']['tmp_name'][$key]['doc'], $fileasd)) {
                        $exp["document_link"] = $fileasd;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['org2']['tmp_name'][$key]['doc'], $fileasd2)) {
                        $exp["document_link2"] = $fileasd2;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if (move_uploaded_file($_FILES['org3']['tmp_name'][$key]['doc'], $fileasd3)) {
                        $exp["document_link3"] = $fileasd3;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }

                    if ($this->addExperience_main($exp)) {
                        $this->session->set_flashdata("sucess", " Professional Experience Information  has been added successfully");
                    } else {
                        $this->session->set_flashdata("error", "Error while adding Professional Experience Information ");
                    }
                }//foreach

                $this->session->set_userdata("page_name", $page);
                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addExperience

    public function addSpecialization($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {


                    $spe["student_id"] = $sid;
                    $spe['verify_by_student'] = 1;
                    $spe["verify_student_date"] = date('Y-m-d h:m:s');
                    $spe['verify_by_PT'] = 2;
                    $spe['verify_PT_id'] = null;
                    $spe['specialization_name'] = $this->input->post('spec');


                    if ($this->addSpecialization_main($spe)) {
                        $this->session->set_flashdata("sucess", "Specialization has been added successfully");
                    } else {
                        $this->session->set_flashdata("error", "Error while adding specialization");
                    }


                    $this->session->set_userdata("page_name", $page);
                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                //placement team code
                $spe["student_id"] = $sid;
                $spe['verify_by_student'] = 2;
                $spe["verify_student_date"] = null;
                $spe['verify_by_PT'] = 1;
                $spe['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $spe['verify_PT_date'] = date('Y-m-d h:m:s');
                $spe['specialization_name'] = $this->input->post('spec');


                if ($this->addSpecialization_main($spe)) {
                    $this->session->set_flashdata("sucess", "Specialization has been added successfully ");
                } else {
                    $this->session->set_flashdata("error", "Error while adding specialization2");
                }

                $this->session->set_userdata("page_name", $page);
                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addSpecialization

    public function addCertification($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {
                    $certi["student_id"] = $sid;
                    $certi['verify_by_student'] = 1;
                    $certi["verify_student_date"] = date('Y-m-d h:m:s');
                    $certi['verify_by_PT'] = 2;
                    $certi['verify_PT_id'] = null;

                    $temp = $this->input->post('certi');

                    foreach ($temp as $key => $value) {
                        $certi["certification_name"] = $value['name'];
                        $certi["certification_domain"] = $value['domain'];
                        $certi["certification_description"] = $value['descrption'];

                        $certi["document_link"] = $_FILES['certi']['name'][$key]['doc'];
                        $ext = end((explode(".", $certi["document_link"])));
                        //echo $_FILES['certi']['name'][$key]['doc'];


                        /* $fileName = $certi["document_link"]; 
                          $fileTmpLoc = $_FILES['certi']['tmp_name'][$key]['doc'];
                          // Path and file name
                          $pathAndName = base_url().'assets/uploads/'.$fileName;
                          // Run the move_uploaded_file() function here
                          //move_uploaded_file($fileTmpLoc, $pathAndName);

                          move_uploaded_file('file.jpg','http://localhost/placementportal/assets/uploads/'); */

                        $uploaddirasd = 'uploaddocs/';

                        $fileasd = $uploaddirasd . basename($certi["student_id"] . _ . $certi["certification_name"] . '.' . $ext);

                        move_uploaded_file($_FILES['certi']['tmp_name'][$key]['doc'], $fileasd);



                        if ($this->addCerification_main($certi)) {
                            $this->session->set_flashdata("sucess", "Certification has been added successfully");
                        } else {
                            $this->session->set_flashdata("error", "Error while adding certification");
                        }
                    }//foreach

                    $this->session->set_userdata("page_name", $page);
                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                //placement team code
                $certi["student_id"] = $sid;
                $certi['verify_by_student'] = 2;
                $certi["verify_student_date"] = null;
                $certi['verify_by_PT'] = 1;
                $certi['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $certi['verify_PT_date'] = date('Y-m-d h:m:s');

                $temp = $this->input->post('certi');

                foreach ($temp as $key => $value) {
                    $certi["certification_name"] = $value['name'];
                    $certi["certification_domain"] = $value['domain'];
                    $certi["certification_description"] = $value['descrption'];

                    if ($this->addCerification_main($certi)) {
                        $this->session->set_flashdata("sucess", "Certification has been added successfully");
                    } else {
                        $this->session->set_flashdata("error", "Error while adding certification");
                    }
                }//foreach

                $this->session->set_userdata("page_name", $page);
                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addCertification

    public function addAdditionalCourse($sid, $page) {
        //echo "asd"; exit;
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {
                    // student side 

                    $addiCourse["student_id"] = $sid;
                    $addiCourse['verify_by_student'] = 1;
                    $addiCourse["verify_student_date"] = date('Y-m-d h:m:s');
                    $addiCourse['verify_by_PT'] = 2;
                    $addiCourse['verify_PT_id'] = null;

                    $otherdomain = $this->input->post('otherdomain');
                    if ($otherdomain != "") {
                        $addiCourse['other_domain'] = $otherdomain;
                    } else {
                        $addiCourse['other_domain'] = "";
                    }

                    $temp = $this->input->post('cors');

                    foreach ($temp as $key => $value) {

                        $addiCourse["course_name"] = $value['name'];
                        $addiCourse["course_domain"] = $value['domain'];
                        $addiCourse["course_description"] = $value['descrption'];

                        $addiCourse["document_link"] = $_FILES['cors']['name'][$key]['doc'];

                        $ext = end((explode(".", $addiCourse["document_link"])));
                        $uploaddirasd = 'uploaddocs/certification_documents/';

                        $fileasd = $uploaddirasd . basename($addiCourse["student_id"] . "_" . $addiCourse["course_name"] . "_" . $addiCourse["course_domain"] . '.' . $ext);

                        if (move_uploaded_file($_FILES['cors']['tmp_name'][$key]['doc'], $fileasd)) {
                            $addiCourse["document_link"] = $fileasd;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");
                        }

                        if ($this->addAdditionalCourse_main($addiCourse)) {
                            $this->session->set_flashdata("sucess", "Additional course has been added successfully");
                        } else {
                            $this->session->set_flashdata("error", "Error while adding additional course");
                        }

                        /* $addiCourse["course_name"] = $value['name'];
                          $addiCourse["course_domain"] = $value['domain'];
                          $addiCourse["course_description"] = $value['descrption'];


                          $addiCourse["document_link"] = $_FILES['cors']['name'][$key]['doc'];

                          if($addiCourse["document_link"])
                          {
                          $ext = end((explode(".", $addiCourse["document_link"])));
                          $uploaddirasd = 'uploaddocs/certification_documents/';

                          $fileasd = $uploaddirasd .basename($addiCourse["student_id"]."_".$addiCourse["course_name"]."_".$addiCourse["course_domain"].'.'.$ext);

                          if (move_uploaded_file($_FILES['cors']['tmp_name'][$key]['doc'], $fileasd))
                          {
                          $addiCourse["document_link"] = $fileasd;
                          }
                          else
                          {
                          //$this->session->set_flashdata("error","Error while uploading document");
                          }

                          if($this->addAdditionalCourse_main($addiCourse))
                          {
                          $this->session->set_flashdata("sucess","Additional course has been added successfully");
                          }
                          else
                          {
                          $this->session->set_flashdata("error","Error while adding additional course");
                          }
                          }
                          else
                          {
                          if($this->addAdditionalCourse_main($addiCourse))
                          {
                          $this->session->set_flashdata("sucess","Additional course has been added successfully");
                          }
                          else
                          {
                          $this->session->set_flashdata("error","Error while adding additional course");
                          }
                          } */

                        /* else
                          {
                          $addiCourse["document_link"] = NULL;
                          if($this->addAdditionalCourse_main($addiCourse))
                          {
                          $this->session->set_flashdata("sucess","Additional course added successfuly");
                          }
                          else
                          {
                          $this->session->set_flashdata("error","Error while adding additional course");
                          }
                          } */
                    }//foreach
                    $this->session->set_userdata("page_name", $page);
                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                //placement team code
                $addiCourse["student_id"] = $sid;
                $addiCourse['verify_by_student'] = 2;
                $addiCourse["verify_student_date"] = null;
                $addiCourse['verify_by_PT'] = 1;
                $addiCourse['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $addiCourse['verify_PT_date'] = date('Y-m-d h:m:s');

                $otherdomain = $this->input->post('otherdomain');
                if ($otherdomain != "") {
                    $addiCourse['other_domain'] = $otherdomain;
                } else {
                    $addiCourse['other_domain'] = "";
                }

                $temp = $this->input->post('cors');


                foreach ($temp as $key => $value) {
                    $addiCourse["course_name"] = $value['name'];
                    $addiCourse["course_domain"] = $value['domain'];
                    $addiCourse["course_description"] = $value['descrption'];

                    $addiCourse["document_link"] = $_FILES['cors']['name'][$key]['doc'];

                    $ext = end((explode(".", $addiCourse["document_link"])));
                    $uploaddirasd = 'uploaddocs/certification_documents/';

                    $fileasd = $uploaddirasd . basename($addiCourse["student_id"] . "_" . $addiCourse["course_name"] . "_" . $addiCourse["course_domain"] . '.' . $ext);

                    if (move_uploaded_file($_FILES['cors']['tmp_name'][$key]['doc'], $fileasd)) {
                        $addiCourse["document_link"] = $fileasd;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");
                    }

                    if ($this->addAdditionalCourse_main($addiCourse)) {
                        $this->session->set_flashdata("sucess", "Additional course has been added successfully");
                    } else {
                        $this->session->set_flashdata("error", "Error while adding additional course");
                    }
                }//foreach
                $this->session->set_userdata("page_name", $page);
                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addAdditionalCourse

    public function addProject($sid, $page, $subPage) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {

                    $proj["student_id"] = $sid;
                    $proj['verify_by_student'] = 1;
                    $proj["verify_student_date"] = date('Y-m-d h:m:s');
                    $proj['verify_by_PT'] = 2;
                    $proj['verify_PT_id'] = null;


                    if ($subPage == "summerProject") {
                        //echo 'sdfsdf';exit;

                        $otherspdomain = $this->input->post('otherspdomain');
                        if ($otherspdomain != "") {
                            $proj['other_domain'] = $otherspdomain;
                        } else {
                            $proj['other_domain'] = "";
                        }

                        $temp = $this->input->post('prj');
                        $proj["project_type"] = "Summer Project";
                        foreach ($temp as $key => $value) {
                            $proj["project_domain"] = $value['domain'];
                            $proj["project_name"] = $value['name'];
                            $proj["project_description"] = $value['descrption'];
                            $proj["project_abstract"] = $value['abstract'];
                            $proj["organization_name"] = $value['org_name'];
                            $proj["start_date"] = date('Y-m-d', strtotime($value['date_in']));
                            $proj["end_date"] = date('Y-m-d', strtotime($value['date_out']));

                            // document Upload
                            $proj["document_link"] = $_FILES['prj']['name'][$key]['doc'];

                            if ($proj["document_link"]) {
                                $ext = end((explode(".", $proj["document_link"])));
                                $uploaddirasd = 'uploaddocs/project_infomation_documents/';

                                $fileasd = $uploaddirasd . basename($proj["student_id"] . "_summerProject_" . $proj["project_name"] . '.' . $ext);

                                if (move_uploaded_file($_FILES['prj']['tmp_name'][$key]['doc'], $fileasd)) {
                                    $proj["document_link"] = $fileasd;

                                    if ($this->addProjects_main($proj)) {
                                        $this->session->set_flashdata("sucess", "Project has been added successfully");
                                    } else {
                                        $this->session->set_flashdata("error", "Error while adding project");
                                    }
                                } else {
                                    $this->session->set_flashdata("error", "Error while uploading document");
                                }
                            } //if($proj["document_link"])
                            else {
                                $proj["document_link"] = NULL;

                                if ($this->addProjects_main($proj)) {
                                    $this->session->set_flashdata("sucess", "Project has been added successfully");
                                } else {
                                    $this->session->set_flashdata("error", "Error while adding project");
                                }
                            } //else --> if($proj["document_link"])
                        }// foreaach for summer project
                    }//if($subPage == "summerProject")


                    if ($subPage == "researchProject") {
                        $otherrpdomain = $this->input->post('otherrpdomain');
                        if ($otherrpdomain != "") {
                            $proj['other_domain'] = $otherrpdomain;
                        } else {
                            $proj['other_domain'] = "";
                        }

                        $temp = $this->input->post('researchPrj');
                        $proj["project_type"] = "Research Project";

                        foreach ($temp as $key => $value) {
                            $proj["project_domain"] = $value['domain'];
                            $proj["project_name"] = $value['name'];
                            $proj["project_description"] = $value['descrption'];
                            $proj["project_abstract"] = $value['abstract'];

                            // document Upload
                            $proj["document_link"] = $_FILES['researchPrj']['name'][$key]['doc'];

                            if ($proj["document_link"]) {
                                $ext = end((explode(".", $proj["document_link"])));
                                $uploaddirasd = 'uploaddocs/project_infomation_documents/';

                                $fileasd = $uploaddirasd . basename($proj["student_id"] . "_researchProject_" . $proj["project_name"] . '.' . $ext);
                                if (move_uploaded_file($_FILES['researchPrj']['tmp_name'][$key]['doc'], $fileasd)) {
                                    $proj["document_link"] = $fileasd;

                                    if ($this->addProjects_main($proj)) {
                                        $this->session->set_flashdata("sucess", "Project has been added successfully");
                                    } else {
                                        $this->session->set_flashdata("error", "Error while adding project");
                                    }
                                } else {
                                    $this->session->set_flashdata("error", "Error while uploading document");
                                }
                            } //if($proj["document_link"])
                            else {
                                $proj["document_link"] = NULL;

                                if ($this->addProjects_main($proj)) {
                                    $this->session->set_flashdata("sucess", "Project has been added successfully");
                                } else {
                                    $this->session->set_flashdata("error", "Error while adding project");
                                }
                            } //else --> if($proj["document_link"])
                        }// foreaach for summer project
                    }//if($subPage == "researchProject")

                    if ($subPage == "assignmentProject") {
                        $otherapdomain = $this->input->post('otherapdomain');
                        if ($otherapdomain != "") {
                            $proj['other_domain'] = $otherapdomain;
                        } else {
                            $proj['other_domain'] = "";
                        }

                        $temp = $this->input->post('assignmentPrj');
                        $proj["project_type"] = "Assignment Project";

                        foreach ($temp as $key => $value) {
                            $proj["project_domain"] = $value['domain'];
                            $proj["project_name"] = $value['name'];
                            $proj["project_description"] = $value['descrption'];


                            if ($this->addProjects_main($proj)) {
                                $this->session->set_flashdata("sucess", "Project has been added successfully");
                            } else {
                                $this->session->set_flashdata("error", "Error while adding project");
                            }
                        }// foreaach for assignment Project
                    }//if($subPage == "assignmentProject")



                    if ($subPage == "extraProject") {
                        $otherepdomain = $this->input->post('otherepdomain');
                        if ($otherepdomain != "") {
                            $proj['other_domain'] = $otherepdomain;
                        } else {
                            $proj['other_domain'] = "";
                        }

                        $temp = $this->input->post('ExtraPrj');
                        $proj["project_type"] = "Extra Project";

                        foreach ($temp as $key => $value) {
                            $proj["project_domain"] = $value['domain'];
                            $proj["project_name"] = $value['name'];
                            $proj["project_description"] = $value['descrption'];


                            if ($this->addProjects_main($proj)) {
                                $this->session->set_flashdata("sucess", "Project has been added successfully");
                            } else {
                                $this->session->set_flashdata("error", "Error while adding project");
                            }
                        }// foreaach for assignment Project
                    }//if($subPage == "assignmentProject")



                    $this->session->set_userdata("page_name", $page);
                    $this->session->set_userdata("subpage_name", $subPage);

                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                //placement team code

                $proj["student_id"] = $sid;
                $proj['verify_by_student'] = 2;
                $proj["verify_student_date"] = null;
                $proj['verify_by_PT'] = 1;
                $proj['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $proj['verify_PT_date'] = date('Y-m-d h:m:s');


                if ($subPage == "summerProject") {
                    $otherspdomain = $this->input->post('otherspdomain');
                    if ($otherspdomain != "") {
                        $proj['other_domain'] = $otherspdomain;
                    } else {
                        $proj['other_domain'] = "";
                    }

                    $temp = $this->input->post('prj');
                    $proj["project_type"] = "Summer Project";
                    foreach ($temp as $key => $value) {
                        $proj["project_domain"] = $value['domain'];
                        $proj["project_name"] = $value['name'];
                        $proj["project_description"] = $value['descrption'];
                        $proj["project_abstract"] = $value['abstract'];
                        $proj["organization_name"] = $value['org_name'];
                        $proj["start_date"] = date('Y-m-d', strtotime($value['date_in']));
                        $proj["end_date"] = date('Y-m-d', strtotime($value['date_out']));

                        // document Upload
                        $proj["document_link"] = $_FILES['prj']['name'][$key]['doc'];

                        if ($proj["document_link"]) {
                            $ext = end((explode(".", $proj["document_link"])));
                            $uploaddirasd = 'uploaddocs/project_infomation_documents/';

                            $fileasd = $uploaddirasd . basename($proj["student_id"] . "_summerProject_" . $proj["project_name"] . '.' . $ext);

                            if (move_uploaded_file($_FILES['prj']['tmp_name'][$key]['doc'], $fileasd)) {
                                $proj["document_link"] = $fileasd;

                                if ($this->addProjects_main($proj)) {
                                    $this->session->set_flashdata("sucess", "Project has been added successfully");
                                } else {
                                    $this->session->set_flashdata("error", "Error while adding project");
                                }
                            } else {
                                $this->session->set_flashdata("error", "Error while uploading document");
                            }
                        } //if($proj["document_link"])
                        else {
                            $proj["document_link"] = NULL;

                            if ($this->addProjects_main($proj)) {
                                $this->session->set_flashdata("sucess", "Project has been added successfully");
                            } else {
                                $this->session->set_flashdata("error", "Error while adding project");
                            }
                        } //else --> if($proj["document_link"])
                    }// foreaach for summer project
                }//if($subPage == "summerProject")


                if ($subPage == "researchProject") {
                    $otherrpdomain = $this->input->post('otherrpdomain');
                    if ($otherrpdomain != "") {
                        $proj['other_domain'] = $otherrpdomain;
                    } else {
                        $proj['other_domain'] = "";
                    }

                    $temp = $this->input->post('researchPrj');
                    $proj["project_type"] = "Research Project";

                    foreach ($temp as $key => $value) {
                        $proj["project_domain"] = $value['domain'];
                        $proj["project_name"] = $value['name'];
                        $proj["project_description"] = $value['descrption'];
                        $proj["project_abstract"] = $value['abstract'];

                        // document Upload
                        $proj["document_link"] = $_FILES['researchPrj']['name'][$key]['doc'];

                        if ($proj["document_link"]) {
                            $ext = end((explode(".", $proj["document_link"])));
                            $uploaddirasd = 'uploaddocs/project_infomation_documents/';

                            $fileasd = $uploaddirasd . basename($proj["student_id"] . "_researchProject_" . $proj["project_name"] . '.' . $ext);
                            if (move_uploaded_file($_FILES['researchPrj']['tmp_name'][$key]['doc'], $fileasd)) {
                                $proj["document_link"] = $fileasd;

                                if ($this->addProjects_main($proj)) {
                                    $this->session->set_flashdata("sucess", "Project has been added successfully");
                                } else {
                                    $this->session->set_flashdata("error", "Error while adding project");
                                }
                            } else {
                                $this->session->set_flashdata("error", "Error while uploading document");
                            }
                        } //if($proj["document_link"])
                        else {
                            $proj["document_link"] = NULL;

                            if ($this->addProjects_main($proj)) {
                                $this->session->set_flashdata("sucess", "Project has been added successfully");
                            } else {
                                $this->session->set_flashdata("error", "Error while adding project");
                            }
                        } //else --> if($proj["document_link"])
                    }// foreaach for summer project
                }//if($subPage == "researchProject")

                if ($subPage == "assignmentProject") {
                    $otherapdomain = $this->input->post('otherapdomain');
                    if ($otherapdomain != "") {
                        $proj['other_domain'] = $otherapdomain;
                    } else {
                        $proj['other_domain'] = "";
                    }

                    $temp = $this->input->post('assignmentPrj');
                    $proj["project_type"] = "Assignment Project";

                    foreach ($temp as $key => $value) {
                        $proj["project_domain"] = $value['domain'];
                        $proj["project_name"] = $value['name'];
                        $proj["project_description"] = $value['descrption'];


                        if ($this->addProjects_main($proj)) {
                            $this->session->set_flashdata("sucess", "Project has been added successfully");
                        } else {
                            $this->session->set_flashdata("error", "Error while adding project");
                        }
                    }// foreaach for assignment Project
                }//if($subPage == "assignmentProject")



                if ($subPage == "extraProject") {
                    $otherepdomain = $this->input->post('otherepdomain');
                    if ($otherepdomain != "") {
                        $proj['other_domain'] = $otherepdomain;
                    } else {
                        $proj['other_domain'] = "";
                    }

                    $temp = $this->input->post('ExtraPrj');
                    $proj["project_type"] = "Extra Project";

                    foreach ($temp as $key => $value) {
                        $proj["project_domain"] = $value['domain'];
                        $proj["project_name"] = $value['name'];
                        $proj["project_description"] = $value['descrption'];


                        if ($this->addProjects_main($proj)) {
                            $this->session->set_flashdata("sucess", "Project has been added successfully");
                        } else {
                            $this->session->set_flashdata("error", "Error while adding project");
                        }
                    }// foreaach for assignment Project
                }//if($subPage == "assignmentProject")



                $this->session->set_userdata("page_name", $page);
                $this->session->set_userdata("subpage_name", $subPage);

                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addProject

    public function addResponsblity($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {

                    $resp["student_id"] = $sid;
                    $resp['verify_by_student'] = 1;
                    $resp["verify_student_date"] = date('Y-m-d h:m:s');
                    $resp['verify_by_PT'] = 2;
                    $resp['verify_PT_id'] = null;

                    $temp = $this->input->post('respo');


                    foreach ($temp as $key => $value) {
                        $resp["p_r_name"] = $value['name'];
                        $resp["p_r_education"] = $value['edu'];
                        $resp["extra_info"] = $value['responsblities'];

                        if ($this->addResponsiblity_main($resp)) {
                            $this->session->set_flashdata("sucess", "Position or responsiblity has been added successfully");
                        } else {
                            $this->session->set_flashdata("error", "Error while adding position or responsiblity");
                        }
                    }//foreach
                    $this->session->set_userdata("page_name", $page);
                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                //placement team code
                $resp["student_id"] = $sid;
                $resp['verify_by_student'] = 2;
                $resp["verify_student_date"] = null;
                $resp['verify_by_PT'] = 1;
                $resp['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $resp['verify_PT_date'] = date('Y-m-d h:m:s');


                $temp = $this->input->post('respo');


                foreach ($temp as $key => $value) {
                    $resp["p_r_name"] = $value['name'];
                    $resp["p_r_education"] = $value['edu'];
                    $resp["extra_info"] = $value['responsblities'];

                    if ($this->addResponsiblity_main($resp)) {
                        $this->session->set_flashdata("sucess", "Position or responsiblity has been added successfully");
                    } else {
                        $this->session->set_flashdata("error", "Error while adding position or responsiblity");
                    }
                }//foreach
                $this->session->set_userdata("page_name", $page);
                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addResponsblity

    public function addAwards($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {
                    $awards["student_id"] = $sid;
                    $awards['verify_by_student'] = 1;
                    $awards["verify_student_date"] = date('Y-m-d h:m:s');
                    $awards['verify_by_PT'] = 2;
                    $awards['verify_PT_id'] = null;

                    $temp = $this->input->post('award');


                    foreach ($temp as $key => $value) {
                        $awards["achivement_title"] = $value['name'];
                        $awards["achivement_description"] = $value['ext_info'];


                        $awards["document_link"] = $_FILES['award']['name'][$key]['doc'];
                        $ext = end((explode(".", $awards["document_link"])));
                        $uploaddirasd = 'uploaddocs/awards_achivements_documents/';

                        $fileasd = $uploaddirasd . basename($awards["student_id"] . _ . $awards["achivement_title"] . '.' . $ext);

                        if (move_uploaded_file($_FILES['award']['tmp_name'][$key]['doc'], $fileasd)) {
                            $awards["document_link"] = $fileasd;
                        } else {
                            //$this->session->set_flashdata("error","Error while uploading document");																		
                        }

                        if ($this->addAwards_main($awards)) {
                            $this->session->set_flashdata("sucess", "Awards or achivement has been successfully added");
                        } else {
                            $this->session->set_flashdata("error", "Error while add awards or achivement ");
                        }
                    }//foreach
                    $this->session->set_userdata("page_name", $page);

                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                //placement team code

                $awards["student_id"] = $sid;
                $awards['verify_by_student'] = 2;
                $awards["verify_student_date"] = null;
                $awards['verify_by_PT'] = 1;
                $awards['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $awards['verify_PT_date'] = date('Y-m-d h:m:s');

                $temp = $this->input->post('award');
                foreach ($temp as $key => $value) {
                    $awards["achivement_title"] = $value['name'];
                    $awards["achivement_description"] = $value['ext_info'];


                    $awards["document_link"] = $_FILES['award']['name'][$key]['doc'];
                    $ext = end((explode(".", $awards["document_link"])));
                    $uploaddirasd = 'uploaddocs/awards_achivements_documents/';

                    $fileasd = $uploaddirasd . basename($awards["student_id"] . _ . $awards["achivement_title"] . '.' . $ext);

                    if (move_uploaded_file($_FILES['award']['tmp_name'][$key]['doc'], $fileasd)) {
                        $awards["document_link"] = $fileasd;
                    } else {
                        //$this->session->set_flashdata("error","Error while uploading document");																		
                    }
                    if ($this->addAwards_main($awards)) {
                        $this->session->set_flashdata("sucess", "Awards or achivement has been successfully added");
                    } else {
                        $this->session->set_flashdata("error", "Error while add awards or achivement ");
                    }
                }//foreach
                $this->session->set_userdata("page_name", $page);

                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addAwards

    public function addSkills($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {

                    $skill["student_id"] = $sid;
                    $skill['verify_by_student'] = 1;
                    $skill["verify_student_date"] = date('Y-m-d h:m:s');
                    $skill['verify_by_PT'] = 2;
                    $skill['verify_PT_id'] = null;

                    $temp = $this->input->post('skill');


                    foreach ($temp as $key => $value) {
                        $skill["skill_category"] = $value['category'];
                        $skill["skill_name"] = $value['name'];
                        $skill["extra_info"] = $value['ext_info'];

                        if ($this->addSkills_main($skill)) {
                            $this->session->set_flashdata("sucess", "Skills has been successfully added ");
                        } else {
                            $this->session->set_flashdata("error", "Error while adding skills");
                        }
                    }//foreach
                    $this->session->set_userdata("page_name", $page);

                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                //placement team code
                $skill["student_id"] = $sid;
                $skill['verify_by_student'] = 2;
                $skill["verify_student_date"] = null;
                $skill['verify_by_PT'] = 1;
                $skill['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $skill['verify_PT_date'] = date('Y-m-d h:m:s');


                $temp = $this->input->post('skill');


                foreach ($temp as $key => $value) {
                    $skill["skill_category"] = $value['category'];
                    $skill["skill_name"] = $value['name'];
                    $skill["extra_info"] = $value['ext_info'];

                    if ($this->addSkills_main($skill)) {
                        $this->session->set_flashdata("sucess", "Skills has been successfully added ");
                    } else {
                        $this->session->set_flashdata("error", "Error while adding skills");
                    }
                }//foreach
                $this->session->set_userdata("page_name", $page);

                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addSkills

    public function addHobbies($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {


                    $hobby["student_id"] = $sid;
                    $hobby['verify_by_student'] = 1;
                    $hobby["verify_student_date"] = date('Y-m-d h:m:s');
                    $hobby['verify_by_PT'] = 2;
                    $hobby['verify_PT_id'] = null;

                    $temp = $this->input->post('hobby');


                    foreach ($temp as $key => $value) {
                        $hobby["hobby_name"] = $value['name'];
                        $hobby["extra_info"] = $value['ext_info'];

                        if ($this->addHobbies_main($hobby)) {
                            $this->session->set_flashdata("sucess", "Hobbies has been successfully added ");
                        } else {
                            $this->session->set_flashdata("error", "Error while adding hobbies");
                        }
                    }//foreach
                    $this->session->set_userdata("page_name", $page);
                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                //placement team code
                $hobby["student_id"] = $sid;
                $hobby['verify_by_student'] = 2;
                $hobby["verify_student_date"] = null;
                $hobby['verify_by_PT'] = 1;
                $hobby['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $hobby['verify_PT_date'] = date('Y-m-d h:m:s');


                $temp = $this->input->post('hobby');


                foreach ($temp as $key => $value) {
                    $hobby["hobby_name"] = $value['name'];
                    $hobby["extra_info"] = $value['ext_info'];

                    if ($this->addHobbies_main($hobby)) {
                        $this->session->set_flashdata("sucess", "Hobbies has been successfully added ");
                    } else {
                        $this->session->set_flashdata("error", "Error while adding hobbies");
                    }
                }//foreach
                $this->session->set_userdata("page_name", $page);
                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addHobbies

    public function addextraActivity($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {

                    $activity["student_id"] = $sid;
                    $activity['verify_by_student'] = 1;
                    $activity["verify_student_date"] = date('Y-m-d h:m:s');
                    $activity['verify_by_PT'] = 2;
                    $activity['verify_PT_id'] = null;

                    $activity["activity_name"] = $this->input->post('activity_name');

                    if ($this->addExtraActivity_main($activity)) {
                        $this->session->set_flashdata("sucess", "Activity has been successfully added ");
                    } else {
                        $this->session->set_flashdata("error", "Error while adding activity");
                    }

                    $this->session->set_userdata("page_name", $page);
                    redirect('student/profile/' . $sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                //placement team code
                $activity["student_id"] = $sid;
                $activity['verify_by_student'] = 2;
                $activity["verify_student_date"] = null;
                $activity['verify_by_PT'] = 1;
                $activity['verify_PT_id'] = $this->ptId($this->session->userdata("credentials_id"));
                $activity['verify_PT_date'] = date('Y-m-d h:m:s');

                $activity["activity_name"] = $this->input->post('activity_name');

                if ($this->addExtraActivity_main($activity)) {
                    $this->session->set_flashdata("sucess", "Activity has been successfully added ");
                } else {
                    $this->session->set_flashdata("error", "Error while adding activity");
                }

                $this->session->set_userdata("page_name", $page);
                redirect('student/profile/' . $sid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//addSkills

    public function changePicture($sid, $page) {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            if ($this->checkkUser() == "STUDENT") {
                if ($sid == $this->session->userdata("student_id")) {

                    echo "work in progress";
                    //redirect('student/profile/'.$sid);
                } else {
                    $this->load->view("STUDENT/500");
                }
            } else {
                
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

//changePicture
//=============================== MASTER FUNCTION ============================ 

    public function studCredentialId($sid) {
        $qry = "SELECT credentials_id
				FROM  `tbl_student_information` 
				WHERE  student_id =$sid";

        $result = mysql_query($qry);

        $results = array();
        while ($row = mysql_fetch_array($result)) {
            $cid = $row['credentials_id'];
        }
        return $cid;
    }

    public function ptId($cid) {
        $qry = "SELECT placement_team_id
				FROM  `tbl_placement_team` 
				WHERE  credentials_id = $cid and status = 1";

        $result = mysql_query($qry);

        $results = array();
        while ($row = mysql_fetch_array($result)) {
            $ptid = $row['placement_team_id'];
        }
        return $ptid;
    }

    public function profile_data($sid) {
        $data['student_id'] = $sid;
        $data['m_specialtization'] = $this->batch_specialization_main($sid);
        $data['m_domain'] = $this->master_domain_main(); //master domain
        $data['m_edu'] = $this->master_education_main($sid); //master education
        $data ['forms'] = $this->forms_main($sid); //master forms
        $data ['info'] = $this->student_information_main($sid); // student inforamtion
        $data ['addlocal'] = $this->student_addLocal_main($this->studCredentialId($sid)); //student address local
        $data ['addperm'] = $this->student_addPerm_main($this->studCredentialId($sid)); // student address permanent
        $data ['education'] = $this->student_education_main($sid); //student education					
        $data ['professional'] = $this->student_professional_main($sid); //student experience 				
        $data ['specialization'] = $this->student_specialization_main($sid); // student specialization				
        $data ['additionalCourse'] = $this->student_additionalCourse_main($sid); // student additional course
        $data ['projects'] = $this->student_projects_main($sid); // student projects														
        $data ['resposiblity'] = $this->student_resposiblity_main($sid); // student responsiblities
        $data ['awards'] = $this->student_awards_main($sid); // student awards
        $data ['skills'] = $this->student_skills_main($sid); // student skills
        $data ['hobbies'] = $this->student_hobbies_main($sid); // stuent hobbies	
        $data ['extraActivity'] = $this->student_extraActivity_main($sid); // stuent hobbies	

        if ($this->checkkUser() == "PLACEMENT_TEAM") {
            $data ['pt_info'] = $this->student_information_main($this->session->userdata("student_id")); // student inforamtion		 	
        }


        $this->load->view('STUDENT/profile_view', $data);
    }

//profile_data
//To Generate  the Pdf Report
    public function Pdf($sid) {
        $this->load->library('mpdf');

        $data['student_id'] = $sid;

        $data ['info'] = $this->student_information_main($sid); // student inforamtion
        $data ['addlocal'] = $this->student_addLocal_main($this->studCredentialId($sid)); //student address local
        $data ['addperm'] = $this->student_addPerm_main($this->studCredentialId($sid)); // student address permanent
        $data ['education'] = $this->student_education_main($sid); //student education					
        $data ['professional'] = $this->student_professional_main($sid); //student experience 				
        $data ['specialization'] = $this->student_specialization_main($sid); // student specialization				
        $data ['additionalCourse'] = $this->student_additionalCourse_main($sid); // student additional course
        $data ['projects'] = $this->student_projects_main($sid); // student projects														
        $data ['resposiblity'] = $this->student_resposiblity_main($sid); // student responsiblities
        $data ['awards'] = $this->student_awards_main($sid); // student awards
        $data ['skills'] = $this->student_skills_main($sid); // student skills
        $data ['hobbies'] = $this->student_hobbies_main($sid); // stuent hobbies	
        $data ['extraActivity'] = $this->student_extraActivity_main($sid); // stuent hobbies	

        $this->load->view('pdf', $data);
    }

// new work
// new work
//----------------------------------------JOB-------------------------------------------------
    public function viewjobs($sid) {
        if ($this->checkkUser() == "STUDENT") {

            $joballdata['data'] = $this->viewjobs_function($sid);

            $joballdata ['info'] = $this->student_information_main($this->session->userdata("student_id")); // student inforamtion

            $joballdata ['info1'] = $this->fetchjobsel();

            $sid = $this->session->userdata("student_id");
            //$this->load->fetch_model();
            $joballdata['info2'] = $this->fetch_model->batch($sid);

            //print_r($joballdata);

            $this->load->view('STUDENT/managejobs_view', $joballdata);
        } else {
            redirect('STUDENT/500');
        }
    }

    public function applyjob($jobid, $sid) {
        if ($this->checkkUser() == "STUDENT") {
            $status = 0;
            $data = array('job_id' => $jobid,
                'student_id' => $sid,
                'status' => $status);
            if ($this->applyjob_function($data)) {
                $this->session->set_flashdata("successmessage", "Job has been applied successfully!");
                redirect('student/viewjobs/' . $this->session->userdata("student_id"));
            } else {
                $this->session->set_flashdata("errormessage", "Error in applying job!");
                redirect('student/viewjobs/' . $this->session->userdata("student_id"));
            }
        } else {
            redirect('STUDENT/500');
        }
    }

    function ppmreport($stud_id) {

        if ($this->checkkUser() == "STUDENT") {
            $query = $this->db->query("select PRN, first_name, middle_name, last_name from tbl_student_information where student_id = " . $stud_id);
            $data['studinfo'] = $query->row();

            $data['info'] = $this->student_information_main($this->session->userdata("student_id")); // student inforamtion
            $data['report1'] = $this->student_report1($stud_id); // guest user inforamtion
            $data['report2'] = $this->student_report2($stud_id);
            $this->load->view('STUDENT/finalreport_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function checkjoiningdate() {
        if ($this->checkkUser() == "STUDENT" || $this->checkkUser() == "PLACEMENT_TEAM") {
            $query = $this->db->query("select date_to from tbl_student_experiences where student_id = " . $_POST['student_id']);
            $result = $query->row();
            echo $result->date_to;
        }
    }

}
