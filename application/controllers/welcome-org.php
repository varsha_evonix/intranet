<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
class Welcome extends MY_Controller{


	public function __construct()
    {
        parent::__construct();
        $this->load->library('session');	
   		$this->load->model('fetch_model');

    }

	public function index() // index function 
	{
			$this->checkLogin();
	}

	public function login() // load login view
	{
		$this->load->view('login_view');
	}


	public function superadmin() //load super_admin/dashboad view
	{
		if($this->checkkUser() == "SUPER_ADMIN")
		{
			redirect('superadmin/home');	
//			$this->load->view('SUPER_ADMIN/dashboard_view');
		}
		else
		{
			redirect('welcome/noauthority');
		}
	}


	public function admin() //load admin/dashboad view
	{
		if($this->checkkUser() == "FACULTY")
		{
			redirect('admin/index');	

		//	$this->load->view('ADMIN/dashboard_view');
		}
		else
		{
			redirect('welcome/noauthority');
		}
	}

	public function student() //load student/dashboad view
	{
		if($this->checkkUser() == "STUDENT")
		{
			redirect('student/index');	

			//$this->load->view('STUDENT/dashboard_view');
		}
		else
		{
			redirect('welcome/noauthority');
		}
		
	}
	
	public function placementteam() //load placementteam/dashboad view
	{
		if($this->checkkUser() == "PLACEMENT_TEAM")
		{
			redirect('placementteam/index');	

			//$this->load->view('PLACEMENT_TEAM/dashboard_view');
		}
		else
		{
			redirect('welcome/noauthority');
		}

	}

	public function block()
	{
		$this->load->view('bolckbyadmin_view');
	}

	// public function noAuthority()
	// {
	// 	$this->load->view('noauthority_view');
	// }


	public function askpage()
	{
		$this->load->view('askpage_view');
	}

	public function blockbyadmin()
	{
		$this->load->view('blockbyadmin_view');
	}


	
	public function noauthority() // for chacking authority of user if unauthorised then go to 500 view
	{
		if($this->checkkUser() == "SUPER_ADMIN")
		{
			$this->load->view('SUPER_ADMIN/500');
		}
		elseif ($this->checkkUser() == "FACULTY")
		{
			$data ['info']= $this->admin_information_main($this->session->userdata("credentials_id")); // student inforamtion			
			if($this->checkPT())
			{
				$this->load->view('ADMIN/500',$data);	//redirect to admin's 500 view
			}
			else
			{
				$this->session->set_flashdata("error","you dont have any authority allocated by super admin");
				redirect('welcome/login');
			}
		}
		else if($this->checkkUser() == "PLACEMENT_TEAM")
		{
		    $data ['info']= $this->student_information_main($this->session->userdata("student_id")); // student inforamtion		 	
			$this->load->view('PLACEMENT_TEAM/500',$data);
		}
		else if($this->checkkUser() == "STUDENT")
		{
			$data ['info']= $this->student_information_main($this->session->userdata("student_id")); // student inforamtion		 	
			redirect('student/noauthority',$data);
		}
		else
		{
			redirect('welcome/login'); //redirect to login	
		}

	}
	
	public function checkkUser() // check user role
	{
		return $this->session->userdata("role");
	}


	public function checkLogin() // check if user is alredy loged id or not
	{

		if($this->session->userdata("role"))
		{
			if($this->session->userdata("role") == "SUPER_ADMIN")	
			{	
				redirect('welcome/superadmin');	//redirect to admin's dashboard
			}
			else if($this->session->userdata("role") == "FACULTY")
			{
				if($this->checkPT())
				
					{
						redirect('welcome/admin');	//redirect to Placement team's dashboard
					}
					else
					{
						redirect('welcome/noauthority');
					}
				
			}
			else if($this->session->userdata("role") == "PLACEMENT_TEAM")
			{
				redirect('welcome/placementteam');	//redirect to student's dashboard
			}
			else
			{
				redirect('welcome/student');	//redirect to student's dashboard
			}
		}	
		else
		{
			redirect('welcome/login'); //redirect to login
		}
	}


	


	public function switchUser()
	{
		$role = $this->session->userdata("role");

		if($role=="STUDENT")
		{
			if($this->checkPT())
			{
				$this->session->set_userdata("role", "PLACEMENT_TEAM");
				redirect('welcome/placementteam');
			}
			else
			{
				$this->noauthority();
			}

		}
		else if ($role=="PLACEMENT_TEAM")
		{
				$this->ask_student();
		}
		else
		{
			$this->noauthority();
		}
		
	}

	public function logout()
	{
			$this->session->unset_userdata("user_id");
			$this->session->unset_userdata("role");
			$this->session->unset_userdata("credentials_id");
			$this->session->unset_userdata("student_id");
			$this->session->unset_userdata("stud_id");
			$this->session->unset_userdata("user_data");												
	
			  redirect('welcome/login');

	}

	public function login1()
	{
		$loginid = $this->input->post('loginid');
		$password = md5($this->input->post('password'));
		
		$data['loginInfo'] = $this->fetch_model->login($loginid,$password);

		if($data['loginInfo'])
		{
		//print_r($data['loginInfo']);exit;
			foreach ($data['loginInfo'] as $r)
			{

				if(($r->status)&&(!$r->isdelete))
				{
		 			$this->session->set_userdata("credentials_id", $r->credentials_id);

			 		if($r->role == "SUPER_ADMIN")
				 	{
				 		$this->session->set_userdata("role", "SUPER_ADMIN");
				 		redirect('welcome/superadmin');	
				 	}
					else if($r->role =="FACULTY") 
					{
					
						$this->session->set_userdata("role", "FACULTY");
						if($this->checkPT())
						{
						//echo 'dsfdsf';exit;
							if($this->check_PT_Status())
							{
								redirect('welcome/admin');	
							}
							else
							{
								$this->session->set_flashdata("error","you dont have any authority allocated by super admin");
								redirect('welcome/blockbyadmin');
							}
						}
						else
						{
							$this->session->set_flashdata("error","you dont have any authority allocated by super admin");
							redirect('welcome/noauthority');

						}
				 	}
				 	else
				 	{
				 		if($this->checkPT())
						{
							redirect('welcome/askpage');	

						}
						else
						{
							$this->ask_student();	
						}
				 	}
				}
				else
				{
					if(($r->status == 0) && ($r->isdelete == 1))
					{
						$this->session->set_flashdata("error","You are deleted & block by admin");
						redirect('welcome/blockbyadmin');
					}					
					else if($r->status == 0)
					{
						$this->session->set_flashdata("error","You are block by admin");
						redirect('welcome/blockbyadmin');
					}
					
					else if($r->isdelete == 1)
					{
						$this->session->set_flashdata("error","You are deleted by admin");
						redirect('welcome/blockbyadmin');
					}


				}
				
			}
		}
		else
		{
			$this->session->set_flashdata("error","worng credentials");
			redirect('welcome/login');
		}
		}
		


	public function ask_placementteam()
	{
		$cid = $this->session->userdata("credentials_id");
		if($this->check_PT_Status($cid))
		{
				 $this->session->set_userdata("role", "PLACEMENT_TEAM");
				 		$selectemail = "select student_id from tbl_student_information where credentials_id = '".$this->session->userdata("credentials_id")."'";
				 			$result = mysql_query($selectemail );
				 			$r = mysql_fetch_array($result);
				 			$num_rows = mysql_num_rows($result);
							
				 			if ( $num_rows > 0 ) 
				 			{
				 				 $this->session->set_userdata("student_id",$r['student_id']);
				 			}

				
				 redirect('welcome/placementteam');	
			}
			else
			{
				$this->session->set_flashdata("error","You are block by admin.");
				redirect('welcome/askpage');	
			}
	}
	
	public function ask_student()
	{
		$this->session->set_userdata("role", "STUDENT");

		$selectemail = "select student_id from tbl_student_information where credentials_id = '".$this->session->userdata("credentials_id")."'";
			$result = mysql_query($selectemail );
			$r = mysql_fetch_array($result);
			$num_rows = mysql_num_rows($result);
			
			if ( $num_rows > 0 ) 
			{
				 $this->session->set_userdata("student_id",$r['student_id']);
			}
		redirect('welcome/student');	
	}



public function checkPT()
{
	$cid = $this->session->userdata("credentials_id");
	$info =$this->fetch_model->checkPlacementTeam($cid);
	//print_r($info);exit;
	foreach ($info as $r) {
		if(($r->status == 1 ) && ($r->isdeleted == 0))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}



	public function check_PT_Status()
	{	
		$cid = $this->session->userdata("credentials_id");
		//echo $cid;
		$info =$this->fetch_model->model_check_PT_group_status($cid);
		//print_r($info);exit;
		if($info)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


}
	