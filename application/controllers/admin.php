<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();

class Admin extends MY_Controller {

    public function __construct() {
        parent::__construct();

        $this->check_session('FACULTY');
    }

//---------------------------------------- REQUIRED FUNCTION FOR VALIDATION -------------------


    public function checkkUser() { // check user role
        return $this->session->userdata("role");
    }

//---------------------------------------- REQUIRED FUNCTION FOR VALIDATION -------------------	


    public function index() { //forword to admin dashboard
        
//        if ($this->checkkUser() == "FACULTY") {
            $data ['info'] = $this->admin_information_main($this->session->userdata("credentials_id")); // student inforamtion			
            $this->load->view('ADMIN/dashboard_view', $data);
//        } else {
//            redirect('welcome/noauthority');
//        }
    }

    // ADMIN
    // Start Manage Groups


    public function createteam_main_new() {
//        if ($this->checkkUser() == "FACULTY") {



            $info['user_type'] = 1;

            // if($info['user_type'] == 1)
            // {
            // 	$info['user_type'] = 'Student';
            // 	$info['credentials_id'] = $this->input->post('prnexists');
            // }
            $info['user_type'] = 'Student';
            $info['group_id'] = $this->input->post('groupname');
            $info['created_by'] = $this->session->userdata("credentials_id"); // dynamic session login value
            $info['StudentBatch'] = $this->input->post('StudentBatch');
            /* $status= $this->input->post('enabled');


              if($status == "on")
              {
              $info['status'] = 1;
              }
              else
              {
              $info['status'] = 0;
              } */
            $info['status'] = 1;
            if ($this->createteam_function_new($info)) {
                $this->session->set_flashdata("successmessage", "Team has been added successfully!");
                redirect('admin/createteam');
            } else {
                $this->session->set_flashdata("errormessage", "Error while add team");
                redirect('admin/createteam');
            }
//        } else {
//            redirect('welcome/noauthority');
//        }
    }

    public function createteam_main() {
//        if ($this->checkkUser() == "FACULTY") {

            // $info['user_type'] = $this->input->post('usertype');
            $info['user_type'] = 1;

            if ($info['user_type'] == 1) {
                $info['user_type'] = 'Student';
                $info['credentials_id'] = $this->input->post('prnexists');
            }
            if ($info['user_type'] == 2) {
                $info['user_type'] = 'Faculty';
                $info['credentials_id'] = $this->input->post('Faculty');
                //print_r($info['credentials_id']);
                //exit;
            }
            if ($info['user_type'] == 3) {
                $info['user_type'] = 'GuestUser';
                $info['credentials_id'] = $this->input->post('GuestUser');
            }
            $info['group_id'] = $this->input->post('groupname');
            $info['created_by'] = $this->session->userdata("credentials_id"); // dynamic session login value
            $info['StudentBatch'] = $this->input->post('StudentBatch');

            $status = $this->input->post('enabled');
            if ($status == "on") {
                $info['status'] = 1;
            } else {
                $info['status'] = 0;
            }
            if ($this->createteam_function($info)) {
                $this->session->set_flashdata("successmessage", "Team has been added successfully!");
                redirect('admin/createteam');
            } else {
                $this->session->set_flashdata("errormessage", "Error while add team");
                redirect('admin/createteam');
            }
//        } else {
//            redirect('welcome/noauthority');
//        }
    }

    public function checkprn() {
//        if ($this->checkkUser() == "FACULTY") {

            $Student = $_POST['Student'];
            if ($Student) {

                $selectemail = "select credentials_id, PRN from tbl_student_information where PRN = '" . $Student . "'";
                $result = mysql_query($selectemail);
                $r = mysql_fetch_array($result);
                $num_rows = mysql_num_rows($result);
                if ($num_rows > 0) {
                    echo $r['credentials_id']; //"This PRN already exists.";
                } else {
                    echo ""; //"This PRN doesn't exits.";
                }
            }
//        } else {
//            redirect('welcome/noauthority');
//        }
    }

    public function changePlacementTeamStatus($placement_team_id, $selectedGroup) {
        $data['created_by'] = $this->session->userdata("credentials_id");
        $data['created_date'] = date('Y-m-d h:m:s');

        if ($this->changePlacementTeamStatus_main($data, $placement_team_id)) {
            $this->session->set_flashdata("sucess", "Placement team status has been change successfully");
        } else {
            $this->session->set_flashdata("error", "Error while updating placement team status");
        }
        $this->session->set_userdata("selectedGroup", $selectedGroup);
        redirect('admin/manageplacementteam');
    }

    public function placementteam($plteamid) { //forword to admin dashboard
//        if ($this->checkkUser() == "FACULTY") {
            //echo "zxc"; exit;
            $plteam['plteamdata'] = $this->plteam_function($plteamid);
            $plteam['data'] = $this->managegroups_function();
            //$plteam['data'] = $this->plteam_function();
            $plteam['studentbatchdata'] = $this->studentbatchdata_function();
            $plteam['studentdata'] = $this->studentdata_function();
            $plteam['facultydata'] = $this->facultydata_function();
            $plteam['guestuser'] = $this->guestuserdata_function();
            $this->load->view('ADMIN/plteam_view.php', $plteam);
//        } else {
//            redirect('welcome/noauthority');
//        }
    }

    public function updateteam_main() { //forword to admin dashboard
//        if ($this->checkkUser() == "FACULTY") {
            $info['user_type'] = $this->input->post('usertype');
            $info['plteamid'] = $this->input->post('plteamid');

            if ($info['user_type'] == 1) {
                $info['user_type'] = 'Student';
                $info['credentials_id'] = $this->input->post('prnexists');
            }
            if ($info['user_type'] == 2) {
                $info['user_type'] = 'Faculty';
                $info['credentials_id'] = $this->input->post('Faculty');
            }
            if ($info['user_type'] == 3) {
                $info['user_type'] = 'GuestUser';
                $info['credentials_id'] = $this->input->post('GuestUser');
            }
            $info['group_id'] = $this->input->post('groupname');
            $info['created_by'] = 2; // dynamic session login value

            $status = $this->input->post('enabled');
            if ($status == "on") {
                $info['status'] = 1;
            } else {
                $info['status'] = 0;
            }
            if ($this->updateteam_function($info)) {
                $this->session->set_flashdata("successmessage", "Successfully updated!");
                redirect('admin/manageplteam');
            } else {
                $this->session->set_flashdata("errormessage", "Error while update team");
                redirect('admin/manageplteam');
            }
//        } else {
//            redirect('welcome/noauthority');
//        }
    }

    /* function for fetchusertypelist */

    public function fetchusertypelist() {
//        if ($this->checkkUser() == "FACULTY") {

            $usertype = $_POST['usertype'];
            if ($usertype == 1) {
                $qry = "SELECT mb.*, tbc.batch_course_id, tbc.course_id, mc.course_name from master_batch mb inner join tbl_batch_course tbc on mb.batch_id = tbc.batch_id inner join master_course mc on tbc.course_id = mc.course_id where mb.isdelete=0 and tbc.enabled=1";
                $result = mysql_query($qry);

                $results = array();
                while ($row = mysql_fetch_array($result)) {
                    $results[] = array(
                        'batch_course_id' => $row['batch_course_id'],
                        'batch_name' => $row['batch_name'],
                        'in_year' => $row['in_year'],
                        'out_year' => $row['out_year'],
                        'course_name' => $row['course_name']
                    );
                }
                echo $json = json_encode($results);
            } else if ($usertype == 2) {
                $cid = $this->session->userdata("credentials_id");
                $groupname = $_POST['groupname'];
                $numfaculty = mysql_query("select * from tbl_placement_team where user_type = 'Faculty' and group_id = '$groupname'");
                if (mysql_num_rows($numfaculty) > 0) {
                    $qry = "select tf.*, tpt.credentials_id as chkcredid from tbl_faculty tf left outer join tbl_placement_team tpt on tf.credentials_id = tpt.credentials_id and tpt.group_id = '$groupname' and tpt.isdeleted = 0 and tpt.credentials_id != $cid order by chkcredid desc";
                    $result = mysql_query($qry);
                    $results = array();
                    while ($row = mysql_fetch_array($result)) {
                        if ($row['chkcredid'] != '') {
                            $facultyy = 1;
                        } else {
                            $facultyy = 0;
                        }
                        $results[] = array(
                            'credentials_id' => $row['credentials_id'],
                            'first_name' => $row['first_name'],
                            'last_name' => $row['last_name'],
                            'cred_id' => $row['cred_id'],
                            'numfaculty' => 1,
                            'facultyy' => $facultyy
                        );
                    }
                    echo $json = json_encode($results);
                } else {
                    $qry = "select * from tbl_faculty where credentials_id != $cid";
                    $result = mysql_query($qry);

                    $results = array();
                    while ($row = mysql_fetch_array($result)) {
                        $results[] = array(
                            'credentials_id' => $row['credentials_id'],
                            'first_name' => $row['first_name'],
                            'last_name' => $row['last_name'],
                            'numfaculty' => 0
                        );
                    }
                    echo $json = json_encode($results);
                }

                /* $numqry = mysql_query("select * from tbl_placement_team where user_type = 'Faculty'");
                  if(mysql_num_rows($numqry) > 0)
                  {
                  $qry ="SELECT tf.credentials_id, tf.first_name, tf.last_name, IFNULL(tpt.credentials_id,0) as cred_id from tbl_faculty tf left join tbl_placement_team tpt on tf.credentials_id = tpt.credentials_id";
                  $result = mysql_query($qry );
                  $count = mysql_num_rows($result);

                  $results = array();
                  while($row = mysql_fetch_array($result))
                  {
                  $results[] = array(
                  'credentials_id' => $row['credentials_id'],
                  'first_name' => $row['first_name'],
                  'last_name' => $row['last_name'],
                  'cred_id' => $row['cred_id'],
                  'numqry' => $count,
                  );
                  }
                  echo $json = json_encode($results);
                  }
                  else
                  {
                  $qry ="select * from tbl_faculty";
                  $result = mysql_query($qry );

                  $results = array();
                  while($row = mysql_fetch_array($result))
                  {
                  $results[] = array(
                  'credentials_id' => $row['credentials_id'],
                  'first_name' => $row['first_name'],
                  'last_name' => $row['last_name'],
                  'numqry' => 0
                  );
                  }
                  echo $json = json_encode($results);
                  } */
            } else if ($usertype == 3) {
                $groupname = $_POST['groupname'];
                $numguestuser = mysql_query("select * from tbl_placement_team where user_type = 'GuestUser' and group_id = '$groupname'");
                //echo "select * from tbl_placement_team where user_type = 'GuestUser'";
                //exit;
                if (mysql_num_rows($numguestuser) > 0) {
                    //$qry ="select * from tbl_guest_user";
                    $qry = "select tg.*, tpt.credentials_id as chkcredid from tbl_guest_user tg left outer join tbl_placement_team tpt on tg.credentials_id = tpt.credentials_id and tpt.group_id = '$groupname' and tpt.isdeleted = 0 order by chkcredid desc";
                    $result = mysql_query($qry);

                    $results = array();
                    while ($row = mysql_fetch_array($result)) {
                        if ($row['chkcredid'] != '') {
                            $guestusery = 1;
                        } else {
                            $guestusery = 0;
                        }
                        $results[] = array(
                            'credentials_id' => $row['credentials_id'],
                            'first_name' => $row['first_name'],
                            'last_name' => $row['last_name'],
                            'numguestuser' => 1,
                            'guestusery' => $guestusery
                        );
                    }
                    echo $json = json_encode($results);
                } else {
                    $qry = "select * from tbl_guest_user";
                    $result = mysql_query($qry);

                    $results = array();
                    while ($row = mysql_fetch_array($result)) {
                        $results[] = array(
                            'credentials_id' => $row['credentials_id'],
                            'first_name' => $row['first_name'],
                            'last_name' => $row['last_name'],
                            'numguestuser' => 0
                        );
                    }
                    echo $json = json_encode($results);
                }
            }
//        } else {
//            redirect('welcome/noauthority');
//        }
    }


    public function fetchstudlist1() {
        if ($this->checkkUser() == "FACULTY") {

            $studvalue = $_POST['studvalue'];
            $groupname = $_POST['groupname'];

            $numstud = mysql_query("select * from tbl_placement_team where user_type = 'Student' and StudentBatch = '$studvalue' and group_id = '$groupname'");
            //echo "select * from tbl_placement_team where user_type = 'Student' and StudentBatch = '$studvalue' and group_id = '$groupname'";
            //exit;
            if (mysql_num_rows($numstud) > 0) {
                //$qry ="select s.`credentials_id`,s.`first_name`,s.`middle_name`,s.`last_name` from tbl_student_information s inner join tbl_batch_student tbs on tbs.`student_id` = s.`student_id` where tbs.batch_course_id = '".$studvalue."'";
                $qry = "select s.`credentials_id`,s.`first_name`,s.`middle_name`,s.`last_name`, `tpt`.credentials_id as chkcredid from tbl_student_information s inner join tbl_batch_student tbs on tbs.`student_id` = s.`student_id` left outer join tbl_placement_team tpt on tpt.`credentials_id` = s.`credentials_id` and tpt.group_id = '$groupname' and tpt.isdeleted = 0 and s.isdelete = 0 where tbs.batch_course_id = '" . $studvalue . "' order by chkcredid desc";

                $result = mysql_query($qry);

                $results = array();
                while ($row = mysql_fetch_array($result)) {
                    if ($row['chkcredid'] != '') {
                        $study = 1;
                    } else {
                        $study = 0;
                    }
                    $results[] = array(
                        'credentials_id' => $row['credentials_id'],
                        'first_name' => $row['first_name'],
                        'last_name' => $row['last_name'],
                        'numstud' => 1,
                        'study' => $study
                    );
                }
                echo $json = json_encode($results);

                //$r = mysql_fetch_array($result);
                //print_r($r);
                /* foreach($r as $val)
                  {
                  echo $val['credentials_id'];
                  } */
                /* $data['result'] = $r;
                  echo json_encode($r);
                  exit; */
            } else {
                $qry = "select s.`credentials_id`,s.`first_name`,s.`middle_name`,s.`last_name` from tbl_student_information s inner join tbl_batch_student tbs on tbs.`student_id` = s.`student_id`and s.isdelete = 0 where tbs.batch_course_id = '" . $studvalue . "'";
                $result = mysql_query($qry);

                $results = array();
                while ($row = mysql_fetch_array($result)) {
                    $results[] = array(
                        'credentials_id' => $row['credentials_id'],
                        'first_name' => $row['first_name'],
                        'last_name' => $row['last_name'],
                        'numstud' => 0
                    );
                }
                echo $json = json_encode($results);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

// admin -> assign student	 
    public function fetchstudlist() {

        if ($this->checkkUser() == "FACULTY") {
            $studvalue = $_POST['studvalue'];
            $groupname = $_POST['groupname'];
            $placement_team_id = $_POST['placement_team_id'];


            $qry = "select s.credentials_id,s.student_id,s.PRN,s.first_name,s.middle_name,s.last_name, tpt.placement_team_id, tpt.student_id as chkcredid from tbl_student_information s inner join tbl_batch_student tbs on tbs.student_id = s.student_id left outer join tbl_pt_students tpt on tpt.student_id = s.student_id where tbs.batch_course_id = $studvalue and s.credentials_id !=(select credentials_id from tbl_placement_team where placement_team_id =  $placement_team_id) order by chkcredid desc";
            $result = mysql_query($qry);

            $results = array();
            while ($row = mysql_fetch_array($result)) {

                if ($row['chkcredid'] != '') {
                    $study = 1;
                } else {
                    $study = 0;
                }

                $results[] = array(
                    'student_id' => $row['student_id'],
                    'credentials_id' => $row['credentials_id'],
                    'PRN' => $row['PRN'],
                    'first_name' => $row['first_name'],
                    'middle_name' => $row['middle_name'],
                    'last_name' => $row['last_name'],
                    'numstud' => 0,
                    'study' => $study,
                    'placement_team_id' => $row['placement_team_id']
                );
            }
            echo $json = json_encode($results);
        } else {
            redirect('welcome/noauthority');
        }
    }


    /* function for fetchplacementteamlist */

    public function fetchplacementteamlist() {
        if ($this->checkkUser() == "FACULTY") {

            $placementteam = $_POST['placementteam'];
            $groupid = $_POST['groupid'];

            if ($placementteam == "Faculty") {
                $tbl = "tbl_faculty";
            }

            if ($placementteam == "Student") {
                $tbl = "tbl_student_information";
            }

            if ($placementteam == "GuestUser") {
                $tbl = "tbl_guest_user";
            }

            $id = $this->session->userdata("credentials_id");
            // original query
            /* $qry ="select tpt.*, tf.first_name, tf.last_name
              from tbl_placement_team tpt
              inner join ".$tbl." tf
              on tpt.credentials_id = tf.credentials_id
              where user_type = '$placementteam'
              and isdeleted != 1
              and tpt.credentials_id !=".$id." "; */

            $qry = "select tpt.*, tf.first_name, tf.last_name
					from tbl_placement_team tpt 
					inner join " . $tbl . " tf 
					on tpt.credentials_id = tf.credentials_id 
					where user_type = '$placementteam'
					and group_id = '$groupid'
					and isdeleted != 1
					and tpt.credentials_id !=" . $id . " ";

            $result = mysql_query($qry);

            $results = array();
            while ($row = mysql_fetch_array($result)) {
                $results[] = array(
                    'placement_team_id' => $row['placement_team_id'],
                    'first_name' => $row['first_name'],
                    'last_name' => $row['last_name'],
                    'status' => $row['status']
                );
            }
            echo $json = json_encode($results);
        } else {
            redirect('welcome/noauthority');
        }
    }

    /* function for fetchauthoritylist */

    public function fetchauthoritylist() {
        if ($this->checkkUser() == "FACULTY") {

            $studvalue = $_POST['studvalue'];
            $numqry = mysql_query("select * from tbl_authority_group where placement_team_id = '$studvalue'");
            //echo "select * from tbl_authority_group where placement_team_id = '$studvalue'";
            //exit;
            if (mysql_num_rows($numqry) > 0) {
                //$qry ="SELECT tbl_authority_group.*, master_authority.authority_name from tbl_authority_group inner join master_authority on tbl_authority_group.authority_id = master_authority.authority_id where placement_team_id = '$studvalue'";
                $qry = "select * from master_authority";
                $result = mysql_query($qry);

                $results = array();
                while ($row = mysql_fetch_array($result)) {
                    $qrychk = "select * from tbl_authority_group where authority_id = $row[authority_id] and placement_team_id = '$studvalue'";
                    $resultchk = mysql_query($qrychk);
                    $y = mysql_num_rows($resultchk);
                    $results[] = array(
                        'authority_id' => $row['authority_id'],
                        'authority_name' => $row['authority_name'],
                        'numqry' => 1,
                        'y' => $y
                    );
                }
                //exit;
                echo $json = json_encode($results);
            } else {
                $qry = "select * from master_authority";
                $result = mysql_query($qry);

                $results = array();
                while ($row = mysql_fetch_array($result)) {
                    $results[] = array(
                        'authority_id' => $row['authority_id'],
                        'authority_name' => $row['authority_name'],
                        'numqry' => 0
                    );
                }
                echo $json = json_encode($results);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function assignauthority_main() {
        if ($this->checkkUser() == "FACULTY") {
            $info['placement_team_id'] = $this->input->post('placement_team_id');
            $info['authority_id'] = $this->input->post('Student');
            $selectedGroup = $this->input->post('selected_GroupName');

            if ($this->assignauthority_function($info)) {
                $this->session->set_flashdata("sucess", "Authority has been assigned successfully!");
            } else {
                $this->session->set_flashdata("error", "Error while assign authority");
            }
            $this->session->set_userdata("selectedGroup1", $selectedGroup);
            redirect('admin/manageplacementteam');
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function assignStudent() {
        if ($this->checkkUser() == "FACULTY") {
            $info["placement_team_id"] = $this->input->post('placement_team_id');

            $selectedGroup = $this->input->post('selected_GroupName');

            if ($this->assignStudents_main($info)) {
                $this->session->set_flashdata("sucess", "Student has been assigned successfully!");
            } else {
                $this->session->set_flashdata("error", "Error while assigning students");
            }

            $this->session->set_userdata("selectedGroup2", $selectedGroup);
            redirect('admin/manageplacementteam');
        } else {
            redirect('welcome/noauthority');
        }
    }

    // End Manage Teams
//........................SANJAY..............................................

    public function insertguestuser() {

        $filename = $_FILES['pan_image']['name'];
        $filesize = $_FILES['pan_image']['size'];
        $ext = end((explode(".", $filename)));

        /* if ($ext == 'jpg' or $ext == 'jpeg' or $ext == 'png') 
          { */
        /* if($filesize <= 61440) // file size max 60kb
          { */
        $status1 = $this->input->post('status');
        if ($status1 == "on") {
            $status = 1;
        } else {
            $status = 0;
        }

        $password = $this->input->post('contact');
        $guestuser = 'GUEST_USER';
        $credentials = array('username' => $this->input->post('email'),
            'password' => md5($password),
            'role' => $guestuser,
            'status' => $status);


        $this->insert_guestuser_main($credentials);
        /* }
          else
          {
          $this->session->set_flashdata('errormsg',"File size should not be greater than 60kb");
          redirect(base_url().'admin/createguestuser');
          } */
        /* } 
          else
          {
          $this->session->set_flashdata('errormsg',"Please upload jpg or jpeg or png file only");
          redirect(base_url().'admin/createguestuser');
          } */
    }

    public function insert_guestuser1($credid) {
        $fname = ucfirst($this->input->post('fname'));
        $mname = ucfirst($this->input->post('mname'));
        $lname = ucfirst($this->input->post('lname'));
        $filename = $_FILES['pan_image']['name'];


        $data = array(
            'first_name' => $fname,
            'middle_name' => $mname,
            'last_name' => $lname,
            'DOB' => date('Y-m-d', strtotime($this->input->post('dob'))),
            'gender' => $this->input->post('gender'),
            /* 'primary_email'=>$this->input->post('email1'), */
            'allocated_email' => $this->input->post('email'),
            'contact_no_1' => $this->input->post('contact'),
            'pan_no' => $this->input->post('pan_no'),
            'account_no' => $this->input->post('account_no'),
            'bankname' => $this->input->post('bankname'),
            'branchname' => $this->input->post('branchname'),
            'ifsccode' => $this->input->post('ifsccode'),
            'pan_image_link' => $filename,
            'credentials_id' => $credid
        );


        if ($guestId = $this->insert_guestuser1_main($data)) {
            $ext = end((explode(".", $filename)));
            $uploaddir = 'uploaddocs/guest_user/pancard/';
            $fileguest = $uploaddir . basename($guestId . "_" . "pan_image" . '.' . $ext);
            //echo $fileguest;exit;
            move_uploaded_file($_FILES['pan_image']['tmp_name'], $fileguest);

            $imagedata = array('pan_image_link' => $fileguest);
            $this->db->where('guest_user_id', $guestId);
            $this->db->update('tbl_guest_user', $imagedata);

            $purpose = $this->input->post('purpose');
            //print_r($purpose);exit;
            $purposedata = array('guestuser_id' => $guestId, 'purpose' => $purpose);
            foreach ($purpose as $values) {
                $purposedata = array('guestuser_id' => $guestId, 'purpose' => $values);
                $this->db->insert('tbl_guestuser_purpose', $purposedata);
            }
            if ($this->checkkUser() == "FACULTY") {
                $this->session->set_flashdata("message", "Guest User has been added successfully!");
                redirect('admin/createguestuser');
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "FACULTY") {
                $this->session->set_flashdata("errormsg", "Error in adding Guest User!");
                redirect('admin/createguestuser');
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function editguestuser($fid) {
        $data['info'] = $this->admin_information_main($this->session->userdata("credentials_id"));
        $data['fac'] = $this->edit_guestuser($fid);
        $data['purposelist'] = $this->get_guestuser_purpose();
        foreach ($data['fac'] as $guser) {
            $guestid = $guser->guest_user_id;
        }
        $data['purpose'] = $this->guestuser_purpose($guestid);
        if ($this->checkkUser() == "FACULTY") {
            $this->load->view("ADMIN/editguestuser_view", $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function updateguestuser() {

        $filename = $_FILES['pan_image']['name'];
        $guestId = $this->input->post('guestuserId');


        if ($filename) {
            $filesize = $_FILES['pan_image']['size'];
            $ext = end((explode(".", $filename)));
            if ($ext == 'jpg' or $ext == 'jpeg' or $ext == 'png') {
                if ($filesize <= 61440) { // file size max 60kb
                    $ext = end((explode(".", $filename)));
                    $uploaddir = 'uploaddocs/guest_user/pancard/';
                    $fileguest = $uploaddir . basename($guestId . "_" . "pan_image" . '.' . $ext);
                    //echo $fileguest;exit;
                    move_uploaded_file($_FILES['pan_image']['tmp_name'], $fileguest);
                } else {
                    $this->session->set_flashdata('errormsg', "File size should not be greater than 60kb");
                    redirect(base_url() . 'admin/editguestuser/' . $this->input->post('credentials_id'));
                }
            } else {
                $this->session->set_flashdata('errormsg', "Please upload jpg or jpeg or png file only");
                redirect(base_url() . 'admin/editguestuser/' . $this->input->post('credentials_id'));
            }
            $panimage_link = $fileguest;
        } else {
            //echo 'rere';exit;
            $panimage_link = $this->input->post('expanimage');
        }
        $data['credentials_id'] = $this->input->post('credentials_id');
        $data['guest_user_id'] = $this->input->post('guest_user_id');
        $data['first_name'] = $this->input->post('fname');
        $data['middle_name'] = $this->input->post('mname');
        $data['last_name'] = $this->input->post('lname');
        $data['username'] = $this->input->post('uname');

        $data['DOB'] = date('Y-m-d', strtotime($this->input->post('dob')));
        $data['gender'] = $this->input->post('gender');
        $data['contact_no_1'] = $this->input->post('contact');
        $data['pan_no'] = $this->input->post('pan_no');
        $data['account_no'] = $this->input->post('account_no');
        $data['bankname'] = $this->input->post('bankname');
        $data['branchname'] = $this->input->post('branchname');
        $data['ifsccode'] = $this->input->post('ifsccode');
        $data['pan_image_link'] = $panimage_link;

        $status = $this->input->post('status');
        if ($status == "on") {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        if ($this->updateguestuser_function($data)) {
            $purpose = $this->input->post('purpose');
            //print_r($purpose);exit;
            $this->db->where('guestuser_id', $guestId);
            $this->db->delete('tbl_guestuser_purpose');
            foreach ($purpose as $values) {
                $purposedata = array('guestuser_id' => $guestId, 'purpose' => $values);
                $this->db->insert('tbl_guestuser_purpose', $purposedata);
            }
            if ($this->checkkUser() == "FACULTY") {
                $this->session->set_flashdata("message", "Guest User has been updated successfully!");
                redirect(base_url() . 'admin/editguestuser/' . $this->input->post('credentials_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        } else {
            if ($this->checkkUser() == "FACULTY") {
                $this->session->set_flashdata("errormsg", "Error in updating Guest User!");
                redirect(base_url() . 'admin/editguestuser/' . $this->input->post('credentials_id'));
            } else {
                redirect('welcome/unAuthority');
            }
        }
    }

    public function deleteguestuser($fid) {
        if ($this->guestuser_delete($fid)) {

            echo "true";
        } else {

            echo "false";
        }
    }

    public function deleteppmreport($sid) {
        if ($this->ppmreport_delete($sid)) {

            echo "true";
        } else {

            echo "false";
        }
    }

    /* End Guest User */


    /* Start Manage Job */

    public function job($jobid) { //forword to admin dashboard
        if ($this->checkkUser() == "FACULTY") {
            $jobdata['jobdata'] = $this->job_function($jobid);
            $jobdata['data'] = $this->fetchorgname_function();
            $jobdata['sp'] = $this->fetchselectionprocess($jobid);
            $jobdata['job_bname'] = $this->get_job_batch_main($jobid);
            $jobdata['verticle'] = $this->master_domain_main();
            $jobdata['job_type'] = $this->master_jobtype_main();
            $jobdata['bname'] = $this->batchcourse_List();
            //$jobdata ['info']= $this->student_information_main($this->session->userdata("student_id")); // student inforamtion		 	
            $jobdata ['info'] = $this->admin_information_main($this->session->userdata("credentials_id")); // student inforamtion	
            $jobdata['selection_process'] = $this->master_selectionprocess_main();
            $jobdata['job_criteria1'] = $this->jobcriteria1_main($jobid);
            $jobdata['job_criteria2'] = $this->jobcriteria2_main($jobid);
            $jobdata['job_criteria3'] = $this->jobcriteria3_main($jobid);
            $jobdata['job_criteria4'] = $this->jobcriteria4_main($jobid);
            $jobdata['job_criteria5'] = $this->jobcriteria5_main($jobid);
            $this->load->view('ADMIN/job_view', $jobdata);
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function updatejob_main() {

        if ($this->checkkUser() == "FACULTY") {
            if ($this->checkkUser() == "FACULTY") {
                if ($_FILES['jdattachment']['name']) {
                    $jdfile = $_FILES['jdattachment']['name'];
                    $ext = end((explode(".", $jdfile)));
                    $uploaddir = 'uploaddocs/jd_docs/';
                    $filejdlink = $uploaddir . basename($this->input->post('job_id') . "_" . "jd_attachment" . '.' . $ext);
                    //echo $_FILES['jdattachment']['tmp_name'];
                    //exit;
                    move_uploaded_file($_FILES['jdattachment']['tmp_name'], $filejdlink);
                } else {
                    $filejdlink = $this->input->post('exitstfile');
                }

                $last_date_to_apply = date('Y-m-d H:i:s', strtotime($this->input->post('datetimepicker')));
                $info['job_id'] = $this->input->post('job_id');
                $info['organization_id'] = $this->input->post('organizationname');
                $info['offered_profile'] = $this->input->post('jobprofile');
                $info['total_requirements'] = $this->input->post('totalrequirements');
                $info['selection_mode'] = $this->input->post('selectionmode');
                $info['package_from'] = $this->input->post('packagefrom');
                $info['package_to'] = $this->input->post('packageto');
                $info['job_description'] = $this->input->post('jobdescription');
                $info['jd_attachment_link'] = $filejdlink;
                $info['last_date_to_applied'] = $last_date_to_apply;
                $info['status'] = $this->input->post('jobstatus');
                $info['domain_id'] = $this->input->post('orgVerticle');
                $info['job_type_id'] = $this->input->post('jobtype');
                $info['jobclosed'] = $this->input->post('jobclosed');
                $info['job_posted_date'] = date('Y-m-d h:m:s');
                $info['posted_by_PT'] = $this->ptId($this->session->userdata("credentials_id"));
                $info['approved_by'] = $this->ptId($this->session->userdata("credentials_id"));
                $info['approved_date'] = date('Y-m-d h:m:s');
                if ($this->updatejob_functionadmin($info)) {
                    $jobid = $this->input->post('job_id');
                    $this->deleteselectionprocess_main($jobid);
                    $this->deletebatchjob_main($jobid);
                    $this->deletejobcriteria_main($jobid);
                    $tempselection = $this->input->post('selectionprocess');
                    $counter = 1;
                    foreach ($tempselection as $e => $value) {
                        $sp['job_id'] = $jobid;
                        $sp['selection_id'] = $value['value'];
                        $sp['process_round'] = $counter;
                        $this->addselectionprocess_main($sp);
                        $counter++;
                    }
                    $sp['job_id'] = $jobid;
                    $sp['selection_id'] = 3;
                    $sp['process_round'] = $counter++;
                    $this->addselectionprocess_main($sp);

                    $sp['job_id'] = $jobid;
                    $sp['selection_id'] = 2;
                    $sp['process_round'] = $counter++;

                    $this->addselectionprocess_main($sp);

                    $x = $_POST['x'];
                    $xii = $_POST['xii'];
                    $grad = $_POST['grad'];
                    $pg = $_POST['pg'];
                    $workex = $_POST['workex'];
                    if ($x > 0) {
                        $cid = 1;
                        $c = array('job_id' => $jobid,
                            'criteria_id' => $cid,
                            'criteria' => $x);
                        $this->addjobcriteria_main($c);
                    }
                    if ($xii > 0) {
                        $cid = 2;
                        $c = array('job_id' => $jobid,
                            'criteria_id' => $cid,
                            'criteria' => $xii);
                        $this->addjobcriteria_main($c);
                    }
                    if ($grad > 0) {
                        $cid = 3;
                        $c = array('job_id' => $jobid,
                            'criteria_id' => $cid,
                            'criteria' => $grad);
                        $this->addjobcriteria_main($c);
                    }
                    if ($pg > 0) {
                        $cid = 4;
                        $c = array('job_id' => $jobid,
                            'criteria_id' => $cid,
                            'criteria' => $pg);
                        $this->addjobcriteria_main($c);
                    }
                    if ($workex > 0) {
                        $cid = 5;
                        $c = array('job_id' => $jobid,
                            'criteria_id' => $cid,
                            'criteria' => $workex);
                        $this->addjobcriteria_main($c);
                    }
                    $bid = $_POST['bname1'];

                    foreach ($bid as $e) {
                        $data = array('job_id' => $jobid,
                            'batch_course_id' => $e);
                        $this->addjobbatch_main($data);
                    }

                    $this->session->set_flashdata("successmessage", "Job has been updated successfully!");

                    redirect('admin/job/' . $this->input->post('job_id'));
                } else {
                    $this->session->set_flashdata("errormessage", "Error while update job");
                    redirect('admin/job/' . $this->input->post('job_id'));
                }
            } else {
                redirect('welcome/noauthority');
            }
        }
    }

    public function deletejob($jobid) {
        if ($this->deletejob_main($jobid)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function finaljobreport() {
        $jobdata['data'] = $this->getApplications_function();
        $jobdata ['info'] = $this->admin_information_main($this->session->userdata("credentials_id"));
        $this->load->view('ADMIN/finaljobreport_view.php', $jobdata);
    }

    public function getapplications($jobid) {
        $jobdata1['info_1'] = $this->getApplications_main($jobid);
        $jobdata1['info1'] = $this->fetchjobselectionprocess($jobid);
        $jobdata1['jobInfo'] = $this->job_function($jobid); // job deiatil
        $jobdata1 ['info'] = $this->admin_information_main($this->session->userdata("credentials_id")); // student inforamtion					
        $this->load->view('ADMIN/jobapplications_view', $jobdata1);
    }

    public function saveselectionstatus1() {
        $status = $_POST['spname'];
        $sid = $_POST['sid'];
        $jobid = $_POST['jobid'];
        if ($this->savestatus_main($status, $sid, $jobid)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    function multistudentstatus() {
        $status = $_POST['spname'];
        $sids = $_POST['sid'];
        $jobid = $_POST['jobid'];

        $statusarr = explode(',', $sids);

        for ($i = 0; $i < (count($statusarr) - 1); $i++) {
            //echo $statusarr[$i];
            $this->savestatus_main($status, $statusarr[$i], $jobid);
        }
        echo "true";
        /* exit;
          if($this->savestatus_main($status,$sids,$jobid))
          {
          echo "true";
          }
          else
          {
          echo "false";
          } */
    }

    public function deletegroup($groupid) {
        if ($this->deletegroup_main($groupid)) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function changeJobStaus($jobId) {
        $data['approved_by'] = $this->ptId($this->session->userdata("credentials_id"));
        $data['approved_date'] = date('Y-m-d h:m:s');

        if ($this->changeJobStaus_main($data, $jobId)) {
            $this->session->set_flashdata("message", "Job status has been changed successfully");
        } else {
            $this->session->set_flashdata("errormsg", "Error while updating job status");
        }
        redirect('admin/viewappliedjobs');
    }

    /* End Manage Job */
    /* End Manage Job */

    public function ptId($cid) {
        $qry = "SELECT placement_team_id
				FROM  `tbl_placement_team` 
				WHERE  credentials_id = $cid and status = 1";

        $result = mysql_query($qry);

        $results = array();
        while ($row = mysql_fetch_array($result)) {
            $ptid = $row['placement_team_id'];
        }
        return $ptid;
    }

    //........................SANJAY..............................................
    //........................For assinged guest user..........................Kaushal (04/05/2015)

    public function assigned_studentsold($credential_id) {
        if ($this->checkkUser() == "FACULTY") {
            $this->session->set_userdata("stud_cred_id", $credential_id);
            //echo $credential_id;exit;
            $data['info'] = $this->admin_information_main($this->session->userdata("credentials_id"));
            $data['ginfo'] = $this->guest_information_main($credential_id); // guest user inforamtion
            //print_r($data['ginfo']);exit;
            $guest_user_id = $data['ginfo']->guest_user_id;
            $data['data'] = $this->getassignedstudentlist_function($guest_user_id);
            $this->session->set_userdata("guest_user_id", $guest_user_id);
            $this->load->view('ADMIN/assigned_guest_students_view', $data);
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function assigned_students($credential_id) {
        if ($this->checkkUser() == "FACULTY") {
            $this->session->set_userdata("stud_cred_id", $credential_id);
            $data['info'] = $this->admin_information_main($this->session->userdata("credentials_id"));
            $data['ginfo'] = $this->guest_information_main($credential_id); // guest user inforamtion
            $data['credid'] = $data['ginfo']->guest_user_id;

            $data['batchInfo'] = $this->get_all_batch_list();

            $this->load->view('ADMIN/assigned_guest_students_view', $data);
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function manage_assigned_student($guestuserid) { //forword to placementteam dashboard
        if ($this->checkkUser() == "FACULTY") {
            $bid = $_POST['bid'];
            $info = $this->guestuser_allocated_studentInfo_main($bid, $guestuserid);
            echo $json = json_encode($info);
        } else {
            redirect('welcome/noauthority');
        }
    }

    /* function to download pmm1reports in excel - by prashant */

    public function download_ppm1report($guest_user_id) {
        if ($this->checkkUser() == "FACULTY") {
            //$guest_user_id = $this->session->userdata("guest_user_id");
            $guest_user_id = $guest_user_id;
            //$data['ppm'] = $this->getstudentsppm1report_function($guest_user_id);
            $data['guest_user_id'] = $guest_user_id;
            //print_r($data);
            $this->load->view('ADMIN/download_ppm1report_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function final_ppm1report() {
        if ($this->checkkUser() == "FACULTY") {
            $this->load->view('ADMIN/final_ppm1report_view');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function final_ppm2report() {
        if ($this->checkkUser() == "FACULTY") {
            $this->load->view('ADMIN/final_ppm2report_view');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function final_ppmreport() {
        if ($this->checkkUser() == "FACULTY") {
            $this->load->view('ADMIN/final_ppmreport_view');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function final_gd1report() {
        if ($this->checkkUser() == "FACULTY") {
            $this->load->view('ADMIN/final_gd1report_view');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function final_gd2report() {
        if ($this->checkkUser() == "FACULTY") {
            $this->load->view('ADMIN/final_gd2report_view');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function final_gdreport() {
        if ($this->checkkUser() == "FACULTY") {
            $this->load->view('ADMIN/final_gdreport_view');
        } else {
            redirect('welcome/unAuthority');
        }
    }

    function ppm1report($stud_id) {
        $data['stud_cred_id'] = $this->session->userdata("stud_cred_id");
        $data['stud_id'] = $stud_id;
        if ($this->checkkUser() == "FACULTY") {
            $data['info'] = $this->admin_information_main($this->session->userdata("credentials_id")); // admin inforamtion
            $data['srinfo'] = $this->guest_student_report1($stud_id, $this->session->userdata("guest_user_id")); // guest user inforamtion
            $this->load->view('ADMIN/ppm1report_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    function ppm2report($stud_id) {
        $data['stud_cred_id'] = $this->session->userdata("stud_cred_id");
        $data['stud_id'] = $stud_id;
        if ($this->checkkUser() == "FACULTY") {
            $data['info'] = $this->admin_information_main($this->session->userdata("credentials_id")); // admin inforamtion
            $data['srinfo'] = $this->guest_student_report2($stud_id, $this->session->userdata("guest_user_id")); // guest user inforamtion
            $this->load->view('ADMIN/ppm2report_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    function finalreport($stud_id) {
        $data['stud_cred_id'] = $this->session->userdata("stud_cred_id");
        if ($this->checkkUser() == "FACULTY") {
            $query = $this->db->query("select PRN, first_name, middle_name, last_name from tbl_student_information where student_id = " . $stud_id);
            $data['studinfo'] = $query->row();

            $data['info'] = $this->admin_information_main($this->session->userdata("credentials_id")); // admin inforamtion
            $data['report1'] = $this->guest_student_report1($stud_id, $this->session->userdata("guest_user_id")); // guest user inforamtion
            $data['report2'] = $this->guest_student_report2($stud_id, $this->session->userdata("guest_user_id"));
            $this->load->view('ADMIN/finalreport_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }


    public function addjob_main() {

        if ($this->checkkUser() == "FACULTY") {
            $jdfile = $_FILES['jdattachment']['name'];
            $last_date_to_apply = date('Y-m-d H:i:s', strtotime($this->input->post('datetimepicker')));
            $info['organization_id'] = $this->input->post('organizationname');
            $info['offered_profile'] = $this->input->post('jobprofile');
            $info['total_requirements'] = $this->input->post('totalrequirements');
            $info['selection_mode'] = $this->input->post('selectionmode');
            $info['package_from'] = $this->input->post('packagefrom');
            $info['package_to'] = $this->input->post('packageto');
            $info['job_description'] = $this->input->post('jobdescription');
            $info['jd_attachment_link'] = $jdfile;
            $info['last_date_to_applied'] = $last_date_to_apply;
            //$info['posted_by_PT'] = $this->ptId($this->session->userdata("credentials_id"));
            $info['job_posted_date'] = date('Y-m-d h:m:s');
            $info['domain_id'] = $this->input->post('orgVerticle');
            $info['job_type_id'] = $this->input->post('jobtype');
            $tempselection = $this->input->post('selectionprocess');
            $info['status'] = 1;
            $info['approved_by'] = null;
            $info['approved_date'] = null;
            $info['jobclosed'] = 0;
            $jobid = $this->addjob_function($info);
            if ($jobid) {
                $counter = 1;
                foreach ($tempselection as $e => $value) {
                    $sp['job_id'] = $jobid;
                    $sp['selection_id'] = $value['value'];
                    $sp['process_round'] = $counter;
                    $this->addselectionprocess_main($sp);
                    $counter++;
                }
                $sp['job_id'] = $jobid;
                $sp['selection_id'] = 3;
                $sp['process_round'] = $counter++;
                $this->addselectionprocess_main($sp);

                $sp['job_id'] = $jobid;
                $sp['selection_id'] = 2;
                $sp['process_round'] = $counter++;

                $this->addselectionprocess_main($sp);

                $bname1 = $_POST['bname1'];
                foreach ($bname1 as $f) {
                    $data = array('job_id' => $jobid,
                        'batch_course_id' => $f);
                    $this->addjobbatch_main($data);
                }
                $x = $_POST['x'];
                $xii = $_POST['xii'];
                $grad = $_POST['grad'];
                $pg = $_POST['pg'];
                $workex = $_POST['workex'];
                if ($x > 0) {
                    $cid = 1;
                    $c = array('job_id' => $jobid,
                        'criteria_id' => $cid,
                        'criteria' => $x);
                    $this->addjobcriteria_main($c);
                }
                if ($xii > 0) {
                    $cid = 2;
                    $c = array('job_id' => $jobid,
                        'criteria_id' => $cid,
                        'criteria' => $xii);
                    $this->addjobcriteria_main($c);
                }
                if ($grad > 0) {
                    $cid = 3;
                    $c = array('job_id' => $jobid,
                        'criteria_id' => $cid,
                        'criteria' => $grad);
                    $this->addjobcriteria_main($c);
                }
                if ($pg > 0) {
                    $cid = 4;
                    $c = array('job_id' => $jobid,
                        'criteria_id' => $cid,
                        'criteria' => $pg);
                    $this->addjobcriteria_main($c);
                }
                if ($workex > 0) {
                    $cid = 5;
                    $c = array('job_id' => $jobid,
                        'criteria_id' => $cid,
                        'criteria' => $workex);
                    $this->addjobcriteria_main($c);
                }

                if ($jdfile) {
                    $ext = end((explode(".", $jdfile)));
                    $uploaddir = 'uploaddocs/jd_docs/';
                    $filejdlink = $uploaddir . basename($jobid . "_" . "jd_attachment" . '.' . $ext);
                    //echo $_FILES['jdattachment']['tmp_name'];exit;
                    move_uploaded_file($_FILES['jdattachment']['tmp_name'], $filejdlink);

                    $imagedata = array('jd_attachment_link' => $filejdlink);
                    $this->db->where('job_id', $jobid);
                    $this->db->update('tbl_job', $imagedata);
                }

                $this->session->set_flashdata("successmessage", "Job has been added successfully!");
                redirect('admin/addjob');
            } else {
                $this->session->set_flashdata("errormessage", "Error while add job");
                redirect('admin/addjob');
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

    function changepassword() {
        if ($this->checkkUser() == "FACULTY") {
            $data['info'] = $this->admin_information_main($this->session->userdata("credentials_id"));
            $this->load->view('ADMIN/change_password_view.php', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function changepass_main($cid) {
        $data['password'] = md5($this->input->post('new_pwd'));

        if ($this->changePassword_main($data, $cid)) {
            $this->session->set_flashdata("sucess", "password has been successfully changed ");
        } else {
            $this->session->set_flashdata("error", "error while changing password");
        }
        redirect('admin/changepassword');
    }

    function checkOldPwd() {
        $cid = $_POST['cid'];
        //echo $cid.''.$_POST['oldpwd'];exit;
        if ($this->checkOldPwd_main($_POST['oldpwd'], $cid)) {
            //echo 'hello'; 
            echo 1;
        } else {
            echo 0;
        }
    }

    function managestudents($batchid = 0) {
        if ($this->checkkUser() == "FACULTY") {
            $data['batchid'] = $batchid;
            $data['info'] = $this->admin_information_main($this->session->userdata("credentials_id"));
            $ptid = $this->ptId($this->session->userdata("credentials_id"));
            $data['batchInfo'] = $this->get_batch_list_pt($ptid);
            $this->load->view('ADMIN/manage_student_view', $data);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    function manage_student() {
        if ($this->checkkUser() == "FACULTY") {
            $bid = $_POST['bid'];
            //$array = array();
            $info[0] = $this->allocated_studentInfo_main($bid);
            $info[1] = $this->get_placementstatus();
            echo $json = json_encode($info);
        } else {
            redirect('welcome/unAuthority');
        }
    }

    function savestatus() {
        if ($this->checkkUser() == "FACULTY") {
            $optId = $_POST['optId'];
            $studid = $_POST['studid'];
            if ($optId == 3) {
                $reason = "PPO";
            } else if ($optId == 2) {
                $reason = "Placed";
            } else {
                $reason = $_POST['reason'];
            }
            //ini_set('memory_limit', '-1');
            //echo $optId."-".$studid."-".$reason;
            if ($this->saveplacementstatus($optId, $studid, $reason)) {
                echo "true";
            } else {
                echo "false";
            }
        } else {
            redirect('welcome/unAuthority');
        }
    }

    public function submitplacementdetails() { //forword to admin dashboard
        if ($this->checkkUser() == "FACULTY") {
            /* $info['stud_id'] = $this->input->post('student_id');
              $info['job_id'] = $this->input->post('job');
              $info['package'] = $this->input->post('package');
              $info['location'] = $this->input->post('location');
              $info['joiningdate'] = $this->input->post('joiningdate'); */

            $stud_id = $this->input->post('student_id');
            $job_id = $this->input->post('job');
            $package = $this->input->post('package');
            $location = $this->input->post('location');
            $joiningdate = $this->input->post('joiningdate');
            $batchid = $this->input->post('bid');

            if ($this->submitplacementdetails_function($stud_id, $job_id, $package, $location, $joiningdate)) {
                $this->session->set_flashdata("sucess", "Placement status has been updated.");
                redirect('admin/managestudents/' . $batchid);
            } else {
                $this->session->set_flashdata("error", "Something going wrong. Can't update placement status !!");
                redirect('admin/managestudents/' . $batchid);
            }
        } else {
            redirect('welcome/noauthority');
        }
    }

    public function formdetails($sid, $bid) {
        $data['stud_id'] = $sid;
        $data['bid'] = $bid;
        if ($this->checkkUser() == "FACULTY") {
            $data['jobInfo'] = $this->getjobinfo($sid);
            $data['studInfo'] = $this->getstudinfo($sid);
            $this->load->view('ADMIN/formdetails_view', $data);
        } else {
            redirect('welcome/noauthority');
        }
    }

}
