<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>

<body>
<div id="wrapper">

		<?php include("header.php"); ?>

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Update Placement Group
                                <small>Update Placement Group Information</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>admin/index">Dashboard</a>                                </li>
                                <li class="active">Manage Placement Group</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

				<?php
				if($this->session->flashdata('successmessage'))
				{
					$successmessage = $this->session->flashdata('successmessage');
					echo "<script>swal('Ok', '$successmessage', 'success')</script>";
				}
				else if($this->session->flashdata('errormessage'))
				{
					$errormessage = $this->session->flashdata('errormessage');
					echo "<script>swal('Opps..!', '$errormessage', 'error')</script>";
				}
				?>

                   <div class="row">
                   	              
                    <!-- /.col-lg-12 -->
             

                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                    <div class="col-lg-12" style="margin-bottom:150px;">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Update Placement Group</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
									<?php
									//print_r($data);
									foreach($data as $r)
									{
									?>
                                    <form class="form-horizontal" id="sky-form" role="form" method="post" action="<?php echo base_url(); ?>admin/updategroup_main">
                                       
                                        <div class="form-group">
                                            <label for="groupname" class="col-sm-3 control-label">Placement Group Name <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" id="groupname" name="groupname" value="<?php echo $r->group_name; ?>" placeholder="Enter Placement Group Name" required />
                                            </div>
                                        </div>
										
										<div class="form-group">
												<label for="enabled" class="col-sm-3 control-label">Enabled</label>
												<div class="col-sm-9">
													<input type="checkbox" id="enabled" name="enabled" <?php if($r->status == 1) echo "checked='checked'"; ?> />
												</div>
										</div>
                                    
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-default">Update</button>
												<input type="hidden" class="form-control" id="groupid" name="groupid" value="<?php echo $r->group_id; ?>" />
                                            </div>
                                        </div>
                                    </form><?php
									}?>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
<!-- /#wrapper -->

    <?php include("alljs.php"); ?>
	
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					groupname:
					{
						required: true
					}
				},
									
				// Messages for form validation
				messages:
				{
					groupname:
					{
						required: '<span style="color:#b81212; margin-left:265px;">Please enter placement group name</span>'
					}
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
				
				
			});
		});
	</script>

	<script type="text/javascript">
		$("#manageplacementgroup").addClass("active");
		$("#pages2").addClass("in");
		$("#managegroups").addClass("active");
	</script>

</body>

</html>
