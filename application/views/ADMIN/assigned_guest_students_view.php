<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>



<body>
<div id="wrapper">

		<?php include("header.php"); ?>


        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Assigned Students
                           <small>Assigned Students</small></h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>admin/"> Dashboard</a>
                                </li>
                                 <li>  <a href="<?php echo base_url() ?>admin/manageguestuser"> Manage Guest User</a>
                                </li>
                                <li class="active">Assigned Students</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
				
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">
				    <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4> Assigned Student List of <?php echo $ginfo->first_name.' '.$ginfo->middle_name.' '.$ginfo->last_name;?></h4>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
								
											<form class="form-horizontal" id="validate" role="form" novalidate="novalidate">
                                    	<h5 style="color:#B81212;margin-left:166px;">* Select batch to show assigned student list</h5>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Select batch</label>
                                            <div class="col-sm-10">
                                            <select name="select" class="form-control" required="" id="batch_id" onchange="viewStudents()" name="bname1">
                                                    <option value="Select">Select One:</option>
                                                      <?php     
                                                          foreach($batchInfo as $b)
                                                          {?>
                                                            <option  value=<?php echo $b->batch_course_id; ?>
                                        
                                                                <?php if($this->session->userdata("selectedGroup") == $b->batch_course_id) {echo "selected";} ?>

                                                            >
                                                            <?php
                                                             echo  $b->batch_name."--".$b->course_name;
                                                             
                                                             echo"</option>";
                                                          }
                                                        ?>

                                                </select>
                                                    <span id="span_batch" style="color:rgb(184, 18, 18);"></span>
                                            </div>
                                        </div>
                                    </form>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                       


                    </div>
                    <!-- /.col-lg-12 -->
							<div id="editspllist">
                                            
                  	</div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
	    <?php include("alljs.php"); ?>
<script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>

<script type="text/javascript">
    function viewStudents()
        {
            var bid = $("#batch_id").val();
            var credid = '<?php echo $credid; ?>';

            if(bid=="Select")
            {
                $('#editspllist').html('');
                $('#span_batch').html('Please select batch');
            }
            else
            {
       $('#span_batch').html('');
           $.ajax({
            url: "<?php echo base_url();?>admin/manage_assigned_student/"+credid,
            type: "POST",
            async: true,
            dataType:'json',
            data: {bid:bid}, //your form data to post goes here
             success: function(response){
                    var json = response;
                    var dataSet = [];
                    $('#editspllist').html('');
                    $('#editspllist').append('<div class="row"><div class="col-lg-12"><div class="portlet portlet-default"><div class="portlet-heading"><div class="portlet-title"></div><a style="float:right;padding:5px;"  href="<?php echo base_url();?>admin/download_ppm1report/<?php echo $ginfo->guest_user_id; ?>">Download PPM1 REPORT</a><div class="clearfix"></div></div><div class="portlet-body"><div class="table-responsive"><table id="example-table" class="table table-striped table-bordered table-hover table-green"><thead><tr><th >Sr. No.</th><th>Student PRN </th><th>Name</th><th>Profile(Please add pdf viewer plugin)</th><th>PPM Report</th></tr></thead><tbody id="tbody">');
                    var j = 1;
                    for(var i = 0; i < json.length; i++) {

                        var obj = json[i];

                        if(obj.status == 1)
                        {
                            data = "<span class='badge green'>Active</span>";
                        }else {
                            data = "<span class='badge red'>Block</span>";
                        }

                        $('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td>'+obj.PRN+'</td><td style="width: 150px;">'+obj.first_name+' '+obj.middle_name+' '+obj.last_name+'</td><td><a href="<?php echo base_url();?>student/pdf/'+obj.student_id+'" target="_blank" ><i class="fa fa-files-o"></i> View</a></td><td><a href="<?php echo base_url();?>admin/finalreport/'+obj.student_id+'"><i class="fa fa-files-o"></i> View Report</a>&nbsp;&nbsp;<a onclick="deleteppmreport('+obj.student_id+')" title="Click Here To Delete" style="cursor: pointer;"><i class="fa fa-times"></i> Delete Report </a></td></tr>');
                        j++;
                    }
                    $('#editspllist').append('</tbody></table></div></div></div></div>');
					$('#example-table').dataTable({
					"data": dataSet,
					"coumns":[]
					});
					
        }
        });
    }//else
    
}
</script>

<script>
function deleteppmreport(sid)
{
	swal({  title: "Are you sure?",  
	type: "warning",    
	showCancelButton: true,   
	confirmButtonColor: "#DD6B55",   
	confirmButtonText: "Yes, delete it!",   
	closeOnConfirm: false
},
	function(){
            $.ajax({
            url: "<?php echo base_url();?>admin/deleteppmreport/"+sid,
            type: "POST",
            async: true, 
             success: function(response){ 
                 if(response.indexOf("true")> -1)
                 {
                    swal("Deleted!", "Student PPM Report has been deleted successfully.", "success");                    
                   setTimeout(function(){location.reload();},2000);                   
                 }
                 else
                 {
                    swal("Oops..!", "Something going wrong. can't delete Student PPM Report !!", "error"); 
                    location.reload();                                                  
                 }

        }
    });

    });
}
</script>
	
	<script>
	
	function deleteOrganization(jobid)
{

  

 swal({  title: "Are you sure?",  
          type: "warning",    
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes, delete it!",   
          closeOnConfirm: false 
     },


      function(){ 
             	  
            $.ajax({
            url: "<?php echo base_url();?>admin/deletejob/"+jobid,
            type: "POST",
            async: true, 
             success: function(response){ 
                 if(response.indexOf("true")> -1)
                 {
                    swal("Deleted!", "Job has been deleted successfully.", "success");                    
                   location.reload();                   
                 }
                 else
                 {
                    swal("Oops..!", "Something going wrong. can't delete Job !!", "error"); 
                    location.reload();                                                  
                 }

        }
    });



    });
}

	</script>
	
	<script>
		$("#manageguestusers").addClass("active");
	</script>
    

</body>

</html>
