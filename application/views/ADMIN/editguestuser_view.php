<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>
<style type="text/css">
em{float: left;width: 100%;}
</style>
<body>
<div id="wrapper">

		<?php include("header.php"); ?>

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Guest User
                                <small>Update Guest User Details</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="#">Dashboard</a>
                                </li>
                                <li class="active">Update Guest User</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->


                   <div class="row">
                   	              
                    <!-- /.col-lg-12 -->
               
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                    <div class="col-lg-12" >
					 <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
					{
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";

					}
                        else if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
                          
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Update Guest User</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
									<?php
									//print_r($purpose);
									foreach($fac as $r)
									{
									//$userarr = explode('@',$r->username);
									//$username = $userarr[0];
									?>
                                <form class="form-horizontal" id="sky-form" role="form" method="post" action="<?php echo base_url();?>admin/updateguestuser" enctype="multipart/form-data">
                                       
									   <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">First Name <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" id="fname" name="fname" style="width:90%"value="<?php echo $r->first_name; ?>" placeholder="Enter First Name" required />
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">Middle Name</label>
                                            <div class="col-sm-7">
                                                <input type="text" style="width:90%"  id="mname" name="mname" value="<?php echo $r->middle_name; ?>" placeholder="Enter Middle Name"/>
                                            </div>
                                        </div>
										
									   <div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">Last Name</label>
                                            <div class="col-sm-7">
                                                <input type="text" id="lname" name="lname" style="width:90%" value="<?php echo $r->last_name; ?>" placeholder="Enter Last Name"/>
                                            </div>
                                        </div>
                                  <div class="form-group">	
                                        <div id="sandbox-container">
                                          <label for="dob" class="col-sm-3 control-label">Date of Birth</label>
										  <div class="col-sm-7">
										 <input class="dateWork" type="text"  name="dob" placeholder="DD-MM-YYYY" value="<?php echo date('d-m-Y',strtotime($r->DOB)); ?>" readonly="readonly" style="width:90%">					  
                                              </div> 
											  </div>
											</div>
        
												<div class="form-group">
												
                                                    <label for="contact" class="col-sm-3 control-label">Mobile Number <span style="color:#b81212">*</span></label>
													<div class="col-sm-7">
                                                    <input type="text"  id="contact" placeholder="Enter Mobile Number" name="contact" maxlength=10 style="width:90%" value="<?php echo $r->contact_no_1; ?>">
                                                </div>
												</div>
												
												
				                                <div class="form-group">
												
                                                    <label for="gender" class="col-sm-3 control-label">Gender</label>
													
                                                    <div class="col-sm-6">
                                                       
														<label class="radio-inline">
                                                           <input type="radio" name="gender" value="M" <?php if($r->gender=='M') echo 'checked';?> > Male
                                                        </label>
														
                                                        
														<label class="radio-inline">
                                                           <input type="radio" name="gender" value="F" <?php if($r->gender=='F') echo 'checked';?>> Female 
                                                        </label>
                                                
												</div>
										</div>
										  <div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">Allocated Email ID <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                               <input type="text" id="uname" name="uname" value="<?php echo $r->allocated_email; ?>" placeholder="Enter Allocated Email Id" style="width:90%;" /><!-- <input type="text" name="emaildomain" value="<?php echo domain_name;?>" readonly="readonly" /> -->
                                            </div>
                                        </div>
                                  <div class="form-group">
                                        <label for="pan_no" class="col-sm-3 control-label">PAN Number</label>
										<div class="col-sm-7">
										 <input type="text" id="pan_no" name="pan_no" placeholder="Enter your PAN Number" style="width:90%" value="<?php if(isset($r->pan_no)) echo $r->pan_no; ?>">				  
                                              </div> 
									</div>
									<div class="form-group">
                                        <label for="email1" class="col-sm-3 control-label">Upload PAN Card <!-- <span style="color:#b81212">*</span> --></label>
										<div class="col-sm-4">
										 <input type="file" id="pan_image" name="pan_image" style="width:100%">				  
										 <i><font color="#B81212">(Upload jpg or jpeg or png file only and max size 60kb)</i></font> 
										 <div class="error col-lg-12" id="imgerrdiv" style="color:#b81212;margin-left: -14px;font-style:italic;"></div>
                                              </div>
                                <!--<div class="col-sm-3"><label><?php if(isset($r->pan_image_link)) {?><a href="<?php echo base_url().$r->pan_image_link;?>" target="_blank"><img src="<?php echo base_url().$r->pan_image_link;?>" alt="" height="100" width="100"></a><?php }?></label></div>-->
                                                    
                               <input type="hidden" name="expanimage" id="expanimage" value="<?php if(isset($r->pan_image_link)) echo $r->pan_image_link;?>" >
									</div>
                            <div class="form-group">
                                        <label for="account_no" class="col-sm-3 control-label">Account Number </label>
										<div class="col-sm-7">
										 <input type="text" id="account_no" name="account_no" placeholder="Enter your Account Number" style="width:90%" value="<?php if(isset($r->account_no)) echo $r->account_no; ?>">				  
                                              </div> 
									</div>
									
									<div class="form-group">
                                        <label for="pan_no" class="col-sm-3 control-label">Bank Name </label>
										<div class="col-sm-7">
										 <input type="text" id="bankname" name="bankname" placeholder="Enter Bank Name" style="width:90%" value="<?php if(isset($r->bankname)) echo $r->bankname; ?>">				  
                                              </div> 
									</div>
									<div class="form-group">
                                        <label for="pan_no" class="col-sm-3 control-label">Branch Name </label>
										<div class="col-sm-7">
										 <input type="text" id="branchname" name="branchname" placeholder="Enter Branch Name" style="width:90%" value="<?php if(isset($r->branchname)) echo $r->branchname; ?>">				  
                                              </div> 
									</div>
									<div class="form-group">
                                        <label for="pan_no" class="col-sm-3 control-label">IFSC Code </label>
										<div class="col-sm-7">
										 <input type="text" id="ifsccode" name="ifsccode" placeholder="Enter IFSC Code" style="width:90%" value="<?php if(isset($r->ifsccode)) echo $r->ifsccode; ?>">				  
                                              </div> 
									</div>  
									<div class="form-group">
                           	<label for="gender" class="col-sm-3 control-label">Purpose <span style="color:#b81212">*</span></label>
                                  <div class="col-sm-6">
                                  	
                                  		<?php foreach($purposelist as $list) {?>
                                  		
													<label class="checkbox-inline">
                                       	<input type="checkbox" name="purpose[]" value="<?php echo $list->id;?>" <?php foreach($purpose as $value) { if($list->id == $value->purpose) echo 'checked'; }?> > <?php echo $list->purpose_name;?>
                                       </label>       
													<?php  
													}?>
												</div>
										</div>
											
										<div class="form-group">
												<label for="enabled" class="col-sm-3 control-label">Enabled</label>
												<div class="col-sm-9">
													<input type="checkbox" id="status" name="status" <?php if($r->status == 1) echo "checked='checked'"; ?> />
												</div>
										</div>
                                    
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-6">
                                                <button type="submit" class="btn btn-default">Update</button>
												<input type="hidden" class="form-control" id="credentials_id" name="credentials_id" value="<?php echo $r->credentials_id; ?>" />
												<input type="hidden" class="form-control" id="guestuserId" name="guestuserId" value="<?php echo $r->guest_user_id; ?>" />
                                            </div>
                                        </div>
                                    </form><?php
									}?>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
<!-- /#wrapper -->

    <?php include("alljs.php"); ?>
	 <script src=" <?php echo base_url(); ?>assets/js/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo/advanced-form-demo.js"></script>
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
			$(function()
		{
		
		
		     $.validator.addMethod("regex", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			 $.validator.addMethod("email", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			 $.validator.addMethod("regeNUM", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
				var ALPHA_REGEX = "[a-zA-Z ]*$";
				var alphaNumeric = "^[(a-z)(A-Z)(0-9)]+$";	
				var email="^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$";
				
					$.validator.addMethod("panNum", function(value, element) {
		//alert(value);
				return this.optional(element) || /^[A-Z]{5}\d{4}[A-Z]{1}$/.test(value); 
		}, "Invalid Pan Number. Letters should be capital.");
			
			$.validator.addMethod("ifscCode", function(value, element) {
				return this.optional(element) || /^[A-Z|a-z]{4}[0][\d]{6}$/i.test(value);
		}, "Code must contain only letters, numbers, or dashes.");
		
			$("#sky-form").validate(
			{	

               
				// Rules for form validation
				rules:
				{
					
					prnno:
					{
					   regeNUM: alphaNumeric,
						required: true
					
					},
					fname:
					{
					   required:true
					   
					},
					/*mname:
				    {
					regex: ALPHA_REGEX
					//required: true
					
				    },
					lname:
				    {
					   regex: ALPHA_REGEX,
					   required: true
					 
					}, */
					contact:
				    {
					required: true,
					number: true,
				    }
					/*
					bname1:
					{
					  required:true,
					  min:1
					},
					
					uname:
					{
					  email:email, 
					  required: true
					},
					pan_no:{
						required: true,
						panNum: true
					},*/
					/*pan_image:{
						required: true,
					},*/
					
					/*account_no:{
						//required: true,
						number: true
					},	
					bankname:{
						//required: true,
						regex: ALPHA_REGEX
					},	
					branchname:{
						//required: true,
						regex: ALPHA_REGEX
					},
					ifsccode:{
						//required: true,
						ifscCode: true	
					},*/
					'purpose[]':
					{
						required: true					
					}
			
				},
									
				// Messages for form validation
				messages:
				{
				
				    prnno:
					{
					   regeNUM: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter alphanumeric charachters only</span>',
					   required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter prn number</span>'
						
					},
					fname:
					{
					    regex: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter alphabets only</span>',
						required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter first name</span>'
						
					},
					mname:
					{
					   regex: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter alphabets only</span>'
						//required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter middle name</span>'
						
			
					},
					
					lname:
					{
					   regex: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter alphabets only</span>',
						required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter last name</span>'
						
					},
					
					contact:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter contact number</span>',
						number: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter a valid number</span>'
						
						
					},
					bname1:
					{
					    required:'<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter password</span>',
						min:'<span style="color:rgb(184, 18, 18); margin-left:265px;">Please select Batch</span>'
					},
					
					uname:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter allocated email id</span>',
						email:'<span style="color:rgb(184, 18, 18); margin-left:265px;">Please Enter a valid Email-id</span>'
						
					},
					pan_no:
					{
						required: '<span style="color:#b81212; margin-left:265px;">Please enter pan number</span>',
						panNum: '<span style="color:#b81212; margin-left:265px;">Invalid pan number, Letters should be capital</span>'
					},
					pan_image:{
						required: '<span style="color:#b81212; margin-left:265px;">Please upload pan image</span>',
					},
					account_no:
					{
						//required: '<span style="color:#b81212; margin-left:265px;">Please enter account number</span>',
						number:'<span style="color:#b81212; margin-left:265px;">Please enter numbers only</span>'
					},
					bankname:
					{
						//required: '<span style="color:#b81212; margin-left:265px;">Please enter bank name</span>',
						regex: '<span style="color:#b81212; margin-left:265px;">Please enter alphabets only</span>'
					},
					branchname:{
						//required: '<span style="color:#b81212; margin-left:265px;">Please enter branch name</span>',
						regex: '<span style="color:#b81212; margin-left:265px;">Please enter alphabets only</span>'
					},
					ifsccode:{
						//required: '<span style="color:#b81212; margin-left:265px;">Please enter ifsc code</span>',
						ifscCode: '<span style="color:#b81212; margin-left:265px;">Please enter valid ifsc code(ABHY0065211)</span>'
					},
					'purpose[]':{
						//required: '<span style="color:#b81212; margin-left:265px;">Please enter ifsc code</span>',
						required: '<span style="color:#b81212;">Please select atleast one purpose</span>'
					},
					
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
				
				
			});
		});
	</script>
	<script>
	$("#pages5").addClass("in");
	$("#manageguestusers").addClass("active");
	$("#manageguestuser").addClass("active");
	</script>
</body>

</html>
