<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>



<body>
<style>
    .form-group .required .control-label:after {
  content:"*";color:red;
}
.required{
	color:red;
}
</style>
<div id="wrapper">

		<?php include("header.php"); ?>


        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Student Report
                                <small>Manage Student Report</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> 
								<a href="<?php echo base_url() ?>admin/">Dashboard</a></li>
								<li>  <a href="<?php echo base_url() ?>admin/assigned_students/<?php echo $stud_cred_id;?>"> Assigned Students</a>
                                </li>
                                <li class="active">Manage Student Report</li>
                                    
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
				
				<?php
				if($this->session->flashdata('successmessage'))
				{
					$successmessage = $this->session->flashdata('successmessage');
					echo "<script>swal('Ok', '$successmessage', 'success')</script>";
				}
				else if($this->session->flashdata('errormessage'))
				{
					$errormessage = $this->session->flashdata('errormessage');
					echo "<script>swal('Opps..!', '$errormessage', 'error')</script>";
				}
				?>
				
                <!-- end PAGE TITLE AREA -->
				<div class="row">

                    <!-- Basic Responsive Table -->
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Student Report</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive" id="reporttable">
								<?php
									//print_r($stud_id);
									//print_r($srinfo);
								?>
								<form class="form-horizontal" id="rating_form" role="form" method="post" action="<?php echo base_url();?>guestuser/studentreport_main">
									
									<input type="hidden" name="startdate" id="startdate" value=""/>
                                    <table class="table table-bordered">
										<thead>
                                            <tr>
                                                <th colspan="6">Work Ex Project / Engineering Project</th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            <tr>
                                                <th>Rating Scale</th>
                                                <th>Excellent</th>
                                                <th>Above Expectations</th>
                                                <th>Meets Expectations</th>
												<th>Below Expectations</th>
                                                <th>Poor</th>
                                            </tr>
                                        </thead>
										<thead>
                                            <tr>
                                                <th>Competencies</th>
                                                <th>5</th>
                                                <th>4</th>
                                                <th>3</th>
												<th>2</th>
                                                <th>1</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr id='anstr'>
                                                <td >Content / Project Knowledge <span class="required">*</span><span class="req"></span></td>
                                                <td><input type="radio" name="ep_cpk" value="5" <?php if(isset($srinfo->ep_cpk) && $srinfo->ep_cpk == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="ep_cpk" value="4" <?php if(isset($srinfo->ep_cpk) && $srinfo->ep_cpk == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="ep_cpk" value="3" <?php if(isset($srinfo->ep_cpk) && $srinfo->ep_cpk == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="ep_cpk" value="2" <?php if(isset($srinfo->ep_cpk) && $srinfo->ep_cpk == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="ep_cpk" value="1" <?php if(isset($srinfo->ep_cpk) && $srinfo->ep_cpk == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Company Knowledge <span class="required">*</span><span class="req"></span></td>
                                                <td><input type="radio" name="ep_ck" value="5" <?php if(isset($srinfo->ep_ck) && $srinfo->ep_ck == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="ep_ck" value="4" <?php if(isset($srinfo->ep_ck) && $srinfo->ep_ck == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="ep_ck" value="3" <?php if(isset($srinfo->ep_ck) && $srinfo->ep_ck == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="ep_ck" value="2" <?php if(isset($srinfo->ep_ck) && $srinfo->ep_ck == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="ep_ck" value="1" <?php if(isset($srinfo->ep_ck) && $srinfo->ep_ck == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Domain Knowledge <span class="required">*</span><span class="req"></span></td>
                                                <td><input type="radio" name="ep_dk" value="5" <?php if(isset($srinfo->ep_dk) && $srinfo->ep_dk == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="ep_dk" value="4" <?php if(isset($srinfo->ep_dk) && $srinfo->ep_dk == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="ep_dk" value="3" <?php if(isset($srinfo->ep_dk) && $srinfo->ep_dk == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="ep_dk" value="2" <?php if(isset($srinfo->ep_dk) && $srinfo->ep_dk == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="ep_dk" value="1" <?php if(isset($srinfo->ep_dk) && $srinfo->ep_dk == 1) echo "checked"; ?> /></td>
                                            </tr>
											<tr>
                                                <td>Overall Remarks <span class="required">*</span></td>
                                                <td colspan="5"><textarea name="ep_remarks" class="form-control" rows="2" placeholder="Enter Your Remarks" style="resize:vertical;"><?php if(isset($srinfo->ep_remarks)) echo $srinfo->ep_remarks; ?></textarea></td>
                                            </tr>
                                        </tbody>
                                    </table>
									<hr>
									<table class="table table-bordered">
										<thead>
                                            <tr>
                                                <th colspan="6">Summer Project</th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            <tr>
                                                <th>Rating Scale</th>
                                                <th>Excellent</th>
                                                <th>Above Expectations</th>
                                                <th>Meets Expectations</th>
												<th>Below Expectations</th>
                                                <th>Poor</th>
                                            </tr>
                                        </thead>
										<thead>
                                            <tr>
                                                <th>Competencies</th>
                                                <th>5</th>
                                                <th>4</th>
                                                <th>3</th>
												<th>2</th>
                                                <th>1</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Content / Project Knowledge <span class="required">*</span></td>
                                                <td><input type="radio" name="sp_cpk" value="5" <?php if(isset($srinfo->sp_cpk) && $srinfo->sp_cpk == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="sp_cpk" value="4" <?php if(isset($srinfo->sp_cpk) && $srinfo->sp_cpk == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="sp_cpk" value="3" <?php if(isset($srinfo->sp_cpk) && $srinfo->sp_cpk == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="sp_cpk" value="2" <?php if(isset($srinfo->sp_cpk) && $srinfo->sp_cpk == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="sp_cpk" value="1" <?php if(isset($srinfo->sp_cpk) && $srinfo->sp_cpk == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Company Knowledge <span class="required">*</span></td>
                                                <td><input type="radio" name="sp_ck" value="5" <?php if(isset($srinfo->sp_ck) && $srinfo->sp_ck == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="sp_ck" value="4" <?php if(isset($srinfo->sp_ck) && $srinfo->sp_ck == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="sp_ck" value="3" <?php if(isset($srinfo->sp_ck) && $srinfo->sp_ck == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="sp_ck" value="2" <?php if(isset($srinfo->sp_ck) && $srinfo->sp_ck == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="sp_ck" value="1" <?php if(isset($srinfo->sp_ck) && $srinfo->sp_ck == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Domain Knowledge <span class="required">*</span></td>
                                                <td><input type="radio" name="sp_dk" value="5" <?php if(isset($srinfo->sp_dk) && $srinfo->sp_dk == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="sp_dk" value="4" <?php if(isset($srinfo->sp_dk) && $srinfo->sp_dk == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="sp_dk" value="3" <?php if(isset($srinfo->sp_dk) && $srinfo->sp_dk == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="sp_dk" value="2" <?php if(isset($srinfo->sp_dk) && $srinfo->sp_dk == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="sp_dk" value="1" <?php if(isset($srinfo->sp_dk) && $srinfo->sp_dk == 1) echo "checked"; ?> /></td>
                                            </tr>
											<tr>
                                                <td>Overall Remarks <span class="required">*</span></td>
                                                <td colspan="5"><textarea name="sp_remarks" class="form-control" rows="2" placeholder="Enter Your Remarks" style="resize:vertical;"></textarea></td>
                                            </tr>
                                        </tbody>
                                    </table>
									<hr>
									<table class="table table-bordered">
										<thead>
                                            <tr>
                                                <th colspan="6">Major Competencies</th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            <tr>
                                                <th>Rating Scale</th>
                                                <th>Excellent</th>
                                                <th>Above Expectations</th>
                                                <th>Meets Expectations</th>
												<th>Below Expectations</th>
                                                <th>Poor</th>
                                            </tr>
                                        </thead>
										<thead>
                                            <tr>
                                                <th>Competencies</th>
                                                <th>5</th>
                                                <th>4</th>
                                                <th>3</th>
												<th>2</th>
                                                <th>1</th>
                                            </tr>
                                        </thead>
										<thead>
                                            <tr>
                                                <th colspan="6">Technical Proficiency</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Knowledge About Certifications <span class="required">*</span></td>
                                                <td><input type="radio" name="tp_certiknowledge" value="5" <?php if(isset($srinfo->tp_certiknowledge) && $srinfo->tp_certiknowledge == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="tp_certiknowledge" value="4" <?php if(isset($srinfo->tp_certiknowledge) && $srinfo->tp_certiknowledge == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="tp_certiknowledge" value="3" <?php if(isset($srinfo->tp_certiknowledge) && $srinfo->tp_certiknowledge == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="tp_certiknowledge" value="2" <?php if(isset($srinfo->tp_certiknowledge) && $srinfo->tp_certiknowledge == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="tp_certiknowledge" value="1" <?php if(isset($srinfo->tp_certiknowledge) && $srinfo->tp_certiknowledge == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Basic IT and Telecom Technologies Knowledge <span class="required">*</span></td>
                                                <td><input type="radio" name="tp_basicitteleknowledge" value="5" <?php if(isset($srinfo->tp_basicitteleknowledge) && $srinfo->tp_basicitteleknowledge == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="tp_basicitteleknowledge" value="4" <?php if(isset($srinfo->tp_basicitteleknowledge) && $srinfo->tp_basicitteleknowledge == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="tp_basicitteleknowledge" value="3" <?php if(isset($srinfo->tp_basicitteleknowledge) && $srinfo->tp_basicitteleknowledge == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="tp_basicitteleknowledge" value="2" <?php if(isset($srinfo->tp_basicitteleknowledge) && $srinfo->tp_basicitteleknowledge == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="tp_basicitteleknowledge" value="1" <?php if(isset($srinfo->tp_basicitteleknowledge) && $srinfo->tp_basicitteleknowledge == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Market Awareness / Industry Knowledge <span class="required">*</span></td>
                                                <td><input type="radio" name="tp_industryknowledge" value="5" <?php if(isset($srinfo->tp_industryknowledge) && $srinfo->tp_industryknowledge == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="tp_industryknowledge" value="4" <?php if(isset($srinfo->tp_industryknowledge) && $srinfo->tp_industryknowledge == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="tp_industryknowledge" value="3" <?php if(isset($srinfo->tp_industryknowledge) && $srinfo->tp_industryknowledge == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="tp_industryknowledge" value="2" <?php if(isset($srinfo->tp_industryknowledge) && $srinfo->tp_industryknowledge == 2) echo "checked"; ?> </td>
                                                <td><input type="radio" name="tp_industryknowledge" value="1" <?php if(isset($srinfo->tp_industryknowledge) && $srinfo->tp_industryknowledge == 1) echo "checked"; ?> /></td>
                                            </tr>
                                        </tbody>
										<thead>
                                            <tr>
                                                <th colspan="6">HR Assessment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Analytical / Logical Approach <span class="required">*</span></td>
                                                <td><input type="radio" name="hra_logicalapproach" value="5" <?php if(isset($srinfo->hra_logicalapproach) && $srinfo->hra_logicalapproach == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_logicalapproach" value="4" <?php if(isset($srinfo->hra_logicalapproach) && $srinfo->hra_logicalapproach == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_logicalapproach" value="3" <?php if(isset($srinfo->hra_logicalapproach) && $srinfo->hra_logicalapproach == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="hra_logicalapproach" value="2" <?php if(isset($srinfo->hra_logicalapproach) && $srinfo->hra_logicalapproach == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_logicalapproach" value="1" <?php if(isset($srinfo->hra_logicalapproach) && $srinfo->hra_logicalapproach == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Situation Based Performance <span class="required">*</span></td>
                                                <td><input type="radio" name="hra_situbasedperformance" value="5" <?php if(isset($srinfo->hra_situbasedperformance) && $srinfo->hra_situbasedperformance == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_situbasedperformance" value="4" <?php if(isset($srinfo->hra_situbasedperformance) && $srinfo->hra_situbasedperformance == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_situbasedperformance" value="3" <?php if(isset($srinfo->hra_situbasedperformance) && $srinfo->hra_situbasedperformance == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="hra_situbasedperformance" value="2" <?php if(isset($srinfo->hra_situbasedperformance) && $srinfo->hra_situbasedperformance == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_situbasedperformance" value="1" <?php if(isset($srinfo->hra_situbasedperformance) && $srinfo->hra_situbasedperformance == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Attitude(Sincerity, Honesty, Trainability, Values, Enthusiasm etc.) <span class="required">*</span></td>
                                                <td><input type="radio" name="hra_attitude" value="5" <?php if(isset($srinfo->hra_attitude) && $srinfo->hra_attitude == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_attitude" value="4" <?php if(isset($srinfo->hra_attitude) && $srinfo->hra_attitude == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_attitude" value="3" <?php if(isset($srinfo->hra_attitude) && $srinfo->hra_attitude == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="hra_attitude" value="2" <?php if(isset($srinfo->hra_attitude) && $srinfo->hra_attitude == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_attitude" value="1" <?php if(isset($srinfo->hra_attitude) && $srinfo->hra_attitude == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Communication Skills(Accent, Presentation of Content and Pace, Substantiating Answers) <span class="required">*</span></td>
                                                <td><input type="radio" name="hra_commskills" value="5" <?php if(isset($srinfo->hra_commskills) && $srinfo->hra_commskills == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_commskills" value="4" <?php if(isset($srinfo->hra_commskills) && $srinfo->hra_commskills == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_commskills" value="3" <?php if(isset($srinfo->hra_commskills) && $srinfo->hra_commskills == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="hra_commskills" value="2" <?php if(isset($srinfo->hra_commskills) && $srinfo->hra_commskills == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_commskills" value="1" <?php if(isset($srinfo->hra_commskills) && $srinfo->hra_commskills == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Self Confidence and Poise(Body Language) <span class="required">*</span></td>
                                                <td><input type="radio" name="hra_selfconfidence" value="5" <?php if(isset($srinfo->hra_selfconfidence) && $srinfo->hra_selfconfidence == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_selfconfidence" value="4" <?php if(isset($srinfo->hra_selfconfidence) && $srinfo->hra_selfconfidence == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_selfconfidence" value="3" <?php if(isset($srinfo->hra_selfconfidence) && $srinfo->hra_selfconfidence == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="hra_selfconfidence" value="2" <?php if(isset($srinfo->hra_selfconfidence) && $srinfo->hra_selfconfidence == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_selfconfidence" value="1" <?php if(isset($srinfo->hra_selfconfidence) && $srinfo->hra_selfconfidence == 1) echo "checked"; ?> /></td>
                                            </tr>
                                            <tr>
                                                <td>Presentation <span class="required">*</span></td>
                                                <td><input type="radio" name="hra_presentation" value="5" <?php if(isset($srinfo->hra_presentation) && $srinfo->hra_presentation == 5) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_presentation" value="4" <?php if(isset($srinfo->hra_presentation) && $srinfo->hra_presentation == 4) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_presentation" value="3" <?php if(isset($srinfo->hra_presentation) && $srinfo->hra_presentation == 3) echo "checked"; ?> /></td>
												<td><input type="radio" name="hra_presentation" value="2" <?php if(isset($srinfo->hra_presentation) && $srinfo->hra_presentation == 2) echo "checked"; ?> /></td>
                                                <td><input type="radio" name="hra_presentation" value="1" <?php if(isset($srinfo->hra_presentation) && $srinfo->hra_presentation == 1) echo "checked"; ?> /></td>
                                            </tr>
                                        </tbody>
                                    </table>
									<input type="hidden" name="guestuser_id" value="<?php echo $this->session->userdata('guest_user_id'); ?>" />
									<input type="hidden" name="stud_id" value="<?php echo $stud_id; ?>" />
									<?php if(!isset($srinfo->ppm_type)) { ?><button class="btn btn-default" type="submit" name="submit">Submit</button><?php } ?>
								</form>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->

                </div>
    </div>	
	</div>
	
    <!-- /#wrapper -->
    </div>
<?php include("alljs.php"); ?>
	 <script src=" <?php echo base_url(); ?>assets/js/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo/advanced-form-demo.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
			$("#rating_form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					ep_cpk:
					{
						required: true
					},
					ep_ck:
					{
						required: true
					},
					ep_dk:
					{
						required: true
					},
					ep_remarks:
					{
						required: true
					},
					sp_cpk:
					{
						required: true
					},
					sp_ck:
					{
						required: true
					},
					sp_dk:
					{
						required: true
					},
					sp_remarks:
					{
						required: true
					},
					tp_certiknowledge:
					{
						required: true
					},
					tp_basicitteleknowledge:
					{
						required: true
					},
					tp_industryknowledge:
					{
						required: true
					},
					hra_logicalapproach:
					{
						required: true
					},
					hra_situbasedperformance:
					{
						required: true
					},
					hra_attitude:
					{
						required: true
					},
					hra_commskills:
					{
						required: true
					},
					hra_selfconfidence:
					{
						required: true
					},
					hra_presentation:
					{
						required: true
					},
				},
									
				// Messages for form validation
				messages:
				{
					ep_cpk:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:15px;">Please give rating</span>'
					}
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					//error.insertAfter(element.parent('#anstr'));
					error.appendTo(element.parent(".req"));
					//error.appendTo(element.parents('#anstr').prev('tr'));
				}
				
				
			});
		});
	</script>
	<script>
		$("#assignedstudents").addClass("active");
		
		function openreport() {
			//alert('dd');
			$('#reporttable').show();
			$('#startdiv').hide();
			var currentdate = new Date($.now());
			//alert(currentdate);
			$('#startdate').val(currentdate);
		}
	</script>
</body>

</html>