<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>



<body>
<div id="wrapper">

		<?php include("header.php"); ?>



        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Change Password
                           <small>Change Password </small></h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>admin/"> Dashboard</a>
                                </li>
                                <li class="active">Change Password</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
				
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">
				    <div class='feedback'>
      						<?php if($this->session->flashdata('sucess'))
     								 echo "
      								<script>
     										 sweetAlert('Ok', '".$this->session->flashdata('sucess')."', 'success');
      								</script>";


      								if($this->session->flashdata('error')) {
      									echo "
      										<script>
      											sweetAlert('Opps..!', '".$this->session->flashdata('error')."', 'error');
      										</script>";

      								}
      							?>
					</div>


                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Change Password</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
								
											<form role="form" id="changePassword-info" method="post" action="<?php echo base_url();?>admin/changepass_main/<?php echo $this->session->userdata('credentials_id');?>" >
  
      <div class="form-group">
          <label>Old Password  <span style="color:#b81212">*</span> </label>
          <input type="password" class="form-control"  name="oldpwd" id="oldpwd" placeholder="Enter Old Password">
          <span id="soldpwd" style="color:rgb(184,18,18);"></span>
      </div>
      <div class="form-group">
          <label>New Password  <span style="color:#b81212">*</span> </label>
          <input type="password" class="form-control" name="new_pwd" id="new_pwd" placeholder="Enter New Password">
          <span id="snew_pwd" style="color:rgb(184,18,18);"></span>
      </div>
      <div class="form-group">
          <label>Re-Type New Password  <span style="color:#b81212">*</span> </label>
          <input type="password" class="form-control" name="new_c_pwd" id="new_c_pwd" placeholder="Confirm New Password">
          <span id="snew_c_pwd" style="color:rgb(184,18,18);"></span>
      </div>
      <button type="button" onclick="changePassword(<?php echo $this->session->userdata("credentials_id"); ?>);" class="btn btn-default">Update Password</button>
  </form>
	                          
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
	    <?php include("alljs.php"); ?>
<script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
	
	
	
	<script>
	
		/*function myFunction(cid) {
			//alert('ddd');
            var oldpwd =  $("#oldpwd").val()
                                   $.ajax({
            url: "<?php echo base_url();?>superadmin/checkOldPwd/",
            type: "POST",
            async: true, 
            data: {oldpwd:oldpwd,cid:cid}, //your form data to post goes here
             success: function(response){ 
             //alert(response);
                    if(response == 1)
                    {   
                        $("#soldpwd").html("");
                        $("#oldpwd").css({ "border": "","background": ""});

                        $("#new_pwd").val("");
                        $("#new_c_pwd").val("");

                        $("#new_pwd").attr("readonly", false);
                        $("#new_c_pwd").attr("readonly", false);                        

                    }else{
                    	
                        $("#new_pwd").val("");
                        $("#new_c_pwd").val("");

                        $("#soldpwd").html("Please enter correct old password");   
                        $("#oldpwd").css({ "border": "1px solid red","background": "#FFCECE"})        

                        $("#new_pwd").attr("readOnly", true);
                        $("#new_c_pwd").attr("readOnly", true);
                        }
        			}
   		 });
		}*/
		
		function changePassword(cid)
		{

			//alert(sid);
    /*var zpwd=0;
    var znpwd=0;
    var zncpwd=0;*/

    var info_pwd =  $("#oldpwd").val();
    var info_npwd =  $("#new_pwd").val();
    var info_ncpwd =  $("#new_c_pwd").val();
    //alert(info_npwd);
    
    if(info_pwd == null || info_pwd == "")
    {
                $("#soldpwd").html("Please enter old password");   
                //zpwd = 0;
                //$("#oldpwd").css({ "border": "1px solid red","background": "#FFCECE"});     

    }
    else if(info_npwd == null || info_npwd == "")
    {
                $("#snew_pwd").html("Please enter new password");
                //znpwd = 0;
                //$("#new_pwd").css({ "border": "1px solid red","background": "#FFCECE"});      

    }
    else if(info_ncpwd == null || info_ncpwd == "")
    {
                $("#snew_c_pwd").html("Please enter confirm password");
                //zncpwd = 0;
                //$("#new_c_pwd").css({ "border": "1px solid red","background": "#FFCECE"});      

    }
    else
    {
    	var oldpwd =  $("#oldpwd").val();
            $.ajax({
            url: "<?php echo base_url();?>superadmin/checkOldPwd/",
            type: "POST",
            async: true, 
            data: {oldpwd:oldpwd,cid:cid}, //your form data to post goes here
             success: function(response){
             			if(response == 1)
             			{
                    		$("#soldpwd").html("");   
        						//zpwd = 1;
        						//$("#oldpwd").css({ "border": "","background": ""});
        						if(info_npwd != info_ncpwd)
            				{
            					alert("New password and re-type password should match");
                    			$("#snew_c_pwd").html("New password and re-type password should match");
                    			//zncpwd = 0;
                    			//$("#new_c_pwd").css({ "border": "1px solid red","background": "#FFCECE"});
            				}
            				else
            				{
                    			$("#snew_c_pwd").html("");
                    			$("#snew_pwd").html("");
                    			$("#changePassword-info").submit();
                    			//zncpwd = 1;
                    			//$("#new_c_pwd").css({ "border": "","background": ""});
   	 						}
                    }
                    else
                    {
                    		alert("Please enter correct old password");
								$("#soldpwd").html("Please enter correct old password");   
                        //$("#oldpwd").css({ "border": "1px solid red","background": "#FFCECE"});
                    }
                    
        }
    });
    }
			
		}
		
		$("#changepassAdmin").addClass("active");
	</script>
    

</body>

</html>





