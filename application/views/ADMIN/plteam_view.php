<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
	<style>
	.toggledDropDown
	{
		display: none;
	}
	</style>
</head>

<body>
<div id="wrapper">

		<?php include("header.php"); ?>

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Update Placement Team
                                <small>Update Placement Team Information</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>admin/index">Dashboard</a>                                </li>
                                <li class="active">Manage Placement Team</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

				<?php
					if($this->session->flashdata('message'))
					{
						echo "<span class='help-block' style='color: #3c763d;'><i class='fa fa-check'></i>". $this->session->flashdata('message') ."</span>";
					}
				?>

                   <div class="row">
                   	              
                    <!-- /.col-lg-12 -->
             

                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                    <div class="col-lg-12" style="margin-bottom:250px;">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Update Placement Team</h4>
                                </div>
                              
                                <div class="clearfix"></div>
                            </div>
							<?php
							//print_r($plteamdata);
							foreach($plteamdata as $rplteam)
							{
							//echo $rplteam->StudentBatch;
							//echo $rplteam->user_type;
							?>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="sky-form" role="form" method="post" action="<?php echo base_url(); ?>admin/createteam_main">
                                       
                                        <div class="form-group">
                                            <label for="groupname" class="col-sm-3 control-label">Placement Group Name</label>
                                            <div class="col-sm-9">
												<?php
												//print_r($data);
												?>
                                                <select class="form-control" id="groupname" name="groupname">
												<?php
												foreach($data as $r)
												{
												?>
													<option value="<?php echo $r->group_id; ?>" <?php if($rplteam->group_id == $r->group_id) echo "selected=selected"; ?>><?php echo $r->group_name; ?></option><?php
												}?>
												</select>
                                            </div>
                                        </div>
										
										<div class="form-group">
                                            <label for="usertype" class="col-sm-3 control-label">User Type</label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="usertype" name="usertype">
													<option value="0">Select</option>
													<option value="1" <?php if($rplteam->user_type == 'Student') echo "selected=selected"; ?>>Student</option>
													<option value="2" <?php if($rplteam->user_type == 'Faculty') echo "selected=selected"; ?>>Faculty</option>
													<option value="3" <?php if($rplteam->user_type == 'GuestUser') echo "selected=selected"; ?>>Guest User</option>
												</select>
                                            </div>
                                        </div>
										
										<?php
											//print_r($studentbatchdata);
											//print_r($studentdata);
										?>
										<div class="form-group">
											<label for="Student" class="col-sm-3 control-label">&nbsp;</label>
											<!--<input type="text" class="form-control toggledDropDown" id="1" name="Student" autocomplete="off" placeholder="Enter Student PRN" required />-->
											<!--<input type="hidden" value="" id="prnexists" name="prnexists" class="form-control">
											<div id="prnmessage"></div>-->
											<div class="col-sm-9">
											<select id="1" name="StudentBatch" class="form-control <?php if($rplteam->user_type != 'Student') echo 'toggledDropDown'; ?>" onchange="studlist()">
											<option value="0">Select</option>
											<?php
												foreach($studentbatchdata as $studentbatch)
												{?>
													<option value="<?php echo $studentbatch->batch_course_id; ?>" <?php if($rplteam->StudentBatch == $studentbatch->batch_course_id) echo "selected=selected"; ?>><?php echo $studentbatch->in_year .'-'. $studentbatch->out_year.' '. $studentbatch->course_name; ?></option><?php
												}?>
											</select>
                                            </div>
										</div>
										
										<div class="form-group">
										<label for="StudentList" class="col-sm-3 control-label">&nbsp;</label>
										<div id="4" class="col-sm-12 table-responsive">
										<?php if($rplteam->user_type == 'Student')
										{?>
											<div class="col-lg-8">
												<div class="portlet portlet-default">
													<table id="example-table7" class="table table-striped table-bordered table-hover table-green">
														<thead>
															<tr>
																<th width="70px;">Sr. No.</th>
																<th width="70px;">Select</th>
																<th>Student Name</th>
															</tr>
														</thead>
														<tbody id="tbody">
															<?php
															 //echo $rplteam->StudentBatch;
															 $sq = mysql_query("select tbl_placement_team.credentials_id, tbl_student_information.* from tbl_placement_team inner join tbl_student_information on tbl_placement_team.credentials_id = tbl_student_information.credentials_id where StudentBatch = ".$rplteam->StudentBatch);
															 $rsq = mysql_fetch_array($sq);
															 //echo $rsq['credentials_id'];
															?>
															<tr class="odd gradeX">
																<td>1</td>
																<td><input type="checkbox" value="<?php echo $rsq['credentials_id']; ?>_<?php echo $rsq['first_name']; ?>" onclick="funforstud()" id="Student" name="Student[]"></td>
																<td><?php echo $rsq['first_name']; ?></td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="portlet portlet-green">
													<table id="example-table8" class="table table-striped table-bordered table-hover table-green" style="height:150px;">
														<thead>
															<tr>
																<th>Student Name</th>
															</tr>
														</thead>
														<tbody id="studentckappend">
														
														</tbody>
													</table>
												</div>
											</div><?php
											}?>
										</div>
										</div>
										
										<div class="form-group">
											<label for="Faculty" class="col-sm-3 control-label">&nbsp;</label>
											<div id="2" class="col-sm-12 table-responsive <?php if($rplteam->user_type != 'Faculty') echo 'toggledDropDown'; ?>">
											<div class="col-lg-8">
												<div class="portlet portlet-default">
													<table id="example-table3" class="table table-striped table-bordered table-hover table-green">
														<thead>
															<tr>
																<th width="70px;">Sr. No.</th>
																<th width="70px;">Select</th>
																<th>Faculty Name</th>
															</tr>
														</thead>
														<tbody>
														<?php
														$i=1;
														foreach($facultydata as $faculty)
														{
														?>
															<tr class="odd gradeX">
																<td><?php echo $i; ?></td>
																<td><input type="checkbox" value="<?php echo $faculty->credentials_id; ?>_<?php echo $faculty->first_name .' '. $faculty->last_name; ?>" id="Faculty" name="Faculty[]" <?php if($rplteam->credentials_id == $faculty->credentials_id) echo "checked='checked'"; ?> /></td>
																<td><?php echo $faculty->first_name .' '. $faculty->last_name; ?></td>
															</tr><?php
														$i++;
														}
														?>
														</tbody>
													</table>
													</div>
												<!-- /.portlet -->
											</div>
											<!-- /.col-lg-8 -->
											<div class="col-lg-4">
												<div class="portlet portlet-green">
													<table id="example-table4" class="table table-striped table-bordered table-hover table-green" style="height:150px;">
														<thead>
															<tr>
																<th>Faculty Name</th>
															</tr>
														</thead>
														<tbody id="facultyckappend">
															
														</tbody>
													</table>
												</div>
											</div>
											</div>
										</div>
										
										<?php
											//print_r($guestuser);
										?>
										
										<div class="form-group">
											<label for="GuestUser" class="col-sm-3 control-label">&nbsp;</label>
											<div id="3" class="col-sm-12 table-responsive <?php if($rplteam->user_type != 'GuestUser') echo 'toggledDropDown'; ?>">
											<div class="col-lg-8">
												<div class="portlet portlet-default">
													<table id="example-table5" class="table table-striped table-bordered table-hover table-green">
														<thead>
															<tr>
																<th width="70px;">Sr. No.</th>
																<th width="70px;">Select</th>
																<th>Guest User Name</th>
															</tr>
														</thead>
														<tbody>
														<?php
														$i=1;
														foreach($guestuser as $guestuser)
														{?>
															<tr class="odd gradeX">
																<td><?php echo $i; ?></td>
																<td><input type="checkbox" value="<?php echo $guestuser->credentials_id; ?>_<?php echo $guestuser->first_name .' '. $guestuser->last_name; ?>" id="GuestUser" name="GuestUser[]" <?php if($rplteam->credentials_id == $guestuser->credentials_id) echo "checked='checked'"; ?> /></td>
																<td><?php echo $guestuser->first_name .' '. $guestuser->last_name; ?></td>
															</tr><?php
														$i++;
														}
														?>
														</tbody>
													</table>
													</div>
												<!-- /.portlet -->
											</div>
											<!-- /.col-lg-8 -->
											<div class="col-lg-4">
												<div class="portlet portlet-green">
													<table id="example-table6" class="table table-striped table-bordered table-hover table-green" style="height:150px;">
														<thead>
															<tr>
																<th>Guest User Name</th>
															</tr>
														</thead>
														<tbody id="guestuserckappend">
															
														</tbody>
													</table>
												</div>
											</div>
											</div>
										</div>
										<div id="checkBoxErrorHolder"></div>
										
										<div class="form-group">
												<label for="enabled" class="col-sm-3 control-label">Enabled</label>
												<div class="col-sm-9">
													<input type="checkbox" id="enabled" name="enabled" <?php if($rplteam->status == 1) echo "checked='checked'"; ?> />
												</div>
										</div>
                                    
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-default">Create</button>
                                            </div>
											
                                        </div>
                                    </form><?php
									}?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->
					
					
					
					

                </div>
                <!-- /.row -->
				
				
				

                </div>
                <!-- /.row -->
				
            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
<!-- /#wrapper -->

    <?php include("alljs.php"); ?>
	
	<!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
	
	<script>
		
		function studlist()
		{
			//alert($("#1").val());
			var studvalue = $("#1").val();
			$.ajax({
        url: "<?php echo base_url();?>admin/fetchstudlist",
        type: "POST",
        async: true, 
		dataType:'json',
        data: { studvalue:studvalue}, //your form data to post goes here
         success: function(response){
					var json = response;
					$('#4').html('');
					$('#4').append('<div class="col-lg-8"><div class="portlet portlet-default"><table id="example-table7" class="table table-striped table-bordered table-hover table-green"><thead><tr><th width="70px;">Sr. No.</th><th width="70px;">Select</th><th>Student Name</th></tr></thead><tbody id="tbody">');
					
					var j = 1;
					for(var i = 0; i < json.length; i++) {
						var obj = json[i];
						
						//alert(obj.first_name);
						
						$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value="'+obj.credentials_id+'_'+obj.first_name+'" onclick="funforstud()" id="Student" name="Student[]"></td><td>'+obj.first_name+'</td></tr>');
						j++;
						//$('#4').append('<div>'+obj.first_name+'</div>');
					}
					
					$('#4').append('</tbody></table></div></div><div class="col-lg-4"><div class="portlet portlet-green"><table id="example-table8" class="table table-striped table-bordered table-hover table-green" style="height:150px;"><thead><tr><th>Student Name</th></tr></thead><tbody id="studentckappend"></tbody></table></div></div>');
					
			}
    });
		}
		
		$('#usertype').change(function() {
		//alert();
		// Hide all drop downs sharing the CSS class "toggledDropDown".
		$('#4').html('');
		$('.toggledDropDown').hide();
		
		// Build a selector for the selected drop down
		var selector = ('#' + $(this).val());
		//alert(selector);
		// Show the selected drop down
		$(selector).show();

	});
	</script>
	
	<!-- for checkbox -->
	<script type="text/javascript">
    $(document).ready(function() {
        $( "input[type=checkbox]" ).click(function(){
			//alert("hi");
            var favorite = [];
            $.each($("input[name='GuestUser[]']:checked"), function(){
				var GuestUservalue = $(this).val();
				var GuestUserlast = GuestUservalue.split("_").pop();
                favorite.push(GuestUserlast);
            });
            //alert(favorite.join(", "));
			$("#guestuserckappend").html('<tr><td>'+favorite.join("<br> ")+'</td></tr>');
        });
    });
	</script>
	
	<script type="text/javascript">
    $(document).ready(function() {
        $( "input[type=checkbox]" ).click(function(){
            var favorite = [];
            $.each($("input[name='Faculty[]']:checked"), function(){            
                var Facultyvalue = $(this).val();
				var Facultylast = Facultyvalue.split("_").pop();
                favorite.push(Facultylast);
            });
            //alert(favorite.join(", "));
			$("#facultyckappend").html('<tr><td>'+favorite.join("<br> ")+'</td></tr>');
        });
    });
	</script>
	
	<script type="text/javascript">
    //$(document).ready(function() {
	function funforstud() {
		//alert("hi");
        $( "input[type=checkbox]" ).click(function(){
		//alert();
            var favorite = [];
            $.each($("input[name='Student[]']:checked"), function(){
				var Studentvalue = $(this).val();
				var Studentlast = Studentvalue.split("_").pop();
                favorite.push(Studentlast);
            });
            //alert(favorite.join(", "));
			$("#studentckappend").html('<tr><td>'+favorite.join("<br> ")+'</td></tr>');
        });
		}
    //});
	</script>
	
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					"Student":
					{
						required: true
					},
					"usertype":
					{
						required: true,
						min:1
					},
					"StudentBatch":
					{
						required: true,
						min:1
					},
					"GuestUser[]":
					{
						required: true
					},
					"Faculty[]":
					{
						required: true
					},
					"Student[]":
					{
						required: true
					},
					"prnexists":
					{
						required: true
					}
				},
									
				// Messages for form validation
				messages:
				{
					"Student":
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:135px;">Please enter student PRN</span>'
					},
					"usertype":
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please select user type</span>',
						min: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please select user type</span>'
					},
					"StudentBatch":
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please select Student Batch</span>',
						min: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please select Student Batch</span>'
					},
					"GuestUser[]":
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:15px;">Please select guest user name</span>'
					},
					"Faculty[]":
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:15px;">Please select faculty name</span>'
					},
					"Student[]":
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:15px;">Please select student name</span>'
					},
					"prnexists":
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:135px;">Please enter valid student PRN</span>'
					}
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					//error.insertAfter(element.parent());
					if ($(element).attr("type") === "checkbox") {
						$("#checkBoxErrorHolder").html(error);
					} else {
						error.insertAfter(element.parent());
					}
					
				}
				
				
			});
		});
	</script>
	
</body>

</html>