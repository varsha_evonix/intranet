<?php
include ('header.php');
?>
        <div id="page-wrapper">
            <div class="page-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>
                                Student Management
                                <small>For Manage students</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>admin/index">Dashboard</a>                                </li>
                                <li class="active">student Management</li>
                            </ol>
                        </div>
                    </div>


    <div class="row">

                    <div class="col-lg-12">

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Student Management </h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <!--  notification message  -->
                                    <script>    
                                       setTimeout(function(){$('.feedback').fadeOut(2000);},2000);
                                    </script>
                                    <div class="feedback">

                                            <div class='alert alert-success alert-dismissable' class='message'>
                                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                                <strong>sucessfully updated</strong>
                                            </div>

                                            <div class='alert alert-error alert-dismissable' class='message'>
                                                <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                                <strong>Error !!!</strong>
                                            </div>

                                    </div>
                                          
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                 <th>PNR</th>
                                                <th>BATCH</th>
                                                <th>COURSE</th>
                                                <th>FIRST NAME</th>
                                                <th>STATUS</th>
                                                <th>PROFILE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                 foreach($student  as $r)
                                                {
                                                    $status = $r->status;
                                                    if($status == '1')
                                                    {
                                                        $temp = "Block";

                                                        $dis= "<span class='badge green'>Unblock</span>";

                                                    }
                                                    else
                                                    {
                                                        $temp = "Unblock";
                                                        $dis = "<span class='badge red'>Block</span>";

                                                    }

                                                    echo " <tr class='odd gradeX'>
                                                                     <td align='center'> 13030142066 </td ><td align='center'>"
                                                                     .$r->batch."</td><td align='center'>"
                                                                     . $r->course."</td><td align='center'>"
                                                                     . $r->f_name."</td><td align='center'>"
                                                                     ."<a href=".base_url()."welcome/changeStatus/".$r->uid."/all>$dis</td><td align='center'>"
                                                                     ."<a href=".base_url()."welcome/studentInfo/".$r->uid."><i class='fa fa-user fa-muted'></td>
                                                            </tr>";
                                                    }

                                                    ?>

                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    <!-- PAGE LEVEL CSS -->
    <link href="<?php echo base_url();?>assets/css/plugins/datatables/datatables.css" rel="stylesheet">



     <!-- HISRC Retina Images -->
    <script src="<?php echo base_url();?>assets/js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/datatables/datatables-bs3.js"></script>

    <!-- THEME SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/flex.js"></script>
    <script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>

</body>

</html>
