<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>

<body>
<div id="wrapper">

		<?php include("header.php"); ?>


        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Jobs
                           <small>Applications List </small></h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>admin/"> Dashboard</a>
                                </li>
                                <li class="active">Applications List</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
				
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">
				    <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>


                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Applications List</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="portlet-body">
                                    <?php if($jobInfo) {
                                      foreach ($jobInfo as $r) {

                                      ?>

                                      <div class="form-group col-sm-12">
                                          <label class="col-sm-3 control-label">Organization Name</label>
                                          <div class="col-sm-9">
                                          <input type="text" class="form-control" placeholder="Organozation name" value="<?php echo $r->organization_name; ?>" disabled="true">
                                          </div>
                                      </div>                        

                                      <div class="form-group col-sm-12">
                                          <label class="col-sm-3 control-label">Job Profile</label>
                                          <div class="col-sm-9">
                                          <input type="text" class="form-control" placeholder="Job Profile" value="<?php echo $r->offered_profile; ?>" disabled>
                                          </div>
                                      </div>
                                      <?php  }// foreach 
                                              }//if ?>

  	                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                              <th>PRN</th>
                                              <th><input type='checkbox' id='selectall'></th>
                                              <th>Student Name</th>
                                              <th>Batch</th>
                                              <th>Profile</th>                                              
                                        <th>Status</th>
                                              <th>Action</th>												
                                            </tr>
                                        </thead>

                                        <tbody>
                  										<tr>
                                    
                                      <?php
                                      $counter=1;
                                      foreach($info_1 as $c) 
                                      {
                                      ?>
                                          <td><?php echo $c->PRN;?></td>
                                          <td><input type="checkbox" class="case" value="<?php echo $c->student_id;?>"></td>
                                          <td><?php echo  $c->first_name;?>&nbsp;<?php echo  $c->middle_name;?>&nbsp;<?php echo  $c->last_name;?></td>
                                          <td><?php echo $c->batch_name;?>--<?php echo $c->course_name;?></td>
                                          <td>
                                        <a href="<?php echo base_url().'student/pdf/'.$c->student_id;?>" target="_blank"> <i class="fa fa-files-o"></i>&nbsp;View</a>

                                          </td>
                                          <td>
										 
                                                  <input type="hidden" name="sid" id="sid" value="<?php echo $c->student_id;?>" />
                                                  <input type="hidden" name="jobid" id="jobid" value="<?php echo $c->job_id;?>" />
                      												    <div class="col-sm-10">
                                                  <select class="form-control" name="spname" id="spname<?php echo $counter;?>" style="width:100%;">
                                                 
                                                  <?php		
                                                  foreach($info1 as $s)
                                                  { ?>
                                                   <option <?php if($s->selection_process==$c->status) 
													  echo "selected=selected"; ?>
                                                  value="<?php echo $s->selection_process;?>">
												  <?php echo $s->selection_process;?>
												  </option>
                                                  <?php
                                                  }
                                                  ?>
                                                            </select>
															</div>
                                          </td>
                                          <td>
                    												<button type="submit"  style=
																	"text-align:center" class="btn btn-green" onclick="saveselectionstatus(<?php echo $c->student_id;?>,<?=$counter;?>)">Save</button><?php $counter++; ?>
                  												</td>
                                          </form>
									 </tr>
                   <?php									  
                        } ?>


										
										
                                        </tbody>
                                    </table>
                                    <div><label>Status</label>
													<label> 
													<select class="form-control" name="mspname" id="mspname" style="width:100%; font-weight:normal">													
													<?php		
                                                  foreach($info1 as $s)
                                                  { ?>
                                                   <option 
                                                  value="<?php echo $s->selection_process;?>">
												  <?php echo $s->selection_process;?>
												  </option>
                                                  <?php
                                                  }
                                                  ?>
																</select>                                                   
                                                  </label>
                                                  <label><button type="submit"  style=
																	"text-align:center" class="btn btn-green" onclick="multistudentstatus()">Save Checked</button></label>
               <input type="hidden" name="checkstud_id[]" id="checkstud_id">                                           
                                    </div>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->
                      </div>
                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
	    <?php include("alljs.php"); ?>
<script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
	
	<script type="text/javascript">
		function saveselectionstatus(sid,counter)
		{
			//alert(counter);
			var spname=$('#spname'+counter).val();
			var sid = sid;
            		var jobid= $("#jobid").val();
            		//alert(spname);
            		//alert(sid);	
            		//alert(jobid);			
			$.ajax({
			url: '<?php echo base_url();?>admin/saveselectionstatus1',
			type: "POST",
			data: { spname:spname,sid:sid,jobid:jobid}, 
			async: true, 
			//your form data to post goes here
			 success: function(response){
						if(response.indexOf("true")> -1)
                 {
                    swal("Updated!", "Selection status has been updated successfully.", "success");                    
                   setTimeout(function(){location.reload();},1000);                   
                 }
                 else
                 {
                    swal("Oops..!", "Somthing going wrong. can't update status!!", "error"); 
                     setTimeout(function(){location.reload();},1000);                                                 
                 }

						
						
				}
			});
			
			
		}
		
		 $(document).ready(function() {

            $("#selectall").click(function () {
                var checkAll = $("#selectall").prop('checked');
                    if (checkAll) {
                        $(".case").prop("checked", true);
              				$('input:checkbox[class=case]').each(function() 
								{    
   			 					if($(this).is(':checked'))
   			 					var checkVall = $(this).val()+",";
   			 					$("#checkstud_id").append(checkVall);
   			 					//alert(checkVall);
     			 					
								});
                    } else {
                        $(".case").prop("checked", false);
                        $("#checkstud_id").text('')
                    }
                });

            $(".case").click(function(){
                if($(".case").length == $(".case:checked").length) {
                    $("#selectall").prop("checked", true);
                    $('input:checkbox[class=case]').each(function() 
								{    
   			 					if($(this).is(':checked'))
   			 					var checkVall = $(this).val()+",";
   			 					$("#checkstud_id").append(checkVall);
   			 					//alert(checkVall);
     			 					
								});
                    
                } else {
                	$("#checkstud_id").text('')
                    $("#selectall").prop("checked", false);
                    	$('input:checkbox[class=case]').each(function() 
								{    
   			 					if($(this).is(':checked'))
   			 					var checkVall = $(this).val()+",";
   			 					$("#checkstud_id").append(checkVall);
   			 					//alert(checkVall);
   			 					
								});
                }

            });	
        });
        
        function multistudentstatus() 
        {
        		var sid = $("#checkstud_id").text();
        		var selected_status = $("#mspname").val();
        		var jobid= $("#jobid").val();
        		//alert(sid);
        		//alert(selected_status);
        		//alert(jobid);
        		if(sid=="")
        		{
        			//alert("Please select record(s)");
        			swal("Oops..!", "Please select atlease one record", "error");
        			return false;
        		}
        		else
        		{
        		$.ajax({
					url: '<?php echo base_url();?>admin/multistudentstatus',
					type: "POST",
					data: { spname:selected_status,sid:sid,jobid:jobid}, 
					async: true, 
					//your form data to post goes here
			 		success: function(response){
			 			//alert(response);
						if(response.indexOf("true")> -1)
                 {
                    swal("Updated!", "Selection status has been updated successfully.", "success");                    
                   setTimeout(function(){window.location.reload(true);},1000);                   
                 }
                 else
                 {
                    swal("Oops..!", "Somthing going wrong. can't update status!!", "error"); 
                     setTimeout(function(){window.location.reload(true);},1000);                                                 
                 }
					}
				});
        	}	
        }
	</script>
	<script>
	$("#managejob").addClass("active");
	$("#pages4").addClass("in");
	$("#viewjobs").addClass("active");
	</script>
	</script>
    

</body>

</html>