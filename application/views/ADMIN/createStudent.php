<?php

include('header.php');
?>

<div id="wrapper">



        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Validation
                                <small>Form Notifications</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>admin/index">Dashboard</a>                                </li>
                                <li class="active">Validation</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->



                   <div class="row">
                   	              <div class="col-lg-6">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Create single student</h4>
                                </div>
                              
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form role="form" action="<?php echo base_url();?>admin/uploadUser" method="post" enctype="multipart/form-data">
                                       
                                       	 <div class="form-group">
                                            <label class="col-sm-2 control-label">Select File </label>
                                            <div class="col-sm-10">
                                            	 <input type="file" id="exampleInputFile" required>
                                                    <p class="help-block">Select your .csv file for insert student</p>
                                               
                                            </div>
                                        </div>

                                                <button type="submit" class="btn btn-default">IMPORT</button>
                                       </form>



                            </div>
                        </div>
                    </div> </div>
                    <!-- /.col-lg-12 -->
             

                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                    <div class="col-lg-6">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Create single student</h4>
                                </div>
                              
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="validate" role="form">
                                       
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">BATCH</label>
                                            <div class="col-sm-10">
                                                <select name="select" class="form-control" required>
                                                    <option value="">Select One:</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">COURSE</label>
                                            <div class="col-sm-10">
                                                <select name="select" class="form-control" required>
                                                    <option value="">Select One:</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">First Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="textArea" name="textArea" placeholder="Placeholder Text" required></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">Middle Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="textArea" name="textArea" placeholder="Placeholder Text" required></textarea>
                                            </div>
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">Last Name</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="textArea" name="textArea" placeholder="Placeholder Text" required></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="textArea" class="col-sm-2 control-label">primary email</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="textArea" name="textArea" placeholder="Placeholder Text" required></textarea>
                                            </div>
                                        </div>

                                    
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-default">Create</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>


    
    

        <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/plugins/validate/jquery.validate.min.js"></script>
    
    <!-- THEME SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/demo/validation-demo.js"></script>

</body>

</html>
