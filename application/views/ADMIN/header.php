<!-- begin TOP NAVIGATION -->
<nav class="navbar-top" role="navigation">

    <!-- begin BRAND HEADING -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
            <i class="fa fa-bars"></i> Menu
        </button>
        <div class="navbar-brand" style="color:white">
            SITM
        </div>
    </div>
    <!-- end BRAND HEADING -->

    <div class="nav-top">


        <!-- begin MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->
        <ul class="nav navbar-right">

            <!-- begin MESSAGES DROPDOWN -->
            <li class="dropdown">
                <!-- /.dropdown-menu -->
            </li>
            <!-- /.dropdown -->
            <!-- end MESSAGES DROPDOWN -->

            <!-- begin ALERTS DROPDOWN -->
            <li class="dropdown">
            </li>
            <!-- /.dropdown -->
            <!-- end ALERTS DROPDOWN -->

            <!-- begin TASKS DROPDOWN -->
            <li class="dropdown">
            </li>
            <!-- /.dropdown -->
            <!-- end TASKS DROPDOWN -->

            <!-- begin USER ACTIONS DROPDOWN -->
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <a href="#">
                            <i class="fa fa-user"></i> My Profile
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a class="logout_open" href="#logout">
                            <i class="fa fa-sign-out"></i> Logout
                        </a>
                    </li>
                </ul>
                <!-- /.dropdown-menu -->
            </li>
            <!-- /.dropdown -->
            <!-- end USER ACTIONS DROPDOWN -->

        </ul>
        <!-- /.nav -->
        <!-- end MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->

    </div>
    <!-- /.nav-top -->
</nav>
<!-- /.navbar-top -->
<!-- end TOP NAVIGATION -->

<!-- begin SIDE NAVIGATION -->
<?php
foreach ($info as $r) {
    ?>
    <nav class="navbar-side" role="navigation">
        <div class="navbar-collapse sidebar-collapse collapse">
            <ul id="side" class="nav navbar-nav side-nav">
                <!-- begin SIDE NAV USER PANEL -->
                <li class="side-user hidden-xs">
    <!--                         <img class="img-circle" src="<?php echo base_url(); ?>assets/img/profile-pic.jpg" alt="">

                    -->

                    <img class="img-circle" src="<?php
                    if ($r->profile_picture) {
                        echo base_url() . "" . $r->profile_picture;
                    } else {
                        echo base_url() . "assets/img/avtar.jpg";
                    }
                    ?>" alt="" 
                         style=" height: 160px; width: 155px;  margin-left: 15px;" >

                    <p class="welcome">
                        <i class="fa fa-key"></i> Logged in as
                    </p>
                    <p class="name tooltip-sidebar-logout">
                        Placement Admin <br/>
                        <span class="last-name"><?php echo ucfirst(strtolower($r->first_name)) . ' ' . ucfirst(strtolower($r->last_name)); ?></span> <a style="color: inherit" class="logout_open" href="#logout" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></a>
                    </p>
                    <div class="clearfix"></div>
                </li>
                </li>
                <!-- end SIDE NAV USER PANEL -->

                <!-- begin DASHBOARD LINK -->
                <li>
                    <a id="dashboard" href="<?php echo base_url(); ?>admin/index">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>


<!--                <li class="panel">
                    <a id="manageplacementgroup" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#pages2">
                        <i class="fa fa-users"></i> Manage Placement Groups <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="collapse nav" id="pages2">
                        <li>
                            <a id="creategroup" href="<?php echo base_url(); ?>admin/creategroup">
                                <i class="fa fa-angle-double-right"></i> Create Placement Group
                            </a>
                        </li>

                        <li>
                            <a id="managegroups" href="<?php echo base_url(); ?>admin/managegroups">
                                <i class="fa fa-angle-double-right"></i> Manage Placement Groups
                            </a>
                        </li>
                    </ul>
                </li>-->

<!--                <li class="panel">
                    <a id="manageplteam" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#pages3">
                        <i class="fa fa-sitemap"></i> Manage Placement Team <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="collapse nav" id="pages3">
                        <li>
                            <a id="createplteam" href="<?php echo base_url(); ?>admin/createteam">
                                <i class="fa fa-angle-double-right"></i> Create Placement Team
                            </a>
                        </li>

                        <li>
                            <a  href="<?php echo base_url(); ?>admin/manageplacementteam">
                                <i class="fa fa-angle-double-right"></i> Manage Placement Team
                            </a>
                        </li>

                         							<li>
                                                        <a id="assignauthority" href="<?php echo base_url(); ?>admin/assignauthority">
                                                            <i class="fa fa-angle-double-right"></i> Assign Authority
                                                        </a>
                                                    </li> 
                    </ul>
                </li>-->

<!--                <li class="panel">
                    <a id="manageguestusers" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#pages5">
                        <i class="fa fa-suitcase"></i> Manage Guest User<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="collapse nav" id="pages5">
                        <li>
                            <a id="createguestuser" href="<?php echo base_url(); ?>admin/createguestuser">
                                <i class="fa fa-angle-double-right"></i> Create Guest User
                            </a>
                        </li>
                        <li>
                            <a id="manageguestuser" href="<?php echo base_url(); ?>admin/manageguestuser">
                                <i class="fa fa-angle-double-right"></i> Manage Guest User
                            </a>
                        </li>
                    </ul>
                </li>-->

<!--                <li class="panel">
                    <a id="managejob" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#pages4">
                        <i class="fa fa-suitcase"></i> Manage Jobs<i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="collapse nav" id="pages4">
                                                     <li>
                                                        <a id="managejobs" href="<?php echo base_url(); ?>admin/managejobs">
                                                            <i class="fa fa-angle-double-right"></i>Manage Jobs
                                                        </a>
                                                    </li> 

                        <li>
                            <a id="addjob" href="<?php echo base_url(); ?>admin/addjob">
                                <i class="fa fa-angle-double-right"></i>Add Job
                            </a>
                        </li>

                        <li>
                            <a id="viewjobs" href="<?php echo base_url(); ?>admin/viewappliedjobs">
                                <i class="fa fa-angle-double-right"></i> View Applied Jobs
                            </a>
                        </li>
                    </ul>
                </li>-->
                <li class="panel">
                    <a id="managestudents" href="<?php echo base_url(); ?>admin/managestudents">
                        <i class="fa fa-group"></i> Manage Students
                    </a>
                </li>
<!--                <li class="panel">
                    <a id="allppmreport" href="<?php echo base_url(); ?>admin/allppmreport">
                        <i class="fa fa-files-o"></i> All PPM Report
                    </a>
                </li>-->
                <li class="panel">
                    <a id="changepassAdmin" href="<?php echo base_url(); ?>admin/changepassword">
                        <i class="fa fa-key"></i> Change Password
                    </a>
                </li>
            </ul>
            <!-- /.side-nav -->
        </div>
        <!-- /.navbar-collapse -->
    </nav>
<?php } ?>
<!-- /.navbar-side -->
<!-- end SIDE NAVIGATION -->