<?php
//echo $stud_id;
//print_r($studInfo);
?>
<?php
foreach($studInfo as $s)
{?>
<form id="sky-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>admin/submitplacementdetails">
                    <div class="form-group">
                    		<label for="prn" class="col-sm-3 control-label">Student PRN</label>
                        <div class="col-sm-9">
                        	<input type="text" class="form-control" id="prn" name="prn" placeholder="Enter Student PRN" required="" readonly value="<?php echo $s->PRN; ?>" />
                       	</div>
                    </div>
                    <div class="form-group">
                    		<label for="name" class="col-sm-3 control-label">Student Name</label>
                        <div class="col-sm-9">
                        	<input type="text" class="form-control" id="name" name="name" placeholder="Enter Student Name" required="" readonly value="<?php echo $s->first_name.' '.$s->middle_name.' '.$s->last_name; ?>" />
                       	</div>
                    </div>
                    <div class="form-group">
                    		<label for="name" class="col-sm-3 control-label">Job</label>
                        <div class="col-sm-9">
                        	<select class="form-control" id="job" name="job">
                        	<?php
                        	foreach($jobInfo as $j)
                        	{?>
                        		<option value="<?php echo $j->job_id; ?>" <?php if($s->job_id == $j->job_id) echo 'selected'; ?>><?php echo $j->organization_name.'-'.$j->offered_profile; ?></option><?php
                        	}?>
                        	</select>
                       	</div>
                    </div>
                    <div class="form-group">
                    		<label for="package" class="col-sm-3 control-label">Package Offer <span style="color:#b81212">*</span></label>
                        <div class="col-sm-9">
                        	<input type="text" class="form-control" id="package" name="package" placeholder="Enter Package" required="" value="<?php echo $s->package; ?>" />
                       	</div>
                    </div>
                    <div class="form-group">
                    		<label for="location" class="col-sm-3 control-label">Location <span style="color:#b81212">*</span></label>
                        <div class="col-sm-9">
                        	<input type="text" class="form-control" id="location" name="location" placeholder="Enter Location" required="" value="<?php echo $s->location; ?>" />
                       	</div>
                    </div>
                    <div class="form-group">
                    		<label for="joiningdate" class="col-sm-3 control-label">Joining Date</label>
                        <div class="col-sm-9">
                        	<input type="text" class="form-control" id="joiningdate" name="joiningdate" placeholder="Enter Joining Date" value="<?php echo $s->joiningdate; ?>" />
                       	</div>
                    </div>
                    <div style="margin-top: 33px;margin-left: 139px;">
                        <div class="col-sm-3"></div>
						
                        <button type="submit" class="btn-default" style="margin-left: 139px;">Submit</button>
                        <input type="hidden" value="<?php echo $s->student_id; ?>" id="student_id" name="student_id" />
                        <input type="hidden" value="<?php echo $bid; ?>" id="bid" name="bid" />
                    </div>
</form><?php
}?>

<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					package:
					{
						required: true,
						number: true,
						maxlength: 8
					},
					location:
					{
						required: true
					}
				},
									
				// Messages for form validation
				messages:
				{
					package:
					{
						required: '<span style="color:#b81212; margin-left:15px;">Please enter package</span>',
						number: '<span style="color:#b81212; margin-left:15px;">Please enter digits only</span>',
						maxlength: '<span style="color:#b81212; margin-left:15px;">Please enter max 8 digits</span>'
					},
					location:
					{
						required: '<span style="color:#b81212; margin-left:15px;">Please enter location</span>'
					}
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
				
				
			});
			});
	</script>