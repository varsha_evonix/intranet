<!DOCTYPE html>
<html lang="en">

<head>

     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Admin" >

    <title>Placement Portal</title>

    <!-- GLOBAL STYLES -->
    <link href="<?php echo base_url(); ?>assets/css/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href='<?php echo base_url(); ?>assets/http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/icons/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- PAGE LEVEL PLUGIN STYLES -->

    <!-- THEME STYLES -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins.css" rel="stylesheet">

    <!-- THEME DEMO STYLES -->
    <link href="<?php echo base_url(); ?>assets/css/demo.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    
    <style>
	.error{color:red;
	font-weight: normal;	
	}
	.errmsg{
		color: red;	
	}
	input.error{
		color: #000;
	}
</style>

</head>

<body class="login">

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-banner text-center">
                    <h1><i class="fa fa-gears"></i> Placement Portal</h1>
                </div>
                <div class="portlet portlet-green">
                    <div class="portlet-heading login-heading">
                    		<?php if($this->session->flashdata('succmsgpass')){?>
                    		<div class="alert alert-success alert-dismissible" role="alert">
                    			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    					<?php echo $this->session->flashdata('succmsgpass'); ?>
                    			</div>
                    			<?php } ?>
                    			<?php if($this->session->flashdata('errmsgpass')){?>
                    		<div class="alert alert-danger alert-dismissible" role="alert">
                    			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    					<?php echo $this->session->flashdata('errmsgpass'); ?>
                    			</div>
                    			<?php } ?>
                        <div class="portlet-title">
                            <h4><strong>Forgot Password?</strong>
                            </h4>
                        </div>
                        <div class="portlet-widgets">
                            <a href="<?php echo base_url();?>welcome/login" class="btn btn-white btn-xs" style="color:#333333;"> Sign In</a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="portlet-body">
                        <form accept-charset="UTF-8" role="form" action="<?php echo base_url();?>welcome/forgotpassword" id="forgotpassForm" method="post">
                            <fieldset>
                            		<div class="form-group">
                                    <select name="role" id="role" class="form-control">
														<option value="">--Select role--</option>														
														<option value="SUPER_ADMIN">Super Admin</option>				                          
														<option value="FACULTY">Faculty</option>
														<option value="STUDENT">Student</option>
														<option value="GUEST_USER">Guest User</option>          
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Enter allocated email id" name="email" type="text" maxlength="50">
                                    <?php echo form_error("email","<p class='error'>");?>
                                </div>
                               
                                <input type="submit" name="submit" value="Submit" class="btn btn-lg btn-green btn-block"/>
                               
                            </fieldset>
                            <br />
                            <p class="small" style="color: #34495e;">
                                This Portal is best viewed in IE10+,Firefox,Chrome,Opera,Safari
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="<?php echo base_url();?>assets/js/1.7.2.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap/bootstrap.min.js"></script>
    
    <!-- for validation -->
    <script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>

	<script type="text/javascript">
	
	$(document).ready(function(){
		$('#forgotpassForm').validate({
			rules:{
				email:{
					required: true,
					email:true
				},
				role:{
					required: true,				
				}
			},
			messages:{
				email:{ 
					required: '<span style="color:rgb(184, 18, 18); margin-left:0px;">Please enter allocated email id</span>',
					email: '<span style="color:rgb(184, 18, 18); margin-left:0px;">Please enter valid email id</span>'							
				},
				role:{ 
					required: '<span style="color:rgb(184, 18, 18); margin-left:0px;">Please select role</span>',
								
				}
			}
		});
	});
</script>
</body>

</html>
