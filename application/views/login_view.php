<!DOCTYPE html>
<html lang="en">
<head>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Madhuri" >

    <title>Intranet</title>

    <!-- GLOBAL STYLES -->
    <link href="<?php echo base_url(); ?>assets/css/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href='<?php echo base_url(); ?>assets/http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic' rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/icons/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- PAGE LEVEL PLUGIN STYLES -->

    <!-- THEME STYLES -->
    <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/css/plugins.css" rel="stylesheet">

    <!-- THEME DEMO STYLES -->
    <link href="<?php echo base_url(); ?>assets/css/demo.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>


    <div class="container">
        <div class="row">


            <div class="col-md-4 col-md-offset-4">
                <div class="login-banner text-center">
                    <h1><i class="fa fa-gears"></i>  Intranet </h1>
                </div>
                <div class="portlet portlet-green">
                    <div class="portlet-heading login-heading">



                        <div class="portlet-title">
                            <h4><strong>Verify Your self !!</strong>
                            </h4>
                        </div>


                        <div class="clearfix"></div>
                    </div>

                    <div class="portlet-body">

                        <?php 
                        if ($this->session->flashdata('error'))
                        {

                             echo "   <div class='alert alert-danger alert-dismissable'>
                                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                             <strong>Invalid credentials </strong><br/> Wrong userid or password !
                            </div>";
                        }
                        ?>

                        <form accept-charset="UTF-8"  id="sky-form" role="form" method="post" action="<?php echo base_url(); ?>welcome/login1/">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" data-provide="typeahead" placeholder="Enter userid" name="loginid"  id="loginid" type="text" required>
                                </div>
                                <div class="form-group">
                                    <input class="form-control"  data-provide="typeahead" placeholder="Enter password" name="password" id="password" type="password" required>
                                </div>
                             
                                <br>
                                <button type="submit" name="signIn" value="Sign In" class="btn btn-lg btn-green btn-block"  data-loading-text="Login...">Sign In</button>
                            </fieldset>
                            <br>
                             <p class="small">
                                <a href="<?php echo base_url()?>welcome/forgotpassword">Forgot your password?</a>
                             </p>
                             <p class="small" style="color: #34495e;">
                                This Portal is best viewed in IE10+,Firefox,Chrome,Opera,Safari
                             </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div id="confirm_model" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" style="margin-top: 100px; width: 600px;min-height: 400px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">RULES FOR STUDENTS REGARDING  PLACEMENTS: BATCH 2015-17</h4>
            </div>
        <div class="modal-body">
            <div id="form_assifnStudent" style="max-height: 400px; overflow-y: auto;">
					<ol>
	<li>
	<p><strong>Placement facility withdrawn from a student in the following cases:</strong></p>
	</li>
</ol>

<ul>
	<li>
	<p>If a student has more than 4 backlogs. Internal and External are counted as separate backlogs.</p>
	</li>
	<li>
	<p><a name="_GoBack"></a> If a student (who is not a member of any student committee) does not comply with minimum 90% overall attendance in each semester.</p>
	</li>
	<li>
	<p>If a student has got more than 3 warnings from the institute for disciplinary actions.</p>
	</li>
	<li>
	<p>If any student is found guilty of copying in internal as well as term end examination.</p>
	</li>
</ul>

<ol>
	<li>
	<p><strong>Rules related to Placement Process:</strong></p>
	</li>
</ol>

<ul>
	<li>
	<p><strong>Cap on the number of interviews</strong>: Any student appearing for the Placement Process would be eligible for 5 attempts to get placed. If the student is not placed in these 5 attempts, he/she will be given a preparation period of 15 days, during which time student will not be allowed to sign up for any company visiting the campus. After the duration of 15 days, he/she will be allowed to sign up for companies coming thereafter.</p>
	</li>
	<li>
	<p>Students who have signed up for any Company Process cannot withdraw their candidature at a later stage. They have to appear for the process failing which placement facility will be taken away.</p>
	</li>
	<li>
	<p>The student must accept the organization in which he or she is selected on the campus. Once selected, the concerned student cannot sit for further companies.</p>
	</li>
	<li>
	<p>The student is not allowed to enter into any negotiation with the company at the time of the interview. If the salary is given as a range, it means that the student is willing to work for the minimum salary on the offer.</p>
	</li>
	<li>
	<p>Any direct contact with any company, bypassing the Institute Placement Committee/ Chairperson Placements for personal or individual placements will result in permanent expulsion of the student from the Placement Process</p>
	</li>
	<li>
	<p>Attendance is compulsory for all Preplacement talks (PPT) conducted by any organization on campus. Even if a candidate is placed, he has to attend all subsequent PPT&rsquo;s conducted on campus.</p>
	</li>
	<li>
	<p>Students opting out of Final Placement need to produce a letter addressed to the Officer- Corporate Interface.</p>
	</li>
</ul>

<ol>
	<li>
	<p><strong>Rules related to CV:</strong></p>
	</li>
</ol>

<ul>
	<li>
	<p>The management and the Placement Committee will scrutinize all supporting documents regarding the entries in the brochure and the CV.</p>
	</li>
	<li>
	<p>The format of the CV will remain fixed and individual CVs will be printed for all the students.</p>
	</li>
	<li>
	<p>Soft copy of the CV format will be circulated and shared with the student who will have to fill it and submit within the stipulated time.</p>
	</li>
</ul>

<ol>
	<li>
	<h4>Work Experience</h4>
	</li>
</ol>

<ul>
	<li>
	<p><strong>Definition</strong>: Paid full time work experience after graduation or post-graduation. (Work-ex has to be proved before the Final Placements brochure is printed. All work-ex shown thereafter will not be entertained.)</p>
	</li>
	<li>
	<p>Supporting documents: Appointment letters, salary slips, work-ex certificates and relieving letters. All 3 have to be submitted. (Only full time work-ex will be mentioned in both the brochure and the CV)</p>
	</li>
</ul>

<ol>
	<li>
	<h4>Training/other projects undertaken:</h4>
	</li>
</ol>

<ul>
	<li>
	<p>Needs to be supported with relevant and supporting documents or certificates. This will be mentioned only in the CVs.</p>
	</li>
</ul>

<ol>
	<li>
	<h4>Other significant achievements:</h4>
	</li>
</ol>

<ul>
	<li>
	<p>Like NCC, sports or cultural activities etc needs to be supported with the related documents or certificates. This will be mentioned only in the CVs.</p>
	</li>
</ul>

<ol>
	<li>
	<h4>Additional qualifications:</h4>
	</li>
</ol>

<ul>
	<li>
	<p>Any additional qualifications of the student like CCNA/MCSE/MCSD/JCP/any other certifications etc</p>
	</li>
	<li>
	<p>If pursuing any of the above courses, the candidate must bring relevant certificates from the concerned institute along with the course completion date.</p>
	</li>
	<li>
	<p>Only completed courses will be entered in the CV and the brochure at the time of printing.</p>
	</li>
</ul>

<ol>
	<li>
	<h4>Any false information provided will result in expulsion of the student from the placement program.</h4>
	</li>
</ol>

<p><strong>9) </strong><strong>Placement Brochure:</strong></p>

<p><strong>The following will be the contents of the Placement brochure:</strong></p>

<ul>
	<li>
	<p>Photograph</p>
	</li>
	<li>
	<p>Name</p>
	</li>
	<li>
	<p>Age</p>
	</li>
	<li>
	<p>Educational Qualification</p>
	</li>
	<li>
	<p>Additional qualification</p>
	</li>
	<li>
	<p>Work Experience &ndash; Organization and Duration</p>
	</li>
	<li>
	<p>Summer Project &ndash; Organization and Title</p>
	</li>
	<li>
	<p>Research Project &ndash; Title and scope</p>
	</li>
	<li>
	<p>System Project- Title</p>
	</li>
	<li>
	<p>Marketing Project- Title</p>
	</li>
</ul>

<p><strong>10) </strong><strong>Placement Addresses / Meetings</strong></p>

<ul>
	<li>
	<p>Director, Chairperson Placements or Placement Committee may call for a Batch Address to discuss issues related to placements.</p>
	</li>
	<li>
	<p>Sometimes, in some urgent situations, it may be called at short notice.</p>
	</li>
	<li>
	<p>Any student who fails to attend such meetings/ addresses without a valid reason will be expelled from the placement process.</p>
	</li>
	<li>
	<p>Also a fine of Rs 500 will be levied for the above absence.</p>
	</li>
	<li>
	<p>All deadlines laid down by the institute or the placement committee on behalf of the institute should be complied to or strict action will be taken against such non-compliance which may result in fine or disciplinary letter/warning/memo by the Chairperson Placements.</p>
	</li>
	<li>
	<p>Non-completion of work assigned by the Placement committee on behalf of the institute within stipulated time will be a punishable offence. Management will take necessary disciplinary action.</p>
	</li>
</ul>

<p>Any student who violates the disciplinary norms will be debarred from the Placement Program at the discretion of the Director/Management, SITM.</p>


<p><strong>Any further additions and amendments to the rules will be intimated as and when required in the form of a circular by the management.</strong></p>

<p>Ms Bhakti Vyawahare</p>

<p>Officer &ndash; Corporate Interface</p>
                    <form id="sky-form-approval" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>welcome/approval">
					<fieldset>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="approval">I agree <span style="color:#b81212">*</span></label>
							<input type="checkbox" name="approval" id="approval" value="1">
						</div>					 
						<br>
						<button type="submit" class="btn-default" style="margin-left: 139px;">Submit</button>
                        <button class="btn-default" style="margin-left: 7px;" data-dismiss="modal">Cancel</button>
					</fieldset>
                </form>
            </div>            
        </div>
     </div>
    </div>
</div>

        <script src="<?php echo base_url();?>assets/js/1.7.2.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap/bootstrap.min.js"></script>
    
    <!-- for validation -->
    <script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
    <script type="text/javascript">
        $(function()
        {
            $("#sky-form").validate(
            {                   
                // Rules for form validation
                rules:
                {
                    
                    loginid:
                    {
                        required: true
                    },
                    password:
                    {
                        required: true
                    }
                },
                                    
                // Messages for form validation
                messages:
                {
                    loginid:
                    {
                        required: '<span style="color:rgb(184, 18, 18); margin-left:0px;">Please enter userid</span>'
                    },
                    password:
                    {
                        required: '<span style="color:rgb(184, 18, 18); margin-left:0px;">Please enter password</span>'
                    }
                    
                },                  
                
                // Do not change code below
                errorPlacement: function(error, element)
                {
                    error.insertBefore(element.parent());
                }
                
                
            });
        });
		
		var cnf = "<?php echo $cnf; ?>";
		if(cnf == "cnf")
		{
			$('#confirm_model').modal('show');
		}
    </script>
	
	<script type="text/javascript">
        $(function()
        {
            $("#sky-form-approval").validate(
            {                   
                // Rules for form validation
                rules:
                {
                    
                    approval:
                    {
                        required: true
                    }
                },
                                    
                // Messages for form validation
                messages:
                {
                    approval:
                    {
                        required: '<span style="color:rgb(184, 18, 18); margin-left:0px;">In order to use our portal, you must agree.</span><br />'
                    }
                    
                },                  
                
                // Do not change code below
                errorPlacement: function(error, element)
                {
                    error.insertAfter(element.parent());
                }
                
                
            });
        });
    </script>
</body>

</html>
