<?php 
        $mpdf = new mPDF('L',  // mode - default ''
                         'A4',    // format - A4, for example, default ''
                         15,     // font size - default 0
                         'Verdana',    // default font family
                         15,    // margin_left
                         15,    // margin right
                         50,     // margin top
                         30,    // margin bottom
                         9,     // margin header
                         9,     // margin footer
                         'L');  //
  


//       

$html .='<div style="margin-top:20px;"> </div>

<div style="width:100%;">

<div style="width:70%;">
            <table style=" font-size:9px; font-family: verdana;" border="0">';
                $i=1; 
                foreach($info as $r ){
                    $dob = $r->DOB;

            $html .='
            <tr>
                    <td ><b>Name:</b></td>
                    <td >'. ucfirst(strtolower($r->first_name)).' '.ucfirst(strtolower($r->middle_name)).' '.ucfirst(strtolower($r->last_name)).'</td>
            </tr>
            <tr>
                    <td ><b>D.O.B:</b></td>
                    <td >'. date("jS F,Y",strtotime($r->DOB)).'</td>
            </tr>

            <tr>
                    <td ><b>MBA 

            ';}


            if($specialization)
            {

                    $temp = 0;

                    foreach ($specialization as $r)
                    {
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {
                                $temp++;
                            }
                    }

                    if($temp > 0)
                    {
                        $html .='
                                Specialization:</b></td>
                                    <td >
                            ';  

                            $x = 0;
                        foreach ($specialization as $r) 
                        {
                             if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {
                                $x++;

                                 if($x == 1)
                                 {
                                     $html .=  $r->sp_name ;  
                                 }
                                 else if($x+1 == $temp)
                                 {
                                     $html .= ', '.$r->sp_name ;  
                                 }    
                                 else if($x == $temp)
                                 {
                                     $html .= ' & '.$r->sp_name ;  
                                 }
                                 else
                                 {
                                    $html .= ', '.$r->sp_name;  
                                 }

                            }
                        }     
                $html .=' </td>';  

                    }
                }

                foreach($info as $r ){
            $html .='
            </tr>  </table>
</div>
<div style="width:30%; float:right; margin-top:-10%; text-align:right;">
        <img src="'.base_url().''. $r->profile_picture.'" style="height:40%; width:40%;">
</div>

</div>

';

}
// objeatives section ------------------------------------------------
foreach($info as $r ){
    if($r->objactives)
    {
        $html .='  
            <div style="margin-top:30px; font-size:8px; font-family: verdana;"> 
                <strong> Objective</strong>
            </div>
            <hr  style="margin-top:0px; font-weight:bold;" />
            <div style="margin-top:-25px; font-size:8px; font-family: verdana;">'.$r->objactives.'</div>';  
    }
}
?>


<!-- =============================================== Starting of Academic Qualification  ==================================================== -->

<?php
if($education)
{
        $temp = 0;
        foreach ($education as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }
        }

        if($temp > 0)
        {
         $html .='
                <div style="margin-top:20px; font-family:verdana; font-size:8px;"> 
                    <strong>Academic Qualification</strong>
                </div>
					<hr style="margin-top:0px; border-color:black;"/>
                <table border="1" style="font-size:8px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:center;margin-top:-10px;"> 
                <thead>
                     <tr style="background:#C7C6C6">
                        <th>#</th>
                        <th>Qualification</th>                                                
                        <th>Degree</th>
                        <th>Year</th>
                        <th> Board/University</th>
                        <th>Percentage</th>
                        <th>Division</th>                                                
                    </tr>
               </thead>
               <tbody>
                   ';
                    $counter=0; 
                    foreach ($education as $r)
                    {
                          // degree name
                          if($r->degree_name == 1){  $quali_name = "X"; } else if($r->degree_name == 2){  $quali_name = "XII"; } else if($r->degree_name == 3){  $quali_name = "Graduation"; } else if($r->degree_name == 4){  $quali_name = "Post-Graduation"; } else{  $quali_name = ""; }
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {
                                $counter++;


                                $html .='<tr> <td>'.$counter
                                        .'</td><td>'.$quali_name
                                        .' </td><td>'.$r->edcutation_name
                                        .' </td><td>'.$r->passing_year
                                        .' </td><td>'.$r->university_board_name
                                        .' </td><td>'.$r->percentage_cgpa
                                        .' </td><td>'.$r->division.'</td></tr>';
                            }
                    }
        }
}
$html .='</table>';
?>
<!-- =============================================== Ending of Academic Qualification  ==================================================== -->



<!-- =============================================== Starting of Professional Experience  ==================================================== -->

<?php

if($professional)
{
              $temp = 0;
        foreach ($professional as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }
        }
        if($temp > 0)
        {
 
                // caluculate total number of experience
                $totalExp=0;
                    foreach ($professional as $r)
                     { 
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                        {
                          $datetime1 = date_create($r->date_from);
                          $datetime2 = date_create($r->date_to);
                          $interval = date_diff($datetime1, $datetime2);
                          $exp=  $interval->m + ($interval->y * 12);
                          $totalExp += $exp;
                      }
                    }


        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%; "> 
                <tr>
                    <td><strong> Professional Experience </strong></td>
                    <td style="text-align:right;">  ( '.$totalExp.' months )</td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>';

                    $counter = 0;

                    foreach ($professional as $r)
                     { 
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                        {
                            $counter++;
                            $datetime1 = date_create($r->date_from);
                            $datetime2 = date_create($r->date_to);
                            $interval = date_diff($datetime1, $datetime2);
                            $exp=  $interval->m + ($interval->y * 12);
                            $totalExp += $exp;

                            if($counter == 1)
                            {

                                $html .='
                                <table border="1" style="font-size:12px; margin-top:-12px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left; "> 
 ';

                            }
                            else
                            {
                                $html .='

                            <table border="1" style="font-size:12px; margin-top:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;"> ';

                            }

                            $html .='
                           <tbody>
                                    <tr><td style="width: 150px; background-color: #C7C6C6;"><strong>Organization </strong></td><td colspan="3" style="width: 355px;">'.$r->company_name.'</td><td colspan="2" style="width: 111px;">'.$exp. ' month ('.date("M Y",strtotime($r->date_from)).' - '.date("M Y",strtotime($r->date_to)).')'. '</td></tr>
                                    <tr><td style="width: 150px; background-color: #C7C6C6;"><strong>Designation </strong></td><td colspan="2" style="width: 300px;">'.$r->job_profile.'</td><td style=" width:15%!important; background-color: #C7C6C6;"><strong>Business Unit </strong></td><td colspan="2" style="width: 200px;">'.$r->business_unit.'</td></tr>
                                    <tr><td style="width: 150px; background-color: #C7C6C6;"><strong>Responsibilities </strong></td><td colspan="5" style="width:172px;">'.$r->responsibilities.'</td></tr>
                            </tbody>
                            </table>
                           ';
                        }//if
                    }//foreach
        }
    }
?>
<!-- =============================================== Ending of Professional Experience  ==================================================== -->





<!-- =============================================== Starting of summer project information  ==================================================== -->

<?php

if($projects)
{
        $tempsummerProject = 0;
        $totalSummerExp=0;
        $totalExp=0;

        foreach ($projects as $r)
        {
                if(($r->project_type =="Summer Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                    {
                        $tempsummerProject++;
                          $datetime1 = date_create($r->start_date);
                          $datetime2 = date_create($r->end_date);
                          $interval = date_diff($datetime1, $datetime2);
                          $exp=  $interval->m + ($interval->y * 12);
                          $totalExp += $exp;

                    }
        }

        if($tempsummerProject > 0)
        {
    

        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%;"> 
                <tr>
                    <td><strong> Summer Project </strong></td>
                    <td style="text-align:right;">  ( '.$totalExp.' months )</td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>';

                    $counter = 0;

                    foreach ($projects as $r)
                     { 
                        if(($r->project_type =="Summer Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                        {
                            $counter++;
                            $datetime1 = date_create($r->start_date);
                            $datetime2 = date_create($r->end_date);
                            $interval = date_diff($datetime1, $datetime2);
                            $exp=  $interval->m + ($interval->y * 12);

                            if($counter == 1)
                            {

                                $html .='
                            <table border="1" style=" margin-top:-12px; font-size:8px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;"> ';

                            }
                            else
                            {
                                $html .='

                            <table border="1" style="font-size:8px; margin-top:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;"> ';

                            }

                            $html .='
                           <tbody>
                                    <tr><td style="width: 150px; background-color: #C7C6C6;"><strong>Organization </strong></td><td colspan="3" style="width: 355px;">'.$r->organization_name.'</td><td colspan="2" style="width: 111px;">'.$exp. ' month ('.date("M Y",strtotime($r->start_date)).' - '.date("M Y",strtotime($r->end_date)).')'. '</td></tr>
                                    <tr><td style="width: 150px; background-color: #C7C6C6;"><strong>Project Title </strong></td><td colspan="5" style="width:172px;">'.$r->project_name.'</td></tr>
                                        <tr><td style="width: 150px; background-color: #C7C6C6;"><strong>Key Deliverables & Learning </strong></td><td colspan="5" style="width:172px;">'.$r->project_description.'</td></tr>
                            </tbody>
                            </table>
                           ';
                        }//if
                    }//foreach
        }
    }
?>
<!-- =============================================== Ending of summer project information  ==================================================== -->

<!-- =============================================== Starting of Research project information  ==================================================== -->

<?php

if($projects)
{
        $tempResearchProject = 0;

        foreach ($projects as $r)
        {
                if(($r->project_type =="Research Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                    {
                        $tempResearchProject++;
                    }
        }

        if($tempResearchProject > 0)
        {
    

        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%;"> 
                <tr>
                    <td><strong> Research Project </strong></td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>';

                    $counter = 0;

                    foreach ($projects as $r)
                     { 
                        if(($r->project_type =="Research Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                        {
                            $counter++;

                            if($counter == 1)
                            {

                                $html .='
                            <table border="1" style="  margin-top:-12px; font-size:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;"> ';

                            }
                            else
                            {
                                $html .='

                            <table border="1" style="font-size:10px; margin-top:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;"> ';

                            }

                            $html .='
                           <tbody>
                                    <tr><td style="width: 150px; background-color: #C7C6C6;"><strong>Project Title </strong></td><td colspan="5" style="width:172px;">'.$r->project_name.'</td></tr>
                                        <tr><td style="width: 150px; background-color: #C7C6C6;" align="center"><strong>Key Deliverables & Learning </strong></td><td colspan="5" style="width:172px;">'.$r->project_description.'</td></tr>
                            </tbody>
                            </table>
                           ';
                        }//if
                    }//foreach
        }
    }
?>
<!-- =============================================== Ending of Research project information  ==================================================== -->


<!-- =============================================== Starting of Research project information  ==================================================== -->

<?php

if($additionalCourse)
{
        $ $temp = 0;
        foreach ($additionalCourse as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }

        }
        if($temp > 0)
        {
    

        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%;"> 
                <tr>
                    <td><strong> Additional Courses</strong></td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>
                    <table border="0" style="  margin-top:-12px; font-size:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;">                     
                     <tbody>';

                    foreach ($additionalCourse as $r)
                    {
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {

                                $html .='<tr> <td><ul><li>'.$r->course_name.' in '.$r->domain.'</li></ul> </td><tr>';
                            }

                    }
                    $html .= '</tbody></table>';
        }
    }
?>
<!-- =============================================== Ending of Research project information  ==================================================== -->

<!-- =============================================== Starting of Assignments project information  ==================================================== -->

<?php

if($projects)
{
        $ $temp = 0;
        foreach ($projects as $r)
        {
            if(($r->project_type == "Assignment Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }

        }
        if($temp > 0)
        {
    

        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%;"> 
                <tr>
                    <td><strong> Assignments </strong></td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>
                    <table border="0" style="  margin-top:-12px; font-size:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;">                     
                     <tbody>';

                    foreach ($projects as $r)
                    {

                        if(($r->project_type == "Assignment Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {
                                $html .='<tr> <td colspan="2"><ul><li>'.$r->project_name.' in '.$r->domain.'</li></ul> </td></tr>';
                            }

                        if(($r->project_description)&&($r->project_type == "Assignment Project"))
                        {
                                $html .='<tr><td align="right" width:"100px;"> <strong>Description </strong> </td><td>'.$r->project_description.'</td></tr>';
                        }
                    }
                    $html .= '</tbody></table>';
        }
    }
?>
<!-- =============================================== Ending of Assignments project information  ==================================================== -->


<!-- =============================================== Starting of Extra Projects Undertaken  ==================================================== -->

<?php

if($projects)
{
        $ $temp = 0;
        foreach ($projects as $r)
        {
            if(($r->project_type == "Extra Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }

        }
        if($temp > 0)
        {
    

        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%;"> 
                <tr>
                    <td><strong> Projects Undertaken </strong></td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>
                    <table border="0" style="  margin-top:-12px; font-size:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;">                     
                     <tbody>';

                    foreach ($projects as $r)
                    {
                        if(($r->project_type == "Extra Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {

                                $html .='<tr> <td colspan="2"><ul><li>'.$r->project_name.' in '.$r->domain.'</li></ul> </td></tr>';
                            }

                        if(($r->project_description)&&($r->project_type == "Extra Project"))
                        {
                                $html .='<tr><td align="right"; width="20%";> <strong>Description </strong> </td><td>'.$r->project_description.'</td></tr>';
                        }

                    }
                    $html .= '</tbody></table>';
        }
    }
?>
<!-- =============================================== Ending of Extra Projects Undertaken  ==================================================== -->


<!-- =============================================== Starting of Skills and Expertise ==================================================== -->

<?php

if($skills)
{
        $ $temp = 0;
        foreach ($skills as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }

        }
        if($temp > 0)
        {
    

        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%;"> 
                <tr>
                    <td><strong> Skills and Expertise </strong></td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>
                    <table border="0" style=" margin-top:-12px; font-size:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;">                     
                     <tbody>';

                    foreach ($skills as $r)
                    {
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {

                                $html .='<tr> <td><ul><li>'.$r->skill_name.'</li></ul> </td><tr>';
                            }

                    }
                    $html .= '</tbody></table>';
        }
    }
?>
<!-- =============================================== Ending of Skills and Expertise ==================================================== -->



<!-- =============================================== Starting of Positions of Responsibility Held  ==================================================== -->

<?php

if($resposiblity)
{
        $ $temp = 0;
        foreach ($resposiblity as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }

        }
        if($temp > 0)
        {
    

        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%;"> 
                <tr>
                    <td><strong> Positions of Responsibility Held</strong></td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>
                    <table border="0" style=" margin-top:-12px; font-size:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;">                     
                     <tbody>';

                    foreach ($resposiblity as $r)
                    {
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {

                                $html .='<tr> <td colspan="2"><ul><li>'.$r->p_r_name.' in '.$r->edcutation_name. ' - '.$r->university_board_name.'</li></ul> </td><tr>';
                            }
                        if(($r->extra_info))
                        {
                                $html .='<tr><td align="right"; width="20%";> <strong>Description </strong> </td><td>'.$r->extra_info.'</td></tr>';
                        }                            


                    }
                    $html .= '</tbody></table>';
        }
    }
?>
<!-- =============================================== Ending of Positions of Responsibility Held ==================================================== -->

<!-- =============================================== Starting of Special Achievements / Awards ==================================================== -->

<?php

if($awards)
{
        $ $temp = 0;
        foreach ($projects as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }

        }
        if($temp > 0)
        {
    

        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%;"> 
                <tr>
                    <td><strong> Awards or Achievements </strong></td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>
                    <table border="0" style=" margin-top:-12px; font-size:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;">                     
                     <tbody>';

                    foreach ($awards as $r)
                    {
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {

                                $html .='<tr> <td colspan="2"><ul><li>'.$r->achivement_title.'</li></ul> </td><tr>';
                            }
                        if($r->achivement_description)
                        {
                                $html .='<tr><td align="right"; width="20%";> <strong>Description </strong> </td><td>'.$r->achivement_description.'</td></tr>';
                        }                                                        


                    }
                    $html .= '</tbody></table>';
        }
    }
?>
<!-- =============================================== Ending of Special Achievements / Awards ==================================================== -->



<!-- =============================================== Starting of Extra Curricular Activities ==================================================== -->

<?php

if($extraActivity)
{
        $ $temp = 0;
        foreach ($extraActivity as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }

        }
        if($temp > 0)
        {
    

        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%;"> 
                <tr>
                    <td><strong> Extra Curricular Activities </strong></td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>
                    <table border="0" style=" margin-top:-12px; font-size:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;">                     
                     <tbody>';

                    foreach ($extraActivity as $r)
                    {
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {

                                $html .='<tr> <td><ul><li>'.$r->activity_name.'</li></ul> </td><tr>';
                            }

                    }
                    $html .= '</tbody></table>';
        }
    }
?>
<!-- =============================================== Ending of Extra Curricular Activities ==================================================== -->





<!-- =============================================== Starting of Hobbies / Interests ==================================================== -->

<?php

if($hobbies)
{
        $ $temp = 0;
        foreach ($hobbies as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }

        }
        if($temp > 0)
        {
    

        $html .='
                <table style="margin-top:20px; font-family:verdana; font-size:8px; width:100%;"> 
                <tr>
                    <td><strong> Hobbies / Interests </strong></td>
                </tr>
                </table>
                    <hr style="margin-top:-2px; border-color:black;"/>
                    <table border="0" style=" margin-top:-12px; font-size:10px; color:#333333; width:100%; border-collapse: collapse; border-color: #000000; text-align:left;">                     
                     <tbody>';

                    foreach ($hobbies as $r)
                    {
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                            {

                                $html .='<tr> <td><ul><li>'.$r->hobby_name.'</li></ul> </td><tr>';
                            }

                    }
                    $html .= '</tbody></table>';
        }
    }
?>
<!-- =============================================== Ending of Hobbies / Interests ==================================================== -->

















<?php


//  starting of header  & footer.......................................................

$header = '<table style=" width:100%; border-collapse: inherit;" border="0">
                <tr>
                    <td rowspan="5" style="text-align:left"><img src="'.base_url().'assets/siu.png" height:"10%" width="15%" alt="collage_heading"/></td>
                    <td colspan="4" style="text-align:center; font-size:30px; font-family:Arial Black; color:#112869;">Symbiosis Institute of Telecom </td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align:center; font-size:30px; font-family:Arial Black; color:#112869;">Management </td>
                </tr>
                 <tr>
                    <td colspan="4" padding:50px; style="text-align:center; font-size:10px; color:#ff0000;">
                        (Constituent of Symbiosis International University (SIU) established under section-3 of the UGC act 1956 vide notification No. F9-12/2001 – U.3 of the Government of India)</td>
                </tr>    
                <tr>
                    <td colspan="4" style="text-align:center; font-size:10px; font-family:Times New Roman; color:#ff0000;"><strong>Accredited by NAAC with ‘A’ grade </strong></td>
                </tr>     
</table><hr style="margin-top:0px;" />';


$footer = '
            <table width="100%" style="border-top: 0.1mm solid #000000; text-align:center;  font-size:8px; color:#112869;">
            <tr style="margin-top:0px"> 
                    <td > 
                        Reach us at: Placements and Corporate Interface Team, Symbiosis Institute of Telecom Management,
                    </td>
            </tr>
            <tr style="margin-top:0px"> 
                    <td > 
                        Symbiosis Knowledge Village,
                    </td>                    
            </tr>
            <tr>            
                    <td > 
                        Village Lavale, Mulshi Tahasil, Pune-412115
                    </td>                                        
            </tr>
            <tr>
                    <td > 
                        Tel: +91-20-39116194, 39116195;    Mobile: +91-9922443009;    Telefax: +91-20-39116181
                    </td>
            <tr>                    
                    <td > 
                        Email: recruit@sitm.ac.in,              Website: www.sitm.ac.in                    
                    </td>                    
            </tr>
            </table>
        ';
// <table width="100%" style="border-top: 0.1mm solid #000000; vertical-align: top; font-family: sans; font-size: 9pt; color: #000055;"><tr>
// <td width="50%"></td>
// <td width="50%" align="right">See <a href="http://mpdf1.com/manual/index.php">documentation manual</a> for further details</td>
// </tr></table>';


//  ending of header  & footer .......................................................
$mpdf->SetHTMLHeader($header);
$mpdf->SetHTMLFooter($footer);
     
    // Numeric Entities
    

     $mpdf->WriteHTML($html);
     $mpdf->Output('Resume.pdf','D');
     
    
    ?>

