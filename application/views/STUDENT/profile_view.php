<?php
// Report all errors except E_NOTICE
error_reporting(E_ALL & ~E_NOTICE);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("alllinks.php"); ?>
    
    <link href="<?php echo base_url(); ?>assets/css/plugins/bootstrap-datepicker/datepicker3.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet">

<script type="text/javascript">
    function functionShowExpDetails(id){
        $("#Expdetails_"+id).modal('show'); 
        
    }


    function functionShowSummerProjectDetails(id){
        $("#summerProjectdetails_"+id).modal('show'); 
        
    }

	function functionShowSummerProjectAbstract(id){
        $("#summerProjectabstract_"+id).modal('show'); 
        
    }
    
    function functionShowResearchProjectDetails(id){
        $("#ResearchProjectdetails_"+id).modal('show'); 
        
    }
    
    function functionShowResearchProjectAbstract(id){
        $("#ResearchProjectabstract_"+id).modal('show'); 
        
    }

    function functionShowAssignmentProjectDetails(id){
        $("#AssignmentProjectdetails_"+id).modal('show'); 
        
    }


    function functionShowExtraProjectDetails(id){
        $("#ExtraProjectdetails_"+id).modal('show'); 
        
    }

    function functionCourseDetails(id){
        $("#Coursedetails_"+id).modal('show'); 
        
    }

    function functionResponsblityDetails(id){
        $("#P_Responsblitydetails_"+id).modal('show'); 
        
    }

    function functionawardDetails(id){
        $("#awardDetails_"+id).modal('show'); 
        
    }

</script>
    
</head>
<body>

    <div id="wrapper">

        <?php include("header.php"); ?>

          <div id="page-wrapper">
            <div class="page-content">
            <div class="page-content">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>
                                Profile
                                <small>Information Collection</small>
                            </h1>
                            <ol class="breadcrumb">
                    

                    <?php if($this->session->userdata("role")== "STUDENT" ){ ?>                                
                                <li>
                                    <i class="fa fa-dashboard"></i>
                                    <a id="dashboard" href="<?php echo base_url();?>student">Dashboard</a>
                                </li>
                                <li class="active">Profile</li>
                           <?php }
                              if($this->session->userdata("role")== "PLACEMENT_TEAM" ){ 
                           ?>
                                <li>
                                     <i class="fa fa-dashboard"></i>
                                     <a href="<?php echo base_url();?>placementteam">Dashboard</a>
                                </li>
                                <li class="active">Student Profile</li>


                            <?php  } ?>
                            </ol>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <div class="portlet-body">
                          
                            <?php if( $this->session->userdata("page_name"))
                                {
                                    echo "
                                <ul id='userTab' class='nav nav-tabs'>
                                    <li id='overView'><a href='#overview' data-toggle='tab'>Overview</a>
                                    </li>
                                    <li id='profile' class='active'><a href='#profile-settings' data-toggle='tab'>Update Profile</a>
                                    </li>
                                </ul>";
                                }   
                            else
                            {
                                 echo "
                                <ul id='userTab' class='nav nav-tabs'>
                                    <li class='active'><a href='#overview' data-toggle='tab'>Overview</a>
                                    </li>
                                    <li><a href='#profile-settings' data-toggle='tab'>Update Profile</a>
                                    </li>
                                </ul>";                       
                            }	
                            ?>




                                <div id="userTabContent" class="tab-content">
								
                                <?php include('profile_overview.php') ?>
                                    <div class="tab-pane fade <?php if( $this->session->userdata("page_name")) { echo "in active";}else {} ?>" id="profile-settings">

                                        <?php
                                            function chkStatus($startDate,$endDate) 
                                            {
                                            	//echo $startDate.'<br>';
                                            	//echo date('Y-m-d H:i:s').'<br>';
                                            	//echo $endDate.'<br>';
                                            if ((strtotime(date("Y-m-d H:i:s")) >= strtotime($startDate)) && (strtotime(date("Y-m-d H:i:s")) <= strtotime($endDate)))
                                                {
                                               		 return 1;
                                                }
                                                else
                                                {
                                                	
                                                        return 0;
                                                }

                                            }

                                            /*$fname =array ("Basic Information", "Address and Contacts Information","Academic Qualification Information","Professional Information","Summer Project Information","Research Project Information","Assignment Project Information","Extra Project Information","Specialization Information","Additional Course Information","Position Or Responsibility Information","Awards or Achievements Information"," Skills and Expertise Information","Hobbies Information","Extra Curricular Activities Information");*/
											
											$fname =array (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15);
											//print_r($fname);
                                            $counter=0;
											//print_r($forms);
											
											 //original code
                                             foreach ($forms as $r ) {
												//echo $r->form_id;
													 if($r->form_id == $fname[$counter])
													 {   

														 if(chkStatus($r->start_date, $r->end_date))
														 {
															//echo 'kaushal';
															 $form_status[$counter] = 1;
														 }
														 else
														 {
															$sid = $this->session->userdata("student_id");
															$count = $this->db->query("select * from tbl_student_forms_rejection where stud_id = '$sid' and form_id = '$r->form_id' order by end_date desc limit 0, 1");
															$result = $count->row();
															if($count->num_rows() == 0)
															{
																$form_status[$counter] = 0;
															}
															else
															{
																if(chkStatus($result->start_date, $result->end_date))
																 {
																	//echo 'kaushal';
																	 $form_status[$counter] = 1;
																 }
															}

														 }
														 $counter++;
													 }
                                            
                                             }
											 
											
											 /*foreach ($forms as $r ) {
												$count = $this->db->query("select * from tbl_student_forms_rejection where stud_id = 2 and form_id = '$r->form_id'");
													 if($r->form_id == $fname[$counter])
													 {   
														if($count->num_rows() == 0)
														{
															 if(chkStatus($r->start_date, $r->end_date))
															 {
																 $form_status[$counter] = 1;
															 }
															 else
															 {
																 $form_status[$counter] = 0;

															 }
														}
														else
														{
															$form_status[$counter] = 1;
														}	 
															 
														 $counter++;
													 }
                                            
                                             }*/

                                        ?>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <ul id="userSettings" class="nav nav-pills nav-stacked">
                                                    


                                                    <?php if( $this->session->userdata("page_name"))
                                                    { ?>
                                                        <li <?php if( $this->session->userdata("page_name")=="page_basic_info") { echo "class='active'";}else {} ?> ><a href="#basicInformation" data-toggle="tab"><i class="fa fa-user fa-fw"></i> Basic Information</a>
                                                        </li>

                                                    <?php 
                                                    }
                                                    else
                                                    {   
                                                      ?>
                                                    <li class="active"><a href="#basicInformation" data-toggle="tab"><i class="fa fa-info-circle"></i> Basic Information</a>
                                                    </li>
                                                    <?php } ?>                                

                                                    <li <?php if( $this->session->userdata("page_name")=="page_student_address") { echo "class='active'";}else {} ?> ><a href="#addressContactInformation" data-toggle="tab"><i class="fa fa-building-o"></i> Address Information</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_edu") { echo "class='active'";}else {} ?> ><a href="#educationalInformation" data-toggle="tab"><i class="fa fa-book"></i> Academic Qualification</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_experience") { echo "class='active'";}else {} ?> ><a href="#professionalInformation" data-toggle="tab"><i class="fa fa-suitcase"></i> Professional Experience</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_project") { echo "class='active'";}else {} ?> ><a href="#projectsInformation" data-toggle="tab"><i class="fa fa-puzzle-piece"></i> Projects Information</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_specialization") { echo "class='active'";}else {} ?> ><a href="#specializationInformation" data-toggle="tab"><i class="fa fa-pencil-square"></i> Specialization Information</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_additional_course") { echo "class='active'";}else {} ?> ><a href="#aditionalCourseInformation" data-toggle="tab"><i class="fa fa-file-text"></i> Additional Course</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_responsiblities") { echo "class='active'";}else {} ?> ><a href="#possiotionOrResponsiblityInformation" data-toggle="tab"><i class="fa fa-thumbs-up"></i> Position or Responsibility</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_awards") { echo "class='active'";}else {} ?>><a href="#awardsInformation" data-toggle="tab"><i class="fa fa-trophy"></i> Awards or Achievements</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_skills") { echo "class='active'";}else {} ?>><a href="#skillsInformation" data-toggle="tab"><i class="fa fa-cogs"></i> Skills Expertise and Proficiency</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_hobbies") { echo "class='active'";}else {} ?>><a href="#hobiesInformation" data-toggle="tab"><i class="fa fa-coffee"></i> Hobbies Information</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_extraActivity") { echo "class='active'";}else {} ?>><a href="#extraActivityInformation" data-toggle="tab"><i class="fa fa-star"></i> Extra Curricular Activities</a></li>
                                                    <li <?php if( $this->session->userdata("page_name")=="page_change_pwd") { echo "class='active'";}else {} ?>><a href="#changePassword" data-toggle="tab"><i class="fa fa-key"></i> Change Password</a></li>
													<li <?php if( $this->session->userdata("page_name")=="page_preview") { echo "class='active'";}else {} ?>><a href="#preview" data-toggle="tab"><i class="fa fa-info-circle"></i> Preview</a></li>
                                                </ul>
                                            </div>
                                            <div class="col-sm-9">
                                                <div id="userSettingsContent" class="tab-content" id="pagesasdf">
                                                    <!-- form_status[0]--><?php include('basic_information.php') ?> 
                                                    <!-- form_status[1]--><?php include('address_contact_information.php') ?>                                                     
                                                    <!-- form_status[1]--><?php include('academic_qualification.php') ?> 
                                                    <!-- form_status[2]--><?php include('professional_information.php') ?>
                                                    <!-- form_status[6]--> <?php include('projects_information.php') ?>        
                                                    <!-- form_status[3]--><?php include('specialization_information.php') ?>
                                                    <!-- form_status[5]--><?php include('aditional_course_information.php') ?>
                                                    <!-- form_status[7]--> <?php include('possiotion_responsiblity_information.php') ?>  
                                                    <!-- form_status[8]--> <?php include('awards_achivements_information.php') ?>
                                                    <!-- form_status[9]--> <?php include('skills_information.php') ?>
                                                    <!-- form_status[10]--> <?php include('hobbies_information.php') ?>                                                    
                                                                             <?php include('extra_activity_information.php') ?>
																			  <?php include('profile_preview.php') ?>
                                                                             <?php include('change_password.php') ?> 

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>

                    <!-- /.col-lg-12 -->
                </div>
            </div>

</div></div>





        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
     </div>
    <?php include("alljs.php"); ?>   
    <?php include("addmore_info.php"); ?>   

    <script src=" <?php echo base_url(); ?>assets/js/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo/advanced-form-demo.js"></script>

<script type="text/javascript">


function deleteStudentInformation(page,id)
{
     swal({  title: "Are you sure?",   
          text: "You will not be able to recover this information!",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes, delete it!",   
          closeOnConfirm: false 
     },

      function(){   
            $.ajax({
            url: "<?php echo base_url();?>placementteam/pt_deleteStudInfo/"+page+"/"+id+"/",
            type: "POST",
            async: true, 
             success: function(response){ 
                 if(response.indexOf("true") > -1 )
                 {
                    swal("Deleted!", "Information has been deleted.", "success");                    
                    setTimeout(function(){location.reload();},1000);
                 }
                 else
                 {
                    swal("Opps..!", "Somthing going wrong. can't delete Information !!", "error"); 
                    setTimeout(function(){location.reload();},1000);
                 }
        }
    });
    });
}






</script>


<?php 
if($this->session->userdata("role")== "STUDENT" )
{
    ?>
<script type="text/javascript">
    $("#studentProfile").addClass("active"); // main container
</script> 

<?php }
else{
?>
<script type="text/javascript">
    $("#manageStudents").addClass("active"); // main container
</script> 

    <?php }?>
</body>

</html>
		