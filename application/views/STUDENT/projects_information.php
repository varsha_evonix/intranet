  <div class='feedback'>
  <?php if($this->session->flashdata('sucess'))
   echo "
      <script>
      sweetAlert('Ok', '".$this->session->flashdata('sucess')."', 'success');
      </script>";

      if($this->session->flashdata('error')) {
   echo "
      <script>
      sweetAlert('Opps..!', '".$this->session->flashdata('error')."', 'error');
      </script>";
  }
      
   ?>
</div>

<div class="tab-pane fade <?php if( $this->session->userdata("page_name")=="page_project") { echo "in active"; $this->session->unset_userdata("page_name");}else {} ?>" id="projectsInformation">
<h4 class="page-header">Project Information:</h4>
                    <div class="col-lg-12">

                        <div class="portlet portlet-green">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Project information</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <ul id="myTab" class="nav nav-tabs">

                                    <?php if( $this->session->userdata("subpage_name"))
                                    { ?>
                                          <li  <?php if($this->session->userdata("subpage_name")=="summerProject") { echo"class='active'";} ?>><a href="#summerProject" data-toggle="tab">Summer Project</a></li>                                    <?php 
                                    }
                                    else{ 

                                    ?>
                                        <li class="active"><a href="#summerProject" data-toggle="tab">Summer Project</a></li>

                                    <?php 
                                    }
                                    ?>




                                    <li <?php if( $this->session->userdata("subpage_name")=="researchProject") { echo"class='active'";} ?> ><a href="#reaserchProject" data-toggle="tab">Research Project</a></li>
                                    <li <?php if( $this->session->userdata("subpage_name")=="assignmentProject") { echo"class='active'";} ?>><a href="#assignmentProjects" data-toggle="tab">Assignment</a></li>
                                    <li <?php if( $this->session->userdata("subpage_name")=="extraProject") { echo"class='active'";} ?>><a href="#extraProjects" data-toggle="tab">Extra Projects</a></li>
                                </ul>
                                <div id="myTabContent" class="tab-content">

                                    <?php if( $this->session->userdata("subpage_name"))
                                    { ?>
                                             <div class="tab-pane fade <?php if( $this->session->userdata("subpage_name")=="summerProject") { echo "in active"; $this->session->unset_userdata("subpage_name");}else {} ?>" id="summerProject">
                                    <?php 
                                    }
                                    else{ 

                                    ?>
                                             <div class="tab-pane fade in active" id="summerProject">

                                    <?php 
                                    }
                                    ?>


                                  <div class="row">
                                  <div class="col-lg-12">

                                                    <form role="form" id="SummerProject-info" method="post" action="<?php echo base_url().'student/addProject/'.$this->session->userdata('stud_id').'/page_project/summerProject/';?>" enctype="multipart/form-data">

                                                            <?php if($projects)
                                                             { 
                                                                $tempSummer = 0;
                                                                foreach ($projects as $r ) {
                                                                  {
                                                                    if($r->project_type == "Summer Project")
                                                                    {
                                                                      $tempSummer++;
                                                                    }
                                                                  }
                                                                }
                                                                if($tempSummer > 0)
                                                                {
                                                              ?>
                                                          <div class="portlet portlet-default" style="margin-top: 25px;">
                                                              <div class="portlet-heading">
                                                                    <div class="portlet-title">
                                                                        <h4>Summer Project</h4>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                              </div>
                                                              <div class="portlet-body">
                                                                  <div class="table-responsive" style="overflow-y: hidden; overflow-x: scroll;">
                                                                      <table class="table table-striped table-bordered table-hover" >
                                                                        <thead>
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Project Title</th>
                                                                                    <th>Project domain</th>                                                                        
                                                                                    <th>Organization name</th>                                                                                    
                                                                                    <th>Start date</th>
                                                                                    <th>End date</th>                                                                        
                                                                                    <th> Key Deliverables & Learning</th>
                                                                                    <th> Project Abstract</th>
                                                                                    <th> Document </th>
                                                                                    <th>Status by student</th>
                                                                                    <th>Status by Placement team</th>
                                                                                   <?php if($this->session->userdata("role") == "PLACEMENT_TEAM") {echo "<th>Delete</th> ";} else{
                                               	 if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )echo "<th>Delete</th> "; }  ?>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                               <?php $counter=0; 
                                                                               foreach ($projects as $r ) 
                                                                               {
                                                                                if($r->project_type == "Summer Project")
                                                                                  {
                                                                                      $counter ++;
                                                                                   
                                                                                     if($this->session->userdata("role") == "PLACEMENT_TEAM")
                                                                                      {
                                                                                          if($r->verify_by_student == 1) {  $temp="<span class='badge green'>Approved</span>";} else {  $temp="<span class='badge orange'>Pending</span>"; }
                                                                                      }
                                                                                      else
                                                                                      {
                                                                                        // student
                                                                                         if($r->verify_by_student == 2) {  $temp="<a href='".base_url()."student/std_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/summerProject/".$r->project_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>";} else {  $temp="<span class='badge green'>Approved</span>"; }                                                 
                                                                                      }

                                                                                      //show lable to placement team
                                                                                      if($r->verify_by_PT == 1) {  $temp1="<span class='badge green'>Approved</span>";} else if($r->verify_by_PT == 2) {  $temp1="<span class='badge orange'>Pending</span>"; }else {  $temp1="<span class='badge red'>Rejected</span>"; }

                                                                                      //show link to placement team

                                                                                      if($r->verify_by_PT == 1) {  $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/summerProject/".$r->project_id."' style='cursor:pointer'><span class='badge green'>Approved</span></a>";} else if($r->verify_by_PT == 2) {  $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/summerProject/".$r->project_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>"; }else { $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/summerProject/".$r->project_id."' style='cursor:pointer'><span class='badge red'>Rejected</span></a>"; }                                                  

                                                                                       $prjDetail="<div id='summerProjectdetails_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Key Deliverables & Learning</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->project_description."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='functionShowSummerProjectDetails(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a></div>";
                                                              
                                                              $sprjAbstract="<div id='summerProjectabstract_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Key Deliverables & Learning</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->project_abstract."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='functionShowSummerProjectAbstract(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a></div>";

                                                                                      if($r->document_link){ $Documentlink = "<a target='_blank' href='".base_url()."".$r->document_link."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink = "";}    

																					if($r->other_domain)
																					  {
																						  $actualdomain = $r->other_domain;
																					  }
																					  else
																					  {
																						  $nqry = mysql_query("select md.domain_name from master_domain md inner join tbl_student_projects sp on md.domain_id = sp.project_domain where sp.student_id = ".$r->student_id ." and project_id = ".$r->project_id);
																						  $rnqry = mysql_fetch_array($nqry);
																						  $actualdomain = $rnqry['domain_name'];
																					  }

                                                                                  if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                                                    echo "<tr> <td> "
                                                                                    .$counter." </td><td>"
                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualdomain." </td><td>"
                                                                                    .$r->organization_name." </td><td>"
                                                                                    .date('d-m-Y',strtotime($r->start_date))." </td><td>"
                                                                                    .date('d-m-Y',strtotime($r->end_date))." </td><td>"                                                                                                                                                
                                                                                    .$prjDetail." </td><td>"                                                                                                                                                
                                                                                    .$sprjAbstract." </td><td>"                                                                                                
                                                                                    .$Documentlink." </td><td>"
                                                                                    .$temp." </td><td>"
                                                                                    .$link." </td><td>
                                                                                      <div class='fa-hover col-sm-2'>
                                                                                        <a onclick=deleteStudentInformation('page_project',".$r->project_id.") title='Delete This Project' style='cursor:pointer;'>
                                                                                          <i class='fa fa-times'></i>
                                                                                      </a></div></td></tr>
                                                                                    </tr>"; 

                                                                                    }
                                                                                    else
                                                                                    {
                      if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )
	 
	 {
                                                                                     echo "<tr> <td> "
                                                                                    .$counter." </td><td>"
                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualdomain." </td><td>"
                                                                                    .$r->organization_name." </td><td>"
                                                                                    .date('d-m-Y',strtotime($r->start_date))." </td><td>"
                                                                                    .date('d-m-Y',strtotime($r->end_date))." </td><td>"                                                                                                                                                
                                                                                    .$prjDetail." </td><td>"                                                                                                                                                
                                                                                    .$sprjAbstract." </td><td>"                                                                                                
                                                                                    .$Documentlink." </td><td>"
                                                                                    .$temp." </td><td>"
                                                                                    .$temp1." </td><td>
                                                                                      <div class='fa-hover col-sm-2'>
                                                                                        <a onclick=deleteStudentInformation('page_project',".$r->project_id.") title='Delete This Project' style='cursor:pointer;'>
                                                                                          <i class='fa fa-times'></i>
                                                                                      </a></div></td></tr>";
                                                                                   }
                                                                                   else
                                                                                   {
                                                                                   
                                                                                   echo "<tr> <td> "
                                                                                    .$counter." </td><td>"
                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualdomain." </td><td>"
                                                                                    .$r->organization_name." </td><td>"
                                                                                    .date('d-m-Y',strtotime($r->start_date))." </td><td>"
                                                                                    .date('d-m-Y',strtotime($r->end_date))." </td><td>"                                                                                                                                                
                                                                                    .$prjDetail." </td><td>"                                                                                                                                                
                                                                                    .$sprjAbstract." </td><td>"                                                                                                
                                                                                    .$Documentlink." </td><td>"
                                                                                    .$temp." </td><td>"
                                                                                    .$temp1." </td></tr>";
                                                                                   
                                                                                   }
                                                                                    }//else

                                                                                  } // ($r->project_type == "Summer Project")
                                                                    
                                                                                    }//foreach

                                                                                    ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                  </div>
                                                                </div>     <!-- /.portlet -->
                                                                <?php 

                                                              } }?>

                                          <h4 class="page-header">Add new summer project information</h4>

                                          <div class="form-group countProjects  col-lg-12 ">

                                                            <div class="form-group col-lg-12">
                                                               <label>Project Title <span style="color:#b81212">*</span> </label>
                                                                <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )echo "{}"; else{echo "READONLY"; } ?> name="prj[1][name]" id="prj_name_1" placeholder="Enter Project Title">
                                                                  <span id="sprj_name_1" style="color:rgb(184,18,18);"></span>
                                                            </div>

                                                            <div class="form-group col-lg-12">
                                                                <div class="form-group col-lg-6">
                                                                     <label>Organization Name<span style="color:#b81212">*</span> </label>
                                                                      <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )echo "{}"; else{echo "READONLY"; } ?> name="prj[1][org_name]" id="porg_name_1" placeholder="Enter Organization Name">
                                                                        <span id="sporg_name_1" style="color:rgb(184,18,18);"></span>
                                                                  </div>
																  
																	<div class="modal fade" id="standardModal" tabindex="-1" role="dialog" aria-labelledby="standardModalLabel" aria-hidden="true" style="display: none;">
																			<div class="modal-dialog">
																				<div class="modal-content" style="margin-top: 100px; width: 300px;min-height: 300px; margin-left: 250px;">
																					<div class="modal-header">
																						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																						<h4 class="modal-title" id="standardModalLabel">Select Project Domain</h4>
																					</div>
																					<div class="modal-body">
																						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
																					</div>
																					<div class="modal-footer">
																						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																					</div>
																				</div>
																				<!-- /.modal-content -->
																			</div>
																			<!-- /.modal-dialog -->
																		</div>
																		
                                                                  <div class="form-group col-lg-6" >
                                                                         <label> Project Domain <span style="color:#b81212">*</span> 
																		<button class="btn btn-default" data-toggle="modal" data-target="#standardModal" style="  height: 20px; padding: 2px; font-size: 10px;">
																			Hint
																		</button></label>
                                                                          <select  class="form-control"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?> name="prj[1][domain]" id="prj_domain_1">
                                                                            <option value=""> Select Project Domain: </option>
                                                                            <?php 
                                                                            foreach ($m_domain as $r) {
                                                                              echo " <option value=".$r->domain_id."> ". $r->domain_name." </option>";
                                                                            }
                                                                            ?>
																			<!--<option value="99">Other</option>-->
                                                                          </select>
																		  <input class="hideme" type="text" id="otherspdomain" name="otherspdomain" size="30">
                                                                            <span id="sprj_domain_1" style="color:rgb(184,18,18);"></span>
                                                                    </div>                        

                                                            </div>


                                                          <div class="form-group col-lg-12">

                                                                  <div class="form-group col-lg-6 datecall" >
                                                                    <label>Start Date <span style="color:#b81212">*</span> </label>
                                                                    <input type="text" class="form-control" name="prj[1][date_in]" id="prjdateIn_1" placeholder="Select date when you start project" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?>>
                                                                    <span id="sprjdateIn_1" style="color:rgb(184,18,18);"></span>
                                                                  </div>

                                                                  <div class="form-group col-lg-6 datecall">
                                                                    <label>End Date <span style="color:#b81212">*</span> </label>
                                                                    <input type="text" class="form-control" name="prj[1][date_out]" id="prjdateOut_1" placeholder="Select date when you complete project" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?>>
                                                                    <span id="sprjdateOut_1" style="color:rgb(184,18,18);"></span>
                                                                  </div>
                                                          </div>



                                                            <div class="form-group col-lg-12" id="prjDesc_1">
                                                              <label>Key Deliverables & Learning</label>
                                                              <textarea  name="prj[1][descrption]" id="prj_descrption_1" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )echo "{}"; else{echo "disabled"; } ?> required></textarea>                                                               
                                                              <span id="sprj_descrption_1" style="color:rgb(184,18,18);"></span>                    
                                                            </div>
															
															<div class="form-group col-lg-12" id="prjAbstract_1">
                                                              <label>Project Abstract <span style="color:#b81212">*</span></label><br />
                                                              <textarea class="form-control"  name="prj[1][abstract]" id="prj_abstract_1" rows="4" cols="70" style="resize:vertical;" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )echo "{}"; else{echo "disabled"; } ?> required></textarea>                                                               
                                                              <span id="sprj_abstract_1" style="color:rgb(184,18,18);"></span>                    
                                                            </div>

                                                          <div class="form-group col-lg-12">
                                                                    <div class="form-group col-lg-6" id="divproj_doc_1">
                                                                      <label>Document</label>
                                                                      <input type="file" id="prj_doc_1" name="prj[1][doc]" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )echo "{}"; else{echo "disabled"; } ?>>
                                                                      <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                                                      <span id="sprj_doc_1" style="color:rgb(184,18,18);"></span>
                                                                    </div>

                                                                    <div class="checkbox col-lg-6" id="divPrjStatus_1" >
                                                                       <input type="checkbox" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )echo "{}"; else{echo "disabled"; } ?> name="prj[1][status]" id="prj_status_1" >                      
                                                                       Status <span style="color:#b81212">*</span>                                                                        
                                                                      <p class="help-block"><i class="fa fa-warning"></i> By clicking on this you confirm that summer project information are correct from your side</p>
                                                                      <span id="sst_project_1" style="color:rgb(184,18,18);"></span>
                                                                    </div>
                                                            </div>
                                              </div>

                                              <?php 
                                                  if( visiblityStatus_empty($this->session->userdata("role"),$form_status[4]) )
                                                  {
                                                  ?>
                                             <div class="form-group col-lg-12">               
                                                  <button type="button" onclick="submitSummerProjects();" class="btn btn-default" >
                                                    Add Projects</button>
                                              </div>
                                              <?php } ?>

                                              </form>
                                    </div>                                    
                                  </div> 
                                </div>



                              <div class="tab-pane fade <?php if( $this->session->userdata("subpage_name")=="researchProject") { echo "in active"; $this->session->unset_userdata("subpage_name");}else {} ?>" id="reaserchProject">
                                  <div class="row">
                                  <div class="col-lg-12">
                                                    <form role="form" id="ResearchProject-info" method="post" action="<?php echo base_url().'student/addProject/'.$this->session->userdata('stud_id').'/page_project/researchProject/';?>" enctype="multipart/form-data">
                                                            <?php if($projects)
                                                             { 
                                                                $tempResearch = 0;
                                                                foreach ($projects as $r ) {
                                                                  {
                                                                    if($r->project_type == "Research Project")
                                                                    {
                                                                      $tempResearch++;
                                                                    }
                                                                  }
                                                                }
                                                                if($tempResearch > 0)
                                                                {
                                                              ?>
                                                          <div class="portlet portlet-default" style="margin-top: 25px;">
                                                              <div class="portlet-heading">
                                                                    <div class="portlet-title">
                                                                        <h4>Research Project</h4>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                              </div>
                                                              <div class="portlet-body">
                                                                  <div class="table-responsive" style="overflow-y: hidden; overflow-x: scroll;">
                                                                      <table class="table table-striped table-bordered table-hover" >
                                                                        <thead>
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Project Title</th>
                                                                                    <th>Project domain</th> 
                                                                                    <th> Key Deliverables & Learning</th>
                                                                                    <th> Project Abstract</th>
                                                                                    <th> Document</th>
                                                                                    <th>Status by student</th>
                                                                                    <th>Status by Placement team</th>
                                                                                   <?php if($this->session->userdata("role") == "PLACEMENT_TEAM") {echo "<th>Delete</th> ";} else{
                                               	 if( visiblityStatus_empty($this->session->userdata("role"),$form_status[5]) )echo "<th>Delete</th> "; }  ?>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                               <?php $counter=0; 
                                                                                      foreach ($projects as $r ) 
                                                                                      {

                                                                                    if($r->project_type == "Research Project")
                                                                                    {


                                                                                          $counter ++;
                                                                                     //show lable to student
                                                                                    
                                                                                     if($this->session->userdata("role") == "PLACEMENT_TEAM")
                                                                                      {
                                                                                          if($r->verify_by_student == 1) {  $temp="<span class='badge green'>Approved</span>";} else {  $temp="<span class='badge orange'>Pending</span>"; }
                                                                                      }
                                                                                      else
                                                                                      {
                                                                                       if($r->verify_by_student == 2) {  $temp="<a href='".base_url()."student/std_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/researchProject/".$r->project_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>";} else {  $temp="<span class='badge green'>Approved</span>"; }                                                 
                                                                                      }

                                                                                      //show lable to placement team
                                                                                      if($r->verify_by_PT == 1) {  $temp1="<span class='badge green'>Approved</span>";} else if($r->verify_by_PT == 2) {  $temp1="<span class='badge orange'>Pending</span>"; }else {  $temp1="<span class='badge red'>Rejected</span>"; }

                                                                                      //show link to placement team

                                                                                      if($r->verify_by_PT == 1) {  $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/researchProject/".$r->project_id."' style='cursor:pointer'><span class='badge green'>Approved</span></a>";} else if($r->verify_by_PT == 2) {  $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/researchProject/".$r->project_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>"; }else { $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/researchProject/".$r->project_id."' style='cursor:pointer'><span class='badge red'>Rejected</span></a>"; }                                                  

                                                                                       $prjDetail="<div id='ResearchProjectdetails_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Key Deliverables & Learning</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->project_description."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='functionShowResearchProjectDetails(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a></div>";
                                                              
                                                              $rprjAbstract="<div id='ResearchProjectabstract_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Key Deliverables & Learning</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->project_abstract."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='functionShowResearchProjectAbstract(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a></div>";

																					if($r->other_domain)
																					  {
																						  $actualrpdomain = $r->other_domain;
																					  }
																					  else
																					  {
																						  $nqry = mysql_query("select md.domain_name from master_domain md inner join tbl_student_projects sp on md.domain_id = sp.project_domain where sp.student_id = ".$r->student_id ." and project_id = ".$r->project_id);
																						  $rnqry = mysql_fetch_array($nqry);
																						  $actualrpdomain = $rnqry['domain_name'];
																					  }

                                                                                      if($r->document_link){ $Documentlink = "<a target='_blank' href='".base_url()."".$r->document_link."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink = "";}                                                 


                                                                                  if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                                                    echo "<tr> <td> "
                                                                                    .$counter." </td><td>"
                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualrpdomain." </td><td>"
                                                                                                                   
                                                                                    .$prjDetail." </td><td>"
                                                                                                                   
                                                                                    .$rprjAbstract." </td><td>"                                                                                                
                                                                                    .$Documentlink." </td><td>"
                                                                                    .$temp." </td><td>"
                                                                                    .$link." </td><td>
                                                                                      <div class='fa-hover col-sm-2'>
                                                                                        <a onclick=deleteStudentInformation('page_project',".$r->project_id.") title='Delete This Project' style='cursor:pointer;'>
                                                                                      <i class='fa fa-times'></i>
                                                                                      </a></div></td></tr>
                                                                                    </tr>"; 

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    if( visiblityStatus_empty($this->session->userdata("role"),$form_status[5]) )
	 
	 {	
	 
	 echo "<tr> <td> "
                                                                                    .$counter." </td><td>"

                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualrpdomain." </td><td>"
                                      
                                                                                    .$prjDetail." </td><td>"
                                      
                                                                                    .$rprjAbstract." </td><td>"                                                                                                
                                                                                    .$Documentlink." </td><td>"
                                                                                    .$temp." </td><td>"
                                                                                    .$temp1." </td><td>
                                                                                      <div class='fa-hover col-sm-2'>
                                                                                        <a onclick=deleteStudentInformation('page_project',".$r->project_id.") title='Delete This Project' style='cursor:pointer;'>
                                                                                      <i class='fa fa-times'></i>
                                                                                      </a></div></td></tr>";
                                                                                     
                                                                                 }
else
{
echo "<tr> <td> "
                                                                                    .$counter." </td><td>"

                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualrpdomain." </td><td>"
                                      
                                                                                    .$prjDetail." </td><td>"
                                      
                                                                                    .$rprjAbstract." </td><td>"                                                                                                
                                                                                    .$Documentlink." </td><td>"
                                                                                    .$temp." </td><td>"
                                                                                    .$temp1." </td></tr>";

}                                                                                    
                                                                                    
                                                                                    }//else

                                                                                  } //  if($r->project_type == "Research Project")
                                                                                    }//foreach

                                                                                    ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                  </div>
                                                                </div>     <!-- /.portlet -->
                                                                <?php 

                                                              } }?>

                                          <h4 class="page-header">Add new research project information</h4>

                                          <div class="form-group countReserchProjects col-lg-12">
                                                            <div class="form-group col-lg-12">
                                                               <label>Project Title <span style="color:#b81212">*</span> </label>
                                                                <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[5]) )echo "{}"; else{echo "READONLY"; } ?> name="researchPrj[1][name]" id="Rprj_name_1" placeholder="Enter Project Title">
                                                                  <span id="sRprj_name_1" style="color:rgb(184,18,18);"></span>
                                                            </div>
															
															<div class="modal fade" id="standardModal2" tabindex="-1" role="dialog" aria-labelledby="standardModalLabel" aria-hidden="true" style="display: none;">
																			<div class="modal-dialog">
																				<div class="modal-content" style="margin-top: 100px; width: 300px;min-height: 300px; margin-left: 250px;">
																					<div class="modal-header">
																						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																						<h4 class="modal-title" id="standardModalLabel">Select Project Domain</h4>
																					</div>
																					<div class="modal-body">
																						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
																					</div>
																					<div class="modal-footer">
																						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																					</div>
																				</div>
																				<!-- /.modal-content -->
																			</div>
																			<!-- /.modal-dialog -->
																		</div>

                                                            <div class="form-group col-lg-12">


                                                                  <div class="form-group col-lg-6" >
                                                                         <label> Project Domain <span style="color:#b81212">*</span> <button class="btn btn-default" data-toggle="modal" data-target="#standardModal2" style="  height: 20px; padding: 2px; font-size: 10px;">
																			Hint
																		</button></label>
                                                                          <select  class="form-control"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[5]) )echo "{}"; else{echo "DISABLED"; } ?> name="researchPrj[1][domain]" id="Rprj_domain_1">
                                                                            <option value=""> Select Project Domain: </option>
                                                                            <?php 
                                                                            foreach ($m_domain as $r) {
                                                                              echo " <option value=".$r->domain_id."> ". $r->domain_name." </option>";
                                                                            }
                                                                            ?>
																			<!--<option value="99">Other</option>-->
                                                                          </select>
																		  <input class="hideme" type="text" id="otherrpdomain" name="otherrpdomain" size="30">
                                                                            <span id="sRprj_domain_1" style="color:rgb(184,18,18);"></span>
                                                                    </div> 


                                                                    <div class="form-group col-lg-6" id="DIV_Rproj_doc_1">
                                                                      <label>Document</label>
                                                                      <input type="file" id="Rprj_doc_1" name="researchPrj[1][doc]" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[5]) )echo "{}"; else{echo "disabled"; } ?>>
                                                                      <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                                                      <span id="sRprj_doc_1" style="color:rgb(184,18,18);"></span>
                                                                    </div>




                                                            </div>


                                                           <div class="form-group col-lg-12" id="reaserchprjDesc_1">
                                                              <label>Key Deliverables & Learning</label>
                                                              <textarea class="ckeditor form-control" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[5]) )echo "{}"; else{echo "disabled"; } ?> name="researchPrj[1][descrption]" id="Rprj_descrption_1" placeholder=" Describe your project"></textarea>
                                                              <span id="sRprj_descrption_1" style="color:rgb(184,18,18);"></span>                    
                                                            </div>
															
															<div class="form-group col-lg-12" id="reaserchprjAbstract_1">
                                                              <label>Project Abstract <span style="color:#b81212">*</span></label><br />
                                                              <textarea class="form-control"  name="researchPrj[1][abstract]" id="Rprj_abstract_1" rows="4" cols="70" style="resize:vertical;" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[5]) )echo "{}"; else{echo "disabled"; } ?> required></textarea>                                                               
                                                              <span id="sRprj_abstract_1" style="color:rgb(184,18,18);"></span>                    
                                                            </div>
                                                                                      

                                                            <div class="checkbox col-lg-12" id="divResearchPrjPrjStatus_1" >
                                                               <input type="checkbox" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[5]) )echo "{}"; else{echo "disabled"; } ?> name="researchPrj[1][status]" id="Rprj_status_1" >                      
                                                              Status <span style="color:#b81212">*</span>                                                                                                                                       
                                                              <p class="help-block"><i class="fa fa-warning"></i> By clicking on this you confirm that research project information are correct from your side</p>
                                                              <span id="rst_Rproject_1" style="color:rgb(184,18,18);"></span>
                                                            </div>

                                              </div>
                                                <?php 
                                                  if( visiblityStatus_empty($this->session->userdata("role"),$form_status[5]) )
                                                  {
                                                  ?>

                                              <div class="form-group col-lg-12">               
                                                  <button type="button" onclick="submitReaserchProjects();" class="btn btn-default" >
                                                    Add Projects</button>
                                              </div>
                                              <?php } ?>

                                              </form>
                                         </div>
                                         </div>
                                  </div> <!-- reserchProject -->

                              <div class="tab-pane fade <?php if( $this->session->userdata("subpage_name")=="assignmentProject") { echo "in active"; $this->session->unset_userdata("subpage_name");}else {} ?>" id="assignmentProjects">
                                  <div class="row">
                                  <div class="col-lg-12">
                                                    <form role="form" id="assignmentProject-info" method="post" action="<?php echo base_url().'student/addProject/'.$this->session->userdata('stud_id').'/page_project/assignmentProject/';?>" enctype="multipart/form-data">
                                                            <?php if($projects)
                                                             { 
                                                                $tempAssignment = 0;
                                                                foreach ($projects as $r ) {
                                                                  {
                                                                    if($r->project_type == "Assignment Project")
                                                                    {
                                                                      $tempAssignment++;
                                                                    }
                                                                  }
                                                                }
                                                                if($tempAssignment > 0)
                                                                {
                                                              ?>
                                                          <div class="portlet portlet-default" style="margin-top: 25px;">
                                                              <div class="portlet-heading">
                                                                    <div class="portlet-title">
                                                                        <h4>Assignement Project</h4>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                              </div>
                                                              <div class="portlet-body">
                                                                  <div class="table-responsive" style="overflow-y: hidden; overflow-x: scroll;">
                                                                      <table class="table table-striped table-bordered table-hover" >
                                                                        <thead>
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Project Title</th>
                                                                                    <th>Project domain</th> 
                                                                                    <th> Key Deliverables & Learning</th>
                                                                                    <th>Status by student</th>
                                                                                    <th>Status by Placement team</th>
                                                                                   <?php if($this->session->userdata("role") == "PLACEMENT_TEAM") {echo "<th>Delete</th> ";} else{
                                               	 if( visiblityStatus_empty($this->session->userdata("role"),$form_status[6]) )echo "<th>Delete</th> "; }  ?>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                               <?php $counter=0; 
                                                                                      foreach ($projects as $r ) 
                                                                                      {

                                                                                    if($r->project_type == "Assignment Project")
                                                                                    {
                                                                                          $counter ++;
                                                                                     //show lable to student
                                                                                    
                                                                                     if($this->session->userdata("role") == "PLACEMENT_TEAM")
                                                                                      {
                                                                                          if($r->verify_by_student == 1) {  $temp="<span class='badge green'>Approved</span>";} else {  $temp="<span class='badge orange'>Pending</span>"; }
                                                                                      }
                                                                                      else
                                                                                      {
                                                                                       if($r->verify_by_student == 2) {  $temp="<a href='".base_url()."student/std_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/assignmentProject/".$r->project_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>";} else {  $temp="<span class='badge green'>Approved</span>"; }                                                 
                                                                                      }

                                                                                      //show lable to placement team
                                                                                      if($r->verify_by_PT == 1) {  $temp1="<span class='badge green'>Approved</span>";} else if($r->verify_by_PT == 2) {  $temp1="<span class='badge orange'>Pending</span>"; }else {  $temp1="<span class='badge red'>Rejected</span>"; }

                                                                                      //show link to placement team

                                                                                      if($r->verify_by_PT == 1) {  $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/assignmentProject/".$r->project_id."' style='cursor:pointer'><span class='badge green'>Approved</span></a>";} else if($r->verify_by_PT == 2) {  $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/assignmentProject/".$r->project_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>"; }else { $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/assignmentProject/".$r->project_id."' style='cursor:pointer'><span class='badge red'>Rejected</span></a>"; }         

																						if($r->other_domain)
																						  {
																							  $actualapdomain = $r->other_domain;
																						  }
																						  else
																						  {
																							  $nqry = mysql_query("select md.domain_name from master_domain md inner join tbl_student_projects sp on md.domain_id = sp.project_domain where sp.student_id = ".$r->student_id ." and project_id = ".$r->project_id);
																							  $rnqry = mysql_fetch_array($nqry);
																							  $actualapdomain = $rnqry['domain_name'];
																						  }

                                                                                       $prjDetail="<div id='AssignmentProjectdetails_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Key Deliverables & Learning</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->project_description."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='functionShowAssignmentProjectDetails(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a></div>";



                                                                                  if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                                                    echo "<tr> <td> "
                                                                                    .$counter." </td><td>"
                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualapdomain." </td><td>"
                                                                                    .$prjDetail." </td><td>"                                                                                                
                                                                                    .$temp." </td><td>"
                                                                                    .$link." </td><td>
                                                                                      <div class='fa-hover col-sm-2'>
                                                                                        <a onclick=deleteStudentInformation('page_project',".$r->project_id.") title='Delete This Project' style='cursor:pointer;'>
                                                                                      <i class='fa fa-times'></i>
                                                                                      </a></div></td></tr>
                                                                                    </tr>"; 

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    	
                                                                                    if( visiblityStatus_empty($this->session->userdata("role"),$form_status[6]) )
	 
	 {
	 	echo "<tr> <td> "
                                                                                    .$counter." </td><td>"
                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualapdomain." </td><td>"
                                                                                    .$prjDetail." </td><td>"                                                                                                
                                                                                    .$temp." </td><td>"
                                                                                    .$temp1." </td><td>
                                                                                      <div class='fa-hover col-sm-2'>
                                                                                        <a onclick=deleteStudentInformation('page_project',".$r->project_id.") title='Delete This Project' style='cursor:pointer;'>
                                                                                      <i class='fa fa-times'></i>
                                                                                      </a></div></td></tr>";
	 }
	 else{
                                                                                     echo "<tr> <td> "
                                                                                    .$counter." </td><td>"
                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualapdomain." </td><td>"
                                                                                    .$prjDetail." </td><td>"                                                                                                
                                                                                    .$temp." </td><td>"
                                                                                    .$temp1." </td></tr>";
                                                                                    
                                                                                    }
                                                                                    }//else

                                                                                  } //  if($r->project_type == "Research Project")
                                                                                    }//foreach

                                                                                    ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                  </div>
                                                                </div>     <!-- /.portlet -->
                                                                <?php 

                                                              } }?>

                                          <h4 class="page-header">Add new assignment project information</h4>

                                          <div class="form-group countAssignmentProjects col-lg-12">
                                                            <div class="form-group col-lg-12">
                                                                  <div class="form-group col-lg-6" >
                                                                     <label>Project Title <span style="color:#b81212">*</span> </label>
                                                                      <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[6]) )echo "{}"; else{echo "READONLY"; } ?> name="assignmentPrj[1][name]" id="assignmentPrj_name_1" placeholder="Enter Project Title">
                                                                        <span id="sassignmentPrj_name_1" style="color:rgb(184,18,18);"></span>
                                                                  </div>
																  
																  <div class="modal fade" id="standardModal3" tabindex="-1" role="dialog" aria-labelledby="standardModalLabel" aria-hidden="true" style="display: none;">
																			<div class="modal-dialog">
																				<div class="modal-content" style="margin-top: 100px; width: 300px;min-height: 300px; margin-left: 250px;">
																					<div class="modal-header">
																						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																						<h4 class="modal-title" id="standardModalLabel">Select Project Domain</h4>
																					</div>
																					<div class="modal-body">
																						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
																					</div>
																					<div class="modal-footer">
																						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																					</div>
																				</div>
																				<!-- /.modal-content -->
																			</div>
																			<!-- /.modal-dialog -->
																		</div>

                                                            <div class="form-group col-lg-6">

                                                                         <label> Project Domain <span style="color:#b81212">*</span> <button class="btn btn-default" data-toggle="modal" data-target="#standardModal3" style="  height: 20px; padding: 2px; font-size: 10px;">
																			Hint
																		</button></label>
                                                                          <select  class="form-control"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[6]) )echo "{}"; else{echo "DISABLED"; } ?> name="assignmentPrj[1][domain]" id="assignmentPrj_domain_1">
                                                                            <option value=""> Select Project Domain: </option>
                                                                            <?php 
                                                                            foreach ($m_domain as $r) {
                                                                              echo " <option value=".$r->domain_id."> ". $r->domain_name." </option>";
                                                                            }
                                                                            ?>
																			<!--<option value="99">Other</option>-->
                                                                          </select>
																		  <input class="hideme" type="text" id="otherapdomain" name="otherapdomain" size="30">
                                                                          </select>
                                                                            <span id="sassignmentPrj_domain_1" style="color:rgb(184,18,18);"></span>
                                                                    </div> 
                                                            </div>
                                                           <div class="form-group col-lg-12" id="assignmentPrjDesc_1">
                                                              <label>Key Deliverables & Learning</label>
                                                              <textarea class="ckeditor form-control" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[6]) )echo "{}"; else{echo "disabled"; } ?> name="assignmentPrj[1][descrption]" id="assignmentPrj_descrption_1" placeholder=" Describe your project"></textarea>
                                                              <span id="sassignmentPrj_descrption_1" style="color:rgb(184,18,18);"></span>                    
                                                            </div>
                                                                                      

                                                            <div class="checkbox col-lg-12" id="divAssignmentStatus_1" >
                                                              <input type="checkbox" id="assignment_status" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[6]) )echo "{}"; else{echo "disabled"; } ?> >
                                                          Status <span style="color:#b81212">*</span>                                                                                                                                      
                                                              <p class="help-block"><i class="fa fa-warning"></i> By clicking on this you confirm that assignment project information are correct from your side</p>
                                                              <span id="sassignmentPrj_1" style="color:rgb(184,18,18);"></span>
                                                            </div>
                                              </div>

                                                <?php 
                                                  if( visiblityStatus_empty($this->session->userdata("role"),$form_status[6]) )
                                                  {

                                                  ?>

                                              <div class="form-group col-lg-12">               
                                                  <button type="button" onclick="submitAssignmentPrjProjects();" class="btn btn-default" >
                                                    Add Projects</button>
                                              </div>

                                              <?php } ?>
                                              </form>
                                         </div>
                                         </div>
                                  </div> <!-- assignmentProjects -->




                              <div class="tab-pane fade <?php if( $this->session->userdata("subpage_name")=="extraProject") { echo "in active"; $this->session->unset_userdata("subpage_name");}else {} ?>" id="extraProjects">
                                  <div class="row">
                                  <div class="col-lg-12">
                                                    <form role="form" id="extraProject-info" method="post" action="<?php echo base_url().'student/addProject/'.$this->session->userdata('stud_id').'/page_project/extraProject/';?>" enctype="multipart/form-data">
                                                            <?php if($projects)
                                                             { 
                                                                $tempExtra = 0;
                                                                foreach ($projects as $r ) {
                                                                  {
                                                                    if($r->project_type == "Extra Project")
                                                                    {
                                                                      $tempExtra++;
                                                                    }
                                                                  }
                                                                }
                                                                if($tempExtra > 0)
                                                                {
                                                              ?>
                                                          <div class="portlet portlet-default" style="margin-top: 25px;">
                                                              <div class="portlet-heading">
                                                                    <div class="portlet-title">
                                                                        <h4>Extra Projects</h4>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                              </div>
                                                              <div class="portlet-body">
                                                                  <div class="table-responsive" style="overflow-y: hidden; overflow-x: scroll;">
                                                                      <table class="table table-striped table-bordered table-hover" >
                                                                        <thead>
                                                                                <tr>
                                                                                    <th>#</th>
                                                                                    <th>Project Title</th>
                                                                                    <th>Project domain</th> 
                                                                                    <th> Key Deliverables & Learning</th>
                                                                                    <th>Status by student</th>
                                                                                    <th>Status by Placement team</th>
                                                                                   <?php if($this->session->userdata("role") == "PLACEMENT_TEAM") {echo "<th>Delete</th> ";} else{
                                               	 if( visiblityStatus_empty($this->session->userdata("role"),$form_status[7]) )echo "<th>Delete</th> "; }  ?>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                               <?php $counter=0; 
                                                                                      foreach ($projects as $r ) 
                                                                                      {

                                                                                    if($r->project_type == "Extra Project")
                                                                                    {
                                                                                          $counter ++;
                                                                                     //show lable to student
                                                                                    
                                                                                     if($this->session->userdata("role") == "PLACEMENT_TEAM")
                                                                                      {
                                                                                          if($r->verify_by_student == 1) {  $temp="<span class='badge green'>Approved</span>";} else {  $temp="<span class='badge orange'>Pending</span>"; }
                                                                                      }
                                                                                      else
                                                                                      {
                                                                                       if($r->verify_by_student == 2) {  $temp="<a href='".base_url()."student/std_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/extraProject/".$r->project_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>";} else {  $temp="<span class='badge green'>Approved</span>"; }                                          
                                                                                      }

                                                                                      //show lable to placement team
                                                                                      if($r->verify_by_PT == 1) {  $temp1="<span class='badge green'>Approved</span>";} else if($r->verify_by_PT == 2) {  $temp1="<span class='badge orange'>Pending</span>"; }else {  $temp1="<span class='badge red'>Rejected</span>"; }

                                                                                      //show link to placement team

                                                                                      if($r->verify_by_PT == 1) {  $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/extraProject/".$r->project_id."' style='cursor:pointer'><span class='badge green'>Approved</span></a>";} else if($r->verify_by_PT == 2) {  $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/extraProject/".$r->project_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>"; }else { $link="<a href='".base_url()."placementteam/pt_changeStudent_project_Status/".$this->session->userdata('stud_id')."/page_project/extraProject/".$r->project_id."' style='cursor:pointer'><span class='badge red'>Rejected</span></a>"; }                                                  

																						if($r->other_domain)
																						  {
																							  $actualepdomain = $r->other_domain;
																						  }
																						  else
																						  {
																							  $nqry = mysql_query("select md.domain_name from master_domain md inner join tbl_student_projects sp on md.domain_id = sp.project_domain where sp.student_id = ".$r->student_id ." and project_id = ".$r->project_id);
																							  $rnqry = mysql_fetch_array($nqry);
																							  $actualepdomain = $rnqry['domain_name'];
																						  }
																					
                                                                                       $prjDetail="<div id='ExtraProjectdetails_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Key Deliverables & Learning</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->project_description."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='functionShowExtraProjectDetails(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a></div>";



                                                                                  if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                                                    echo "<tr> <td> "
                                                                                    .$counter." </td><td>"
                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualepdomain." </td><td>"
                                                                                    .$prjDetail." </td><td>"                                                                                                
                                                                                    .$temp." </td><td>"
                                                                                    .$link." </td><td>
                                                                                      <div class='fa-hover col-sm-2'>
                                                                                        <a onclick=deleteStudentInformation('page_project',".$r->project_id.") title='Delete This Project Information' style='cursor:pointer;'>
                                                                                      <i class='fa fa-times'></i>
                                                                                      </a></div></td></tr>
                                                                                    </tr>"; 

                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    	
                                                                                    	if( visiblityStatus_empty($this->session->userdata("role"),$form_status[7]) )
	 
	 {
                                     echo "<tr> <td> "
                                                                                    .$counter." </td><td>"
                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualepdomain." </td><td>"
                                                                                    .$prjDetail." </td><td>"                                                                                                
                                                                                    .$temp." </td><td>"
                                                                                    .$temp1." </td><td>
                                                                                      <div class='fa-hover col-sm-2'>
                                                                                        <a onclick=deleteStudentInformation('page_project',".$r->project_id.") title='Delete This Project Information' style='cursor:pointer;'>
                                                                                      <i class='fa fa-times'></i>
                                                                                      </a></div></td></tr>";                                                
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                    echo "<tr> <td> "
                                                                                    .$counter." </td><td>"
                                                                                    .$r->project_name." </td><td>"                                                                        
                                                                                    .$actualepdomain." </td><td>"
                                                                                    .$prjDetail." </td><td>"                                                                                                
                                                                                    .$temp." </td><td>"
                                                                                    .$temp1." </td></tr>";
                                                                                    }
                                                                                    
                                                                                    }//else

                                                                                  } //  if($r->project_type == "Research Project")
                                                                                    }//foreach

                                                                                    ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                  </div>
                                                                </div>     <!-- /.portlet -->
                                                                <?php 

                                                              } }?>

                                          <h4 class="page-header">Add new extra project information</h4>

                                          <div class="form-group countExtraProjects col-lg-12">
                                                            <div class="form-group col-lg-12">
                                                                  <div class="form-group col-lg-6" >
                                                                     <label>Project Title <span style="color:#b81212">*</span> </label>
                                                                      <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[7]) )echo "{}"; else{echo "READONLY"; } ?> name="ExtraPrj[1][name]" id="ExtraPrj_name_1" placeholder="Enter Project Title">
                                                                        <span id="sExtraPrj_name_1" style="color:rgb(184,18,18);"></span>
                                                                  </div>
																  
																  <div class="modal fade" id="standardModal4" tabindex="-1" role="dialog" aria-labelledby="standardModalLabel" aria-hidden="true" style="display: none;">
																			<div class="modal-dialog">
																				<div class="modal-content" style="margin-top: 100px; width: 300px;min-height: 300px; margin-left: 250px;">
																					<div class="modal-header">
																						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
																						<h4 class="modal-title" id="standardModalLabel">Select Project Domain</h4>
																					</div>
																					<div class="modal-body">
																						<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
																					</div>
																					<div class="modal-footer">
																						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																					</div>
																				</div>
																				<!-- /.modal-content -->
																			</div>
																			<!-- /.modal-dialog -->
																		</div>

                                                            <div class="form-group col-lg-6">

                                                                         <label> Project Domain <span style="color:#b81212">*</span> <button class="btn btn-default" data-toggle="modal" data-target="#standardModal4" style="  height: 20px; padding: 2px; font-size: 10px;">
																			Hint
																		</button></label>
                                                                          <select  class="form-control"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[7]) )echo "{}"; else{echo "DISABLED"; } ?> name="ExtraPrj[1][domain]" id="ExtraPrj_domain_1">
                                                                            <option value=""> Select Project Domain: </option>
                                                                            <?php 
                                                                            foreach ($m_domain as $r) {
                                                                              echo " <option value=".$r->domain_id."> ". $r->domain_name." </option>";
                                                                            }
                                                                            ?>
																			<!--<option value="99">Other</option>-->
                                                                          </select>
																		  <input class="hideme" type="text" id="otherepdomain" name="otherepdomain" size="30">
                                                                            <span id="sExtraPrj_domain_1" style="color:rgb(184,18,18);"></span>
                                                                    </div> 
                                                            </div>
                                                           <div class="form-group col-lg-12" id="ExtraPrjDesc_1">
                                                              <label>Key Deliverables & Learning</label>
                                                              <textarea class="ckeditor form-control" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[7]) )echo "{}"; else{echo "disabled"; } ?> name="ExtraPrj[1][descrption]" id="ExtraPrj_descrption_1" placeholder=" Describe your project"></textarea>
                                                              <span id="sExtraPrj_descrption_1" style="color:rgb(184,18,18);"></span>                    
                                                            </div>
                                                                                      

                                                            <div class="checkbox col-lg-12" id="divExtraStatus_1" >
                                                              <input type="checkbox" id="extra_status" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[7]) )echo "{}"; else{echo "disabled"; } ?> >
                                                              Status <span style="color:#b81212">*</span>                                                                                                                                      
                                                              <p class="help-block"><i class="fa fa-warning"></i> By clicking on this you confirm that extra project information are correct from your side</p>
                                                              <span id="sExtraPrj_1" style="color:rgb(184,18,18);"></span>
                                                            </div>
                                              </div>

                                                    <?php 

                                                    if( visiblityStatus_empty($this->session->userdata("role"),$form_status[7]) )
                                                    { ?>

                                              <div class="form-group col-lg-12">               
                                                  <button type="button" onclick="submitExtraProjects();" class="btn btn-default">
                                                    Add Projects</button>
                                              </div>

                                              <?php } ?>
                                              </form>
                                         </div>
                                         </div>
                                  </div>







                                </div>
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->
                </div>