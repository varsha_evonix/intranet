<?php 
foreach ($info as $r) { 
?>
<div class="tab-pane fade <?php if( $this->session->userdata("page_name")) { }else {echo "in active";} ?>" id="overview">
            <div class="row">
                <div class="col-lg-2 col-md-3">
                    <img class="img-responsive img-profile" src="<?php if($r->profile_picture) {echo base_url()."".$r->profile_picture;} else {echo base_url()."assets/img/avtar.jpg";} ?>" alt="">
                </div>

                  <a href="<?php echo base_url().'student/pdf/'.$r->student_id;?>"class="btn btn-default" target="_blank" download>Export as PDF</a>
                  
                <div class="col-lg-7 col-md-5">

                    <h3> <?php echo ucfirst(strtolower($r->first_name)).' '.ucfirst(strtolower($r->middle_name)).' '.ucfirst(strtolower($r->last_name));  }?> </h3>


                    <?php if($r->objactives)
                    {?>
                    <h4 class="page-header" style="border-color: #000000;">Objectives</h4>   
                    <p><?php  echo $r->objactives;?></p>
                    <?php } ?>
                </div>


                <div class="col-lg-3 col-md-4">
                    <?php 


                    foreach ($info as $r) {  ?>

                    <h3>Contact Details</h3>
                    <?php if($r->primary_email) { ?> <p><i class="fa fa-globe fa-muted fa-fw"></i> <?php echo $r->primary_email; ?></p> <?php } ?>
                    <?php if($r->allocated_email) { ?> <p><i class="fa fa-globe fa-muted fa-fw"></i> <?php echo $r->allocated_email; ?></p> <?php } ?>
                    <?php if($r->contact_no_1) { ?> <p><i class="fa fa-phone fa-muted fa-fw"></i> <?php echo $r->contact_no_1; } ?> </p> <?php } ?>
                    <?php
                    if($addlocal)
                    {
                        foreach ($addlocal as $local) 
                        { 
                    ?>

                            <p><i class="fa fa-building-o fa-muted fa-fw"></i> <?php  echo "".$local->address_detail." <br>".$local->city.",".$local->state."<br>".$local->country.",".$local->pincode."</p>"; 
                        }
                    }

                        if($r->social_linkedin_id){
                    ?>


                    <p><i class="fa fa-linkedin fa-muted fa-fw"></i>  <a href="<?php echo$r->social_linkedin_id; ?>" target="_blank"><?php echo$r->social_linkedin_id; ?></a>
                    </p>
                    <?php } ?>
                </div>
<div class="row">  


<!-- =============================================== Starting of Academic Qualification  ==================================================== -->

<?php 
    if($education)
    { 
       $temp = 0;
        foreach ($education as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }
        }

        if($temp > 0)
        {

        ?>

    <div class="col-lg-12 education ">
            <div class="col-lg-12">
                <h4 class="page-header" style="border-color: #000000;">Academic Qualification</h4>   
                    <div class="table-responsive">
                        <table class="table  table-bordered">
                            <thead>
                                <tr style="background:#C7C6C6">
                                    <th>#</th>
                                    <th>Qualification</th>                                                
                                    <th>Degree</th>
                                    <th>Year</th>
                                    <th> Board/University</th>
                                    <th>Percentage</th>
                                    <th>Division</th>                                                
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                            $counter=0; 
                                            foreach ($education as $r)
                                            {
                                                  // degree name
                                                  if($r->degree_name == 1){  $quali_name = "X"; } else if($r->degree_name == 2){  $quali_name = "XII"; } else if($r->degree_name == 3){  $quali_name = "Graduation"; } else if($r->degree_name == 4){  $quali_name = "Post-Graduation"; } else{  $quali_name = ""; }
                                                if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                                                    {
                                                        $counter++;
                                                            echo "<tr> <td> "
                                                            .$counter." </td><td>"
                                                            .$quali_name." </td><td>"
                                                            .$r->edcutation_name." </td><td>"
                                                            .$r->passing_year." </td><td>"
                                                            .$r->university_board_name." </td><td>"
                                                            .$r->percentage_cgpa." </td><td>"
                                                            .$r->division."</td></tr>"; 
                                                      }// if
                                                  }// foreach of education
                                    ?>
                            </tbody>
                        </table>
                    </div>
            </div>  
    </div> <!-- div education-->

<?php 
   }//if($education)
}//if($temp)
?>
<!-- =============================================== Ending of Academic Qualification  ==================================================== -->



<!-- =============================================== Starting of Professional Experience  ==================================================== -->

<?php

if($professional)
{
              $temp = 0;
        foreach ($professional as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }
        }
        if($temp > 0)
        {

 ?>
    <div class="col-lg-12 experience"   style='margin-top: 20px;'>
            <div class="col-lg-12">
                <?php
                    $totalExp=0;
                    foreach ($professional as $r)
                     { 
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                        {
                          $datetime1 = date_create($r->date_from);
                          $datetime2 = date_create($r->date_to);
                          $interval = date_diff($datetime1, $datetime2);
                          $exp=  $interval->m + ($interval->y * 12);
                          $totalExp += $exp;
                      }
                    }
                    ?>
            <div class="col-lg-12"> 
                <div class="row"> 
                    <div class="col-lg-9">
                       <h4 > Professional Experience </h4>              
                    </div>
                <div class="col-lg-3" style=" margin-top: 14px; text-align:right"><?php echo "( ".$totalExp." months )"; ?></div>
            </div>
            <hr style="border-color: #000000;margin-top: 0px;"/>
            </div>



                <?php
                    foreach ($professional as $r)
                     { 
                        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                        {
                          $datetime1 = date_create($r->date_from);
                          $datetime2 = date_create($r->date_to);
                          $interval = date_diff($datetime1, $datetime2);
                          $exp=  $interval->m + ($interval->y * 12);
                          $totalExp += $exp;
                    ?>

                    <div class="table-responsive">
                        <table class="table table-bordered ">
                            <tbody>
                                <?php 
                                        
                                            echo "
                                            <tr><td style='width: 150px; background-color: #C7C6C6;'><strong>Organization :</strong></td><td colspan='3' style='width: 355px;'>".$r->company_name."</td><td colspan='2' style='width: 111px;'>".$exp. " month (".date("M Y",strtotime($r->date_from))." - ".date("M Y",strtotime($r->date_to)).")". "</td></tr>
                                            <tr><td style='width: 150px; background-color: #C7C6C6;'><strong>Designation :</strong></td><td colspan='2' style='width:172px;'>".$r->job_profile."</td><td style=' width:150px; background-color: #C7C6C6;'><strong>Business Unit :</strong></td><td colspan='2' style='width: 111px;'>".$r->business_unit."</td></tr>
                                            <tr><td style='width: 150px; background-color: #C7C6C6;' align='center'>Responsibilities :</strong></td><td colspan='5' style='width:172px;'>".$r->responsibilities."</td></tr>


                                            ";
                                                                  
                                    ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } }?>
            </div>
        </div> <!-- div row experience-->   

<?php 
}//if($temp)
   }//if($professional)
?>

<!-- =============================================== Ending of Professional Experience  ==================================================== -->


<!-- =============================================== Starting of Summer Project   ==================================================== -->
<?php
    if($projects)
    { 
        $tempsummerProject = 0;
        $totalSummerExp=0;
        $totalExp=0;

        foreach ($projects as $r)
        {
                if(($r->project_type =="Summer Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                    {
                        $tempsummerProject++;
                          $datetime1 = date_create($r->start_date);
                          $datetime2 = date_create($r->end_date);
                          $interval = date_diff($datetime1, $datetime2);
                          $exp=  $interval->m + ($interval->y * 12);
                          $totalExp += $exp;

                    }
        }

        if($tempsummerProject > 0)
        {
?>


   <div class="col-lg-12 summerProject"   style='margin-top: 20px;'>
        <div class="col-lg-12">
            <div class="col-lg-12"> 
                <div class="row"> 
                    <div class="col-lg-9">
                       <h4 > Summer Project </h4>              
                    </div>
                <div class="col-lg-3" style=" margin-top: 14px; text-align:right"><?php echo "( ".$totalExp." months )"; ?></div>
            </div>
            <hr style="border-color: #000000;margin-top: 0px;"/>
            </div>
                <?php
                    foreach ($projects as $r)
                     { 
                        if(($r->project_type =="Summer Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                        {
                    ?>

                    <div class="table-responsive">
                        <table class="table table-bordered ">
                            <tbody>
                                <?php 
                                        
                                            echo "
                                            <tr><td style='width: 150px; background-color: #C7C6C6;'><strong>Organization :</strong></td><td colspan='3' style='width:300px'>".$r->organization_name."</td><td colspan='2' style='width: 111px;'>".$exp. " month (".date("M Y",strtotime($r->start_date))." - ".date("M Y",strtotime($r->end_date)).")". "</td></tr>
                                            <tr><td style='width: 150px; background-color: #C7C6C6;'><strong>Title :</strong></td><td colspan='5' style='width:172px;'>".$r->project_name."</td></tr>
                                            <tr><td style='width: 150px; background-color: #C7C6C6;'><strong>Key Deliverables & Learning :</strong></td><td colspan='5' style='width:172px;'>".$r->project_description."</td></tr>


                                            ";
                                                                  
                                    ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } }?>
            </div>
        </div> <!-- div row-->   


<?php 
    }//if($temp)
}//if($projects)
?>

<!-- =============================================== Ending of Summer Project   ==================================================== -->



<!-- =============================================== Starting of Research Project   ==================================================== -->

<?php

   if($projects)
    { 
        $tempResearchProject = 0;

        foreach ($projects as $r)
        {
            if($r->project_type =="Research Project")
            {
                if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                    {
                        $tempResearchProject++;
                    }
            }
        }

        if($tempResearchProject > 0)
        {
?>


   <div class="col-lg-12 summerProject"   style='margin-top: 20px;'>
    <div class="col-lg-12">

       <h4 > Research Project </h4>              
            <hr style="border-color: #000000;margin-top: 0px;"/>

                <?php
                    foreach ($projects as $r)
                     { 
                        if(($r->project_type =="Research Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                        {
                    ?>

                    <div class="table-responsive">
                        <table class="table table-bordered ">
                            <tbody>
                                <?php 
                                        
                                            echo "
                                            <tr><td style='width: 150px; background-color: #C7C6C6;'><strong>Title :</strong></td><td colspan='5' style='width:172px;'>".$r->project_name."</td></tr>
                                            <tr><td style='width: 150px; background-color: #C7C6C6;'><strong>Key Deliverables & Learning :</strong></td><td colspan='5' style='width:172px;'>".$r->project_description."</td></tr>


                                            ";
                                                                  
                                    ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } }?>
        </div> <!-- div row-->   
  </div> <!-- div row-->           


<?php 
    }//if($temp)
}//if($projects)


?>


<!-- =============================================== Ending of Research Project   ==================================================== -->

<!-- =============================================== Starting of Additional Courses   ==================================================== -->

<?php
if($additionalCourse)
{ 
        $temp = 0;
        foreach ($additionalCourse as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                {
                    $temp++;
                }

        }
        if($temp > 0)
        {

?>
    <div class="col-lg-12 eduCation"   style='margin-top: 20px;'>
            <div class="col-lg-12">
                <h4 class="page-header" style="border-color: #000000; margin-top:0px">Additional Courses</h4>   

                                <?php 
                                        $counter =0;
                                            foreach ($additionalCourse as $r)
                                            {

                                                if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                                                    {
                                                        $counter++;

                                                        if($counter == 1)
                                                        {       echo "<div class='col-lg-12'><ul><li>".$r->course_name." in ".$r->domain."</li> </ul></div>";}
                                                        else
                                                            {       echo "<div class='col-lg-12' style='margin-top:7px;'><ul><li>".$r->course_name." in ".$r->domain."</li> </ul></div>";}
                                                         
                                                                if($r->course_description) 
                                                                {
                                                                    echo "<div class='col-lg-12' style='margin-top:-8px;'><div class='row'><div class='col-lg-1'></div><div class='col-lg-2' style='text-align:right'> <strong>Description :</strong></div><div class='col-lg-8'>".$r->course_description."</div></div></div>";
                                                                  }   
                                                      }// if
                                                  }// foreach of professional
                                       
                                    ?>
                           
        </div>
        </div>
<?php 
    }//if($temp)
}//if($projects)

?>
<!-- =============================================== Ending of Additional Courses   ==================================================== -->



<!-- =============================================== Starting  of Assignments Projects   ==================================================== -->
<?php 

if($projects)
{ 
        $temp = 0;
        foreach ($projects as $r)
        {
            if(($r->project_type == "Assignment Project") &&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
            {
                $temp++;
            }

        }
        if($temp > 0)
        {

?>
    <div class="col-lg-12 assignmentProject"   style='margin-top: 20px;'>
            <div class="col-lg-12">
                <h4 class="page-header" style="border-color: #000000; margin-top:0px">Assignments</h4>   


                                <?php 
                                        $counter =0;
                                            foreach ($projects as $r)
                                            {

                                                if( ($r->project_type == "Assignment Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                                                    {
                                                        $counter++;

                                                        if($counter == 1)
                                                        {       
                                                            echo "<div class='col-lg-12'><ul><li>".$r->project_name." in ".$r->domain." </li></ul></div>";
                                                        }
                                                        else
                                                        {       
                                                                echo "<div class='col-lg-12' style='margin-top:7px;'><ul><li>".$r->project_name." in ".$r->domain." </li></ul></div>";
                                                        }
                                                         
                                                        if(($r->project_type == "Assignment Project")&&($r->project_description)) 
                                                        {
                                                            echo "<div class='col-lg-12' style='margin-top:-8px;'><div class='row'><div class='col-lg-1'></div><div class='col-lg-2' style='text-align:right'> <strong>Description :</strong></div><div class='col-lg-8'>".$r->project_description."</div></div></div>";
                                                          }   
                                                      }// if
                                                  }// foreach of professional
                                       
                                    ?>
        </div>
    </div>
<?php 
    }//if($temp)
}//if($projects)

?>
<!-- =============================================== Ending of Assignments Projects ==================================================== -->


<!-- =============================================== Starting of Extra Projects Undertaken ==================================================== -->
<?php 

if($projects)
{ 
        $temp = 0;
        foreach ($projects as $r)
        {
            if(($r->project_type == "Extra Project") &&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
            {
                $temp++;
            }

        }
        if($temp > 0)
        {

?>
    <div class="col-lg-12 assignmentProject"   style='margin-top: 20px;'>
            <div class="col-lg-12">
                <h4 class="page-header" style="border-color: #000000; margin-top:0px">Projects Undertaken</h4>   
                                <?php 
                                        $counter =0;
                                            foreach ($projects as $r)
                                            {

                                                if( ($r->project_type == "Extra Project")&&($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                                                    {
                                                        $counter++;

                                                        if($counter == 1)
                                                        {       echo "<div class='col-lg-12'><ul><li>".$r->project_name." in ".$r->domain."</li> </ul></div>";}
                                                        else
                                                            {       echo "<div class='col-lg-12' style='margin-top:7px;'><ul><li>".$r->project_name." in ".$r->domain."</li> </ul></div>";}
                                                         
                                                                if($r->project_description) 
                                                                {
                                                                    echo "<div class='col-lg-12' style='margin-top:-8px;'><div class='row'><div class='col-lg-1'></div><div class='col-lg-2' style='text-align:right'> <strong>Description :</strong></div><div class='col-lg-8'>".$r->project_description."</div></div></div>";
                                                                  }   
                                                      }// if
                                                  }// foreach of professional
                                    ?>
        </div>
    </div>
<?php 
    }//if($temp)
}//if($projects)

?>
<!-- =============================================== Ending of Extra Projects Undertaken==================================================== -->


<!-- =============================================== Starting of skills==================================================== -->
<?php
if($skills)
{ 
        $temp = 0;
        foreach ($skills as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
            {
                $temp++;
            }

        }
        if($temp > 0)
        {

?>
    <div class="col-lg-12 assignmentProject"   style='margin-top: 20px;'>
        <div class="col-lg-12">
            <h4 class="page-header" style="border-color: #000000; margin-top:0px">Skills Expertise and Proficiency</h4>   
                        <?php 
                                $counter =0;
                                    foreach ($skills as $r)
                                    {

                                        if( ($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                                            {
                                                $counter++;

                                                if($counter == 1)
                                                {       echo "<div class='col-lg-12'><ul><li>".$r->skill_category.' : '.$r->skill_name."</li></ul></div>";}
                                                else
                                                {       echo "<div class='col-lg-12' style='margin-top:0px;'><ul><li>".$r->skill_category.' : '.$r->skill_name."</li> </ul></div>";}
                                              }// if
                                          }// foreach of professional
                            ?>
        </div>
    </div>
<?php 
    }//if($temp)
}//if($projects)

?>
<!-- =============================================== Ending of skills ==================================================== -->


<!-- =============================================== Starting of Positions of Responsibility Held ==================================================== -->

<?php 

if($resposiblity)
{ 
        $temp = 0;
        foreach ($resposiblity as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
            {
                $temp++;
            }

        }
        if($temp > 0)
        {

?>
    <div class="col-lg-12 assignmentProject"   style='margin-top: 20px;'>
            <div class="col-lg-12">
                <h4 class="page-header" style="border-color: #000000; margin-top:0px">Positions of Responsibility Held</h4>   

                <?php 
                        $counter =0;
                            foreach ($resposiblity as $r)
                            {
                                if( ($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                                    {
                                        $counter++;

                                        if($counter == 1)
                                        {       echo "<div class='col-lg-12'><ul><li>".$r->p_r_name." in ".$r->edcutation_name." - ".$r->university_board_name." </li></ul></div>";}
                                        else
                                            {       echo "<div class='col-lg-12' style='margin-top:7px;'><ul><li>".$r->p_r_name." in ".$r->edcutation_name." - ".$r->university_board_name." </li></ul></div>";}
                                         
                                                if($r->extra_info) 
                                                {
                                                    echo "<div class='col-lg-12' style='margin-top:-8px;'><div class='row'><div class='col-lg-1'></div><div class='col-lg-2' style='text-align:right'> <strong>Description :</strong></div><div class='col-lg-8'>".$r->extra_info."</div></div></div>";
                                                  }   
                                      }// if
                                  }// foreach of professional
                    ?>

        </div>
    </div>
<?php 
    }//if($temp)
}//if($projects)

?>

<!-- =============================================== Ending of Positions of Responsibility Held ==================================================== -->




<!-- =============================================== Starting of Special Achievements / Awards ==================================================== -->

<?php 

if($awards)
{ 
        $temp = 0;
        foreach ($awards as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
            {
                $temp++;
            }

        }
        if($temp > 0)
        {

?>

    <div class="col-lg-12 assignmentProject"   style='margin-top: 20px;'>
            <div class="col-lg-12">
                <h4 class="page-header" style="border-color: #000000; margin-top:0px">Awards or Achievements</h4>   


                <?php 
                        $counter =0;
                            foreach ($awards as $r)
                            {

                                if( ($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                                    {

                                        $counter++;

                                        if($counter == 1)
                                        {     
                                          echo "<div class='col-lg-12'><ul><li>".$r->achivement_title."</li></ul></div>";
                                        }
                                        else
                                            { 
                                                    echo "<div class='col-lg-12' style='margin-top:7px;'><ul><li>".$r->achivement_title." </li></ul></div>";}
                                         
                                                if($r->achivement_description) 
                                                {
                                                    echo "<div class='col-lg-12' style='margin-top:-8px;'><div class='row'><div class='col-lg-1'></div><div class='col-lg-2' style='text-align:right'> <strong>Description :</strong> </div><div class='col-lg-8'>".$r->achivement_description."</div> </div></div>";
                                                  }   

                                      }// if
                                  }// foreach of professional
                    ?>


        </div>
    </div>
<?php 
    
    }//if($temp)
}//if($projects)


?>


<!-- =============================================== Ending of Special Achievements / Awards ==================================================== -->



<!-- =============================================== Starting of Extra Curricular Activities ==================================================== -->

<?php 

if($extraActivity)
{ 

        $temp = 0;
        foreach ($extraActivity as $r)
        {
            if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
            {
                $temp++;
            }
        }
        if($temp > 0)
        {

?>
    <div class="col-lg-12 assignmentProject"   style='margin-top: 20px;'>
            <div class="col-lg-12">
                <h4 class="page-header" style="border-color: #000000; margin-top:0px">Extra Curricular Activities</h4>   
                <?php 
                        $counter =0;
                            foreach ($extraActivity as $r)
                            {

                                if( ($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                                    {
                                        $counter++;
                                        if($counter == 1)
                                        {       echo "<div class='col-lg-12'><ul><li>".$r->activity_name."</li></ul></div>";}
                                        else
                                        {       echo "<div class='col-lg-12' style='margin-top:0px;'><ul><li>".$r->activity_name." </li></ul></div>";}
                                      }// if
                                  }// foreach of professional
                    ?>
        </div>
    </div>
<?php 
    }//if($temp)
}//if($projects)

?>


<!-- =============================================== Ending of Extra Curricular Activities ==================================================== -->


<!-- =============================================== Starting of Hobbies / Interests ==================================================== -->

<?php 
if($hobbies)
{ 
    $temp = 0;
    foreach ($hobbies as $r)
    {
        if(($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
        {
            $temp++;
        }
   }
    if($temp > 0)
    {

?>
    <div class="col-lg-12 assignmentProject"   style='margin-top: 20px;'>
            <div class="col-lg-12">
                <h4 class="page-header" style="border-color: #000000; margin-top:0px">Hobbies / Interests</h4>   
                <?php 
                        $counter =0;
                            foreach ($hobbies as $r)
                            {

                                if( ($r->verify_by_PT == 1 ) && ($r->verify_by_student == 1 ))
                                    {
                                        $counter++;
                                        if($counter == 1)
                                        {       echo "<div class='col-lg-12'><ul><li>".$r->hobby_name."</li></ul></div>";}
                                        else
                                        {       echo "<div class='col-lg-12' style='margin-top:0px;'><ul><li>".$r->hobby_name."</li> </ul></div>";}
                                    }// if
                            }// foreach of professional
                    ?>
        </div>
    </div>
<?php 
    }//if($temp)
}//if($projects)

?>
<!-- =============================================== Ending of Hobbies / Interests ==================================================== -->
</div> <!-- div row-->                                           
</div>
</div>