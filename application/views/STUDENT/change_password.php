<div class='feedback'>
      <?php if($this->session->flashdata('sucess'))
      echo "
      <script>
      sweetAlert('Ok', '".$this->session->flashdata('sucess')."', 'success');
      </script>";


      if($this->session->flashdata('error')) {
      echo "
      <script>
      sweetAlert('Opps..!', '".$this->session->flashdata('error')."', 'error');
      </script>";

      }

      ?>
</div>
<div class="tab-pane fade <?php if( $this->session->userdata("page_name")=="page_change_pwd") { echo "in active"; $this->session->unset_userdata("page_name");}else {} ?>" id="changePassword">


<?php 
  if($this->session->userdata("role") == "STUDENT")
  {
?>  
<form role="form" id="changePassword-info" method="post" action="<?php echo base_url().'student/changePassword/'.$this->session->userdata('stud_id').'/page_change_pwd/';?>" >
  <h4 class="page-header">Change Password:</h4>
      <div class="form-group">
          <label>Old Password  <span style="color:#b81212">*</span> </label>
          <!--<input type="password" class="form-control"  name="oldpwd" id="oldpwd" onfocusout="myFunction(<?php echo $this->session->userdata("stud_id"); ?>)" placeholder="Enter Old Password">-->
          <input type="password" class="form-control"  name="oldpwd" id="oldpwd" placeholder="Enter Old Password">
          <span id="soldpwd" style="color:rgb(184,18,18);"></span>
      </div>
      <div class="form-group">
          <label>New Password  <span style="color:#b81212">*</span> </label>
          <input type="password" class="form-control" name="new_pwd" id="new_pwd" placeholder="Enter New Password">
          <span id="snew_pwd" style="color:rgb(184,18,18);"></span>
      </div>
      <div class="form-group">
          <label>Re-Type New Password  <span style="color:#b81212">*</span> </label>
          <input type="password" class="form-control" name="new_c_pwd" id="new_c_pwd" placeholder="Confirm New Password">
          <span id="snew_c_pwd" style="color:rgb(184,18,18);"></span>
      </div>
      <button type="button" onclick="changePassword(<?php echo $this->session->userdata("stud_id"); ?>);" class="btn btn-default">Update Password</button>
  </form>


<?php 
}
  if(($this->session->userdata("role") == "PLACEMENT_TEAM")||($this->session->userdata("role") == "FACULTY"))
  {
?>

<form role="form" id="changePassword-info" method="post" action="<?php echo base_url().'student/changePassword/'.$this->session->userdata('stud_id').'/page_change_pwd/';?>" >
  <h4 class="page-header">Change Password:</h4>
      <div class="form-group">
          <label>New Password  <span style="color:#b81212">*</span> </label>
          <input type="password" class="form-control" name="new_pwd" id="new_pwd"  placeholder="Enter New Password">
          <span id="snew_pwd" style="color:rgb(184,18,18);"></span>
      </div>
      <div class="form-group">
          <label>Re-Type New Password  <span style="color:#b81212">*</span> </label>
          <input type="password" class="form-control" name="new_c_pwd" id="new_c_pwd" placeholder="Confirm New Password">
          <span id="snew_c_pwd" style="color:rgb(184,18,18);"></span>
      </div>
      <button type="button" onclick="changePasswordPT();" class="btn btn-default">Update Password</button>
  </form>


<?php 
}
?>

</div>