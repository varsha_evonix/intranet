<div class="tab-pane fade <?php if( $this->session->userdata("page_name")=="page_student_address") { echo "in active"; $this->session->unset_userdata("page_name");}else {} ?>" id="addressContactInformation">
<form role="form" id="student_address_info" method="post" action="<?php echo base_url().'student/updateAddress/'.$this->session->userdata('stud_id').'/page_student_address/';?>" >
    <h4 class="page-header">Address Information:</h4>

   <!-- =====================ADDRESS INFORMATION===================================== -->             
                  <?php 

                    if($addlocal)
                    {
                      foreach ($addlocal as $local)
                        { 
                    ?>
                          <h5 class="page-header">Local Address Information:</h5>
                          <input type="hidden" name="ladd[id]" value="<?php  echo $local->address_id; ?>" >                
                              <div class="form-group">
                                  <label>Address <span style="color:#b81212">*</span> </label>
                                 <textarea class="form-control" name="l_dadd" id="ladd_detail" placeholder="Enter your local address" required 
                                      <?php if( visiblityStatus($this->session->userdata("role"),$local->verify_by_student,$form_status[1]) )echo "{}";
                                        else{echo "READONLY"; } ?> > <?php  echo $local->address_detail; ?></textarea>


                                  <span id="sl_add_detail" style="color:rgb(184,18,18);"></span>                                                                                    

                              </div>

                               <div class="form-group ">
                                      <div class="form-group col-sm-6">
                                          <label>ZIP <span style="color:#b81212">*</span> </label>
                                          <input type="text" maxlength="6"  class="form-control" name="l_zip" id="lzip" required  <?php if( visiblityStatus($this->session->userdata("role"),$local->verify_by_student,$form_status[1]) )echo "{}"; else{echo "READONLY"; } ?> value="<?php  echo $local->pincode; ?>" placeholder="Enter zip code">
                                          <span id="sl_zip" style="color:rgb(184,18,18);"></span>                                                                                                                    
                                      </div>

                                      <div class="form-group  col-sm-6">
                                          <label>City <span style="color:#b81212">*</span> </label>
                                          <input type="text" class="form-control" name="l_city"  id="lcity"  required <?php if( visiblityStatus($this->session->userdata("role"),$local->verify_by_student,$form_status[1]) )echo "{}"; else{echo "READONLY"; } ?> value="<?php  echo $local->city; ?>" placeholder="Enter city" >
                                          <span id="sl_city" style="color:rgb(184,18,18);"></span>                                                                                                                    
                                      </div>
                                </div>


                              <div class="form-group">
                                          <div class="form-group col-sm-6">
                                              <label>State <span style="color:#b81212">*</span> </label>
                                              <input type="text" class="form-control" name="l_state" id="lstate" required <?php if( visiblityStatus($this->session->userdata("role"),$local->verify_by_student,$form_status[1]) )echo "{}"; else{echo "READONLY"; } ?> value="<?php  echo $local->state; ?>" placeholder="Enter state">
                                              <span id="sl_state" style="color:rgb(184,18,18);"></span>                                                                                                                        
                                          </div>


                                          <div class="form-group col-sm-6">
                                              <label>Country <span style="color:#b81212">*</span> </label>
                                              <input type="text" class="form-control" name="l_country" id="lcountry" required <?php if( visiblityStatus($this->session->userdata("role"),$local->verify_by_student,$form_status[1]) )echo "{}";else{echo "READONLY"; } ?> value="<?php  echo $local->country; ?>"  placeholder="Enter country">
                                              <span id="sl_country" style="color:rgb(184,18,18);"></span>                                                                                                                        
                                          </div>
                              </div>

                    <?php 
                      } // foreach
                        }//if

                    else{  

                          // no local address found
                        ?>

                                    <h5 class="page-header">Local Address Information:</h5>
                                    <input type="hidden" name="ladd[id]" value=null >                    
                                    <div class="form-group">
                                        <label>Address <span style="color:#b81212">*</span> </label>


                                   <textarea class="form-control" name="l_dadd" id="ladd_detail" required
                                        <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[1]) )echo "{}"; else {echo "READONLY"; } ?> 
                                        placeholder="Enter your local address"
                                        ></textarea>

                                        <span id="sl_add_detail" style="color:rgb(184,18,18);"></span>                                                                                    

                                    </div>

                                    <div class="form-group">
                                                <div class="form-group col-sm-6">
                                                    <label>Zip <span style="color:#b81212">*</span> </label>
                                                    <input type="text" maxlength="6"  class="form-control" name="l_zip" id="lzip" required <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[1]) )echo "{}"; else{echo "READONLY"; } ?> placeholder="Enter zip code">
                                                    <span id="sl_zip" style="color:rgb(184,18,18);"></span>                                    
                                                </div>


                                                <div class="form-group col-sm-6">
                                                    <label>City <span style="color:#b81212">*</span> </label>
                                                    <input type="text" class="form-control" name="l_city"  id="lcity" required <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[1]) )echo "{}";else{echo "READONLY"; } ?> laceholder="Enter city" >
                                                    <span id="sl_city" style="color:rgb(184,18,18);"></span>                                    

                                                </div>
                                    </div>
                                      <div class="form-group">
                                                <div class="form-group col-sm-6">
                                                    <label>State <span style="color:#b81212">*</span> </label>
                                                    <input type="text" class="form-control" name="l_state" id="lstate" required <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[1]) )echo "{}"; else{echo "READONLY"; } ?> placeholder="Enter state">
                                                    <span id="sl_state" style="color:rgb(184,18,18);"></span>                                                                                                     
                                                </div>


                                                <div class="form-group col-sm-6">
                                                    <label>Country <span style="color:#b81212">*</span> </label>
                                                    <input type="text" class="form-control" name="l_country" id="lcountry" required <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[1]) )echo "{}";else{echo "READONLY"; } ?> placeholder="Enter country"> 
                                                    <span id="sl_country" style="color:rgb(184,18,18);"></span>                                                                
                                                </div>
                                    </div>

                          <?php 

                              }// else

                            ?>
                
                    
            <h5 class="page-header">Permanent Address Information:</h5>


                    <?php
                      
                      if($addperm)
                      { 
                          foreach ($addperm as $perm)
                          {
                     ?>

                              <div class="form-group">
                                  <input type="hidden" name="padd_id" value="<?php  echo $perm->address_id; ?>" >
                                  <label>Address <span style="color:#b81212">*</span> </label>
                                   <textarea class="form-control" class="form-control" name="p_dadd" id="p_detail"   required <?php if( visiblityStatus($this->session->userdata("role"),$perm->verify_by_student,$form_status[1]) )echo "{}";
                                   else{echo "READONLY"; } ?> value="<?php  echo $perm->address_detail; ?>" placeholder="Enter your permanent address"><?php  echo $perm->address_detail; ?></textarea>

                                   <span id="sp_add_detail" style="color:rgb(184,18,18);"></span>                      
                              </div>

                              <div class="form-group">
                                  <div class="form-group col-sm-6">
                                         <label>ZIP <span style="color:#b81212">*</span> </label>
                                          <input type="text"  maxlength="6"  class="form-control" name="p_zip" id="p_zip" required <?php if( visiblityStatus($this->session->userdata("role"),$perm->verify_by_student,$form_status[1]) )echo "{}";else{echo "READONLY"; } ?> value="<?php  echo $perm->pincode; ?>" placeholder="Enter zip code">
                                          <span id="sp_zip" style="color:rgb(184,18,18);"></span> 
                                  </div>

                                  <div class="form-group col-sm-6">
                                          <label>City <span style="color:#b81212">*</span> </label>
                                          <input type="text" class="form-control"  name="p_city" id="p_city" required <?php if( visiblityStatus($this->session->userdata("role"),$perm->verify_by_student,$form_status[1]) )echo "{}"; else{echo "READONLY"; } ?> value="<?php  echo $perm->city; ?>" placeholder="Enter city" >
                                          <span id="sp_city" style="color:rgb(184,18,18);"></span>                                   


                                  </div>
                              </div>
                              <div class="form-group">

                                  <div class="form-group col-sm-6">
                                          <label>State <span style="color:#b81212">*</span> </label>

                                          <input type="text" class="form-control" name="p_state" id="pstate" required <?php if( visiblityStatus($this->session->userdata("role"),$perm->verify_by_student,$form_status[1]) )echo "{}"; else{echo "READONLY"; } ?> value="<?php  echo $perm->state; ?>" placeholder="Enter state">
                                              <span id="sp_state" style="color:rgb(184,18,18);"></span>                              
                                  </div>

                                  <div class="form-group col-sm-6">
                                          <label>Country <span style="color:#b81212">*</span> </label>
                                          <input type="text" class="form-control" name="p_country" id="pcountry" required <?php if( visiblityStatus($this->session->userdata("role"),$perm->verify_by_student,$form_status[1]) )echo "{}"; else{echo "READONLY"; } ?> value="<?php  echo $perm->country; ?>" placeholder="Enter country">
                                              <span id="sp_country" style="color:rgb(184,18,18);"></span>                            
                                  </div>
                              </div>  

                    <?php } //foreach
                            }//if
                            else
                            {
                              // no permanent address found

                    ?>

                    <div class="form-group">
                        <input type="hidden" name="padd[id]" value=null >                                            
                        <label>Address <span style="color:#b81212">*</span> </label>

                       <textarea class="form-control"  name="p_dadd" id="p_detail" placeholder="Enter your permanent address" required <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[1]) )echo "{}";
                         else{echo "disabled"; } ?> ></textarea>
                        <span id="sp_add_detail" style="color:rgb(184,18,18);"></span>                      
                    </div>

                    <div class="form-group">
                        <div class="form-group col-sm-6">
                               <label>ZIP <span style="color:#b81212">*</span> </label>
                                <input type="text"  maxlength="6"  class="form-control" name="p_zip" id="p_zip" required <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[1]) )echo "{}";else{echo "disabled"; } ?> placeholder="Enter zip code">
                                <span id="sp_zip" style="color:rgb(184,18,18);"></span>                                    
                        </div>

                        <div class="form-group col-sm-6">
                                <label>City <span style="color:#b81212">*</span> </label>
                                <input type="text" class="form-control"  name="p_city" id="p_city" required <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[1]) )echo "{}"; else{echo "disabled"; } ?>  placeholder="Enter city" >
                                <span id="sp_city" style="color:rgb(184,18,18);"></span>                                   
                        </div>                    
                    </div>

                    <div class="form-group">
                        <div class="form-group col-sm-6">
                               <label>State <span style="color:#b81212">*</span> </label>
                                <input type="text"  class="form-control" name="p_state" id="pstate" required <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[1]) )echo "{}";else{echo "disabled"; } ?> placeholder="Enter state">
                                    <span id="sp_state" style="color:rgb(184,18,18);"></span>                              
                        </div>

                        <div class="form-group col-sm-6">
                                <label>Country <span style="color:#b81212">*</span> </label>
                                <input type="text" class="form-control"  name="p_country" id="pcountry" required <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[1]) )echo "{}"; else{echo "disabled"; } ?>  placeholder="Enter country">
                                    <span id="sp_country" style="color:rgb(184,18,18);"></span>                            
                        </div>                    
                    </div>
                    <?php
                            }
                     ?>


                  <?php if(($addperm)&& ($addlocal))
                    {
                      foreach ($addperm as $p) 
                      {

                        foreach ($addlocal as $l) 
                        {

                         if((visiblityStatus($this->session->userdata("role"),$p->verify_by_student,$form_status[1]))||(visiblityStatus($this->session->userdata("role"),$l->verify_by_student,$form_status[1])) )
                         { 
                          ?>
                              <div class="form-group col-lg-12" >
                                  <button type="button" onclick="submitStudAddress()" class="btn btn-default" style="height: 33px;">
                                    Update Address  <address></address></button>
                              </div>
                         <?php 
                          } // if

                        }


                    } //(#addperm)
                  }//if


                  //--------- if new student

                        else if(visiblityStatus_empty($this->session->userdata("role"),$form_status[1]))
                         { 
                          ?>
                              <div class="form-group col-lg-12" >
                                  <button type="button" onclick="submitStudAddress()" class="btn btn-default" style="height: 33px;">
                                    Update Address <address></address></button>
                              </div>
                         <?php 
                          } // if


                     ?>

                </form>
       
             </form>     
          </div>