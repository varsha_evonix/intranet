  <div class='feedback'>
  <?php if($this->session->flashdata('sucess'))
   echo "
      <script>
      sweetAlert('Ok', '".$this->session->flashdata('sucess')."', 'success');
      </script>";


      if($this->session->flashdata('error')) {
   echo "
      <script>
      sweetAlert('Opps..!', '".$this->session->flashdata('error')."', 'error');
      </script>";

  }
      
   ?>
</div>
<div class="tab-pane fade <?php if( $this->session->userdata("page_name")=="page_skills") { echo "in active"; $this->session->unset_userdata("page_name");}else {} ?>" id="skillsInformation">
  <form role="form" id="skills_info" method="post" action="<?php echo base_url().'student/addSkills/'.$this->session->userdata('stud_id').'/page_skills/';?>" enctype="multipart/form-data">

<h4 class="page-header"> Skills Expertise and Proficiency Information:</h4>
<?php if($skills)
 { ?>
      <div class="row">
                  <div class="col-lg-12">
                  <div class="portlet portlet-green">
                  <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4> Skills expertise and proficiency information</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive" style="overflow-y: hidden; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover" >
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Status by student</th>
                                                <th>Check Status by Placement team</th>
                                               <?php if($this->session->userdata("role") == "PLACEMENT_TEAM") {echo "<th>Delete</th> ";} else{
                                               	 if( visiblityStatus_empty($this->session->userdata("role"),$form_status[12]) )echo "<th>Delete</th> "; } ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php $counter=0; foreach ($skills as $r ) {
                                                  $counter ++;
                                                                                                 //show lable to student
                                                
                                                 if($this->session->userdata("role") == "PLACEMENT_TEAM")
                                                  {
                                                      if($r->verify_by_student == 1) {  $temp="<span class='badge green'>Approved</span>";} else {  $temp="<span class='badge orange'>Pending</span>"; }
                                                  }
                                                  else
                                                  {
                                                   if($r->verify_by_student == 2) {  $temp="<a href='".base_url()."student/std_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_skills/".$r->skills_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>";} else {  $temp="<span class='badge green'>Approved</span>"; }                                                 
                                                  }

                                                  //show lable to placement team
                                                  if($r->verify_by_PT == 1) {  $temp1="<span class='badge green'>Approved</span>";} else if($r->verify_by_PT == 2) {  $temp1="<span class='badge orange'>Pending</span>"; }else {  $temp1="<span class='badge red'>Rejected</span>"; }

                                                  //show link to placement team

                                                  if($r->verify_by_PT == 1) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_skills/".$r->skills_id."' style='cursor:pointer'><span class='badge green'>Approved</span></a>";} else if($r->verify_by_PT == 2) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_skills/".$r->skills_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>"; }else { $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_skills/".$r->skills_id."' style='cursor:pointer'><span class='badge red'>Rejected</span></a>"; }                                                  


                                              if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->skill_category.' : '.$r->skill_name." </td><td>"
                                                .$temp." </td><td>"
                                                .$link." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                                   <a onclick=deleteStudentInformation('page_skills',".$r->skills_id.") title='Delete This Skill Information' style='cursor:pointer;'>
                                                   <i class='fa fa-times'></i>
                                                  </a></div></td></tr>
                                                </tr>"; 

                                                }
                                                else
                                                {
                                                if( visiblityStatus_empty($this->session->userdata("role"),$form_status[12]) )
                                                {
                                                echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->skill_category.' : '.$r->skill_name." </td><td>"
                                                .$temp." </td><td>"
                                                .$temp1." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                                   <a onclick=deleteStudentInformation('page_skills',".$r->skills_id.") title='Delete This Skill Information' style='cursor:pointer;'>
                                                   <i class='fa fa-times'></i>
                                                  </a></div></td></tr>";
                                                }	
                                                else
                                                {
                                                 echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->skill_category.' : '.$r->skill_name." </td><td>"
                                                .$temp." </td><td>"
                                                .$temp1." </td></tr>";
                                                }
                                                
                                                }//else

                                                }//foreach

                                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
              </div>
              <?php } ?>


<h4 class="page-header">Add new skill</h4>

<div class="form-group countSkill  col-lg-12 ">
            <div class="form-group col-lg-6">
               <label>Skill Category <span style="color:#b81212">*</span> </label>
                <!--<input type="text" class="form-control" <?php //if( visiblityStatus_empty($this->session->userdata("role"),$form_status[12]) )echo "{}"; else{echo "READONLY"; } ?> name="skill[1][category]" id="skillCategory_1" placeholder="Enter Category" required>-->
				<select class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[12]) )echo "{}"; else{echo "DISABLED"; } ?> name="skill[1][category]" id="skillCategory_1" required="">
					<option value="">Select Category</option>
					<option value="Tools">Tools</option>
					<option value="Certifications">Certifications</option>
					<option value="Language">Language</option>
          <option value="Language Proficiency">Language Proficiency</option>
					<option value="Other">Other</option>
                </select>
                <span id="sskillCategory_1" style="color:rgb(184,18,18);"></span>
            </div>
            <div class="form-group col-lg-6">
               <label>Name <span style="color:#b81212">*</span> </label>
                <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[12]) )echo "{}"; else{echo "READONLY"; } ?> name="skill[1][name]" id="skillName_1" placeholder="Enter Name" required>
                <span id="sskillName_1" style="color:rgb(184,18,18);"></span>
            </div>
    </div>
	
<div class="form-group col-lg-12 ">
            <div class="checkbox col-lg-6"  id="divSkillStatus_1">
               <input type="checkbox" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[12]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?> name="skill[1][status]" id="skillStatus_1" required>
               Status <span style="color:#b81212">*</span> </label>
              <p class="help-block"><i class="fa fa-warning"></i> By clicking on this you confirm that skills information are correct from your side</p>               
               <span id="sskillStatus_1" style="color:rgb(184,18,18);"></span>
            </div>
    </div> <!-- countEdu  -->



      <?php
        if( visiblityStatus_empty($this->session->userdata("role"),$form_status[12]) )
        { 

        ?>
          <div class="form-group col-lg-12">               
              <button type="button" onclick="submitSkills()" class="btn btn-default">
                Add Skill</button>
          </div>

          <?php } ?>
</form>
</div>

