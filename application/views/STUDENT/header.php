<?php
if($this->session->userdata("role")== "STUDENT" )
{ 
    echo "student"
?>
        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">
            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand" style="color:white">
                    SITM
                </div>
            </div>
            <!-- end BRAND HEADING -->

            <div class="nav-top">

             
                <!-- begin MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->
                <ul class="nav navbar-right">

                    <!-- begin MESSAGES DROPDOWN -->
                    <li class="dropdown">
                        
                    </li>
                    <!-- /.dropdown -->
                    <!-- end MESSAGES DROPDOWN -->

                    <!-- begin ALERTS DROPDOWN -->
                    <li class="dropdown">
                        
                    </li>
                    <!-- /.dropdown -->
                    <!-- end ALERTS DROPDOWN -->

                    <!-- begin TASKS DROPDOWN -->
                    <li class="dropdown">
                        
                    </li>
                    <!-- /.dropdown -->
                    <!-- end TASKS DROPDOWN -->

                    <!-- begin USER ACTIONS DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#">
                                    <i class="fa fa-user"></i> My Profile
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/switchUser">
                                    <i class="fa fa-gear"></i> Switch account
                                </a>
                            </li>
                            <li>
                                <a class="logout_open" href="#logout">
                                    <i class="fa fa-sign-out"></i> Logout
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- end USER ACTIONS DROPDOWN -->

                </ul>
                <!-- /.nav -->
                <!-- end MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->

            </div>
            <!-- /.nav-top -->
        </nav>
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
               <nav class="navbar-side" role="navigation">
            <div class="navbar-collapse sidebar-collapse collapse">
                <ul id="side" class="nav navbar-nav side-nav">
                    <!-- begin SIDE NAV USER PANEL -->


                    <?php 
                    foreach ($info as $r) { 
                        ?>

                    <li class="side-user hidden-xs">
<!--                         <img class="img-circle" src="<?php echo base_url();?>assets/img/profile-pic.jpg" alt=""> -->


                       <img class="img-circle" src="<?php if($r->profile_picture) {echo base_url()."".$r->profile_picture;} else {echo base_url()."assets/img/avtar.jpg";} ?>" alt="" 
                                        style=" height: 160px; width: 155px;  margin-left: 15px;" >





                        <p class="welcome">
                            <i class="fa fa-key"></i> Logged in as
                        </p>
                            <p class="name tooltip-sidebar-logout">
                                Student <br/>
                                <span class="last-name"><?php echo ucfirst(strtolower($r->first_name)).' '.ucfirst(strtolower($r->last_name)); ?></span> <a style="color: inherit" class="logout_open" href="#logout" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></a>
                            </p>
                            <div class="clearfix"></div>
                        </li>
                    <!-- end SIDE NAV USER PANEL -->
                    <?php } ?>

                    <!-- begin DASHBOARD LINK -->
                    <li>
                        <a id="dashboard" href="<?php echo base_url();?>student">
                            <i class="fa fa-dashboard"></i> Dashboard
                        </a>
                    </li>
                    <li>
                        <a  id="studentProfile" href="<?php echo base_url();?>student/profile/<?php echo $this->session->userdata("student_id"); ?>">
                            <i class="fa fa-user"></i> Profile
                        </a>
                    </li>
                    <li>
                        <a  id="jobs" href="<?php echo base_url();?>student/viewjobs/<?php echo $this->session->userdata("student_id"); ?>">
                            <i class="fa fa-suitcase"></i> View Jobs
                        </a>
                    </li>
                    <li>
                        <a  id="ppmreport" href="<?php echo base_url();?>student/ppmreport/<?php echo $this->session->userdata("student_id"); ?>">
                            <i class="fa fa-files-o"></i> PPM Report
                        </a>
                    </li>
                                     
                                        
                  
                  <!-- end PAGES DROPDOWN -->
                </ul>
                <!-- /.side-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

<?php 
}// if student
        else
// placement team
{?>

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">
            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand" style="color:white">
                    SITM
                </div>
            </div>
            <!-- end BRAND HEADING -->

            <div class="nav-top">

             
                <!-- begin MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->
                <ul class="nav navbar-right">

                    <!-- begin MESSAGES DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" class="messages-link dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-envelope"></i>
                            <span class="number">4</span> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-scroll dropdown-messages">

                            <!-- Messages Dropdown Heading -->
                            <li class="dropdown-header">
                                <i class="fa fa-envelope"></i> 4 New Messages
                            </li>

                            <!-- Messages Dropdown Body - This is contained within a SlimScroll fixed height box. You can change the height using the SlimScroll jQuery features. -->
                            <li id="messageScroll">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#">
                                            <div class="row">
                                                <div class="col-xs-2">
                                                    <img class="img-circle" src="<?php echo base_url();?>assets/img/user-profile-1.jpg" alt="">
                                                </div>
                                                <div class="col-xs-10">
                                                    <p>
                                                        <strong>Jane Smith</strong>: Hi again! I wanted to let you know that the order...
                                                    </p>
                                                    <p class="small">
                                                        <i class="fa fa-clock-o"></i> 12 minutes ago
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                 
                                </ul>
                            </li>

                            <!-- Messages Dropdown Footer -->
                            <li class="dropdown-footer">
                                <a href="#">Read All Messages</a>
                            </li>

                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- end MESSAGES DROPDOWN -->

                    <!-- begin ALERTS DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" class="alerts-link dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-bell"></i> 
                            <span class="number">9</span><i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-scroll dropdown-alerts">

                            <!-- Alerts Dropdown Heading -->
                            <li class="dropdown-header">
                                <i class="fa fa-bell"></i> 9 New Alerts
                            </li>

                            <!-- Alerts Dropdown Body - This is contained within a SlimScroll fixed height box. You can change the height using the SlimScroll jQuery features. -->
                            <li id="alertScroll">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#">
                                            <div class="alert-icon green pull-left">
                                                <i class="fa fa-money"></i>
                                            </div>
                                            Order #2931 Received
                                            <span class="small pull-right">
                                                <strong>
                                                    <em>3 minutes ago</em>
                                                </strong>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <div class="alert-icon blue pull-left">
                                                <i class="fa fa-comment"></i>
                                            </div>
                                            New Comments
                                            <span class="badge blue pull-right">15</span>
                                        </a>
                                    </li>
                                   
                                </ul>
                            </li>

                            <!-- Alerts Dropdown Footer -->
                            <li class="dropdown-footer">
                                <a href="#">View All Alerts</a>
                            </li>

                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- end ALERTS DROPDOWN -->

                    <!-- begin TASKS DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" class="tasks-link dropdown-toggle" data-toggle=dropdown>
                            <i class="fa fa-tasks"></i> 
                            <span class=number>10</span><i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-scroll dropdown-tasks">

                            <!-- Tasks Dropdown Header -->
                            <li class="dropdown-header">
                                <i class="fa fa-tasks"></i> 10 Pending Tasks
                            </li>

                            <!-- Tasks Dropdown Body - This is contained within a SlimScroll fixed height box. You can change the height using the SlimScroll jQuery features. -->
                            <li id="taskScroll">
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="#">
                                            <p>
                                                Software Update 2.1
                                                <span class="pull-right">
                                                    <strong>60%</strong>
                                                </span>
                                            </p>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"></div>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <p>
                                                Server #2 Hardware Upgrade
                                                <span class="pull-right">
                                                    <strong>90%</strong>
                                                </span>
                                            </p>
                                            <div class="progress progress-striped">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;"></div>
                                            </div>
                                        </a>
                                    </li>
                                     <li>
                                        <a href="#">
                                            <p>
                                                Server #2 Hardware Upgrade
                                                <span class="pull-right">
                                                    <strong>90%</strong>
                                                </span>
                                            </p>
                                            <div class="progress progress-striped">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;"></div>
                                            </div>
                                        </a>
                                    </li>
                                 
                                 
                                </ul>
                            </li>

                            <!-- Tasks Dropdown Footer -->
                            <li class="dropdown-footer">
                                <a href="#">View All Tasks</a>
                            </li>

                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- end TASKS DROPDOWN -->

                    <!-- begin USER ACTIONS DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#">
                                    <i class="fa fa-user"></i> My Profile
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-envelope"></i> My Messages
                                    <span class="badge green pull-right">4</span>
                                </a>
                            </li>
                         
                            <li>
                                <a href="#">
                                    <i class="fa fa-calendar"></i> My Calendar
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/switchUser">
                                    <i class="fa fa-gear"></i> Switch account
                                </a>
                            </li>
                            <li>
                                <a class="logout_open" href="#logout">
                                    <i class="fa fa-sign-out"></i> Logout
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- end USER ACTIONS DROPDOWN -->

                </ul>
                <!-- /.nav -->
                <!-- end MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->

            </div>
            <!-- /.nav-top -->
        </nav>
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
               <nav class="navbar-side" role="navigation">
            <div class="navbar-collapse sidebar-collapse collapse">
                <ul id="side" class="nav navbar-nav side-nav">
                    <?php 
                    foreach ($pt_info as $r) { 
                        ?>
                    <!-- begin SIDE NAV USER PANEL -->
                    <li class="side-user hidden-xs">
<!--                         <img class="img-circle" src="<?php echo base_url();?>assets/img/profile-pic.jpg" alt=""> -->
                       <img class="img-circle" src="<?php if($r->profile_picture) {echo base_url()."".$r->profile_picture;} else {echo base_url()."assets/img/avtar.jpg";} ?>" alt="" 
                                        style=" height: 160px; width: 155px;  margin-left: 15px;" >
                        <p class="welcome">
                            <i class="fa fa-key"></i> Logged in as
                        </p>
                        <p class="name tooltip-sidebar-logout">
                            Placement Team 
                            <span class="last-name">
                                <?php       echo ucfirst(strtolower($r->first_name)).' '.ucfirst(strtolower($r->last_name)); ?> 
                            </span> <a style="color: inherit" class="logout_open" href="#logout" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></a>
                        </p>
                        <div class="clearfix"></div>
                    </li>
                                        <?php } ?>

                    <!-- end SIDE NAV USER PANEL -->

                    <!-- begin DASHBOARD LINK -->
                    <li>
                        <a id="dashboard" href="<?php echo base_url();?>placementteam">
                            <i class="fa fa-dashboard"></i> Dashboard
                        </a>
                    </li>
                    <!-- end DASHBOARD LINK -->
                   
                    <!-- end MESSAGE CENTER DROPDOWN -->
                    <!-- begin PAGES DROPDOWN -->
                    <li class="panel">
                        <a id="manageOrganization" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#pages">
                            <i  class="fa fa-hospital-o"></i> Manage Organization<i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="pages">
                            <li>
                                <a id="addOrganization" href="<?php echo base_url();?>placementteam/addorganization">
                                    <i class="fa fa-angle-double-right"></i>Add Organization
                                </a>
                            </li>
                            <li>
                                <a id="mngOrganization" href="<?php echo base_url();?>placementteam/allorganization">
                                    <i class="fa fa-angle-double-right"></i>Manage Organization
                                </a>
                            </li>                            
                        </ul>
                    </li>

                    <li>
                           <a id="manageForms" href="<?php echo base_url();?>placementteam/forms">
                            <i class="fa fa-files-o"></i> Manage Forms
                        </a>
                    </li>

                    <li>
                            <a id="manageStudents" href="<?php echo base_url();?>placementteam/students">
                            <i class="fa fa-group"></i> Manage Students
                        </a>
                    </li>

        <li class="panel">
                        <a id="managejobs" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#pages2">
                            <i class="fa fa-suitcase"></i> Manage Jobs <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="pages2">
                            <li>
                                <a id="addjob" href="<?php echo base_url();?>placementteam/addjob">
                                    <i class="fa fa-angle-double-right"></i>Add Job
                                </a>
                            </li>
<!--                                 <li>
                                <a id="managejobs" href="<?php echo base_url();?>placementteam/managejobs">
                                    <i class="fa fa-angle-double-right"></i>Manage Jobs
                                </a>
                            </li> -->
                            <li>
                                <a id="viewjobs" href="<?php echo base_url();?>placementteam/viewappliedjobs">
                                    <i class="fa fa-angle-double-right"></i> View Applied Jobs
                                </a>
                            </li>                            
                        </ul>
                    </li>
                 
                </ul>
                <!-- /.side-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->
<?php }?>