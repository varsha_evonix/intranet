<div class='feedback'>
<?php if($this->session->flashdata('sucess'))
 echo "
    <script>
    sweetAlert('Ok', '".$this->session->flashdata('sucess')."', 'success');
    </script>";


    if($this->session->flashdata('error')) {
 echo "
    <script>
    sweetAlert('Opps..!', '".$this->session->flashdata('error')."', 'error');
    </script>";
}
 ?>
</div>


<div class="tab-pane fade <?php if( $this->session->userdata("page_name")=="page_responsiblities")  { echo "in active"; $this->session->unset_userdata("page_name");}else {} ?>" id="possiotionOrResponsiblityInformation">
<form role="form" id="poss_res_info" method="post" action="<?php echo base_url().'student/addResponsblity/'.$this->session->userdata('stud_id').'/page_responsiblities/';?>" enctype="multipart/form-data">  
<h4 class="page-header">Position or Responsibility Information</h4>
<?php if($resposiblity)
 {    ?>

      <div class="row">
                  <div class="col-lg-12">
                  <div class="portlet portlet-green">
                  <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Position or responsibility information</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive" style="overflow-y: hidden; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover" >
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Position or Responsibility</th>
                                                <!--<th>When</th>-->
                                                <th>Responsibilities</th>                                                
                                                <th>Status by student</th>
                                                <th>Check Status by Placement team</th>
                                               <?php if($this->session->userdata("role") == "PLACEMENT_TEAM") {echo "<th>Delete</th> ";} else{
                                               	 if( visiblityStatus_empty($this->session->userdata("role"),$form_status[10]) )echo "<th>Delete</th> "; }  ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php $counter=0; foreach ($resposiblity as $r ) {
                                                  $counter ++;

                                                 //show lable to student
                                                
                                                 if($this->session->userdata("role") == "PLACEMENT_TEAM")
                                                  {
                                                      if($r->verify_by_student == 1) {  $temp="<span class='badge green'>Approved</span>";} else {  $temp="<span class='badge orange'>Pending</span>"; }
                                                  }
                                                  else
                                                  {
                                                   if($r->verify_by_student == 2) {  $temp="<a href='".base_url()."student/std_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_responsiblities/".$r->p_r_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>";} else {  $temp="<span class='badge green'>Approved</span>"; }                                                 
                                                  }

                                                  //show lable to placement team
                                                  if($r->verify_by_PT == 1) {  $temp1="<span class='badge green'>Approved</span>";} else if($r->verify_by_PT == 2) {  $temp1="<span class='badge orange'>Pending</span>"; }else {  $temp1="<span class='badge red'>Rejected</span>"; }

                                                  //show link to placement team

                                                  if($r->verify_by_PT == 1) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_responsiblities/".$r->p_r_id."' style='cursor:pointer'><span class='badge green'>Approved</span></a>";} else if($r->verify_by_PT == 2) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_responsiblities/".$r->p_r_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>"; }else { $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_responsiblities/".$r->p_r_id."' style='cursor:pointer'><span class='badge red'>Rejected</span></a>"; }                                                  

                                                  $ResponsblityDetail="<div id='P_Responsblitydetails_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Responsibilities</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->extra_info."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='functionResponsblityDetails(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a></div>";



                                              /*if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->p_r_name." </td><td>"
                                                .$r->edcutation_name."-".$r->university_board_name." </td><td>"
                                                .$ResponsblityDetail." </td><td>" 
                                                .$temp." </td><td>"
                                                .$link." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                               <a onclick=deleteStudentInformation('page_responsiblities',".$r->p_r_id.") title='Delete This Additional Possiotion or Responsiblity Information'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>
                                                </tr>";

                                                }
                                                else
                                                {
                                                 echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->p_r_name." </td><td>"
                                                .$r->edcutation_name." - " .$r->university_board_name."</td><td>"
                                                .$ResponsblityDetail." </td><td>"                                                                     .$temp." </td><td>"
                                                .$temp1." </td></tr>";
                                                }*/
												
												if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->p_r_name." </td><td>"
                                                .$ResponsblityDetail." </td><td>" 
                                                .$temp." </td><td>"
                                                .$link." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                               <a onclick=deleteStudentInformation('page_responsiblities',".$r->p_r_id.") title='Delete This Additional Possiotion or Responsiblity Information' style='cursor:pointer;'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>
                                                </tr>";

                                                }
                                                else
                                                {
                                                	if( visiblityStatus_empty($this->session->userdata("role"),$form_status[10]) )
	 
	 {
	 	echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->p_r_name." </td><td>"
                                                .$ResponsblityDetail." </td><td>"                                                                     .$temp." </td><td>"
                                                .$temp1." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                               <a onclick=deleteStudentInformation('page_responsiblities',".$r->p_r_id.") title='Delete This Additional Possiotion or Responsiblity Information' style='cursor:pointer;'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>";
	 
	 }
	 else
	 {
                                                 echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->p_r_name." </td><td>"
                                                .$ResponsblityDetail." </td><td>"                                                                     .$temp." </td><td>"
                                                .$temp1." </td></tr>";
                                             }
                                                
                                                }
												
												//else

                                                }//foreach

                                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
              </div>
              <?php } ?>


<h4 class="page-header">Add new position or responsibility</h4>

<div class="form-group countRespo  col-lg-12 ">
              <div class="form-group col-lg-6">
               <label>Position Name <span style="color:#b81212">*</span> </label>
                <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[10]) )echo "{}"; else{echo "READONLY"; } ?> name="respo[1][name]" id="respoName_1" placeholder="Enter position name" required>
                <span id="srespoName_1" style="color:rgb(184,18,18);"></span>
            </div>
            <div class="form-group col-lg-6">
               <label>When you take position</label>
                <select  class="form-control"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[10]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?> name="respo[1][edu]" id="respoEdu_1" required>
                  <option value=""> Select Education </option>
                  <?php 
                  foreach ($education as $r) {
                    echo " <option value=".$r->edu_id."> ". $r->edcutation_name." - ". $r->university_board_name. " </option>";
                  }
                  ?>
                </select>
                <span id="srespoEdu_1" style="color:rgb(184,18,18);"></span>                
            </div>

            <div class="form-group col-lg-12">
               <label>Responsibilities</label>
                  <textarea  name="respo[1][responsblities]" id="P_Respondblities_1" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[10]) )echo "{}"; else{echo "disabled"; } ?>></textarea> 
                 <span id="sP_Respondblities_1" style="color:rgb(184,18,18);"></span>

            </div>
            <div class="checkbox col-lg-12"  id="divRespoStatus_1">
               <input type="checkbox" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[10]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?> name="respo[1][status]" id="respoStatus_1" required>
               Status <span style="color:#b81212">*</span> </label>
              <p class="help-block"><i class="fa fa-warning"></i> By clicking on this you confirm that position and responsibility information are correct from your side</p>
               <span id="srespoStatus_1" style="color:rgb(184,18,18);"></span>
            </div>
    </div> <!-- countEdu  -->

               <?php 
        if( visiblityStatus_empty($this->session->userdata("role"),$form_status[10]) )
        { 

        ?>

          <div class="form-group col-lg-12">               
              <button type="button" onclick="submitResponsiblity()" class="btn btn-default">
                Add Responsibility</button>
          </div>
          <?php } ?>
</form>
</div>