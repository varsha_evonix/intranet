<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("alllinks.php"); ?>
  
</head>

<body>
<div id="wrapper">

        <?php include("header.php"); ?>

          <div id="page-wrapper">

            <div class="page-content">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>
                                No authority
                                <small>Trying to access unauthorized page/function</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url();?>student/index">Dashboard</a>
                                </li>
                                <li class="active">No authority</li>
                            </ol>
                        </div>
                    </div>

  <div class="col-lg-6 col-lg-offset-3">
<!--                         <h1 class="error-title">500</h1> -->
                        <h1 class="error-msg"><i class="fa fa-warning text-red"></i> No authority</h>
                        <p class="lead">Sorry !!! <br/> You don't have authority <br/> or <br/>
                                You are trying to access unauthorized page/function</p>

                    </div>
                    <!-- /.col-lg-6 -->
                </div>
                <!-- /.row -->

            </div>
              <?php include("alljs.php"); ?>   

    


</body>

</html>
