<div class='feedback'>
<?php if($this->session->flashdata('sucess'))
 echo "
    <script>
    sweetAlert('Ok', '".$this->session->flashdata('sucess')."', 'success');
    </script>";


    if($this->session->flashdata('error')) {
 echo "
    <script>
    sweetAlert('Opps..!', '".$this->session->flashdata('error')."', 'error');
    </script>";

}
    
 ?>
</div>
<div class="tab-pane fade <?php if( $this->session->userdata("page_name")=="page_awards") { echo "in active"; $this->session->unset_userdata("page_name");}else {} ?>"  id="awardsInformation">
<form role="form" id="awards_info" method="post" action="<?php echo base_url().'student/addAwards/'.$this->session->userdata('stud_id').'/page_awards/';?>" enctype="multipart/form-data">
<h4 class="page-header">Awards or Achievements Information:</h4>
<?php if($awards)
 { ?>
      <div class="row">
                  <div class="col-lg-12">
                  <div class="portlet portlet-green">
                  <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Awards or achievements information</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive" style="overflow-y: hidden; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover" >
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Award or Achievement Name</th>
                                                <th>Description</th>
                                                <th>Document</th>                                                
                                                <th>Status by student</th>
                                                <th>Check Status by Placement team</th>
                                               <?php if($this->session->userdata("role") == "PLACEMENT_TEAM") {echo "<th>Delete</th> ";} else{
                                               	 if( visiblityStatus_empty($this->session->userdata("role"),$form_status[11]) )echo "<th>Delete</th> "; }  ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php $counter=0; foreach ($awards as $r ) {
                                                  $counter ++;
                                                  
                                                                                                 //show lable to student
                                                
                                                 if($this->session->userdata("role") == "PLACEMENT_TEAM")
                                                  {
                                                      if($r->verify_by_student == 1) {  $temp="<span class='badge green'>Approved</span>";} else {  $temp="<span class='badge orange'>Pending</span>"; }
                                                  }
                                                  else
                                                  {
                                                   if($r->verify_by_student == 2) {  $temp="<a href='".base_url()."student/std_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_awards/".$r->achivement_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>";} else {  $temp="<span class='badge green'>Approved</span>"; }                                                 
                                                  }

                                                  //show lable to placement team
                                                  if($r->verify_by_PT == 1) {  $temp1="<span class='badge green'>Approved</span>";} else if($r->verify_by_PT == 2) {  $temp1="<span class='badge orange'>Pending</span>"; }else {  $temp1="<span class='badge red'>Rejected</span>"; }

                                                  //show link to placement team

                                                  if($r->verify_by_PT == 1) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_awards/".$r->achivement_id."' style='cursor:pointer'><span class='badge green'>Approved</span></a>";} else if($r->verify_by_PT == 2) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_awards/".$r->achivement_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>"; }else { $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_awards/".$r->achivement_id."' style='cursor:pointer'><span class='badge red'>Rejected</span></a>"; }                                                  
                                             
                                                  if($r->document_link){ $Documentlink = "<a target='_blank' href='".base_url()."".$r->document_link."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink = "";}


                                                  $awardDetails="<div id='awardDetails_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Project descrption</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->achivement_description."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='functionawardDetails(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a></div>";

                                              if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->achivement_title." </td><td>"
                                                .$awardDetails." </td><td>"
                                                .$Documentlink." </td><td>"                                                
                                                .$temp." </td><td>"
                                                .$link." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                               <a onclick=deleteStudentInformation('page_awards',".$r->achivement_id.") title='Delete This Awards Information' style='cursor:pointer;'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>
                                                </tr>"; 
                                                }
                                                else
                                                {
                                                if( visiblityStatus_empty($this->session->userdata("role"),$form_status[11]) )
                                                {
                                                echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->achivement_title." </td><td>"
                                                .$awardDetails."</td><td>"
                                                .$Documentlink." </td><td>"                                                
                                                .$temp." </td><td>"
                                                .$temp1." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                               <a onclick=deleteStudentInformation('page_awards',".$r->achivement_id.") title='Delete This Awards Information' style='cursor:pointer;'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>";
                                                }	
                                                else
                                                {	
                                                 echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->achivement_title." </td><td>"
                                                .$awardDetails."</td><td>"
                                                .$Documentlink." </td><td>"                                                
                                                .$temp." </td><td>"
                                                .$temp1." </td></tr>";
                                                }
                                                
                                                }//else

                                                }//foreach

                                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
              </div>
              <?php } ?>


<h4 class="page-header">Add new award or achievement</h4>

<div class="form-group countAwards  col-lg-12 ">

              <div class="form-group col-lg-12">
               <label>Awards or Achievements Name <span style="color:#b81212">*</span> </label>
                <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[11]) )echo "{}"; else{echo "READONLY"; } ?> name="award[1][name]" id="awardName_1" placeholder="Enter Award or Achievements Name" required>
                <span id="sawardName_1" style="color:rgb(184,18,18);"></span>
            </div>
  
            <div class="form-group col-lg-12">
               <label>Description</label>
               <textarea class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[11]) )echo "{}"; else{echo "READONLY"; } ?> name="award[1][ext_info]" id="awardExtraInfo_1" placeholder="Value added information" required></textarea>
                <span id="sawardExtraInfo_1" style="color:rgb(184,18,18);"></span>               
            </div>
  
  <div class="form-group countAwards  col-lg-12 ">
            <div class="form-group col-lg-6"  id="divAwardDoc_1">
            <label>Document</label>
                    <input type="file" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[11]) )echo "{}"; else{echo "disabled"; }  ?>  name="award[1][doc]" id="awardDoc_1" required>
            <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                    <span id="sawardDoc_1" style="color:rgb(184,18,18);"></span>
            </div>
            <div class="checkbox col-lg-6"  id="divAwardStatus_1">
               <input type="checkbox" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[11]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?> name="award[1][status]" id="awardStatus_1" required>
               Status <span style="color:#b81212">*</span> 
              <p class="help-block"><i class="fa fa-warning"></i> By clicking on this you confirm that awards or Achievements information are correct from your side</p>
               <span id="sawardStatus_1" style="color:rgb(184,18,18);"></span>
            </div>
    </div> <!-- countAward  -->

</div>

        <?php 

          if( visiblityStatus_empty($this->session->userdata("role"),$form_status[11]) )
          { 
          ?>



          <div class="form-group col-lg-12">               
              <button type="button" onclick="submitAwards()" class="btn btn-default" >
                Add Awards </button>
          </div>

          <?php } ?>
</form>
</div>

