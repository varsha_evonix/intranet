<div class="tab-pane fade <?php if( $this->session->userdata("page_name")=="page_edu") { echo "in active"; $this->session->unset_userdata("page_name"); }else {} ?>" id="educationalInformation">
<form role="form" id="education-info" method="post" action="<?php echo base_url().'student/addEducation/'.$this->session->userdata('stud_id').'/page_edu/';?>" enctype="multipart/form-data">

<style type="text/css">
.hideDiv{
  display:  none;
}
</style>

<h4 class="page-header">Academic Qualification Information:</h4>
<?php //print_r($education); ?>
<?php if($education)
 { ?>
      <div class="row">
                  <div class="col-lg-12">
                  <div class="portlet portlet-green">
                  <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Academic Qualification information</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive" style="overflow-y: hidden; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover" >
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Qualification</th>                                                
                                                <th>Degree</th>
                                                <th>Year</th>
                                                <th> Board/University</th>
                                                <th>Percentage</th>
                                                <th>Division</th>                                                
                                                <th>Document 1</th>
												<th>Document 2</th>
												<th>Document 3</th>
												<th>Document 4</th>
												<th>Document 5</th>
												<th>Document 6</th>
												<th>Document 7</th>
												<th>Document 8</th>
												<th>Document 9</th>
												<th>Document 10</th>
                                                <th>Status by Student</th>
                                                <th>Status by Placement team</th>
                                               <?php if($this->session->userdata("role") == "PLACEMENT_TEAM") 
                                               {
                                               echo "<th>Delete</th> ";
                                               }else{
                                               	 if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "<th>Delete</th> "; }
                                               	    ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php $counter=0; foreach ($education as $r ) {
                                                  $counter ++;
                                                  //show lable to student
                                                 if($this->session->userdata("role") == "PLACEMENT_TEAM")
                                                  {
                                                      if($r->verify_by_student == 1) {  $temp="<span class='badge green'>Approved</span>";} else {  $temp="<span class='badge orange'>Pending</span>"; }
                                                  }
                                                  else
                                                  {
                                                   if($r->verify_by_student == 2) {  $temp="<a href='".base_url()."student/std_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_edu/".$r->edu_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>";} else {  $temp="<span class='badge green'>Approved</span>"; }                                                 
                                                  }

                                                  //show lable to placement team
                                                  if($r->verify_by_PT == 1) {  $temp1="<span class='badge green'>Approved</span>";} else if($r->verify_by_PT == 2) {  $temp1="<span class='badge orange'>Pending</span>"; }else {  $temp1="<span class='badge red'>Rejected</span>"; }


                                                  if($r->verify_by_PT == 1) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_edu/".$r->edu_id."' style='cursor:pointer'><span class='badge green'>Approved</span></a>";} else if($r->verify_by_PT == 2) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_edu/".$r->edu_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>"; }else { $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_edu/".$r->edu_id."' style='cursor:pointer'><span class='badge red'>Rejected</span></a>"; }                                                  

                                                  // degree name
                                                  if($r->degree_name == 1){  $quali_name = "X"; } else if($r->degree_name == 2){  $quali_name = "XII"; } else if($r->degree_name == 3){  $quali_name = "Graduation"; } else if($r->degree_name == 4){  $quali_name = "Post-Graduation"; } else{  $quali_name = ""; }

                                                  if($r->document_link){ $Documentlink = "<a target='_blank' href='".base_url()."".$r->document_link."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink = "";}

												  if($r->document_link2){ $Documentlink2 = "<a target='_blank' href='".base_url()."".$r->document_link2."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink2 = "";}

												  if($r->document_link3){ $Documentlink3 = "<a target='_blank' href='".base_url()."".$r->document_link3."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink3 = "";}

												  if($r->document_link4){ $Documentlink4 = "<a target='_blank' href='".base_url()."".$r->document_link4."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink4 = "";}

												  if($r->document_link5){ $Documentlink5 = "<a target='_blank' href='".base_url()."".$r->document_link5."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink5 = "";}

												  if($r->document_link6){ $Documentlink6 = "<a target='_blank' href='".base_url()."".$r->document_link6."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink6 = "";}

												  if($r->document_link7){ $Documentlink7 = "<a target='_blank' href='".base_url()."".$r->document_link7."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink7 = "";}

												  if($r->document_link8){ $Documentlink8 = "<a target='_blank' href='".base_url()."".$r->document_link8."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink8 = "";}

												  if($r->document_link9){ $Documentlink9 = "<a target='_blank' href='".base_url()."".$r->document_link9."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink9 = "";}

												  if($r->document_link10){ $Documentlink10 = "<a target='_blank' href='".base_url()."".$r->document_link10."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink10 = "";}

                                              if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$quali_name." </td><td>"
                                                .$r->edcutation_name." </td><td>"
                                                .$r->passing_year." </td><td>"
                                                .$r->university_board_name." </td><td>"
                                                .$r->percentage_cgpa." </td><td>"
                                                .$r->division." </td><td>"                                                
                                                .$Documentlink." </td><td>"
												.$Documentlink2." </td><td>"
												.$Documentlink3." </td><td>"
												.$Documentlink4." </td><td>"
												.$Documentlink5." </td><td>"
												.$Documentlink6." </td><td>"
												.$Documentlink7." </td><td>"
												.$Documentlink8." </td><td>"
												.$Documentlink9." </td><td>"
												.$Documentlink10." </td><td>"
                                                .$temp." </td><td>"
                                                .$link." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                               <a onclick=deleteStudentInformation('page_edu',".$r->edu_id.") title='Delete This Qualification Information' style='cursor:pointer;'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>
                                                </tr>";

                                                }
                                                else
                                                {
                                                	
	 if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )
	 
	 {
	 
	 
                                               	
                                                 echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$quali_name." </td><td>"                                                
                                                .$r->edcutation_name." </td><td>"
                                                .$r->passing_year." </td><td>"
                                                .$r->university_board_name." </td><td>"
                                                .$r->percentage_cgpa." </td><td>"
                                                .$r->division." </td><td>"
                                                .$Documentlink." </td><td>"
												.$Documentlink2." </td><td>"
												.$Documentlink3." </td><td>"
												.$Documentlink4." </td><td>"
												.$Documentlink5." </td><td>"
												.$Documentlink6." </td><td>"
												.$Documentlink7." </td><td>"
												.$Documentlink8." </td><td>"
												.$Documentlink9." </td><td>"
												.$Documentlink10." </td><td>"
                                                .$temp." </td><td>"
                                                .$temp1." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                               <a onclick=deleteStudentInformation('page_edu',".$r->edu_id.") title='Delete This Qualification Information' style='cursor:pointer;'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>";	 
	 
	 }else{                                                	
                                                	
                                                 echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$quali_name." </td><td>"                                                
                                                .$r->edcutation_name." </td><td>"
                                                .$r->passing_year." </td><td>"
                                                .$r->university_board_name." </td><td>"
                                                .$r->percentage_cgpa." </td><td>"
                                                .$r->division." </td><td>"
                                                .$Documentlink." </td><td>"
												.$Documentlink2." </td><td>"
												.$Documentlink3." </td><td>"
												.$Documentlink4." </td><td>"
												.$Documentlink5." </td><td>"
												.$Documentlink6." </td><td>"
												.$Documentlink7." </td><td>"
												.$Documentlink8." </td><td>"
												.$Documentlink9." </td><td>"
												.$Documentlink10." </td><td>"
                                                .$temp." </td><td>"
                                                .$temp1." </td></tr>";
                                               }
                                                }//else

                                                }//foreach

                                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
              </div>
              <?php } ?>


<h4 class="page-header">Add new academic qualification</h4>

<input type="hidden" id="studnet_id" value="<?php  echo $this->session->userdata('stud_id'); ?>">

<div class="form-group countEdu  col-lg-12 ">
        <div class="col-lg-12">
          <div class="row">

             <div class="form-group col-lg-6">
               <label>Qualification<span style="color:#b81212">*</span> </label>
                <select  class="form-control"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "READONLY"; echo " disabled"; } ?> name="education[1][edegree]" id="edegree_1" required>
                  <option value=""> Select Qualification </option>
                  <option value="1"> X </option>
                  <option value="2"> XII </option>
                  <option value="3"> Graduation </option>
                  <option value="4"> Post-Graduation </option>
                </select>
                <span id="sedegree_1" style="color:rgb(184,18,18);"></span>
              </div>
 
              <div class="form-group col-lg-6">

               <label>Degree<span style="color:#b81212">*</span> </label>
                <select  class="form-control"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "READONLY"; echo " disabled"; } ?> name="education[1][ename]" id="ename_1" onchange="changeDegree();" required>
                  <option value=""> Select Degree </option>
                  <?php 
                  foreach ($m_edu as $r) {
                    echo " <option value=".$r->education_id."> ". $r->edcutation_name." </option>";
                  }
                                      echo " <option value='other'>Other</option>";
                  ?>
                </select>
                <span id="sname_1" style="color:rgb(184,18,18);"></span>

            </div>

               <div class="form-group col-lg-12 hideDiv" id="div_other_course" >
                <label>Degree Name <span style="color:#b81212">*</span> </label>                
                  <input class="form-control" type='text' placeholder='Enter degree name' name='other_degree' id='other_degree' onfocusout="checkDegree()" visiblity=false>
                    <input type="hidden" id="existDegreeName">
                <span id="sother_degree" style="color:rgb(184,18,18);"></span>
            </div> 
          </div>
        </div>

        <div class="form-group col-lg-12">
           <label>Board or University <span style="color:#b81212">*</span> </label>
            <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "READONLY"; } ?> name="education[1][bordUni]" id="bordUni_1" placeholder="Enter Board or University name" required>
            <span id="sbordUni_1" style="color:rgb(184,18,18);"></span>                
        </div>


        <div class="col-lg-12">
        <div class="row">

            <div class="form-group col-lg-6">
               <label>Percentage <i>(xx.xx)</i> <span style="color:#b81212">*</span> </label>
                <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "READONLY"; } ?> name="education[1][perc_cgpa]" id="perc_cgpa_1" placeholder="Enter Percentage" required>

                <span id="sperc_cgpa_1" style="color:rgb(184,18,18);"></span>
            </div>

             <div class="form-group col-lg-6">
               <label>Division<span style="color:#b81212">*</span> </label>
                <select  class="form-control"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?> name="education[1][division]" id="edudivision_1" required>
                  <option value=""> Select Division </option>
                  <option value="Distinction"> Distinction </option>
                  <option value="First"> First </option>
                  <option value="Second"> Second </option>
                  <option value="Third"> Third </option>
                </select>
                <span id="sedudivision_1" style="color:rgb(184,18,18);"></span>
            </div>

          </div>
        </div>


        <div class="col-lg-12">
          <div class="row">

              <div class="form-group col-lg-6">
                 <label>Passing Year <i>(yyyy)</i> <span style="color:#b81212">*</span> </label>
                  <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "READONLY"; } ?> name="education[1][pass_year]" id="pass_year_1" placeholder="Enter year of passing" onfocusout="checkQualificationYear();"  required list="paasYear">
                  <input type="hidden"  id="previouce_year" >
                  <span id="spass_year_1" style="color:rgb(184,18,18);"></span>                

              </div>

        </div>
      </div>
	  
	  <div class="col-lg-12" id="divDoc_1">
          <div class="row">

              <div class="form-group col-lg-7">

                  <label>Document 1</label>
                          <input type="file" name="education[1][doc]" id="doc_1"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; }  ?> required>
                          
                     <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                      <span id="sdoc_1" style="color:rgb(184,18,18);"></span>
              </div>
			  <div class="form-group col-lg-2 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreAcadDocument(2)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa ">+</i>Add</a>
				</div>

        </div>
      </div>
	  
	  <div class="col-lg-12" id="divDoc_2" style="display:none;">
          <div class="row">

              <div class="form-group col-lg-7">

                  <label>Document 2</label>
                          <input type="file" name="education2[1][doc]" id="doc_2"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; }  ?> required>
                          
                     <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                      <span id="sdoc_2" style="color:rgb(184,18,18);"></span>
              </div>
			  <div class="form-group col-lg-2 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreAcadDocument(3)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-plus"></i>Add</a>
				</div>
				<div class="form-group col-lg-3 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreAcadDocument(2)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
				</div>

        </div>
      </div>
	  
	  <div class="col-lg-12" id="divDoc_3" style="display:none;">
          <div class="row">

              <div class="form-group col-lg-7">

                  <label>Document 3</label>
                          <input type="file" name="education3[1][doc]" id="doc_3"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; }  ?> required>
                          
                     <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                      <span id="sdoc_3" style="color:rgb(184,18,18);"></span>
              </div>
			  <div class="form-group col-lg-2 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreAcadDocument(4)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-plus"></i>Add</a>
				</div>
				<div class="form-group col-lg-3 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreAcadDocument(3)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
				</div>

        </div>
      </div>
	  
	  <div class="col-lg-12" id="divDoc_4" style="display:none;">
          <div class="row">

              <div class="form-group col-lg-7">

                  <label>Document 4</label>
                          <input type="file" name="education4[1][doc]" id="doc_4"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; }  ?> required>
                          
                     <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                      <span id="sdoc_4" style="color:rgb(184,18,18);"></span>
              </div>
			  <div class="form-group col-lg-2 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreAcadDocument(5)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-plus"></i>Add</a>
				</div>
				<div class="form-group col-lg-3 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreAcadDocument(4)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
				</div>

        </div>
      </div>
	  
	  <div class="col-lg-12" id="divDoc_5" style="display:none;">
          <div class="row">

              <div class="form-group col-lg-7">

                  <label>Document 5</label>
                          <input type="file" name="education5[1][doc]" id="doc_5"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; }  ?> required>
                          
                     <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                      <span id="sdoc_5" style="color:rgb(184,18,18);"></span>
              </div>
			  <div class="form-group col-lg-2 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreAcadDocument(6)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-plus"></i>Add</a>
				</div>
				<div class="form-group col-lg-3 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreAcadDocument(5)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
				</div>

        </div>
      </div>
	  
	  <div class="col-lg-12" id="divDoc_6" style="display:none;">
          <div class="row">

              <div class="form-group col-lg-7">

                  <label>Document 6</label>
                          <input type="file" name="education6[1][doc]" id="doc_6"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; }  ?> required>
                          
                     <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                      <span id="sdoc_6" style="color:rgb(184,18,18);"></span>
              </div>
			  <div class="form-group col-lg-2 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreAcadDocument(7)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-plus"></i>Add</a>
				</div>
				<div class="form-group col-lg-3 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreAcadDocument(6)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
				</div>

        </div>
      </div>
	  
	  <div class="col-lg-12" id="divDoc_7" style="display:none;">
          <div class="row">

              <div class="form-group col-lg-7">

                  <label>Document 7</label>
                          <input type="file" name="education7[1][doc]" id="doc_7"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; }  ?> required>
                          
                     <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                      <span id="sdoc_7" style="color:rgb(184,18,18);"></span>
              </div>
			  <div class="form-group col-lg-2 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreAcadDocument(8)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-plus"></i>Add</a>
				</div>
				<div class="form-group col-lg-3 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreAcadDocument(7)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
				</div>

        </div>
      </div>
	  
	  <div class="col-lg-12" id="divDoc_8" style="display:none;">
          <div class="row">

              <div class="form-group col-lg-7">

                  <label>Document 8</label>
                          <input type="file" name="education8[1][doc]" id="doc_8"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; }  ?> required>
                          
                     <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                      <span id="sdoc_8" style="color:rgb(184,18,18);"></span>
              </div>
			  <div class="form-group col-lg-2 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreAcadDocument(9)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-plus"></i>Add</a>
				</div>
				<div class="form-group col-lg-3 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreAcadDocument(8)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
				</div>

        </div>
      </div>
	  
	  <div class="col-lg-12" id="divDoc_9" style="display:none;">
          <div class="row">

              <div class="form-group col-lg-7">

                  <label>Document 9</label>
                          <input type="file" name="education9[1][doc]" id="doc_9"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; }  ?> required>
                          
                     <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                      <span id="sdoc_9" style="color:rgb(184,18,18);"></span>
              </div>
			  <div class="form-group col-lg-2 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreAcadDocument(10)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-plus"></i>Add</a>
				</div>
				<div class="form-group col-lg-3 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreAcadDocument(9)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
				</div>

        </div>
      </div>
	  
	  <div class="col-lg-12" id="divDoc_10" style="display:none;">
          <div class="row">

              <div class="form-group col-lg-7">

                  <label>Document 10</label>
                          <input type="file" name="education10[1][doc]" id="doc_10"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; }  ?> required>
                          
                     <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                      <span id="sdoc_10" style="color:rgb(184,18,18);"></span>
              </div>
			  <div class="form-group col-lg-3 social-buttons">
					<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreAcadDocument(10)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
				</div>

        </div>
      </div>

        <div class="checkbox col-lg-12"  id="divStatus_1">
           <input type="checkbox" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; } ?> name="education[1][status]" id="status_1" required>
           Status <span style="color:#b81212">*</span> 
           <p class="help-block"><i class="fa fa-warning"></i> By clicking on this you confirm that academic qualification information are correct from your side</p>
           <span id="sstatus_1" style="color:rgb(184,18,18);"></span>
        </div>
    </div> <!-- countEdu  -->

        <?php 
            if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) ) {?>
        <div class="form-group col-sm-12 ">
          <button type="button" onclick="submitEducation()" class="btn btn-default" >
                Add Qualification</button>
        </div>
        <?php } ?>


              <datalist id="paasYear">
          <?php 
                  $today = date("Y", strtotime('+1 year'));
                  for($i=1956; $i<=$today;$i++)
                  {
                    echo "<option value=".$i.">";
                  }
                ?>
              </datalist>



</form>
</div>
<script type="text/javascript">
function addMoreAcadDocument(div_id)
	{
        $("#divDoc_"+div_id).css("display", "block");
    }
	
function delMoreAcadDocument(option_id)
        {
            if(confirm('Are you sure you want to delete?')){
			$("#divDoc_"+option_id).css("display", "none");
            }
        }
</script>