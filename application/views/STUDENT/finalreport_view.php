<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>



<body>
<style>
    .form-group .required .control-label:after {
  content:"*";color:red;
}
label{
	font-weight: normal;
}
</style>
<div id="wrapper">

		<?php include("header.php"); ?>


        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>PPM Student Report
                                <small>PPM Student Report</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> 
								<a href="<?php echo base_url() ?>student/">Dashboard</a></li>
                                <li class="active">PPM Student Report</li>
                                    
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
				
				<?php
				if($this->session->flashdata('successmessage'))
				{
					$successmessage = $this->session->flashdata('successmessage');
					echo "<script>swal('Ok', '$successmessage', 'success')</script>";
				}
				else if($this->session->flashdata('errormessage'))
				{
					$errormessage = $this->session->flashdata('errormessage');
					echo "<script>swal('Opps..!', '$errormessage', 'error')</script>";
				}
				?>
				
                <!-- end PAGE TITLE AREA -->
				<div class="row">

                    <!-- Basic Responsive Table -->
                    <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <!--<h4>Student PPM Report</h4>-->
                                    <h4>Student PRN : <?php echo $studinfo->PRN; ?> Student Name : <?php echo $studinfo->first_name.' '.$studinfo->middle_name.' '.$studinfo->last_name; ?></h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                            <?php if($report1) {?>
                               <div style="font-weight:bold;margin-bottom: 5px;">Rating Scale Competencies: </div>
                               <div style="margin: 0 0 10px 44px;">5 => <span style="font-weight:bold;">Excellent</span>, 4 => <span style="font-weight:bold;">Above Expectations</span>, 3 => <span style="font-weight:bold;">Meets Expectations</span>, 2 => <span style="font-weight:bold;">Below Expectations</span>, 1 => <span style="font-weight:bold;">Poor</span> </div>
                                <div class="table-responsive">
								<?php
									//print_r($stud_id);
									//print_r($srinfo);
								?>
							
                                    <table class="table table-bordered">
										<thead>
                                            <tr>
                                                <th colspan="3">Work Ex Project / Engineering Project</th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            <tr>
                                                <th style="width: 25%;">Parameters</th>
                                                <th style="width: 38%;">PPM1</th>
                                                <th style="width: 36%;">PPM2</th>
                                                <!-- <th>Meets Expectations</th>
												<th>Below Expectations</th>
                                                <th>Poor</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td >Content / Project Knowledge </td>
                                                <td><label><?php if(isset($report1->ep_cpk)) echo $report1->ep_cpk; ?></label></td>
                                                <td><label><?php if(isset($report2->ep_cpk)) echo $report2->ep_cpk; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Company Knowledge </td>
                                                <td><label><?php echo $report1->ep_ck; ?></label></td>
                                                <td><label><?php if(isset($report2->ep_ck)) echo $report2->ep_ck; ?></label></td>
                                               
                                            </tr>
                                            <tr>
                                                <td>Domain Knowledge </td>
                                                <td><label><?php echo $report1->ep_dk; ?></label></td>
                                                <td><label><?php if(isset($report2->ep_dk)) echo $report2->ep_dk; ?></label></td>
                                                
                                            </tr>
											<tr>
                                                <td>Overall Remarks </td>
                                                <td><label><?php echo $report1->ep_remarks; ?></label></td>
                                                <td ><label><?php if(isset($report2->ep_remarks)) echo $report2->ep_remarks; ?></label></td>
                                            </tr>
                                        </tbody>
                                    </table>
									<hr>
									<table class="table table-bordered">
										<thead>
                                            <tr>
                                                <th colspan="3">Summer Project</th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            <tr>
                                                <th style="width: 25%;">Parameters</th>
                                                <th style="width: 38%;">PPM1</th>
                                                <th style="width: 36%;">PPM2</th>
                                            </tr>
                                        </thead>
										
                                        <tbody>
                                            <tr>
                                                <td>Content / Project Knowledge </td>
                                                <td><label><?php echo $report1->sp_cpk; ?></label></td>
                                                <td><label><?php if(isset($report2->sp_cpk)) echo $report2->sp_cpk; ?></label></td>
                                               
                                            </tr>
                                            <tr>
                                                <td>Company Knowledge </td>
                                                <td><label><?php echo $report1->sp_ck; ?></label></td>
                                                <td><label><?php if(isset($report2->sp_ck)) echo $report2->sp_ck; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Domain Knowledge </td>
                                                <td><label><?php echo $report1->sp_dk; ?></label></td>
                                                <td><label><?php if(isset($report2->sp_dk)) echo $report2->sp_dk; ?></label></td>
                                                
                                            </tr>
											<tr>
                                                <td>Overall Remarks </td>
                                                <td><label><?php echo $report1->sp_remarks; ?></label></td>
                                                <td><label><?php if(isset($report2->sp_remarks)) echo $report2->sp_remarks; ?></label></td>
                                            </tr>
                                        </tbody>
                                    </table>
									<hr>
									<table class="table table-bordered">
										<thead>
                                            <tr>
                                                <th colspan="3">Major Competencies</th>
                                            </tr>
                                        </thead>
                                        <thead>
                                            <tr>
                                                <th style="width: 65%;">Parameters</th>
                                                <th style="width: 18%;">PPM1</th>
                                                <th style="width: 17%;">PPM2</th>
                                            </tr>
                                        </thead>
										
										<thead>
                                            <tr>
                                                <th colspan="3">Technical Proficiency</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Knowledge About Certifications </td>
                                                <td><label><?php echo $report1->tp_certiknowledge; ?></label></td>
                                                <td><label><?php if(isset($report2->tp_certiknowledge)) echo $report2->tp_certiknowledge; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Basic IT and Telecom Technologies Knowledge </td>
                                                <td><label><?php echo $report1->tp_basicitteleknowledge; ?></label></td>
                                                <td><label><?php if(isset($report2->tp_basicitteleknowledge)) echo $report2->tp_basicitteleknowledge; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Market Awareness / Industry Knowledge </td>
                                                <td><label><?php echo $report1->tp_industryknowledge; ?></label></td>
                                                <td><label><?php if(isset($report2->tp_industryknowledge)) echo $report2->tp_industryknowledge; ?></label></td>
                                                
                                            </tr>
                                        </tbody>
										<thead>
                                            <tr>
                                                <th colspan="3">HR Assessment</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Analytical / Logical Approach </td>
                                                <td><label><?php echo $report1->hra_logicalapproach; ?></label></td>
                                                <td><label><?php if(isset($report2->hra_logicalapproach)) echo $report2->hra_logicalapproach; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Situation Based Performance </td>
                                                <td><label><?php echo $report1->hra_situbasedperformance; ?></label></td>
                                                <td><label><?php if(isset($report2->hra_situbasedperformance)) echo $report2->hra_situbasedperformance; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Attitude(Sincerity, Honesty, Trainability, Values, Enthusiasm etc.) </td>
                                                <td><label><?php echo $report1->hra_attitude; ?></label></td>
                                                <td><label><?php if(isset($report2->hra_attitude)) echo $report2->hra_attitude; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Communication Skills(Accent, Presentation of Content and Pace, Substantiating Answers) </td>
                                                <td><label><?php echo $report1->hra_commskills; ?></label></td>
                                                <td><label><?php if(isset($report2->hra_commskills)) echo $report2->hra_commskills; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Self Confidence and Poise(Body Language) </td>
                                                <td><label><?php echo $report1->hra_selfconfidence; ?></label></td>
                                                <td><label><?php if(isset($report2->hra_selfconfidence)) echo $report2->hra_selfconfidence; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Presentation </td>
                                                <td><label><?php echo $report1->hra_presentation; ?></label></td>
                                                <td><label><?php if(isset($report2->hra_presentation)) echo $report2->hra_presentation; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td><b>Strengths</b> </td>
                                                <td><label><?php echo $report1->strengths; ?></label></td>
                                                <td><label><?php if(isset($report2->strengths)) echo $report2->strengths; ?></label></td>
                                            </tr>
                                            <tr>
                                                <td><b>Weaknesses</b> </td>
                                                <td><label><?php echo $report1->weaknesses; ?></label></td>
                                                <td><label><?php if(isset($report2->weaknesses)) echo $report2->weaknesses; ?></label></td>
                                            </tr>
                                        </tbody>
                                        <thead>
                                            <tr>
                                                <th colspan="3">GD</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Participation level (x.x) </td>
                                                <td><label><?php echo $report1->gd_pl; ?></label></td>
                                                <td><label><?php if(isset($report2->gd_pl)) echo $report2->gd_pl; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Situational Reactions (x.x) </td>
                                                <td><label><?php echo $report1->gd_reactions; ?></label></td>
                                                <td><label><?php if(isset($report2->gd_reactions)) echo $report2->gd_reactions; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Content Quality (x.x) </td>
                                                <td><label><?php echo $report1->gd_quality; ?></label></td>
                                                <td><label><?php if(isset($report2->gd_quality)) echo $report2->gd_quality; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Communication Skills (x.x) </td>
                                                <td><label><?php echo $report1->gd_skills; ?></label></td>
                                                <td><label><?php if(isset($report2->gd_skills)) echo $report2->gd_skills; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Total </td>
                                                <td><label><?php echo $report1->gd_total; ?></label></td>
                                                <td><label><?php if(isset($report2->gd_total)) echo $report2->gd_total; ?></label></td>
                                                
                                            </tr>
                                            <tr>
                                                <td>Remarks </td>
                                                <td><label><?php echo $report1->gd_remarks; ?></label></td>
                                                <td><label><?php if(isset($report2->gd_remarks)) echo $report2->gd_remarks; ?></label></td>
                                                
                                            </tr>
                                        </tbody>
                                    </table>
								
							
                                </div>
                                <?php } else{ echo "No Report has been submitted";}?>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->

                </div>
    </div>	
	</div>
	
    <!-- /#wrapper -->
    </div>
<?php include("alljs.php"); ?>
	 <script src=" <?php echo base_url(); ?>assets/js/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo/advanced-form-demo.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
			$("#rating_form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					ep_cpk:
					{
						required: true
					},
					ep_ck:
					{
						required: true
					},
					ep_dk:
					{
						required: true
					},
					ep_remarks:
					{
						required: true
					},
					sp_cpk:
					{
						required: true
					},
					sp_ck:
					{
						required: true
					},
					sp_dk:
					{
						required: true
					},
					sp_remarks:
					{
						required: true
					},
					tp_certiknowledge:
					{
						required: true
					},
					tp_basicitteleknowledge:
					{
						required: true
					},
					tp_industryknowledge:
					{
						required: true
					},
					hra_logicalapproach:
					{
						required: true
					},
					hra_situbasedperformance:
					{
						required: true
					},
					hra_attitude:
					{
						required: true
					},
					hra_commskills:
					{
						required: true
					},
					hra_selfconfidence:
					{
						required: true
					},
					hra_presentation:
					{
						required: true
					},
				},
									
				// Messages for form validation
				messages:
				{
					ep_cpk:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:15px;">Please give rating</span>'
					}
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					//error.insertAfter(element.parent('#anstr'));
					error.appendTo(element.parent(".req"));
					//error.appendTo(element.parents('#anstr').prev('tr'));
				}
				
				
			});
		});
	</script>
	<script>
		$("#ppmreport").addClass("active");
	
	</script>
</body>

</html>