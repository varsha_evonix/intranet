<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
	<!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">
	
	<link href="<?php echo base_url();?>assets/css/jquery.datetimepicker.css" rel="stylesheet">
</head>

<body>
<div id="wrapper">

		<?php include("header.php"); ?>

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>View Jobs
                                <small>View All Jobs</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="#">Dashboard</a>
                                </li>
                                <li class="active">View Jobs</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
             
                   
				
					<?php
				if($this->session->flashdata('successmessage'))
				{
					$successmessage = $this->session->flashdata('successmessage');
					echo "<script>swal('Ok', '$successmessage', 'success')</script>";
				}
				else if($this->session->flashdata('errormessage'))
				{
					$errormessage = $this->session->flashdata('errormessage');
					echo "<script>swal('Opps..!', '$errormessage', 'error')</script>";
				}
				?>

                   <div class="row">
                   	              
                    <!-- /.col-lg-12 -->
             

                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                    <div class="col-lg-12">
                        <h4>View Jobs</h4>

                        <div class="row pricing-basic">
							<?php 
							if($data)
							{
								$counter=0;
							foreach($data as $r)
							{
								
								$counter++;
							$old_date = $r->last_date_to_applied;
							$old_date_timestamp = strtotime($old_date);
							$new_date = date('jS', $old_date_timestamp);
							$new_month = date('M', $old_date_timestamp);
							$new_year = date('Y', $old_date_timestamp);
							$date=time();
							?> <div class="col-md-3">
                                <ul class="plan plan1" style="text-align: left; border-bottom:1">
							<?php if($r->job_type=="PPO") { ?>
                           
                                    <li class="plan-name" style="font-size:14px; text-align: center; background:#444!important;">
                                        <?php echo $r->offered_profile; ?>
                                    </li>
							<?php } else {?>
							         <li class="plan-name" style="font-size:14px; text-align: center;  ">
                                        <?php echo $r->offered_profile; ?>
                                    </li>
							<?php } ?>
                                    <li class="plan-price">
                                        <b style="color:#333;">Company Name : </b><b style="color:#34495e;"><?php echo $r->organization_name; ?></b>
                                    </li>
                                    <li class="plan-price" style="border-top: 1px solid #E2DCDC;">
                                        <b style="color:#333;">Total Requirements : </b><b style="color:#34495e;"><?php echo $r->total_requirements; ?></b>
                                    </li>
									<li class="plan-price" style="border-top: 1px solid #E2DCDC;">
                                        <b style="color:#333;">Selection Mode : </b><b style="color:#34495e;"><?php echo $r->selection_mode; ?></b>
                                    </li>
									<li class="plan-price" style="border-top: 1px solid #E2DCDC;">
                                        <b style="color:#333;">Package : </b><b style="color:#34495e;"><?php echo $r->package_tobe_offered; ?></b>
                                    </li>
									
									<li class="plan-price" style="border-top: 1px solid #E2DCDC;">
                                        <b style="color:#333;">Apply Date: </b><b style="color:#34495e;"><?php echo $new_date.' '.$new_month.' '.$new_year; ?></b>
                                    </li>
									   
                                        <li class="plan-price" style="border-top: 1px solid #E2DCDC;"><b style="color:#333;">Job Description: </b><b style="color:#34495e;">
											
										<?php echo"
			<div id='jobdescription_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Job Description</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->job_description."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='showJobDescription(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a>";							
										
										
										
										
										?></b>
										
									   </li>
                                    <li class="plan-action" style="border-top: 1px solid #E2DCDC; text-align: center;">

									    <?php 

									    $jobid=$r->job_id;
									    $sid = $this->session->userdata('student_id');
										$query = mysql_query("select * from tbl_job_applied where job_id = '$jobid' and  student_id = $sid ");
							if(mysql_num_rows($query)== 0)
							{  if($old_date_timestamp > $date)
								{?>
                                        <a href="<?php echo base_url();?>student/applyjob/<?php echo $r->job_id; ?>/<?php echo $this->session->userdata("student_id"); ?>" class="btn btn-default btn-lg" style="padding: 5px 15px; font-size:14px;">Apply</a>
								<?php } else {?>
							   <a class="btn btn-default btn-lg" style="padding: 5px 15px; font-size:14px; cursor:default;border-color: #db4839;
  color: #fff;
  background-color: #e74c3c;">Job Expired</a>
							<?php } }
							  else
							  {
								  
								?>
                                <a class="btn btn-default btn-lg" style="padding: 5px 15px; font-size:14px; cursor:default; background:#149077; border-color:#138871;">Applied</a>
								<?php  foreach($info1 as $i) {
							  if($i->job_id==$r->job_id) echo $i->selection_process; } ?>
                              <?php								
								  }  ?>
                                    </li>
                                </ul>
                            </div><?php
							}
							}
							else
							{
								echo "<h4>No job available</h4>";
							}?>
                            <!--/.col-md-3-->
                        </div>
                        <!--/.pricing-basic-->

                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
<!-- /#wrapper -->

    <?php include("alljs.php"); ?>
	
	<!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/plugins/summernote/summernote.min.js"></script>
	
    <!-- THEME SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/flex.js"></script>
    <script src="<?php echo base_url();?>assets/js/demo/wysiwyg-demo.js"></script>
	
	<!-- for datetimepicker -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.datetimepicker.js"></script>
	<script type="text/javascript">
		$('#datetimepicker').datetimepicker({
		dayOfWeekStart : 1,
		lang:'en'
		});
	</script>
	
	<!-- for ckeditor -->
    <script src="<?php echo base_url();?>assets/js/lib/ckeditor/ckeditor.js"></script>
	<script type="text/javascript">
		CKEDITOR.replace('jobdescription',{
		toolbar :
		[
			{ name: 'basicstyles', items : [ 'Bold','Italic','Underline' ] },
			{ name: 'paragraph', items : [ 'NumberedList','BulletedList' ] }
		]
		});
	</script>
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					organizationname:
					{
						required: true,
						min:1
					},
					jobprofile:
					{
						required: true
					},
					totalrequirements:
					{
						required: true,
						number: true
					},
					selectionmode:
					{
						selectcheck: true
					},
					offerpackage:
					{
						selectcheck: true
					},
					datetimepicker:
					{
						required: true
					}
				},
									
				// Messages for form validation
				messages:
				{
					organizationname:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please select organization name</span>',
						min: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please select organization name</span>'
					},
					jobprofile:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter job profile name</span>'
					},
					totalrequirements:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter total requirements</span>',
						number: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please enter numbers only</span>'
					},
					selectionmode:
					{
						selectcheck: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please select selection mode</span>'
					},
					offerpackage:
					{
						selectcheck: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please select package</span>'
					},
					datetimepicker:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:265px;">Please select last date to apply</span>'
					}
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				},
				
				submitHandler: function(form) {
						$("#err_jobdesc").hide();
						//alert();
						if ($.trim($(CKEDITOR.instances.jobdescription.getData()).text()) == '') {
							$("#err_jobdesc").show();
							CKEDITOR.instances.jobdescription.focus();
						}
						
						//alert($(".bootstrap-tagsinput .tag").length);
						
						if($("em[id^='err_']:visible").length == 0)
						{
							form.submit();
						}
					
					}
				
			});
			
			/* for selectionmode select box */
			jQuery.validator.addMethod('selectcheck', function (value) {
				return (value != '0');
			}, "required");
			
		});
	</script>
<script type="text/javascript">
	function showJobDescription(id){
        $("#jobdescription_"+id).modal('show'); 
        
    }
</script>
	<script type="text/javascript">
		$("#jobs").addClass("active");
	</script>

</body>

</html>