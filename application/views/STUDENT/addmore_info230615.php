<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>

<script type="text/javascript">

function myFunction(sid) {
            var oldpwd =  $("#oldpwd").val()
                                   $.ajax({
            url: "<?php echo base_url();?>student/checkOldPwd/",
            type: "POST",
            async: true, 
            data: {oldpwd:oldpwd,sid:sid}, //your form data to post goes here
             success: function(response){ 
                    if(response == "true")
                    {   
                        $("#soldpwd").html("");
                        $("#oldpwd").css({ "border": "","background": ""});

                        $("#new_pwd").val("");
                        $("#new_c_pwd").val("");

                        $("#new_pwd").attr("readonly", false);
                        $("#new_c_pwd").attr("readonly", false);                        

                    }else{

                        $("#new_pwd").val("");
                        $("#new_c_pwd").val("");

                        $("#soldpwd").html("Please enter correct old password");   
                        $("#oldpwd").css({ "border": "1px solid red","background": "#FFCECE"})        

                        $("#new_pwd").attr("readOnly", true);
                        $("#new_c_pwd").attr("readOnly", true);
                        }
        }
    });
}




function changePassword()
{
    var zpwd=0;
    var znpwd=0;
    var zncpwd=0;

    var info_pwd =  $("#oldpwd").val();
    if(info_pwd == null || info_pwd == "")
    {
                $("#soldpwd").html("Please enter old password");   
                zpwd = 0;
                $("#oldpwd").css({ "border": "1px solid red","background": "#FFCECE"})        

    }
    else
    {
 
        $("#soldpwd").html("");   
        zpwd = 1;
        $("#oldpwd").css({ "border": "","background": ""});

        var info_npwd =  $("#new_pwd").val();
        if (info_npwd == null || info_npwd == "") 
        {
                $("#snew_pwd").html("Please enter new password");
                znpwd = 0;
                $("#new_pwd").css({ "border": "1px solid red","background": "#FFCECE"});
        }
        else
        {
                $("#snew_pwd").html("");
                znpwd = 1;
                $("#new_pwd").css({ "border": "","background": ""});
        }



        var info_ncpwd =  $("#new_c_pwd").val();
        if (info_ncpwd == null || info_ncpwd == "") 
        {
                $("#snew_c_pwd").html("Please enter confirm password");
                zncpwd = 0;
                $("#new_c_pwd").css({ "border": "1px solid red","background": "#FFCECE"});
        }
        else
        {
                $("#snew_c_pwd").html("");
                zncpwd = 1;
                $("#new_c_pwd").css({ "border": "","background": ""});
        
      

            if(info_npwd != info_ncpwd)
            {
                    $("#snew_c_pwd").html("Both password should match");
                    zncpwd = 0;
                    $("#new_c_pwd").css({ "border": "1px solid red","background": "#FFCECE"});
            }
            else
            {
                    $("#snew_c_pwd").html("");
                    zncpwd = 1;
                    $("#new_c_pwd").css({ "border": "","background": ""});
            }
        }
    }

    if( zpwd==1 && znpwd==1 &&  zncpwd==1 )
    {
                $("#changePassword-info").submit();
    }

}




function changePasswordPT()
{
    var znpwd=0;
    var zncpwd=0;


        var info_npwd =  $("#new_pwd").val();
        if (info_npwd == null || info_npwd == "") 
        {
                $("#snew_pwd").html("Please enter new password");
                znpwd = 0;
                $("#new_pwd").css({ "border": "1px solid red","background": "#FFCECE"});
        }
        else
        {
                $("#snew_pwd").html("");
                znpwd = 1;
                $("#new_pwd").css({ "border": "","background": ""});
        }



        var info_ncpwd =  $("#new_c_pwd").val();
        if (info_ncpwd == null || info_ncpwd == "") 
        {
                $("#snew_c_pwd").html("Please enter confirm password");
                zncpwd = 0;
                $("#new_c_pwd").css({ "border": "1px solid red","background": "#FFCECE"});
        }
        else
        {
                $("#snew_c_pwd").html("");
                zncpwd = 1;
                $("#new_c_pwd").css({ "border": "","background": ""});
        
      

            if(info_npwd != info_ncpwd)
            {
                    $("#snew_c_pwd").html("Both password should match");
                    zncpwd = 0;
                    $("#new_c_pwd").css({ "border": "1px solid red","background": "#FFCECE"});
            }
            else
            {
                    $("#snew_c_pwd").html("");
                    zncpwd = 1;
                    $("#new_c_pwd").css({ "border": "","background": ""});
            }
        }

    if( znpwd==1 &&  zncpwd==1 )
    {
                $("#changePassword-info").submit();
    }

}



function submitBasicInfo(){


    var exp_no = /^\d{10}$/; 
    var exp_zip = /^\d{6}$/; 

    var exp_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;                                           

    var exp_name = /^[a-zA-Z ]*$/;

        var zfname=0;
        var zmname=0;        
        var zlname=0;
        var zdob=0;
        var zladdd=0;
        var zlzip=0;
        var zlcity=0;                
        var zlstate=0;                        
        var zlcountry=0;

        var zpaddd=0;
        var zpzip=0;
        var zpcity=0;                
        var zpstate=0;                        
        var zpcountry=0;

        var zcon1=0;        
        var zcon2=0;        
        var zpemail=0;        
        var zlinkedin=0;

        var zprofile_picture = 0;
        var zpassport = 0;
        var zvoterid = 0;                



            var info_objectives = $.trim($(CKEDITOR.instances.stud_objectives.getData()).text());
                if (info_objectives == null || info_objectives == "") 
                {
                         $("#sstud_objactives").html("Please enter objectives");
                         zobjactives = 0;
                }
                else
                {
                        $("#sstud_objactives").html("");
                        zobjactives =1;
                }




                var info_fname =  $("#fname").val()
                if (info_fname == null || info_fname == "") 
                {
                         $("#sfname").html("Please enter first name");
                         zfname = 0;
                        $("#fname").css({ "border": "1px solid red","background": "#FFCECE"});
                        $("#fname").focus();
                }
                else if(exp_name.test(info_fname) == false)
                {
                         $("#sfname").html("Please enter alphabets only");
                         zfname = 0;
                        $("#fname").css({ "border": "1px solid red","background": "#FFCECE"});                
                        $("#fname").focus();                        
                }
                else
                {
                        $("#sfname").html("");
                        zfname =1
                        $("#fname").css({ "border": "","background": ""});
                }

              var info_mname =  $("#mname").val()
                if (info_mname == null || info_mname == "") 
                {
                         $("#smname").html("Please enter middle name");
                         zmname = 0;
                        $("#mname").css({ "border": "1px solid red","background": "#FFCECE"});
                        $("#mname").focus();                        
                }
                else if(exp_name.test(info_mname) == false)
                {
                         $("#smname").html("Middle name should be alphabets");
                         zmname = 0;
                        $("#mname").css({ "border": "1px solid red","background": "#FFCECE"});                
                        $("#mname").focus();                                                
                }                
                else
                {
                        $("#smname").html("");
                        zmname =1
                        $("#mname").css({ "border": "","background": ""});
                }

                var info_lname =  $("#lname").val()
                if (info_lname == null || info_lname == "") 
                {
                         $("#slname").html("Please enter last name");
                         zlname = 0;
                        $("#lname").css({ "border": "1px solid red","background": "#FFCECE"});
                        $("#lname").focus();                                                
                }
                else if(exp_name.test(info_lname) == false)
                {
                         $("#slname").html("Please enter alphabets only");
                         zlname = 0;
                        $("#lname").css({ "border": "1px solid red","background": "#FFCECE"}); 
                        $("#lname").focus();                                                                                       
                }                                
                else
                {
                        $("#slname").html("");
                        zlname =1
                        $("#lname").css({ "border": "","background": ""});
                }                

                var info_dob =  $("#bdate").val()
                if (info_dob == null || info_dob == "") 
                {
                         $("#sbdate").html("Please select date of birth");
                         zdob = 0;
                        $("#bdate").css({ "border": "1px solid red","background": "#FFCECE"});
                        $("#bdate").focus();                                                                        
                }
                else
                {
                        $("#sbdate").html("");
                        zdob =1
                        $("#bdate").css({ "border": "","background": ""});
                }       

               var info_con1 =  $("#con1").val()
                 if (info_con1 == null || info_con1 == "")
                 {
                        $("#scon1").html("Please enter contact number");
                        zcon1 = 0;
                        $("#con1").css({ "border": "1px solid red","background": "#FFCECE"});                                           
                        $("#con1").focus();                                                                                                     
                    
                 }
                 else
                 {

                    if (exp_no.test(info_con1) == false) 
                    {
                            $("#scon1").html("Contact number should be 10 digit number");
                            zcon1 = 0;
                             $("#con1").css({ "border": "1px solid red","background": "#FFCECE"});                                           
                            $("#con1").focus();                                                                                                                                  
                    }
                    else
                    {
                        $("#scon1").html("");
                        zcon1 =1;
                        $("#con1").css({ "border": "","background": ""});

                    } 
                }


               var info_con2 =  $("#con2").val()
                 if (info_con2 == null || info_con2 == "")
                 {
                       $("#scon2").html("");
                        zcon2 =1;
                        $("#con2").css({ "border": "","background": ""});
                    
                 }
                 else
                 {

                    if (exp_no.test(info_con2) == false) 
                    {
                            $("#scon2").html("Contact number should be 10 digit number");
                            zcon2 = 0;
                             $("#con2").css({ "border": "1px solid red","background": "#FFCECE"});                                           
                        $("#con2").focus();                                                                                                                                  
                    }
                    else
                    {
                        $("#scon2").html("");
                        zcon2 =1;
                        $("#con2").css({ "border": "","background": ""});

                    } 
                }


               var info_pemail =  $("#pemail").val()
                 if (info_pemail == null || info_pemail == "")
                 {
                       $("#spemail").html("Please enter primary email address");
                        zpemail =0;
                        $("#pemail").css({ "border": "","background": ""});
                        $("#pemail").focus();                        
                    
                 }
                 else
                 {

                    if (exp_email.test(info_pemail) == false) 
                    {
                            $("#spemail").html("Please enter valid email address");
                            zpemail = 0;
                             $("#pemail").css({ "border": "1px solid red","background": "#FFCECE"});
                        $("#pemail").focus();                        

                    }
                    else
                    {
                        $("#spemail").html("");
                        zpemail =1;
                        $("#pemail").css({ "border": "","background": ""});

                    } 
                }


               var info_linkedin =  $("#social_linkedin").val()
                 if (info_linkedin == null || info_linkedin == "")
                 {
                       $("#ssocial_linkedin").html("Please enter linkedin profile link");
                        zlinkedin =0;
                        $("#social_linkedin").css({ "border": "","background": ""});
                        $("#social_linkedin").focus();                        
                    
                 }
                else
                {
                    $("#ssocial_linkedin").html("");
                    zlinkedin =1;
                    $("#social_linkedin").css({ "border": "","background": ""});

                } 

                var info_pp =  $("#profile_picture").val();
                var valid_extensions = /(.gif|.jpg|.png|.GIF|.JPG|.PNG)$/i;
                var hpp = $("#hiddenPP").val()

                if(hpp == "true")
                {
                    $("#sprofile_picture").html("");
                    zprofile_picture =1
                    $("#divProfilePicture").css({ "border": "","background": ""});


                     if (info_pp == null || info_pp == "")
                     {
                            $("#sprofile_picture").html("");
                            zprofile_picture =1
                            $("#divProfilePicture").css({ "border": "","background": ""});
                     }
                     else if (!valid_extensions.test(info_pp)) 
                     {
                            $("#sprofile_picture").html("Invalid format !!! Please select proper file type");
                            zprofile_picture = 0;
                            $("#divProfilePicture").css({ "border": "1px solid red","background": "#FFCECE"});
                            $("#profile_picture").focus();                        

                     }
                     else
                     {
                             $("#sprofile_picture").html("");
                             zprofile_picture =1
                             $("#divProfilePicture").css({ "border": "","background": ""});
                     }

                }
               else
                {

                     if (info_pp == null || info_pp == "")
                     {
                              $("#sprofile_picture").html("Please select profile picture");
                              zprofile_picture = 0;
                             $("#divProfilePicture").css({ "border": "1px solid red","background": "#FFCECE"});
                            $("#profile_picture").focus();                        

                     }
                     else if (!valid_extensions.test(info_pp)) 
                     {
                             $("#sprofile_picture").html("Invalid format !!! Please select proper file type");
                              zprofile_picture = 0;
                             $("#divProfilePicture").css({ "border": "1px solid red","background": "#FFCECE"});
                            $("#profile_picture").focus();                        

                     }
                     else
                     {
                             $("#sprofile_picture").html("");
                             zprofile_picture =1
                             $("#divProfilePicture").css({ "border": "","background": ""});
                     }
                
                }



                var info_passport =  $("#doc_passport").val();
                var hpassport = $("#hiddenPassport").val()

                if(hpassport == "true")
                {
                    $("#sdoc_passport").html("");
                    zpassport =1
                    $("#divpassport").css({ "border": "","background": ""});


                     if (info_passport == null || info_passport == "")
                     {
                            $("#sdoc_passport").html("");
                            zpassport =1
                            $("#divpassport").css({ "border": "","background": ""});
                     }
                     else if (!valid_extensions.test(info_passport)) 
                     {
                            $("#sdoc_passport").html("Invalid format !!! Please select proper file type");
                            zpassport = 0;
                            $("#divpassport").css({ "border": "1px solid red","background": "#FFCECE"});
                            $("#doc_passport").focus();                        

                     }
                     else
                     {
                             $("#sdoc_passport").html("");
                             zpassport =1
                             $("#divpassport").css({ "border": "","background": ""});
                     }

                }
               else
                {

                    if (info_passport == null || info_passport == "")
                     {
                            $("#sdoc_passport").html("");
                            zpassport =1
                            $("#divpassport").css({ "border": "","background": ""});
                     }
                     else if (!valid_extensions.test(info_passport)) 
                     {
                            $("#sdoc_passport").html("Invalid format !!! Please select proper file type");
                            zpassport = 0;
                            $("#divpassport").css({ "border": "1px solid red","background": "#FFCECE"});
                            $("#doc_passport").focus();                        

                     }
                     else
                     {
                             $("#sdoc_passport").html("");
                             zpassport =1
                             $("#divpassport").css({ "border": "","background": ""});
                     }
                
                }



                var info_voterID =  $("#doc_voter_id").val();
                var hvoterid = $("#hiddenPassport").val()

                if(hvoterid == "true")
                {
                    $("#sdoc_voter_id").html("");
                    zvoterid =1
                    $("#divVoterId").css({ "border": "","background": ""});


                     if (info_voterID == null || info_voterID == "")
                     {
                            $("#sdoc_voter_id").html("");
                            zvoterid =1
                            $("#divVoterId").css({ "border": "","background": ""});
                     }
                     else if (!valid_extensions.test(info_voterID)) 
                     {
                            $("#sdoc_voter_id").html("Invalid format !!! Please select proper file type");
                            zvoterid = 0;
                            $("#divVoterId").css({ "border": "1px solid red","background": "#FFCECE"});
                            $("#divVoterId").focus();                        

                     }
                     else
                     {
                             $("#sdoc_voter_id").html("");
                             zvoterid =1
                             $("#divVoterId").css({ "border": "","background": ""});
                     }

                }
               else
                {

                     if (info_voterID == null || info_voterID == "")
                     {
                            $("#sdoc_voter_id").html("");
                            zvoterid =1
                            $("#divVoterId").css({ "border": "","background": ""});
                     }
                     else if (!valid_extensions.test(info_voterID)) 
                     {
                            $("#sdoc_voter_id").html("Invalid format !!! Please select proper file type");
                            zvoterid = 0;
                            $("#divVoterId").css({ "border": "1px solid red","background": "#FFCECE"});
                            $("#divVoterId").focus();                        

                     }
                     else
                     {
                             $("#sdoc_voter_id").html("");
                             zvoterid =1
                             $("#divVoterId").css({ "border": "","background": ""});
                     }
                
                }


 if( zobjactives ==1 && zfname==1 &&  zmname==1 && zlname== 1 && zdob==1 &&  zcon1==1 && zcon2==1 && zpemail==1 && zlinkedin==1 && zvoterid ==1 && zpassport ==1 && zprofile_picture ==1)
 {
         $("#basic-info").submit();
 }
}//submitBAsicInfo



function submitStudAddress(){


    var exp_no = /^\d{10}$/; 
    var exp_zip = /^\d{6}$/; 

    var exp_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;                                           

    var exp_name = /^[a-zA-Z ]*$/;

        var zladdd=0;
        var zlzip=0;
        var zlcity=0;                
        var zlstate=0;                        
        var zlcountry=0;

        var zpaddd=0;
        var zpzip=0;
        var zpcity=0;                
        var zpstate=0;                        
        var zpcountry=0;


                var info_ladd =  $("#ladd_detail").val()
                if (info_ladd == null || info_ladd == "") 
                {
                         $("#sl_add_detail").html("Please enter local address");
                         zladdd = 0;
                        $("#ladd_detail").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else
                {
                        $("#sl_add_detail").html("");
                        zladdd =1
                        $("#ladd_detail").css({ "border": "","background": ""});
                }       


                var info_lpin =  $("#lzip").val()
                 if (info_lpin == null || info_lpin == "")
                 {
                            $("#sl_zip").html("please enter zip code");
                            zlzip = 0;
                             $("#lzip").css({ "border": "1px solid red","background": "#FFCECE"});                                           
                    
                 }
                 else
                 {

                    if (exp_zip.test(info_lpin) == false) 
                    {
                            $("#sl_zip").html("zip code should be 6 digit number");
                            zlzip = 0;
                             $("#lzip").css({ "border": "1px solid red",
                                            "background": "#FFCECE"});                                           
                    }
                    else
                    {
                        $("#sl_zip").html("");
                        zlzip =1;
                        $("#lzip").css({ "border": "","background": ""});

                    } 
                }



                var info_lcity =  $("#lcity").val()
                if (info_lcity == null || info_lcity == "") 
                {
                         $("#sl_city").html("Please enter city name");
                         zlcity = 0;
                        $("#lcity").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else if(exp_name.test(info_lcity) == false)
                {
                         $("#sl_city").html("Please enter alphabets only");
                         zlcity = 0;
                        $("#lcity").css({ "border": "1px solid red","background": "#FFCECE"});                
                }                                
                else
                {
                        $("#sl_city").html("");
                        zlcity =1
                        $("#lcity").css({ "border": "","background": ""});
                }       

                var info_lstate =  $("#lstate").val()
                if (info_lstate == null || info_lstate == "") 
                {
                         $("#sl_state").html("Please enter state name");
                         zlstate = 0;
                        $("#lstate").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else if(exp_name.test(info_lstate) == false)
                {
                         $("#sl_state").html("Please enter alphabets only");
                         zlstate = 0;
                        $("#lstate").css({ "border": "1px solid red","background": "#FFCECE"});                
                }                                                
                else
                {
                        $("#sl_state").html("");
                        zlstate =1
                        $("#lstate").css({ "border": "","background": ""});
                }       

                var info_lcountry =  $("#lcountry").val()
                if (info_lcountry == null || info_lcountry == "") 
                {
                        $("#sl_country").html("Please enter country name");
                        zlcountry = 0;
                        $("#lcountry").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else if(exp_name.test(info_lcountry) == false)
                {
                         $("#sl_country").html("Please enter alphabets only");
                         zlcountry = 0;
                        $("#lcountry").css({ "border": "1px solid red","background": "#FFCECE"});                
                }                                                                
                else
                {
                        $("#sl_country").html("");
                        zlcountry =1
                        $("#lcountry").css({ "border": "","background": ""});
                }       




                var info_padd =  $("#p_detail").val()
                if (info_padd == null || info_padd == "") 
                {
                         $("#sp_add_detail").html("Please enter address");
                         zpaddd = 0;
                        $("#p_detail").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else
                {
                        $("#sp_add_detail").html("");
                        zpaddd =1
                        $("#p_detail").css({ "border": "","background": ""});
                }       



                var info_ppin =  $("#p_zip").val()
                 if (info_ppin == null || info_ppin == "")
                 {
                            $("#sp_zip").html("please enter zip code");
                            zpzip = 0;
                             $("#p_zip").css({ "border": "1px solid red","background": "#FFCECE"});                                           
                    
                 }
                 else
                 {
                    if (exp_zip.test(info_ppin) == false) 
                    {
                            $("#sp_zip").html("zip code should be 6 digit number");
                            zpzip = 0;
                             $("#p_zip").css({ "border": "1px solid red",
                                            "background": "#FFCECE"});                                           
                    }
                    else
                    {
                        $("#sp_zip").html("");
                        zpzip =1;
                        $("#p_zip").css({ "border": "","background": ""});

                    } 
                }

               
                var info_pcity =  $("#p_city").val()
                if (info_pcity == null || info_pcity == "") 
                {
                         $("#sp_city").html("Please enter city name");
                         zpcity = 0;
                        $("#p_city").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else if(exp_name.test(info_pcity) == false)
                {
                         $("#sp_city").html("Please enter alphabets only");
                         zpcity = 0;
                        $("#p_city").css({ "border": "1px solid red","background": "#FFCECE"});                
                }                                                                
                else
                {
                        $("#sp_city").html("");
                        zpcity =1
                        $("#p_city").css({ "border": "","background": ""});
                }       

                var info_pstate =  $("#pstate").val()
                if (info_pstate == null || info_pstate == "") 
                {
                         $("#sp_state").html("Please enter state name");
                         zpstate = 0;
                        $("#pstate").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else if(exp_name.test(info_pstate) == false)
                {
                         $("#sp_state").html("Please enter alphabets only");
                         zpstate = 0;
                        $("#pstate").css({ "border": "1px solid red","background": "#FFCECE"});                
                }                                                                                
                else
                {
                        $("#sp_state").html("");
                        zpstate =1
                        $("#pstate").css({ "border": "","background": ""});
                }       

                var info_pcountry =  $("#pcountry").val()
                if (info_pcountry == null || info_pcountry == "") 
                {
                         $("#sp_country").html("Please enter country name");
                         zpcountry = 0;
                        $("#pcountry").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else if(exp_name.test(info_pcountry) == false)
                {
                         $("#sp_country").html("Please enter alphabets only");
                         zpcountry = 0;
                        $("#pcountry").css({ "border": "1px solid red","background": "#FFCECE"});                
                }                                                                                                
                else
                {
                        $("#sp_country").html("");
                        zpcountry =1
                        $("#pcountry").css({ "border": "","background": ""});
                }      




if( zladdd==1 && zlzip==1 && zlcity==1 &&  zlstate==1 && zlcountry==1 && zpaddd==1 && zpzip==1 && zpcity==1 && zpstate==1 && zpcountry==1  )
{
                $("#student_address_info").submit();

}
}// submitStudentAddress


function changeDegree()
{
    var degreeName = $("#ename_1").val();
    if(degreeName == 'other')
    {
        $("#div_other_course").show();
        $("#other_degree").focus();
        $("#existDegreeName").val("true");        
    }
    else
    {
        $("#div_other_course").hide();
    }
}

function checkDegree()
{

    var degreeName =  $("#other_degree").val()
            $.ajax({
            url: "<?php echo base_url();?>student/checkDegree",
            type: "POST",
            async: true, 
            data: {degreeName:degreeName}, //your form data to post goes here
             success: function(response){ 
                    if(response == "true")
                    {
                        $("#sother_degree").html("Degree name already exist please select from list");
                        $("#other_degree").css({ "border": "1px solid red","background": "#FFCECE"});
                        $("#ename_1").focus();
                        $("#existDegreeName").val("true");                                
                    }
                    else
                    {
                        $("#sother_degree").html("");
                        $("#other_degree").css({ "border": "","background": ""});
                        $("#existDegreeName").val("false");                                                        
                    }        
                }
    });

}



function checkQualificationYear()
{

    var studentId =  $("#studnet_id").val()
    var qualificationName =  $("#edegree_1").val()
    var passingYear =  $("#pass_year_1").val()

            $.ajax({
            url: "<?php echo base_url();?>student/checkEducationYear",
            type: "POST",
            async: true, 
            data: {qualificationName:qualificationName,passingYear:passingYear,studentId:studentId}, //your form data to post goes here
             success: function(response){ 
                    
                     if(response == "true")
                     {
                        $("#previouce_year").val("true");                        
                        $("#spass_year_1").html("");
                        $("#pass_year_1").css({ "border": "","background": ""});


                     }
                     else
                     {
                        $("#previouce_year").val("false");                                                
                        $("#spass_year_1").html("Passing year should be  greater than previous qualification passing year");
                        $("#pass_year_1").css({ "border": "1px solid red","background": "#FFCECE"});
                     }        
                }
    });

}



    function submitEducation(){


        var totalEducation = $(".countEdu").length;

        var zbordUni=0;
        var zeduname=0;
        var zeduquali=0;        
        var zeduDivision=0;  
        var zmarks=0;
        var zpyear=0;
        var zdoc=0;
        var zstatus=0;      

            var exp_name = /^[a-zA-Z ]*$/;                                  


        for(var i=1 ; i<= totalEducation; i++)
        {
         

            //check whether select education name or not

                var info_eduquali =  $("#edegree_"+i+"").val()
                if (info_eduquali == null || info_eduquali == "") 
                {
                         $("#sedegree_"+i+"").html("Please select qualification");
                         zeduquali = 0;
                        $("#edegree_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else
                {
                        $("#sedegree_"+i+"").html("");
                        zeduquali =1
                        $("#edegree_"+i+"").css({ "border": "","background": ""});
                }


                var info_eduname =  $("#ename_"+i+"").val()
                if (info_eduname == null || info_eduname == "") 
                {
                         $("#sname_"+i+"").html("Please select degree");
                         zeduname = 0;
                        $("#ename_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                }

                else if(info_eduname == "other")
                {
                        var info_otherDegree =  $("#other_degree").val()

                        if(info_otherDegree == null || info_otherDegree == "") 
                        {
                            $("#sother_degree").html("Please enter degree name");
                            zeduname = 0;
                            $("#other_degree").css({ "border": "1px solid red","background": "#FFCECE"});
                            $("#sname_"+i+"").html("");
                            $("#ename_"+i+"").css({ "border": "","background": ""});

                        }
                        else if( $("#existDegreeName").val()=="true")
                        {
                                $("#sother_degree").html("Degree name already exist please select from list");
                                $("#other_degree").css({ "border": "1px solid red","background": "#FFCECE"});
                                $("#ename_1").focus();
                                zeduname = 0;                                
                        }
                     

                        else
                        {
                            $("#sname_"+i+"").html("");
                            zeduname = 1;                            
                            $("#ename_"+i+"").css({ "border": "","background": ""});
                                $("#sother_degree").html("");
                                $("#other_degree").css({ "border": "","background": ""});                            
                        }
                }
                else
                {
                        $("#sname_"+i+"").html("");
                        zeduname =1
                        $("#ename_"+i+"").css({ "border": "","background": ""});
                }


                var info_edudivision =  $("#edudivision_"+i+"").val()
                if (info_edudivision == null || info_eduname == "") 
                {
                         $("#sedudivision_"+i+"").html("Please select division");
                         zeduDivision = 0;
                        $("#edudivision_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else
                {
                        $("#sedudivision_"+i+"").html("");
                        zeduDivision =1
                        $("#edudivision_"+i+"").css({ "border": "","background": ""});
                }


                // checking enter bord / university name or not
                var info_bordUni =  $("#bordUni_"+i+"").val()
                if (info_bordUni == null || info_bordUni == "") 
                {
                         $("#sbordUni_"+i+"").html("Please enter board/university name");
                         zbordUni = 0;
                        $("#bordUni_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else if(exp_name.test(info_bordUni) == false)
                {
                         $("#sbordUni_"+i+"").html("Please enter alphabets only");
                         zbordUni = 0;
                        $("#bordUni_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                }                                                                                                                
                else
                {
                        $("#sbordUni_"+i+"").html("");
                        zbordUni =1
                        $("#bordUni_"+i+"").css({ "border": "","background": ""});
                }

                // checking enter marks or not
                var info_marks =  $("#perc_cgpa_"+i+"").val()
                reMarks = /^((0|[1-9]\d?)(\.\d{1,2})?(\.00?)?)$/

                if (info_marks == null || info_marks == "") 
                {
                        $("#sperc_cgpa_"+i+"").html("Please enter marks");
                        zmarks = 0;
                        $("#perc_cgpa_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                }

               else if(reMarks.test(info_marks) == false) 
                {
                        $("#sperc_cgpa_"+i+"").html("Value should less than 100 or 2 digit after floating point");
                        zmarks = 0;
                        $("#perc_cgpa_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});

                }

                else
                {
                        $("#sperc_cgpa_"+i+"").html("");
                        zmarks =1
                        $("#perc_cgpa_"+i+"").css({ "border": "","background": ""});
                }


                // checking enter marks or not
                var info_pyear =  $("#pass_year_"+i+"").val()
                var _thisYear = new Date().getFullYear();  

                if (info_pyear == null || info_pyear == "") 
                {
                         $("#spass_year_"+i+"").html("Please enter passing year");
                         zpyear = 0;
                        $("#pass_year_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});

                }
                else if(!info_pyear.match(/\d{4}/))
                {

                         $("#spass_year_"+i+"").html("Passing year should be 4 digit number ");
                         zpyear = 0;
                        $("#pass_year_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});                                             
                }
                else if(info_pyear.length != 4)
                {
                        $("#spass_year_"+i+"").html("Passing year must contain 4 digits");
                        zpyear = 0;
                        $("#pass_year_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});                    
                }
                else if(_thisYear< info_pyear)
                {
                        $("#spass_year_"+i+"").html("Passing year should be less than current year");
                        zpyear = 0;
                        $("#pass_year_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"}); 
                }
                else
                {
                        $("#spass_year_"+i+"").html("");
                        zpyear =1
                        $("#pass_year_"+i+"").css({ "border": "","background": ""});
                }

                if($("#previouce_year").val() == "true" )
                {
                       zpyear =1;
                        $("#spass_year_1").html("");
                        $("#pass_year_1").css({ "border": "","background": ""});


                }else
                {
                    zpyear =0;
                        $("#spass_year_1").html("Passing year should be  greater than previous qualification passing year");
                        $("#pass_year_1").css({ "border": "1px solid red","background": "#FFCECE"});

                }



                // checking document is selected or not
             var valid_extensions = /(.gif|.jpg|.png|.GIF|.JPG|.PNG)$/i;
                
                var info_doc =  $("#doc_"+i+"").val()

                     if (info_doc == null || info_doc == "")
                     {
                         $("#sdoc_"+i+"").html("Please select document");
                         zdoc = 0;
                        $("#divDoc_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                     }
                     else if (!valid_extensions.test(info_doc)) 
                     {
                         $("#sdoc_"+i+"").html("Invalid format !!! Please select proper file type");
                         zdoc = 0;
                        $("#divDoc_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});

                     }
                     else
                     {
                        $("#sdoc_"+i+"").html("");
                        zdoc =1
                        $("#divDoc_"+i+"").css({ "border": "","background": ""});
                     }



                // checking status = true

                if ($("#status_"+i+"").is(":checked")) 
                {
                    $("#sstatus_"+i+"").html("");
                    zstatus =1
                    $("#divStatus_"+i+"").css({ "border": "","background": ""});
                    
                 }
                 else
                 {

                    $("#sstatus_"+i+"").html("Please approve education details from your side");
                    zstatus = 0;
                    $("#divStatus_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                 }
              
        }

        if( zbordUni==1 && zeduname==1 &&  zeduquali==1 &&  zeduDivision== 1&& zmarks==1 &&  zpyear==1 && zdoc==1 && zstatus==1 )
        {

             $("#education-info").submit();
             form.submit();
        }
    }//submitEducation 

//============================ ADD MORE EDUATION=====================================================



    function submitExperience(){


        var totalExperience = $(".countEexp").length;
        var zorgname=0;
        var zorgdomain=0;
        var zposition=0;
        var zjdate=0;
        var zldate=0;        
        var zorgdoc=0;
        var zorgstatus=0;    
        
        var exp_name = /^[a-zA-Z ]*$/;                                  



        for(var i=1 ; i<= totalExperience; i++)
        {
         


            //check whether organizaation name is empty or not
            var info_orgname =  $("#orgName_"+i+"").val()
            if (info_orgname == null || info_orgname == "") 
            {
                     $("#sorgName_"+i+"").html("Please enter Organization name");
                     zorgname = 0;
                    $("#orgName_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }
            else if(exp_name.test(info_orgname) == false)
            {
                         $("#sorgName_"+i+"").html("Please enter alphabets only");
                         zbordUni = 0;
                        $("#orgName_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }                                                                                                                            
            else
            {
                    $("#sorgName_"+i+"").html("");
                    zorgname =1;
                    $("#orgName_"+i+"").css({ "border": "","background": ""});
            }

            //check whether organizaation domain name is selected or not
            var info_orgdomain =  $("#orgDomain_"+i+"").val()
            if (info_orgdomain == null || info_orgdomain == "") 
            {
                     $("#sorgDomain_"+i+"").html("Please enter business unit name");
                     zorgdomain = 0;
                    $("#orgDomain_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }

            else
            {
                    $("#sorgDomain_"+i+"").html("");
                    zorgdomain =1;
                    $("#orgDomain_"+i+"").css({ "border": "","background": ""});
            }



            //check whether organizaation domain name is selected or not
            var info_orgpost =  $("#jobPost_"+i+"").val()
            if (info_orgpost == null || info_orgpost == "") 
            {
                     $("#sjobPost_"+i+"").html("Please enter your job position");
                     zposition = 0;
                    $("#jobPost_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }
            else if(exp_name.test(info_orgpost) == false)
            {
                         $("#sjobPost_"+i+"").html("Please enter alphabets only");
                         zposition = 0;
                        $("#jobPost_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }                                                                                                                                       
            else
            {
                    $("#sjobPost_"+i+"").html("");
                    zposition =1;
                    $("#jobPost_"+i+"").css({ "border": "","background": ""});
            }

            //check whether joining date is enterd or not
            var info_orgjdate =  $("#dateIn_"+i+"").val();
			
            if (info_orgjdate == null || info_orgjdate == "") 
            {
                     $("#sdateIn_"+i+"").html("Please enter your joining date");
                     zjdate = 0;
                    $("#dateIn_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }
            else
            {
                    $("#sdateIn_"+i+"").html("");
                    zjdate =1;
                    $("#dateIn_"+i+"").css({ "border": "","background": ""});
            }
          
            //check whether leaving date is enterd or not
            var info_orgldate =  $("#dateOut_"+i+"").val()
            if (info_orgldate == null || info_orgldate == "") 
            {
                     $("#sdateOut_"+i+"").html("Please enter your leave date");
                     zldate = 0;
                    $("#dateOut_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }
			
			
			/*else if(11-06-2013 < 17-06-2014 )*/
            /*else if(info_orgldate < info_orgjdate )
            {
                    $("#sdateOut_"+i+"").html("Leaving date should be greater than joining date");
                    zldate = 0;
                    $("#dateOut_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});         
            }*/
			
			else if(info_orgldate < info_orgjdate )
            {
                    $("#sdateOut_"+i+"").html("Leaving date should be greater than joining date");
                    zldate = 0;
                    $("#dateOut_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});         
            }

            else 
            {
                    $("#sdateOut_"+i+"").html("");
                    zldate =1;
                    $("#dateOut_"+i+"").css({ "border": "","background": ""});
            }


            // check document is selected or not
            var info_doc =  $("#doc_pro_"+i+"").val()
            if (info_doc == null || info_doc == "") 
            {
                     $("#sdoc_pro_"+i+"").html("Please select document");
                     zorgdoc = 0;
                    $("#divOrgDoc_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }
            else
            {
                    $("#sdoc_pro_"+i+"").html("");
                    zorgdoc =1
                    $("#divOrgDoc_"+i+"").css({ "border": "","background": ""});
            }
            // check status = true
            if ($("#status_pro_"+i+"").is(":checked")) 
            {
                $("#sstatus_pro_"+i+"").html("");
                zorgstatus =1
                $("#divOrgStatus_"+i+"").css({ "border": "","background": ""});
             }
             else
             {
                $("#sstatus_pro_"+i+"").html("Please approve professional experience from your side");
                zorgstatus = 0;
                $("#divOrgStatus_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }



        }//for
             if( zorgname==1 &&  zorgdomain==1 && zposition==1 && zjdate==1 && zldate==1 && zorgdoc==1 && zorgstatus==1)
             {
                  $("#professional-info").submit();
             }// if
    }   
//============================ ADD MORE EXPERIENCE=====================================================

//============================ ADD MORE SPECIALIZATION=====================================================




    function submitSpecialization()
    {
                var totalSpecialization = $(".countSpec").length;
                var zspecname=0;  
                var zspestatus=0;  


        for(var i=1 ; i<= totalSpecialization; i++)
        {
         
            //check whether organizaation name is empty or not
            var info_spename =  $("#spec_name_"+i+"").val()
            if (info_spename == null || info_spename == "") 
            {
                     $("#sspec_name_"+i+"").html("Please enter specialization");
                     zspecname = 0;
                    $("#spec_name_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }
            else
            {
                    $("#sspec_name_"+i+"").html("");
                    zspecname =1;
                    $("#spec_name_"+i+"").css({ "border": "","background": ""});
            }


            // check status = true
            if ($("#specStatus_"+i+"").is(":checked")) 
            {
                $("#sspecStatus_"+i+"").html("");
                zspestatus =1
                $("#spesStatus_"+i+"").css({ "border": "","background": ""});
             }
             else
             {
                $("#sspecStatus_"+i+"").html("Please approve specialization from your side");
                zspestatus   = 0;
                $("#spesStatus_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }



    } //for


      if(  zspecname==1 &&  zspestatus==1 )
             {
                  $("#specialization-info").submit();
             }// if


}   //submitSpecialization 

//============================ ADD MORE SPECIALIZATION=====================================================


//============================ ADD MORE COURSE=====================================================


function submitAddCourse()
{
       var totalAddCourse = $(".countAddCourse").length;
       var zcorsname = 0;
       var zcorsdomain = 0; 
       var zcorsstatus=0; 
       var zcorsdoc=0;
       var zcorsdesc = 0;    


    // var certi_name = /^[a-zA-Z0-9\-]*$/;                                  


         for(var i=1 ; i<= totalAddCourse; i++)
         {
         
            // check whether certification name is empty or not
             var info_corsname =  $("#cors_name_"+i+"").val()
             if (info_corsname == null || info_corsname == "") 
             {
                      $("#scors_name_"+i+"").html("Please enter course name");
                      zcorsname = 0;
                     $("#cors_name_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
            // else if(certi_name.test(info_corsname) == false)
            // {
            //              $("#scors_name_"+i+"").html("Please enter alphanumeric or - characters only");
            //              zcorsname = 0;
            //             $("#cors_name_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            // }                
            else
            {
                     $("#scors_name_"+i+"").html("");
                     zcorsname =1;
                     $("#cors_name_"+i+"").css({ "border": "","background": ""});
            }

            // check whether organizaation name is empty or not
             var info_corsdomain =  $("#cors_domain_"+i+"").val()
             if (info_corsdomain == null || info_corsdomain == "") 
             {
                      $("#scors_domain_"+i+"").html("Please select domain of additional course");
                      zcorsdomain = 0;
                     $("#cors_domain_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#scors_domain_"+i+"").html("");
                     zcorsdomain =1;
                     $("#cors_domain_"+i+"").css({ "border": "","background": ""});
             }



            // check status = true
            if ($("#corsStatus_"+i+"").is(":checked")) 
            {
                $("#scorsStatus_"+i+"").html("");
                zcorsstatus =1
                $("#cors_status_"+i+"").css({ "border": "","background": ""});
             }
             else
             {
                $("#scorsStatus_"+i+"").html("Please approve course from your side");
                zcorsstatus = 0;
                $("#cors_status_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }

             var info_coursDoc =  $("#corsDoc_1").val();
             var valid_extensions = /(.gif|.jpg|.png|.GIF|.JPG|.PNG)$/i;

              if (info_coursDoc == null || info_coursDoc == "")
              {
                     $("#scorsDoc_1").html("");
                     zcorsdoc = 1;
                     $("#divcourse_doc_1").css({ "border": "","background": ""});
              }
              else if (!valid_extensions.test(info_coursDoc)) 
              {
                     $("#scorsDoc_1").html("Invalid format !!! Please select proper file type");
                     zcorsdoc = 0;
                     $("#divcourse_doc_1").css({ "border": "1px solid red","background": "#FFCECE"});
                     $("#Rprj_doc_1").focus();                        
              }
              else
              {
                      $("#scorsDoc_1").html("");
                      zcorsdoc =1
                      $("#divcourse_doc_1").css({ "border": "","background": ""});
              }

             var info_coursedesc = $.trim($(CKEDITOR.instances.cors_descrption_1.getData()).text());
            if (info_coursedesc == null || info_coursedesc == "") 
            {
                     $("#scors_descrption_"+i+"").html("Please enter additional course description");
                     zcorsdesc = 0;
                    $("#cors_descrption_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }
            else
            {
                    $("#scors_descrption_"+i+"").html("");
                    zcorsdesc =1;
                    $("#cors_descrption_"+i+"").css({ "border": "","background": ""});
            }




    } //for


        if( zcorsname ==1 &&  zcorsdomain == 1 &&  zcorsstatus==1 &&zcorsdoc ==1 && zcorsdesc ==1)
        {   

              $("#additionalCourse_info").submit();

         }// iF   

} //submitAddCourse 

//============================ ADD MORE COURSE=====================================================

//============================ ADD MORE PROJECTS=====================================================

function submitSummerProjects()
    {
       
       var totalProjects = $(".countProjects").length;

       var zprojdomain = 0; 
       var zprojname = 0; 
       var zprojOrgname = 0;
       var zprojOrgJDate = 0;               
       var zprojOrgLDate = 0;
       var zprojddesc = 0;
       var zprojstatus=0; 
       var zprojDocument=0;        


       
         for(var i=1 ; i<= totalProjects; i++)
         {

            
            // check whether project name is not empty
             var info_projname=  $("#prj_name_"+i+"").val()
             if (info_projname == null || info_projname == "") 
             {
                      $("#sprj_name_"+i+"").html("Please enter project name");
                      zprojname = 0;
                     $("#prj_name_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sprj_name_"+i+"").html("");
                     zprojname =1;
                     $("#prj_name_"+i+"").css({ "border": "","background": ""});
             }
         
            // check whether project domain is selected or not
             var info_projdomain=  $("#prj_domain_"+i+"").val()
             if (info_projdomain == null || info_projdomain == "") 
             {
                      $("#sprj_domain_"+i+"").html("Please select project domain");
                      zprojdomain = 0;
                     $("#prj_domain_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sprj_domain_"+i+"").html("");
                     zprojdomain =1;
                     $("#prj_domain_"+i+"").css({ "border": "","background": ""});
             }


             var info_projOrgname=  $("#porg_name_"+i+"").val()
             if (info_projOrgname == null || info_projOrgname == "") 
             {
                      $("#sporg_name_"+i+"").html("Please enter organization name where you completed your summer project");
                      zprojOrgname = 0;
                     $("#porg_name_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sporg_name_"+i+"").html("");
                     zprojOrgname =1;
                     $("#porg_name_"+i+"").css({ "border": "","background": ""});
             }             

            
            var info_projdesc = $.trim($(CKEDITOR.instances.prj_descrption_1.getData()).text());
                if (info_projdesc == null || info_projdesc == "") 
                {
                         $("#sprj_descrption_"+i+"").html("Please enter key deliverables & learning");
                         zprojddesc = 0;
                        $("#prj_descrption_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                }
                else
                {
                        $("#sprj_descrption_"+i+"").html("");
                        zprojddesc =1;
                        $("#prj_descrption_"+i+"").css({ "border": "","background": ""});
                }



            //check whether joining date is enterd or not
            var info_prjjdate =  $("#prjdateIn_"+i+"").val()
            if (info_prjjdate == null || info_prjjdate == "") 
            {
                     $("#sprjdateIn_"+i+"").html("Please enter project started date");
                     zprojOrgJDate = 0;
                    $("#prjdateIn_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }
            else
            {
                    $("#sprjdateIn_"+i+"").html("");
                    zprojOrgJDate =1;
                    $("#prjdateIn_"+i+"").css({ "border": "","background": ""});
            }
          
            //check whether leaving date is enterd or not
            var info_prjldate =  $("#prjdateOut_"+i+"").val()
            if (info_prjldate == null || info_prjldate == "") 
            {
                     $("#sprjdateOut_"+i+"").html("Please enter project complited date");
                     zprojOrgLDate = 0;
                    $("#prjdateOut_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }

            else if(info_prjjdate > info_prjldate )
            {
                    $("#sprjdateOut_"+i+"").html("project complited date should be grater then project starting date");
                    zprojOrgLDate = 0;
                    $("#prjdateOut_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});         
            }

            else 
            {
                    $("#sprjdateOut_"+i+"").html("");
                    zprojOrgLDate =1;
                    $("#prjdateOut_"+i+"").css({ "border": "","background": ""});
            }


                var info_sprjDoc =  $("#prj_doc_1").val();
                var valid_extensions = /(.gif|.jpg|.png|.GIF|.JPG|.PNG)$/i;

                 if (info_sprjDoc == null || info_sprjDoc == "")
                 {
                        $("#sprj_doc_1").html("");
                        zprojDocument =1
                        $("#divproj_doc_1").css({ "border": "","background": ""});
                 }
                 else if (!valid_extensions.test(info_sprjDoc)) 
                 {
                        $("#sprj_doc_1").html("Invalid format !!! Please select proper file type");
                        zprojDocument = 0;
                        $("#divproj_doc_1").css({ "border": "1px solid red","background": "#FFCECE"});
                        $("#prj_doc_1").focus();                        
                 }
                 else
                 {
                         $("#sprj_doc_1").html("");
                         zprojDocument =1
                         $("#divproj_doc_1").css({ "border": "","background": ""});
                 }

             // check status = true
            if ($("#prj_status_"+i+"").is(":checked")) 
            {
                $("#sst_project_"+i+"").html("");
                zprojstatus =1
                $("#divPrjStatus_"+i+"").css({ "border": "","background": ""});
             }
             else
             {
                $("#sst_project_"+i+"").html("Please approve project from your side");
                zprojstatus = 0;
                $("#divPrjStatus_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
          

    } //for
        if( zprojdomain == 1 &&  zprojname == 1 &&  zprojOrgname ==1 &&  zprojOrgJDate == 1 &&  zprojOrgLDate==1 && zprojstatus ==1  && zprojddesc == 1 && zprojDocument ==1)
        {   
               $("#SummerProject-info").submit();

         }// iF   

} //submitProjects 





function submitReaserchProjects()
{
        var totalRProjects = $(".countReserchProjects").length;

       var zrprrojname = 0; 
       var zrprojdomain = 0; 
       var zrprojddesc = 0;
       var zrprojstatus=0; 
       var zrprojDocument=0;        


       
         for(var i=1 ; i<= totalRProjects; i++)
         {
             var info_Rprojname=  $("#Rprj_name_"+i+"").val()
             if (info_Rprojname == null || info_Rprojname == "") 
             {
                      $("#sRprj_name_"+i+"").html("Please enter project name");
                      zrprrojname = 0;
                     $("#Rprj_name_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sRprj_name_"+i+"").html("");
                     zrprrojname =1;
                     $("#Rprj_name_"+i+"").css({ "border": "","background": ""});
             }
         
    
              var info_Rprojdomain=  $("#Rprj_domain_"+i+"").val()
              if (info_Rprojdomain == null || info_Rprojdomain == "") 
              {
                       $("#sRprj_domain_"+i+"").html("Please select project domain");
                      zrprojdomain = 0;
                     $("#Rprj_domain_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sRprj_domain_"+i+"").html("");
                     zrprojdomain =1;
                     $("#Rprj_domain_"+i+"").css({ "border": "","background": ""});
             }




            
             var info_Rprojdesc = $.trim($(CKEDITOR.instances.Rprj_descrption_1.getData()).text());
                 if (info_Rprojdesc == null || info_Rprojdesc == "") 
                 {
                          $("#sRprj_descrption_"+i+"").html("Please enter key deliverables & learning");
                         zrprojddesc = 0;
                         $("#Rprj_descrption_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                 }
                 else
                 {
                         $("#sRprj_descrption_"+i+"").html("");
                         zrprojddesc =1;
                         $("#Rprj_descrption_"+i+"").css({ "border": "","background": ""});
                 }




                 var info_rprjDoc =  $("#Rprj_doc_1").val();
                 var valid_extensions = /(.gif|.jpg|.png|.GIF|.JPG|.PNG)$/i;

                  if (info_rprjDoc == null || info_rprjDoc == "")
                  {
                         $("#sRprj_doc_1").html("");
                         zrprojDocument =1
                         $("#DIV_Rproj_doc_1").css({ "border": "","background": ""});
                  }
                  else if (!valid_extensions.test(info_rprjDoc)) 
                  {
                         $("#sRprj_doc_1").html("Invalid format !!! Please select proper file type");
                         zrprojDocument = 0;
                         $("#DIV_Rproj_doc_1").css({ "border": "1px solid red","background": "#FFCECE"});
                         $("#Rprj_doc_1").focus();                        
                  }
                  else
                  {
                          $("#sRprj_doc_1").html("");
                          zrprojDocument =1
                          $("#DIV_Rproj_doc_1").css({ "border": "","background": ""});
                  }

              // check status = true
                if ($("#Rprj_status_"+i+"").is(":checked")) 
                {
                    $("#rst_Rproject_"+i+"").html("");
                    zrprojstatus =1
                    $("#divResearchPrjPrjStatus_"+i+"").css({ "border": "","background": ""});
                }
                else
                {
                    $("#rst_Rproject_"+i+"").html("Please approve project from your side");
                    zrprojstatus = 0;
                    $("#divResearchPrjPrjStatus_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                }


    } //for
         if( zrprrojname == 1 && zrprojdomain == 1 &&  zrprojddesc == 1 &&  zrprojstatus == 1 &&  zrprojDocument==1 )
         {   

             $("#ResearchProject-info").submit();

          }// iF   

} //submitProjects 






function submitAssignmentPrjProjects()
{
        var totalAProjects = $(".countAssignmentProjects").length;

       var zAprrojname = 0; 
       var zAprojdomain = 0; 
       var zAprojddesc = 0;
       var zAprojstatus=0;

       
         for(var i=1 ; i<= totalAProjects; i++)
         {
             var info_Aprojname=  $("#assignmentPrj_name_"+i+"").val()
             if (info_Aprojname == null || info_Aprojname == "") 
             {
                      $("#sassignmentPrj_name_"+i+"").html("Please enter project name");
                      zAprrojname = 0;
                     $("#assignmentPrj_name_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sassignmentPrj_name_"+i+"").html("");
                     zAprrojname =1;
                     $("#assignmentPrj_name_"+i+"").css({ "border": "","background": ""});
             }
         
    
              var info_Aprojdomain=  $("#assignmentPrj_domain_"+i+"").val()
              if (info_Aprojdomain == null || info_Aprojdomain == "") 
              {
                       $("#sassignmentPrj_domain_"+i+"").html("Please select project domain");
                      zAprojdomain = 0;
                     $("#assignmentPrj_domain_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sassignmentPrj_domain_"+i+"").html("");
                     zAprojdomain =1;
                     $("#assignmentPrj_domain_"+i+"").css({ "border": "","background": ""});
             }

             var info_Rprojdesc = $.trim($(CKEDITOR.instances.assignmentPrj_descrption_1.getData()).text());
                 if (info_Rprojdesc == null || info_Rprojdesc == "") 
                 {
                          $("#sassignmentPrj_descrption_"+i+"").html("Please enter key deliverables & learning");
                         zAprojddesc = 0;
                         $("#Rprj_descrption_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                 }
                 else
                 {
                         $("#sassignmentPrj_descrption_"+i+"").html("");
                         zAprojddesc =1;
                         $("#Rprj_descrption_"+i+"").css({ "border": "","background": ""});
                 }

                if ($("#assignment_status").is(":checked")) 
                {
                    $("#sassignmentPrj_"+i+"").html("");
                    zAprojstatus =1
                    $("#divAssignmentStatus_"+i+"").css({ "border": "","background": ""});
                }
                else
                {
                    $("#sassignmentPrj_"+i+"").html("Please approve project from your side");
                    zAprojstatus = 0;
                    $("#divAssignmentStatus_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                }


    } //for
          if( zAprrojname == 1 && zAprojdomain == 1 &&  zAprojddesc == 1 &&  zAprojstatus == 1 )
          {   

              $("#assignmentProject-info").submit();

           }// iF   

} //submitAssignmentPrjProjects 




function submitExtraProjects()
{
        var totalEProjects = $(".countExtraProjects").length;

       var zEprrojname = 0; 
       var zEprojdomain = 0; 
       var zEprojddesc = 0;
       var zEprojstatus=0;

       
         for(var i=1 ; i<= totalEProjects; i++)
         {
             var info_Eprojname=  $("#ExtraPrj_name_"+i+"").val()
             if (info_Eprojname == null || info_Eprojname == "") 
             {
                      $("#sExtraPrj_name_"+i+"").html("Please enter project name");
                      zEprrojname = 0;
                     $("#ExtraPrj_name_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sExtraPrj_name_"+i+"").html("");
                     zEprrojname =1;
                     $("#ExtraPrj_name_"+i+"").css({ "border": "","background": ""});
             }
         
    
              var info_Eprojdomain=  $("#ExtraPrj_domain_"+i+"").val()
              if (info_Eprojdomain == null || info_Eprojdomain == "") 
              {
                       $("#sExtraPrj_domain_"+i+"").html("Please select project domain");
                      zEprojdomain = 0;
                     $("#ExtraPrj_domain_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sExtraPrj_domain_"+i+"").html("");
                     zEprojdomain =1;
                     $("#ExtraPrj_domain_"+i+"").css({ "border": "","background": ""});
             }

             var info_Eprojdesc = $.trim($(CKEDITOR.instances.ExtraPrj_descrption_1.getData()).text());
                 if (info_Eprojdesc == null || info_Eprojdesc == "") 
                 {
                          $("#sExtraPrj_descrption_"+i+"").html("Please enter key deliverables & learning");
                         zEprojddesc = 0;
                         $("#ExtraPrj_descrption_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
                 }
                 else
                 {
                         $("#sExtraPrj_descrption_"+i+"").html("");
                         zEprojddesc =1;
                         $("#ExtraPrj_descrption_"+i+"").css({ "border": "","background": ""});
                 }

            if ($("#extra_status").is(":checked")) 
            {
                $("#sExtraPrj_"+i+"").html("");
                zEprojstatus =1
                $("#divExtraStatus_"+i+"").css({ "border": "","background": ""});
            }
            else
            {
                $("#sExtraPrj_"+i+"").html("Please approve project from your side");
                zEprojstatus = 0;
                $("#divExtraStatus_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
            }


    } //for
          if( zEprrojname == 1 && zEprojdomain == 1 &&  zEprojddesc == 1 &&  zEprojstatus == 1 )
          {   

              $("#extraProject-info").submit();

           }// iF   

} //submitProjects 

//============================ ADD MORE PROJECTS=====================================================

//============================ ADD MORE RESPONSIBLITY=====================================================

function submitResponsiblity()
    {
       
       var totalRespo = $(".countRespo").length;

       var zrespoName = 0;
       var zrespoEdu = 0; 
       var zprespostatus=0; 
       var zPRddesc=0;

         for(var i=1 ; i<= totalRespo; i++)
         {
         
            // check whether responsiblity name is not empty
             var info_responame =  $("#respoName_"+i+"").val()
             if (info_responame == null || info_responame == "") 
             {
                      $("#srespoName_"+i+"").html("Please enter responsiblity");
                      zrespoName = 0;
                     $("#respoName_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#srespoName_"+i+"").html("");
                     zrespoName =1;
                     $("#respoName_"+i+"").css({ "border": "","background": ""});
             }


            // check whether education name is selected or not
             var info_respoedu=  $("#respoEdu_"+i+"").val()
             if (info_respoedu == null || info_respoedu == "") 
             {
                      $("#srespoEdu_"+i+"").html("Please select your education");
                      zrespoEdu = 0;
                     $("#respoEdu_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#srespoEdu_"+i+"").html("");
                     zrespoEdu =1;
                     $("#respoEdu_"+i+"").css({ "border": "","background": ""});
             }


            // check status = true
            if ($("#respoStatus_"+i+"").is(":checked")) 
            {
                $("#srespoStatus_"+i+"").html("");
                zprespostatus =1
                $("#divRespoStatus_"+i+"").css({ "border": "","background": ""});
             }
             else
             {
                $("#srespoStatus_"+i+"").html("Please approve responsiblity from your side");
                zprespostatus = 0;
                $("#divRespoStatus_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }


             var info_p_R_desc = $.trim($(CKEDITOR.instances.P_Respondblities_1.getData()).text());
             if (info_p_R_desc == null || info_p_R_desc  == "") 
             {
                      $("#sP_Respondblities_1").html("Please enter responsiblity");
                     zPRddesc = 0;
                     $("#P_Respondblities_1").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sP_Respondblities_1").html("");
                     zPRddesc =1;
                     $("#P_Respondblities_1").css({ "border": "","background": ""});
             }
          
    } //for

        if(zrespoName == 1 &&  zrespoEdu == 1 &&  zprespostatus==1 && zPRddesc ==1  )
        {   
               $("#poss_res_info").submit();

         }// iF   



} //submitResponsiblity 

//============================ ADD MORE RESPONSIBLITY=====================================================
//============================ ADD MORE AWARDS & ACHIVEMENTS ========================================

function submitAwards()
    {
       
       var totalAwards = $(".countAwards").length;

       var zawardsName = 0;
       var zawardoc = 0;               
       var zawardsstatus=0; 
       var zawardsDesc = 0;



            // check whether responsiblity name is not empty
             var info_awaname =  $("#awardName_1").val()
             if (info_awaname == null || info_awaname == "") 
             {
                      $("#sawardName_1").html("Please enter awards or achivement name");
                      zawardsName = 0;
                     $("#awardName_1").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sawardName_1").html("");
                     zawardsName =1;
                     $("#awardName_1").css({ "border": "","background": ""});
             }

            // check status = true
            if ($("#awardStatus_1").is(":checked")) 
            {
                $("#sawardStatus_1").html("");
                zawardsstatus =1
                $("#divAwardStatus_1").css({ "border": "","background": ""});
             }
             else
             {
                $("#sawardStatus_1").html("Please approve awards or achivements from your side");
                zawardsstatus = 0;
                $("#divAwardStatus_1").css({ "border": "1px solid red","background": "#FFCECE"});
             }


             var info_award_desc = $.trim($(CKEDITOR.instances.awardExtraInfo_1.getData()).text());
             if (info_award_desc == null || info_award_desc  == "") 
             {
                      $("#sawardExtraInfo_1").html("Please enter description");
                     zawardsDesc = 0;
                     $("#awardExtraInfo_1").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sawardExtraInfo_1").html("");
                     zawardsDesc =1;
                     $("#awardExtraInfo_1").css({ "border": "","background": ""});
             }

             var info_awardDoc =  $("#awardDoc_1").val();
             var valid_extensions = /(.gif|.jpg|.png|.GIF|.JPG|.PNG)$/i;

              if (info_awardDoc == null || info_awardDoc == "")
              {
                     $("#sawardDoc_1").html("Please select document");
                     zawardoc = 0;
                     $("#divAwardDoc_1").css({ "border": "1px solid red","background": "#FFCECE"}); 
              }
              else if (!valid_extensions.test(info_awardDoc)) 
              {
                     $("#sawardDoc_1").html("Invalid format !!! Please select proper file type");
                     zawardoc = 0;
                     $("#divAwardDoc_1").css({ "border": "1px solid red","background": "#FFCECE"});
                     $("#awardDoc_1").focus();                        
              }
              else
              {
                      $("#sawardDoc_1").html("");
                      zawardoc =1
                      $("#divAwardDoc_1").css({ "border": "","background": ""});
              }


          
    if(zawardsName ==1 &&   zawardsstatus==1 && zawardoc ==1 && zawardsDesc==1 ) 
        {         $("#awards_info").submit();

         }// iF   



} //submitAwards 
//============================ ADD MORE AWARDS & ACHIVEMENTS ========================================

function submitSkills()
    {
       
       var totalSkill = $(".countSkill").length;
       var zskillName = 0;
       var zskillstatus=0; 

         for(var i=1 ; i<= totalSkill; i++)
         {
         
            // check whether responsiblity name is not empty
             var info_skillname =  $("#skillName_"+i+"").val()
             if (info_skillname == null || info_skillname == "") 
             {
                      $("#sskillName_"+i+"").html("Please enter skill name");
                      zskillName = 0;
                     $("#skillName_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sskillName_"+i+"").html("");
                     zskillName =1;
                     $("#skillName_"+i+"").css({ "border": "","background": ""});
             }


            // check status = true
            if ($("#skillStatus_"+i+"").is(":checked")) 
            {
                $("#sskillStatus_"+i+"").html("");
                zskillstatus =1
                $("#divSkillStatus_"+i+"").css({ "border": "","background": ""});
             }
             else
             {
                $("#sskillStatus_"+i+"").html("Please approve skill from your side");
                zskillstatus = 0;
                $("#divSkillStatus_"+i+"").css({ "border": "1px solid red","background": "#FFCECE"});
             }

          
    } //for

    if(zskillName ==1 && zskillstatus==1 ) 
        {   
               $("#skills_info").submit();

         }// iF   



} //submitSkills 
//============================ ADD MORE skill =======================================================



function submitHobby()
    {
       
       var zhobbyName = 0;
       var zhobbystatus=0; 

            // check whether responsiblity name is not empty
             var info_hobbyname =  $("#hobbyName_1").val()
             if (info_hobbyname == null || info_hobbyname == "") 
             {
                      $("#shobbyName_1").html("Please enter hobby name");
                      zhobbyName = 0;
                     $("#hobbyName_1").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#shobbyName_1").html("");
                     zhobbyName =1;
                     $("#hobbyName_1").css({ "border": "","background": ""});
             }


            // check status = true
            if ($("#hobbyStatus_1").is(":checked")) 
            {
                $("#shobbyStatus_1").html("");
                zhobbystatus =1
                $("#divHobbyStatus_1").css({ "border": "","background": ""});
             }
             else
             {
                $("#shobbyStatus_1").html("Please approve hobby from your side");
                zhobbystatus = 0;
                $("#divHobbyStatus_1").css({ "border": "1px solid red","background": "#FFCECE"});
             }
    
    if(zhobbyName ==1 && zhobbystatus==1 ) 
        {   

            $("#hobbies_info").submit();

         }// iF   



} //submitHobby 
//============================ ADD MORE hobbies =======================================================





// add extra activity ......................................... 


function submitActivity()
    {
           var zActivityName = 0;
           var zActivitystatus=0; 
             
            // check whether responsiblity name is not empty
             var info_Activityname =  $("#activityName_1").val()
             if (info_Activityname == null || info_Activityname == "") 
             {
                      $("#sactivityName_1").html("Please enter activity name");
                      zActivityName = 0;
                     $("#activityName_1").css({ "border": "1px solid red","background": "#FFCECE"});
             }
             else
             {
                     $("#sactivityName_1").html("");
                     zActivityName =1;
                     $("#activityName_1").css({ "border": "","background": ""});
             }


            // check status = true
            if ($("#activityStatus_1").is(":checked")) 
            {
                $("#sactivityStatus_1").html("");
                zActivitystatus =1
                $("#divActivityStatus_1").css({ "border": "","background": ""});
             }
             else
             {
                $("#sactivityStatus_1").html("Please approve skill from your side");
                zActivitystatus = 0;
                $("#divActivityStatus_1").css({ "border": "1px solid red","background": "#FFCECE"});
             }

    if(zActivityName ==1 && zActivitystatus==1 ) 
        {   
               $("#ExtraActivity_info").submit();

         }// iF   



} //submitSkills 
</script>