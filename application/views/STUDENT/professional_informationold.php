<div class="tab-pane fade <?php if( $this->session->userdata("page_name")=="page_experience") { echo "in active"; $this->session->unset_userdata("page_name");}else {} ?>" id="professionalInformation">
<form role="form" id="professional-info" method="post" action="<?php echo base_url().'student/addExperience/'.$this->session->userdata('stud_id').'/page_experience/';?>" enctype="multipart/form-data">

<h4 class="page-header">Professional Experience Information:</h4>
<?php if($professional)
 { ?>
      <div class="row">
                  <div class="col-lg-12">
                  <div class="portlet portlet-green">
                  <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Professional Experience information</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive" style="overflow-y: hidden; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover" >
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Org. name</th>
												<!--<th>City</th>-->
                                                <th>Business Unit</th>
                                                <th>Designation</th>
                                                <th>Joining date</th>
                                                <th>Leaving date</th>                                                                                                
                                                <th>Exp. in months</th>
                                                <th>Responsibilities</th>
                                                <th>Document 1</th>
												<th>Document 2</th>
												<th>Document 3</th>
                                                <th>Status by Student</th>
                                                <th>Status by Placement team</th>
                                               <?php if($this->session->userdata("role") == "PLACEMENT_TEAM") {echo "<th>Delete</th> ";}  ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php $counter=0; foreach ($professional as $r ) {
                                                  $counter ++;
                                                  //show lable to student
                                                 if($this->session->userdata("role") == "PLACEMENT_TEAM")
                                                  {
                                                      if($r->verify_by_student == 1) {  $temp="<span class='badge green'>Approved</span>";} else {  $temp="<span class='badge orange'>Pending</span>"; }
                                                  }
                                                  else
                                                  {
                                                   if($r->verify_by_student == 2) {  $temp="<a href='".base_url()."student/std_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_experience/".$r->experience_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>";} else {  $temp="<span class='badge green'>Approved</span>"; }                                                 
                                                  }

                                                  //show lable to placement team
                                                  if($r->verify_by_PT == 1) {  $temp1="<span class='badge green'>Approved</span>";} else if($r->verify_by_PT == 2) {  $temp1="<span class='badge orange'>Pending</span>"; }else {  $temp1="<span class='badge red'>Rejected</span>"; }

                                                  //show link to placement team

                                                  if($r->verify_by_PT == 1) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_experience/".$r->experience_id."' style='cursor:pointer' ><span class='badge green'>Approved</span></a>";} else if($r->verify_by_PT == 2) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_experience/".$r->experience_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>"; }else { $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_experience/".$r->experience_id."' style='cursor:pointer'><span class='badge red'>Rejected</span></a>"; }                                                  

                                                  $ExpDetail="<div id='Expdetails_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Responsibilities</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->responsibilities."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='functionShowExpDetails(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a></div>";

                                                  if($r->document_link){ $Documentlink = "<a target='_blank' href='".base_url()."".$r->document_link."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink = "";}
													
												  if($r->document_link2){ $Documentlink2 = "<a target='_blank' href='".base_url()."".$r->document_link2."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink2 = "";}
												  
												  if($r->document_link3){ $Documentlink3 = "<a target='_blank' href='".base_url()."".$r->document_link3."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink3 = "";}


                                              $datetime1 = date_create($r->date_from);
                                              $datetime2 = date_create($r->date_to);
                                              $interval = date_diff($datetime1, $datetime2);
                                              $exp=  $interval->m + ($interval->y * 12);


                                              if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->company_name.', '.$r->city_name." </td><td>"
                                                .$r->business_unit." </td><td>"
                                                .$r->job_profile." </td><td>"
                                                .date('d-m-Y',strtotime($r->date_from))." </td><td>"
                                                .date('d-m-Y',strtotime($r->date_to))." </td><td>"                                                
                                                .$exp." </td><td>"                                                
                                                .$ExpDetail." </td><td>"                                                       
                                                .$Documentlink." </td><td>"
												.$Documentlink2." </td><td>"
												.$Documentlink3." </td><td>"
                                                .$temp." </td><td>"
                                                .$link." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                                  <a title='Delete This Qualification Information' style='cursor:pointer' 
                                                  title='Delete this experience'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>
                                                </tr>";

                                                }
                                                else
                                                {
                                                 echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->company_name.', '.$r->city_name." </td><td>"
                                                .$r->business_unit." </td><td>"
                                                .$r->job_profile." </td><td>"
                                                .date('d-m-Y',strtotime($r->date_from))." </td><td>"
                                                .date('d-m-Y',strtotime($r->date_to))." </td><td>"                                                                
                                                .$exp." </td><td>"       
                                                .$ExpDetail." </td><td>"                                                       
                                                .$Documentlink." </td><td>"
												.$Documentlink2." </td><td>"
												.$Documentlink3." </td><td>"
                                                .$temp." </td><td>"
                                                .$temp1." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                                  <a href='".base_url()."placementteam/pt_deleteStudInfo/".$this->session->userdata('stud_id')."/page_experience/".$r->experience_id."' style='cursor:pointer' 
                                                  title='Delete this experience'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>";
                                                }//else

                                                }//foreach

                                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
              </div>
              <?php } ?>
      <h4 class="page-header">Add new professional experience information</h4>


<div class="form-group countEexp  col-lg-12 " > 

                  <div class="form-group col-lg-6">
                    <label>Organization Name <span style="color:#b81212">*</span> </label>
                    <input type="text" class="form-control"  name="org[1][name]" id="orgName_1" placeholder="Enter Organization Name" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "READONLY"; } ?>>
                    <span id="sorgName_1" style="color:rgb(184,18,18);"></span>
                  </div>
				  
				  <div class="form-group col-lg-6">
                    <label>City <span style="color:#b81212">*</span> </label>
                    <input type="text" class="form-control"  name="org[1][city]" id="orgCity_1" placeholder="Enter City Name" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "READONLY"; } ?>>
                    <span id="sorgCity_1" style="color:rgb(184,18,18);"></span>
                  </div>


                <div class="form-group col-lg-12">
                  <div class="row">

                        <div class="form-group col-lg-6">
                          <label>Business Unit <span style="color:#b81212">*</span> </label>
                          <input type="text" class="form-control"  name="org[1][domain]" id="orgDomain_1" placeholder="Enter Business Unit Name" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "READONLY"; } ?>>
                          <span id="sorgDomain_1" style="color:rgb(184,18,18);"></span>
                        </div>


                      <div class="form-group col-lg-6">
                          <label>Designation <span style="color:#b81212">*</span> </label>
                          <input type="text" class="form-control" name="org[1][org_post]" id="jobPost_1" placeholder="Enter your designation" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "READONLY"; } ?>>
                          <span id="sjobPost_1" style="color:rgb(184,18,18);"></span>
                      </div>

                  </div>
               </div>

                <div class="form-group col-lg-12">
                  <div class="row">

                        <div class="form-group col-lg-6 datecall" >
                          <label>Joining Date <span style="color:#b81212">*</span> </label>
                          <input type="text" class="form-control" name="org[1][date_in]" id="dateIn_1" placeholder="Select date when you joined" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "READONLY"; echo " disabled"; } ?>>
                          <span id="sdateIn_1" style="color:rgb(184,18,18);"></span>
                        </div>

                        <div class="form-group col-lg-6 datecall">
                          <label>Leaving Date <span style="color:#b81212">*</span> </label>
                          <input type="text" class="form-control" name="org[1][date_out]" id="dateOut_1" placeholder="Select date when you left" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?>>
                          <span id="sdateOut_1" style="color:rgb(184,18,18);"></span>
                        </div>

                  </div>
                </div>



                  <div class="form-group col-lg-12">
                     <label>Responsibilities </label>
                        <textarea  name="org[1][respo]" id="respo_1" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>></textarea>
                        <span id="srespo_1" style="color:rgb(184,18,18);"></span>
<!--                       <textarea class="ckeditor form-control" name="org[1][respo]" id="respo_1" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[2]) )echo "{}"; else{echo "disabled"; } ?>></textarea> -->
                  </div>
                <div class="form-group col-lg-12 countclass"  id="divOrgDoc_1">
                            <div class="form-group col-lg-7">
                              <label>Document</label>
                              <input name="org[1][doc]" type="file" id="doc_pro_1" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>>
                              <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                              <span id="sdoc_pro_1" style="color:rgb(184,18,18);"></span>
                            </div>
							<div class="form-group col-lg-2 social-buttons">
								<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreDocument(2)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa ">+</i>Add</a>
                            </div>
                  </div>
				  
				  <div id="addDocument">
                                                
                  </div>
				  
				  <div class="form-group col-lg-12" id="divOrgDoc_2" style="display:none;">

                            <div class="form-group col-lg-7">
                              <label>Document 2 </label>
                              <input name="org2[1][doc]" type="file" id="doc_pro_2" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>>
                              <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                              <span id="sdoc_pro_2" style="color:rgb(184,18,18);"></span>
                            </div>
							<div class="form-group col-lg-2 social-buttons">
								<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="addMoreDocument(3)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-plus"></i>Add</a>
                            </div>
							<div class="form-group col-lg-3 social-buttons">
								<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreDocument(2)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
                            </div>
                  </div>
				  
				  <div class="form-group col-lg-12" id="divOrgDoc_3" style="display:none;">
                            <div class="form-group col-lg-7">
                              <label>Document 3 </label>
                              <input name="org3[1][doc]" type="file" id="doc_pro_3" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>>
                              <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                              <span id="sdoc_pro_3" style="color:rgb(184,18,18);"></span>
                            </div>
							<div class="form-group col-lg-3 social-buttons">
								<a id="anchoradd" class="btn btn-block btn-social btn-bitbucket" onclick="delMoreDocument(3)" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>><i class="fa fa-times"></i>Delete</a>
                            </div>
                  </div>
				  
				  <div class="form-group col-lg-12">
                            <div class="checkbox col-lg-12" id="divOrgStatus_1" >
                              <input type="checkbox" class="form-control" name="status_1" id="status_pro_1" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) )echo "{}"; else{echo "disabled"; } ?>>Status <span style="color:#b81212">*</span> </label> 
                              <p class="help-block"><i class="fa fa-warning"></i> By clicking on this you confirm that professional information are correct from your side</p>
                           <span id="sstatus_pro_1" style="color:rgb(184,18,18);"></span>
                            </div> 
                  </div>


</div>

        <?php 
            if( visiblityStatus_empty($this->session->userdata("role"),$form_status[3]) ) {
				$query = mysql_query("select date_to from tbl_student_experiences where student_id = ".$this->session->userdata('stud_id'). " order by experience_id desc limit 1");
				$result = mysql_fetch_array($query);          	
            	?>
          <div class="form-group col-lg-12">
          		<input type="hidden" id="newdatavalue" value="<?php echo date('d-m-Y',strtotime($result['date_to'])); ?>" />  
              <button type="button" onclick="submitExperience();" class="btn btn-default" >
                Add Experience</button>
          </div>
        <?php } ?>

</form>
</div>
<script type="text/javascript">
function addMoreDocument(div_id)
	{
        $("#divOrgDoc_"+div_id).css("display", "block");
    }
	
function delMoreDocument(option_id)
        {
            if(confirm('Are you sure you want to delete?')){
			$("#divOrgDoc_"+option_id).css("display", "none");
            }
        }
</script>