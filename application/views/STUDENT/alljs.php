<!-- GLOBAL SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/1.7.2.jquery.min.js"></script>

    <script src="<?php echo base_url();?>assets/js/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/popupoverlay/defaults.js"></script>
	
    <!-- Logout Notification Box -->
    <div id="logout">
        <div class="logout-message">

        <?php 
         if($this->session->userdata("role")== "STUDENT" )
            { 
                    foreach ($info as $r){ 

                        ?>
                       <img class="img-circle img-logout" src="<?php if($r->profile_picture) {echo base_url()."".$r->profile_picture;} else {echo base_url()."assets/img/avtar.jpg";} ?>" alt="" 
                                        style=" height: 160px; width: 150px;  margin-left: 15px;" >
            <?php }
        }

         else 
            { 
                    foreach ($pt_info as $r){ 

                        ?>
                       <img class="img-circle img-logout" src="<?php if($r->profile_picture) {echo base_url()."".$r->profile_picture;} else {echo base_url()."assets/img/avtar.jpg";} ?>" alt="" 
                                        style=" height: 160px; width: 150px;  margin-left: 15px;" >
            <?php }
        }
        ?>




            <h3>
                <i class="fa fa-sign-out text-green"></i> Ready to go?
            </h3>
            <p>Select "Logout" below if you are ready<br> to end your current session.</p>
            <ul class="list-inline">
                <li>





                    <a href="<?php echo base_url();?>welcome/logout" class="btn btn-green">
                        <strong>Logout</strong>
                    </a>
                </li>
                <li>
                    <button class="logout_close btn btn-green">Cancel</button>
                </li>
            </ul>
        </div>
    </div>
    <!-- /#logout -->
    <!-- Logout Notification jQuery -->
    <script src="<?php echo base_url();?>assets/js/plugins/popupoverlay/logout.js"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo base_url();?>assets/js/plugins/hisrc/hisrc.js"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    
    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <!-- HubSpot Messenger -->
    <script src="<?php echo base_url();?>assets/js/plugins/messenger/messenger.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/messenger/messenger-theme-flat.js"></script>
    <!-- Date Range Picker -->
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/moment.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Morris Charts -->
    <script src="<?php echo base_url();?>assets/js/plugins/morris/raphael-2.1.0.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/morris/morris.js"></script>
    <!-- Flot Charts -->
    <script src="<?php echo base_url();?>assets/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/flot/jquery.flot.resize.js"></script>
    <!-- Sparkline Charts -->
    <script src="<?php echo base_url();?>assets/js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Moment.js -->
    <script src="<?php echo base_url();?>assets/js/plugins/moment/moment.min.js"></script>
    <!-- jQuery Vector Map -->
    <script src="<?php echo base_url();?>assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script src="<?php echo base_url();?>assets/js/demo/map-demo-data.js"></script>
    <!-- Easy Pie Chart -->
    <script src="<?php echo base_url();?>assets/js/plugins/easypiechart/jquery.easypiechart.min.js"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url();?>assets/js/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/datatables/datatables-bs3.js"></script>

    <!-- THEME SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/flex.js"></script>
    <script src="<?php echo base_url();?>assets/js/demo/dashboard-demo.js"></script>


    <!-- What you see what you get -->
    <script src="<?php echo base_url();?>assets/js/plugins/summernote/summernote.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/demo/wysiwyg-demo.js"></script>

    <!--  CK EDITOR-->

       <script src="<?php echo base_url();?>assets/js/lib/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
        //Professional Information
        CKEDITOR.replace('respo_1',{
        toolbar :
        [
            {   
                 name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], 
                //items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
                items: [ 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
            },
            { 
                name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
                items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'insert', items: [  'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
        ]
        });

        //Summer Project
        CKEDITOR.replace('prj_descrption_1',{
        toolbar :
        [
            {   
                 name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], 
                //items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
                items: [ 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
            },
            { 
                name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
                items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'insert', items: [  'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
        ]
        });

        //Research Project
        CKEDITOR.replace('Rprj_descrption_1',{
        toolbar :
        [
            {   
                 name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], 
                //items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
                items: [ 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
            },
            { 
                name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
                items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'insert', items: [  'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
        ]
        });

        //Assignement Project
        CKEDITOR.replace('assignmentPrj_descrption_1',{
        toolbar :
        [
            {   
                 name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], 
                //items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
                items: [ 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
            },
            { 
                name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
                items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'insert', items: [  'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
        ]
        });

        //Extra Project
        CKEDITOR.replace('ExtraPrj_descrption_1',{
        toolbar :
        [
            {   
                 name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], 
                //items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
                items: [ 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
            },
            { 
                name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
                items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'insert', items: [  'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
        ]
        });

        //Additional course Information:
        CKEDITOR.replace('cors_descrption_1',{
        toolbar :
        [
            {   
                 name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], 
                //items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
                items: [ 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
            },
            { 
                name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
                items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'insert', items: [  'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
        ]
        });



        //Additional course Information:
        CKEDITOR.replace('P_Respondblities_1',{
        toolbar :
        [
            {   
                 name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], 
                //items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
                items: [ 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
            },
            { 
                name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
                items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'insert', items: [  'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
        ]
        });


        //Awards or Achivements Information:
        CKEDITOR.replace('awardExtraInfo_1',{
        toolbar :
        [
            {   
                 name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], 
                //items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
                items: [ 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
            },
            { 
                name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
                items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'insert', items: [  'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
        ]
        });


        //basic information
        CKEDITOR.replace('stud_objactives',{
        toolbar :
        [
            {   
                 name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], 
                //items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
                items: [ 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] 
            },
            { 
                name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
                items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'insert', items: [  'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
        ]
        });

    </script>