                    <div class='feedback'>
                    <?php if($this->session->flashdata('sucess'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('sucess')."', 'success');
                        </script>";


                        if($this->session->flashdata('error')) {
                     echo "
                        <script>
                        sweetAlert('Opps..!', '".$this->session->flashdata('error')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
<div class="tab-pane fade <?php if( $this->session->userdata("page_name")=="page_additional_course") { echo "in active"; $this->session->unset_userdata("page_name");}else {} ?>" id="aditionalCourseInformation">
<form role="form" id="additionalCourse_info" method="post" action="<?php echo base_url().'student/addAdditionalCourse/'.$this->session->userdata('stud_id').'/page_additional_course/';?>" enctype="multipart/form-data">
<h4 class="page-header">Additional course Information:</h4>
<!-- from student controller function profile_data -->
<?php if($additionalCourse)
 { ?>
      <div class="row">
                  <div class="col-lg-12">
                  <div class="portlet portlet-green">
                  <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Additional course information</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive" style="overflow-y: hidden; overflow-x: scroll;">
                                    <table class="table table-striped table-bordered table-hover" >
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Course name</th>
                                                <th>Course Domain</th>
                                                <th>Description</th>
                                                <th>Document</th>                                                                                                
                                                <th>Status by student</th>
                                                <th>Status by Placement team</th>
                                               <?php if($this->session->userdata("role") == "PLACEMENT_TEAM") {echo "<th>Delete</th> ";} else{
                                               	 if( visiblityStatus_empty($this->session->userdata("role"),$form_status[9]) )echo "<th>Delete</th> "; }  ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <?php $counter=0; foreach ($additionalCourse as $r ) {
                                                  $counter ++;

                                                  //show lable to student
                                                
                                                 if($this->session->userdata("role") == "PLACEMENT_TEAM")
                                                  {
                                                      if($r->verify_by_student == 1) {  $temp="<span class='badge green'>Approved</span>";} else {  $temp="<span class='badge orange'>Pending</span>"; }
                                                  }
                                                  else
                                                  {
                                                   if($r->verify_by_student == 2) {  $temp="<a href='".base_url()."student/std_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_additional_course/".$r->a_c_id."' style='cursor:pointer;'><span class='badge orange'>Pending</span></a>";} else {  $temp="<span class='badge green'>Approved</span>"; }                                                 
                                                  }

                                                  //show lable to placement team
                                                  if($r->verify_by_PT == 1) {  $temp1="<span class='badge green'>Approved</span>";} else if($r->verify_by_PT == 2) {  $temp1="<span class='badge orange'>Pending</span>"; }else {  $temp1="<span class='badge red'>Rejected</span>"; }

                                                  //show link to placement team

                                                  if($r->verify_by_PT == 1) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_additional_course/".$r->a_c_id."' style='cursor:pointer'><span class='badge green'>Approved</span></a>";} else if($r->verify_by_PT == 2) {  $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_additional_course/".$r->a_c_id."' style='cursor:pointer'><span class='badge orange'>Pending</span></a>"; }else { $link="<a href='".base_url()."placementteam/pt_changeStudentInfoStatus/".$this->session->userdata('stud_id')."/page_additional_course/".$r->a_c_id."' style='cursor:pointer'><span class='badge red'>Rejected</span></a>"; }                                                  

                                                  if($r->document_link){ $Documentlink = "<a target='_blank' href='".base_url()."".$r->document_link."' style='cursor:pointer' title='Click Here To View'><i class='fa fa-files-o'> view </i></a>";} else  { $Documentlink = "";}

                                                  $CourseDetail="<div id='Coursedetails_".$counter."' class='modal fade'><div class='modal-dialog'><div class='modal-content' style='width: 500px;'><div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>&times;</button><h3 class='modal-title'>Additional Course Descrption</h3></div><div class='modal-body'><div class='row pricing-circle'><div class='col-md-3'></div><div class='col-md-6'>".$r->course_description."</div></div></div></div></div></div> 
                                                              <i class='fa fa-tasks'></i><a onClick='functionCourseDetails(".$counter.");' style='cursor:pointer' title='Click Here To View'> view</a></div>";
 
												  //if(isset($r->other_domain)) { $actualdomain = $r->domain } else { $actualdomain = $r->other_domain };
												  if($r->other_domain)
												  {
													  $actualdomain = $r->other_domain;
												  }
												  else
												  {
													  $nqry = mysql_query("select md.domain_name from master_domain md inner join tbl_student_additional_courses sc on md.domain_id = sc.course_domain where sc.student_id = ".$r->student_id ." and a_c_id = ".$r->a_c_id);
													  $rnqry = mysql_fetch_array($nqry);
													  $actualdomain = $rnqry['domain_name'];
												  }


                                              if($this->session->userdata("role") == "PLACEMENT_TEAM") {
                                                echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->course_name." </td><td>"
                                                .$actualdomain." </td><td>"
                                                .$CourseDetail." </td><td>"
                                                .$Documentlink." </td><td>"                                                                                                
                                                .$temp." </td><td>"
                                                .$link." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                               <a onclick=deleteStudentInformation('page_additional_course',".$r->a_c_id.") title='Delete This Additional Couese Information' style='cursor:pointer;'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>
                                                </tr>"; 

                                                }
                                                else
                                                {
                                                	if( visiblityStatus_empty($this->session->userdata("role"),$form_status[9]) )
	 
	 {
                                                 echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->course_name." </td><td>"
                                                .$actualdomain." </td><td>"
                                                .$CourseDetail." </td><td>"
                                                .$Documentlink." </td><td>"                                                                                                
                                                .$temp." </td><td>"
                                                .$temp1." </td><td>
                                                  <div class='fa-hover col-sm-2'>
                                               <a onclick=deleteStudentInformation('page_additional_course',".$r->a_c_id.") title='Delete This Additional Couese Information' style='cursor:pointer;'>
                                                  <i class='fa fa-times'></i>
                                                  </a></div></td></tr>";
                                                  }
                                                  else
                                                  {
                                                  echo "<tr> <td> "
                                                .$counter." </td><td>"
                                                .$r->course_name." </td><td>"
                                                .$actualdomain." </td><td>"
                                                .$CourseDetail." </td><td>"
                                                .$Documentlink." </td><td>"                                                                                                
                                                .$temp." </td><td>"
                                                .$temp1." </td></tr>";
                                                  }
                                                }//else

                                                }//foreach

                                                ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
              </div>
              <?php } ?>
      <h4 class="page-header">Add new additional course</h4>

  <div class="form-group countAddCourse  col-lg-12 ">          

        <div class="col-lg-12">          
            <div class="form-group col-lg-6">
              <label>Course Name <span style="color:#b81212">*</span> </label>
              <input type="text" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[9]) )echo "{}"; else{echo "READONLY"; } ?> name="cors[1][name]" id="cors_name_1" placeholder="Enter Course Name">
              <span id="scors_name_1" style="color:rgb(184,18,18);"></span>
            </div>

            <div class="form-group col-lg-6">
               <label> Course Domain <span style="color:#b81212">*</span> </label>
                <select  class="form-control"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[9]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?> name="cors[1][domain]" id="cors_domain_1">
                  <option value=""> Select Course Domain: </option>
                  <?php 
                  foreach ($m_domain as $r) {
                    echo " <option value=".$r->domain_id."> ". $r->domain_name." </option>";
                  }
                  ?>
				  <!--<option value="99">Other</option>-->
                </select>
				  <input class="hideme" type="text" id="otherdomain" name="otherdomain" size="30">
                  <span id="scors_domain_1" style="color:rgb(184,18,18);"></span>
            </div>
          </div>
            
            <div class="form-group col-lg-12">
              <label>Description</label>
                <textarea class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[9]) )echo "{}"; else{echo "disabled"; } ?> name="cors[1][descrption]" id="cors_descrption_1" placeholder="Value added information"></textarea>
                <span id="scors_descrption_1" style="color:rgb(184,18,18);"></span>
            </div>

        <div class="col-lg-12">          
            <div class="form-group col-lg-6" id="divcourse_doc_1">
              <label>Document</label>
              <input type="file" id="corsDoc_1" name="cors[1][doc]" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[9]) )echo "{}"; else{echo "disabled"; } ?>>
              <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
              <span id="scorsDoc_1" style="color:rgb(184,18,18);"></span>
            </div>


            <div class="checkbox col-lg-6" id="cors_status_1">
               <input type="checkbox" class="form-control" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[9]) )echo "{}"; else{echo "READONLY"; echo " disabled";} ?> name="cors[1][status]" id="corsStatus_1" >
               Status <span style="color:#b81212">*</span> </label>
           <p class="help-block"><i class="fa fa-warning"></i> By clicking on this you confirm that additional course information are correct from your side</p>

                  <span id="scorsStatus_1" style="color:rgb(184,18,18);"></span>
            </div>
        </div>

      </div> <!--  countAddcourse -->

      <?php 
          if( visiblityStatus_empty($this->session->userdata("role"),$form_status[9]) )
          {
            ?>


          <div class="form-group col-lg-12">               
              <button type="button" onclick="submitAddCourse();" class="btn btn-default">
                Add Course</button>
          </div>
          <?php } ?>
</form>
</div>