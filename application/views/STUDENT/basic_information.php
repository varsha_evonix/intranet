<?php 
function visiblityStatus($role,$checkStatus,$formStatus)
{
            if(($role=="PLACEMENT_TEAM") || ($role == "FACULTY"))
            {
                return true;
            }
            else
            {
                //if(($checkStatus == 0 )&& ($formStatus == 1)) //original
				if($formStatus == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        function visiblityStatus_empty($role,$formStatus)
        {

            if(($role=="PLACEMENT_TEAM") || ($role == "FACULTY"))
            {
                return true;
            }
            else
            {
                if($formStatus == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }


?>


<div class='feedback'>
<?php if($this->session->flashdata('sucess')){
         echo "
        <script>
        sweetAlert('Ok', '".$this->session->flashdata('sucess')."', 'success');
        </script>";
    }

    if($this->session->flashdata('error')) {
     echo "
        <script>
        sweetAlert('Opps..!', '".$this->session->flashdata('error')."', 'error');
        </script>";
    }
    
 ?>
</div>

<?php if( $this->session->userdata("page_name"))
{ ?>
    <div class="tab-pane fade <?php if( $this->session->userdata("page_name")=="page_basic_info") { echo "in active"; $this->session->unset_userdata("page_name"); }else {} ?>" id="basicInformation">
<?php 
}
else{ 

?>
    <div class="tab-pane fade in active" id="basicInformation">
<?php 
}
?>
<?php
  foreach ($info as $r) {

           $this->session->set_userdata("stud_id",$r->student_id); 
		   //echo $r->PRN;
		   $qry = mysql_query("select student_id from tbl_student_information where PRN = ".$r->PRN);
		   $rqry = mysql_fetch_array($qry);
		   $rqry['student_id'];
		   $q2 = mysql_query("select batch_course_id from tbl_batch_student where student_id = ".$rqry['student_id']);
		   $rq2 = mysql_fetch_array($q2);
		   $rq2['batch_course_id'];
		   $q3 = mysql_query("select batch_id, course_id from tbl_batch_course where batch_course_id = ".$rq2['batch_course_id']);
		   $rq3 = mysql_fetch_array($q3);
		   $rq3['batch_id'];
		   $rq3['course_id'];
		   $q4 = mysql_query("select batch_name from master_batch where batch_id = ".$rq3['batch_id']);
		   $rq4 = mysql_fetch_array($q4);
		   $rq4['batch_name'];
		   $q5 = mysql_query("select course_name from master_course where course_id = ".$rq3['course_id']);
		   $rq5 = mysql_fetch_array($q5);
		   $rq5['course_name'];
   ?>
    <form role="form" id="basic-info" method="post" action="<?php echo base_url().'student/updateinfo/'.$this->session->userdata('stud_id').'/page_basic_info/';?>" enctype="multipart/form-data">
                <h4 class="page-header">Basic Information:</h4>
                      <div class="form-group ">
                            <div class="form-group col-sm-6">
                               <label>PRN</label>
                                <input type="text" class="form-control" value="<?php  echo $r->PRN; ?>" disabled>
                            </div>
                            <div class="form-group  col-sm-6">
                                <label>Batch</label>
                                <input type="text" class="form-control" value="<?php echo $rq4['batch_name']; ?>" disabled>
                            </div>
                        </div>

                    <h4 class="page-header col-sm-12">Personal Information:</h4>
                    <div class="form-group">
                        <label>First Name <span style="color:#b81212">*</span> </label>
                        <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter first name" value="<?php  echo $r->first_name; ?>"  <?php /*if($this->session->userdata("role")== "PLACEMENT_TEAM" ){ } else {echo "READONLY"; }*/ ?> <?php if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]) )echo "{}";
                             else{echo "disabled"; } ?> required> 
                       <span id="sfname" style="color:rgb(184,18,18);"></span>
                    </div>
                    <div class="form-group">
                        <label>Middle Name</label>
                        <input type="text" class="form-control" name="mname" id="mname"  placeholder="Enter middle name" value="<?php  echo $r->middle_name; ?>"  <?php /*if($this->session->userdata("role")== "PLACEMENT_TEAM" ){ } else {echo "READONLY"; }*/ ?> <?php if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]) )echo "{}";
                             else{echo "disabled"; } ?> required>
                        <span id="smname" style="color:rgb(184,18,18);"></span>
                    </div>
                    <div class="form-group">
                        <label>Last Name <span style="color:#b81212">*</span> </label>
                        <input type="text" class="form-control"  name="lname" id="lname" placeholder=" Enter last name" value="<?php  echo $r->last_name; ?>" <?php /*if($this->session->userdata("role")== "PLACEMENT_TEAM" ){ } else {echo "READONLY"; }*/ ?> <?php if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]) )echo "{}";
                             else{echo "disabled"; } ?> >
                       <span id="slname" style="color:rgb(184,18,18);"></span>
                    </div>

                    <div class="form-group ">

                    <div class="form-group col-sm-6 " >
                           <label>Date Of Birth <span style="color:#b81212">*</span> </label>
                           <div id="sandbox-container">
                                <input class="form-control dateWork" type="text" placeholder="Select date of birth" name="bdate" id="bdate"  value="<?php  echo date('d-m-Y',strtotime($r->DOB)); ?>" required <?php /*if($this->session->userdata("role")== "PLACEMENT_TEAM" ){ } else {echo "READONLY"; }*/ ?> <?php if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]) )echo "{}";
                             else{echo "disabled"; } ?> />
                            </div>
                      <span id="sbdate" style="color:rgb(184,18,18);"></span>                                                        
                    </div>

                    <div class="form-group" id="genderInfo">
                                <label >Gender <span style="color:#b81212">*</span> </label>
                                <?php 

                                /*if($this->session->userdata('role')== 'PLACEMENT_TEAM' )*/
								if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]) )
                                    {
                                            $dis = "";
                                    } else 
                                    {
                                            $dis = "disabled"; 
                                    }

                                if($r->gender == "Female")
                                {

                                    echo "<label class='radio-inline'>
                                        <input type='radio' id='gender' name='gender' value='Male' $dis>Male
                                    </label>
                                    <label class='radio-inline'>
                                        <input type='radio' id='gender' name='gender' value='Female' $dis checked>Female
                                    </label>";


                                }else 
                                 {

                                    echo "<label class='radio-inline'>
                                        <input type='radio' id='gender' name='gender' value='Male' $dis checked >Male
                                    </label>
                                    <label class='radio-inline'>
                                        <input type='radio' id='gender' name='gender' value='Female' $dis >Female
                                    </label>";                                 


                                 }
                                ?>
                        
                         <span id="sgender" style="color:rgb(184,18,18);"></span>                                                                                    
                     </div>
                <?php }  // basic Information?> 


   <!-- =====================CONTACTS INFORMATION===================================== -->             

                    <?php foreach ($info as $r) { ?>
                    <h4 class="page-header col-sm-12">Contact Details:</h4>

                    <div class="form-group">

                        <div class="form-group col-sm-6">
                            <label><i class="fa fa-envelope-o fa-fw"></i> Contact 1 <span style="color:#b81212">*</span> </label>
                            <input placeholder="Contact no 1" type="text" maxlength= "10" id="con1" name="con1" class="form-control" value="<?php  echo $r->contact_no_1; ?>"  required 
                            <?php  /*if($this->session->userdata("role")== "PLACEMENT_TEAM" ){ } else {echo "disabled"; }*/ ?> <?php if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]) )echo "{}";
                             else{echo "disabled"; } ?>>
                            <span id="scon1" style="color:rgb(184,18,18);"></span>                            

                        </div>

                        <div class="form-group col-sm-6">
                            <label><i class="fa fa-globe fa-fw"></i> Contact 2</label>
                            <input placeholder="Contact no 2" type="text" maxlength= "10" id="con2" name="con2" class="form-control" value="<?php  echo $r->contact_no_2; ?>" 
                            <?php if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]) )echo "{}";
                             else{echo "disabled"; } ?> >
                            <span id="scon2" style="color:rgb(184,18,18);"></span>                            

                        </div>
        
                    </div>

                   <div class="form-group">
                    
                        <div class="form-group col-sm-6">
                            <label><i class="fa fa-envelope-o fa-fw"></i> Primary Email Address <span style="color:#b81212">*</span> </label>
                            <input placeholder="Primary email address" type="text" class="form-control" name="p_email" id="pemail" value="<?php  echo $r->primary_email; ?>"  required <?php /*if($this->session->userdata("role")== "PLACEMENT_TEAM" ){ } else {echo "disabled"; }*/ ?> <?php if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]) )echo "{}";
                             else{echo "disabled"; } ?>>
                            <span id="spemail" style="color:rgb(184,18,18);"></span>                            
                        </div>

                        <div class="form-group col-sm-6">
                            <label><i class="fa fa-envelope-o fa-fw"></i> Allocated Email Address</label>
                            <input placeholder="Allocated email address" type="text" class="form-control" name="allocated_email" value="<?php  echo $r->allocated_email; ?>"  required disabled> 
                            <span id="saemail" style="color:rgb(184,18,18);"></span>                            
                        </div>

                    </div>

                    <?php 

                  } // contact information 
                  ?>

   <!-- =====================Objective===================================== -->             

            <div class="form-group col-lg-12">
              <label>Objective <span style="color:#b81212">*</span></label>
              <textarea class="form-control" name="stud_objactives" id="stud_objectives"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[0]) )echo "{}"; else{echo "disabled"; }  ?> required > <?php if($r->objactives) { echo $r->objactives; } else{ }  ?></textarea>
                            <span id="sstud_objactives" style="color:rgb(184,18,18);"></span>                            

            </div>



   <!-- =====================SOCIAL PROFILES===================================== -->             


                    <h4 class="page-header">Social Profile:</h4>
                    <div class="form-group">

                            <label></i> LinkedIn Profile URL</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-linkedin"></i>
                                </span>
                                <input type="text"  name="social_linkedin" id="social_linkedin"  class="form-control" value="<?php if($r->social_linkedin_id) { echo $r->social_linkedin_id; } else{ } ?>" required <?php if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]) )echo "{}";
                                     else{echo "disabled"; } ?> >
                            </div>
                            <span id="ssocial_linkedin" style="color:rgb(184,18,18);"></span>                            
                    </div>

   <!-- =====================Documents and profile picture===================================== -->             

                   <h4 class="page-header">Current Profile picture:</h4>

                   <div class="form-group col-lg-12" id="divProfilePicture">
                       <div class="form-group col-lg-6">
                          <label>Choose New Profile Picture : <span style="color:#b81212">*</span> </label>
                          <input type="file" name="profile_picture" id="profile_picture"  <?php /*if( $this->session->userdata("role") =="PLACEMENT_TEAM" )echo "{}"; else{echo "disabled"; }*/  ?> <?php if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]) )echo "{}";
                             else{echo "disabled"; } ?> required>
                          <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG and max size 500kb</p>
                          <span id="sprofile_picture" style="color:rgb(184,18,18);"></span>
                        </div>

                        <div class="form-group col-lg-6">
                              <input type="hidden" id="hiddenPP" value="<?php if($r->profile_picture) {echo "true";} else {echo "false";} ?>">
                             <img class="img-responsive img-profile" src=" <?php if($r->profile_picture) { echo ''.base_url().''.$r->profile_picture.'';} else {echo ''.base_url().'assets/img/avtar.jpg'; } ?>" style="height:150px" alt=""> 
                        </div>
                    </div>

                   <h4 class="page-header">Personal Documents:</h4>
                   <div class="form-group col-lg-12">
                       <div class="form-group col-lg-12" id="divpassport">
                               <h4 class="">Passport:</h4>
                               <hr />
                               <div class="form-group col-lg-6">
                                  <label>Choose File :</label>
                                  <input type="file" name="doc_passport" id="doc_passport"  <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[0]) )echo "{}"; else{echo "disabled"; }  ?> required>
                                  <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                  <span id="sdoc_passport" style="color:rgb(184,18,18);"></span>

                                </div>
                               <div class="form-group col-lg-6">
                              <input type="hidden" id="hiddenPassport" value="<?php if($r->doc_passport) {echo "true";} else {echo "false";} ?>">   
								<?php
									$r->doc_passport;
									$ext = end((explode(".", $r->doc_passport)));
									if($ext != pdf && $ext != PDF)
									{?>							  
										<img class="img-responsive img-profile" src=" <?php if($r->doc_passport) { echo ''.base_url().''.$r->doc_passport.'';} else {echo ''.base_url().'assets/img/no-image.gif'; } ?>" style="height:150px" alt=""><?php
									}
									else
									{?>
										<a title="Click Here To View" style="cursor:pointer" href="<?php echo ''.base_url().''.$r->doc_passport; ?>" target="_blank"><i class="fa fa-files-o"> Click here to view pdf </i></a><?php
									}?>
                            </div>
                        </div>
                       <div class="form-group col-lg-12" id="divVoterId">
                               <h4 class="">Voter Id / Pan Card:</h4>
                               <hr />
                               <div class="form-group col-lg-6">

                                  <label>Choose File :</label>
                                  <input type="file" name="doc_voter_id" id="doc_voter_id" <?php if( visiblityStatus_empty($this->session->userdata("role"),$form_status[0]) )echo "{}"; else{echo "disabled"; }  ?> required>
                                  <p class="help-block"><i class="fa fa-warning"></i> Supported formats: JPG, JPEG, GIF, PNG, PDF and max size 500kb</p>
                                  <span id="sdoc_voter_id" style="color:rgb(184,18,18);"></span>

                                </div>                            
                               <div class="form-group col-lg-6">
                              <input type="hidden" id="hiddenVoterId" value="<?php if($r->doc_voter_id) {echo "true";} else {echo "false";} ?>">
							  <?php
									$r->doc_voter_id;
									$ext = end((explode(".", $r->doc_voter_id)));
									if($ext != pdf && $ext != PDF)
									{?>
										<img class="img-responsive img-profile" src=" <?php if($r->doc_voter_id) { echo ''.base_url().''.$r->doc_voter_id.'';} else {echo ''.base_url().'assets/img/no-image.gif'; } ?>" style="height:150px" alt=""><?php
									}
									else
									{?>
										<a title="Click Here To View" style="cursor:pointer" href="<?php echo ''.base_url().''.$r->doc_voter_id; ?>" target="_blank"><i class="fa fa-files-o"> Click here to view pdf </i></a><?php
									}?>
                            </div>
                        </div>                        
                </div>



                    <?php if($info)
                    {
                     if( visiblityStatus($this->session->userdata("role"),$r->verify_by_student,$form_status[0]))
                     { 
                        ?>
                          <div class="form-group col-lg-12">               
                              <button type="button" onclick="submitBasicInfo()" class="btn btn-default">
                                Update Profile</button>
                          </div>
                     <?php 
                        }
						// if
                    } //(#addperm)

                     ?>

                </form>
    </div>
</div>