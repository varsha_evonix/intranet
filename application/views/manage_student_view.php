<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("alllinks.php"); ?>	
</head>

<?php 

//echo $this->session->userdata("selectedGroup"); ?>

<body onload="viewStudents()">
<div id="wrapper">
                    <div class='feedback'>
                    <?php if($this->session->flashdata('sucess'))
                     echo "
                        <script>
                        swal('Ok', '".$this->session->flashdata('sucess')."', 'success'); 
                        </script>";


                        if($this->session->flashdata('error')) {
                     echo "
                        <script>
                        swal('Opps..!', '".$this->session->flashdata('error')."', 'error');
                        </script>";
                    }
                     ?>
                </div>    

        <?php include("header.php"); ?>
        <link href="<?php echo base_url();?>assets/css/plugins/datatables/datatables.css" rel="stylesheet">        

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Student
                                <small>manage students</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> <a href="#">Dashboard</a></li>
                                <li class="active">student</li>
                            </ol>
                        </div>
                    </div><!-- /.col-lg-12 -->


                        <div class="col-lg-12">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Student List</h4>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="validate" role="form" novalidate="novalidate">
                                    	<h5 style="color:#B81212;margin-left:166px;">* Select batch to show student list</h5>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Select batch</label>
                                            <div class="col-sm-10">
                                            <select name="select" class="form-control" required="" id="batch_id" onchange="viewStudents()" name="bname1">
                                                    <option value="Select" >Select One:</option>
                                                      <?php     
                                                          foreach($batchInfo as $b)
                                                          {?>
                                                            <option <?php if($b->batch_course_id == $batchid) echo 'selected'; ?> value=<?php echo $b->batch_course_id; ?>
                                        
                                                                <?php //if($this->session->userdata("selectedGroup") == $b->batch_course_id) {echo "selected";} ?>

                                                            >
                                                            <?php
                                                             echo  $b->batch_name."--".$b->course_name;
                                                             
                                                             echo"</option>";
                                                          }
                                                        ?>

                                                </select>
                                                    <span id="span_batch" style="color:rgb(184, 18, 18);"></span>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>

                        </div>
                            <div id="editspllist">
                                            
                            </div>                      
                    </div>
                </div>
            </div>
        </div>
</div>

<?php include("alljs.php"); ?>

    <script src="<?php echo base_url();?>assets/js/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/datatables/datatables-bs3.js"></script>

    <script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
    <script type="text/javascript">
    function viewStudents()
        {
            var bid = $("#batch_id").val();
            if(bid=="Select")
            {
                $('#editspllist').html('');
                $('#span_batch').html('Please select batch');
            }
            else
            {
       $('#span_batch').html('');
           $.ajax({
            url: "<?php echo base_url();?>admin/manage_student",
            type: "POST",
            async: true, 
            dataType:'json',
            data: {bid:bid}, //your form data to post goes here
             success: function(response){
             	//alert(response);
                    var json = response;
                    //alert(json);
                    var dataSet = [];
                    $('#editspllist').html('');
                    $('#editspllist').append('<div class="row"><div class="col-lg-12"><div class="portlet portlet-default"><div class="portlet-heading"><div class="portlet-title"><h4>Student Details</h4></div><div class="clearfix"></div></div><div class="portlet-body"><div class="table-responsive"><table id="example-table" class="table table-striped table-bordered table-hover table-green"><thead><tr><th width="10">#</th><th width="20">PRN </th><th width="50">Name</th><th width="180">Placement Status</th><th width="20">Action</th> </tr></thead><tbody id="tbody">');
                    var j = 1;
                    for(var i = 0; i < json[0].length; i++) {

                        var obj = json[0][i];

                        if(obj.status == 1)
                        {
                            data = "<span class='badge green'>Active</span>";
                        }else {
                            data = "<span class='badge red'>Block</span>";
                        }

                        $('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td>'+obj.PRN+'</td><td>'+obj.first_name+' '+obj.middle_name+' '+obj.last_name+'</td><td><select class="form-control" id="optId_'+obj.student_id+'" onchange="showtxtbox('+obj.student_id+')">'+getoptions(json[1],obj.pl_status)+'</select><div id="txtbox_'+obj.student_id+'" style="float:right;">'+getreason(obj.reason,obj.pl_status,obj.student_id)+'</div></td><td><button type="submit" class="btn btn-green" onclick="getstatus('+obj.student_id+')">Save</button></td></tr>');
                        j++;
                    }
                    $('#editspllist').append('</tbody></table></div></div></div></div>');
					$('#example-table').dataTable({
					"data": dataSet,
					"coumns":[]
					});
					
        }
        });
    }//else
    
}

function getoptions(optData,selId)
{
	var returnData = "";
	for(var i = 0; i < optData.length; i++) {
		var obj = optData[i];
		returnData += "<option value="+obj.id+" "+(obj.id == selId ? 'selected' : '')+">"+obj.pl_status+"</option>";
	
	}
	return returnData;
}

function getreason(optReason,pl_status,tId)
{
	var returnData = "";
	if(optReason == null)
	{
		returnData = '';
	}
	else {
		returnData = optReason;
	} 
	if (pl_status==2 || pl_status == 3) {
		returnData = "<a class='fancybox fancybox.ajax' href='<?php echo base_url();?>admin/formdetails/"+tId+"/"+$('#batch_id').val()+"' style='cursor:pointer'>Click here to details</a>";
	}
	return returnData;
}

function showtxtbox(tId)
{
	var optId = $("#optId_"+tId).val();
	if(optId == 4 || optId == 5 || optId == 6)
	{
		$("#txtbox_"+tId).html('');
		/*$("#txtbox_"+tId).html("<input type='text' class='form-control' id='reason_"+tId+"' name='reason' placeholder='Enter Your Reason'>");*/
		$("#txtbox_"+tId).html("<textarea class='form-control' name='reason' id='reason_"+tId+"' placeholder='Enter Your Reason' style='resize:vertical;' maxlength='100'></textarea>");
		
	}
	else if(optId == 2 || optId == 3)
	{
		$("#txtbox_"+tId).html('');
		$("#txtbox_"+tId).html("<a class='fancybox fancybox.ajax' href='<?php echo base_url();?>admin/formdetails/"+tId+"/"+$('#batch_id').val()+"' style='cursor:pointer'>Click here to details</a>");
	}
	else
	{
		$("#txtbox_"+tId).html('');
	}
}
</script>

<script type="text/javascript">
		function getstatus(studid)
		{
			var optId = $("#optId_"+studid).val();
			var studid = studid;
			var reason = $("#reason_"+studid).val();
			//alert(reason);
			if(reason !='')
			{
			$.ajax({
			url: "<?php echo base_url();?>admin/savestatus",
			type: "POST",
			data: { optId:optId,studid:studid, reason:reason }, 
			async: true,
			 success: function(response){
						if(response == "true")
					 {
						swal("Updated!", "Placement status has been updated.", "success");
					 }
					 else
					 {
						swal("Oops..!", "Something going wrong. Can't update placement status !!", "error");
					 }	
						
				}
			});
			}
			else
			{
				swal("Oops..!", "Please enter proper reason", "error");
			}
			
			
		}
		
		/*function submitdetails(sid)
		{
			var sid=sid;
			$("#submitdetails_model").modal('show');
		}*/
</script>


    <!-- Add fancyBox main JS and CSS files -->
	 
	 <script type="text/javascript" src="<?php echo base_url();?>assets/popup/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/popup/jquery.fancybox.css?v=2.1.5" media="screen" />

	<script type="text/javascript">
			$('.fancybox').fancybox();
	</script>
	
<script type="text/javascript">
    $("#managestudents").addClass("active"); // main container
</script> 
</body>
</html>