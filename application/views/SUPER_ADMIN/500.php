<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("alllinks.php"); ?>
  
</head>

<body>
<div id="wrapper">

        <?php include("header.php"); ?>

          <div id="page-wrapper">

            <div class="page-content">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>
                                Blank Page
                                <small>For Customization</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="index.html">Dashboard</a>
                                </li>
                                <li class="active">Blank Page</li>
                            </ol>
                        </div>
                    </div>

  <div class="col-lg-6 col-lg-offset-3">
                        <h1 class="error-title">500</h1>
                        <h4 class="error-msg"><i class="fa fa-warning text-red"></i> No authority</h4>
                        <p class="lead">The web server encountered an unexpected condition that prevented it from fulfilling your request. Please try again or contact the Flex Admin support staff to resolve the issue.</p>
                        <ul class="list-unstyled">
                            <li>
                                <a class="btn btn-default" href="#">Open Support Ticket</a>
                            </li>
                            <li>
                                <br>
                            </li>
                            <li>
                                <a class="btn btn-default" href="#">Back to Dashboard</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.col-lg-6 -->
                </div>
                <!-- /.row -->

            </div>
              <?php include("alljs.php"); ?>   

    


</body>

</html>
