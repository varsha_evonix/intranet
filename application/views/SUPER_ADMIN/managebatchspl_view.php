<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
		
	<style>
	.toggledDropDown
	{
		display: none; /* Hiding the optional drop down lists */
	}
	</style>
	
	
</head>

<body>
<div id="wrapper">

		<?php include("header.php"); ?>

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Batches
                                <small>View Batch Specializations</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a>
                                </li>
                                <li class="active">View Batch Specializations</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
             
                <!-- /.row -->
                <!-- end PAGE TITLE AREA -->

				
				
                <!-- begin DASHBOARD CIRCLE TILES -->
				 
				
				  <div class="col-lg-12">
			
                        <div class="row">

						

                            <!-- Basic Form Example -->
                            <div class="col-lg-6">
							   <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
							       <h4>View Batch Specializations</h4>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                      <div id="validationExamples" class="panel-collapse collapse in">
                                        <div class="portlet-body">
							<form class="form-horizontal" id="sky-form" role="form" action="<?php echo base_url();?>superadmin/managebatches" method="post">
							
										
                         					  <div class="form-group">
                                                  <label for="bname" class="col-sm-4 control-label">Select Batch <span style="color:#b81212">*</span></label>
   
	                                            <div class="col-sm-4">

														<select id="1" class="form-control" onchange="viewspl()" name="bname1">
                                                      <option selected="selected">
													 Select Batch
													 </option>
                                                      <?php		
                                                          foreach($bname as $b)
			                                              {
														    echo "<option  value=".$b->batch_course_id.">";
														     echo  $b->batch_name."--".$b->course_name;
															 
															 echo"</option>";
			                                              }
                                                       ?>
                                                      </select>
													  </div>
													  </div>
													  
										    <div class="form-group">
											
											<div id="editspllist">
											
											</div>
											</div>

                                                              
											 
											 
                                                                                                                     <div class="form-group">
                                                  <label class="col-sm-4 control-label"></label>
												  <div class="col-sm-4">
                                                <button  type="submit" class="btn btn-default">Edit</button>
												</div>
												</div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

				
                <!-- end DASHBOARD CIRCLE TILES -->
				
                
                <!-- /.row -->
             <!-- /.col-lg-12 -->

                        </div>
                    </div>
                </div>

            </div>
            <!-- /.page-content -->

        </div>

              
                   	              
                    <!-- /.col-lg-12 -->
               
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                  
                        <!-- /.portlet -->
                 
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
<!-- /#wrapper -->

    <?php include("alljs.php"); ?>
	
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
		     
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					
					bname1:
					{
					   required:true,
					   min:1
					}
					
					
					
				},
									
				// Messages for form validation
				messages:
				{
					bname1:
					{
					   required: '<span style="color:rgb(184, 18, 18); margin-left:20px;">Please Select course</span>',
					   min:'<span style="color:rgb(184, 18, 18); margin-left:182px;">Please select Batch</span>'
						
					}
			
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
				
				
			});
		});
	</script>

	
	
	
	
	<script>
		function viewspl()
		{
			//alert($("#1").val());
			var bname = $("#1").val();
			
			$.ajax({
        url: "<?php echo base_url();?>superadmin/viewbatchspl",
        type: "POST",
        async: true, 
		dataType:'json',
        data: {bname:bname}, //your form data to post goes here
         success: function(response){ 
		       // alert(response);
					var json = response;
					$('#editspllist').html('');
					$('#editspllist').append('<br><br><br><div id="4" class="col-sm-12 table-responsive toggledDropDown" style="display: block;margin-left:13%;"><div class="col-lg-8"><div class="portlet portlet-default"><table id="example-table7" class="table table-striped table-bordered table-hover table-green" width="20%"><thead><tr><th width="190px;">Sr. No.</th><th width="290px">Specialization Name</th></tr></thead><tbody id="tbody">');
					
					var j = 1;
					for(var i = 0; i < json.length; i++) {
						var obj = json[i];
						
					//alert(obj.specialization_name);
						
						$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td>'+obj.splecialization_name+'</td></tr>');
						j++;
						//$('#studlist').append('<div>'+obj.first_name+'</div>');
					}
					
					$('#editspllist').append('</tbody></table></div></div></div>');
					
			}
    });
	
}


		$('#1').change(function() {
		//alert();
		// Hide all drop downs sharing the CSS class "toggledDropDown".
		$('.toggledDropDown').hide();
		
		// Build a selector for the selected drop down
		var selector = ('#' + $(this).val());
		//alert(selector);
		// Show the selected drop down
		$(selector).show();

	});



		
	</script>
	<script>
	$("#pages2").addClass("in");
	$("#managebatches").addClass("active");
	$("#viewbatchspecialization").addClass("active");
	</script>
	
	</body>

</html>
