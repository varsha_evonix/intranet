<html>
    <head>
	<?php include('alllinks.php');?>
        <meta charset="utf-8">
       <style>
td{
padding:6px !important;
font-size:14px !important;
}
<SCRIPT LANGUAGE="JavaScript">
<!-- 

<!-- Begin
function CheckAll(chk)
{
for (i = 0; i < chk.length; i++)
chk[i].checked = true ;
}

function UnCheckAll(chk)
{
for (i = 0; i < chk.length; i++)
chk[i].checked = false ;
}
// End -->
</script>
</style>
    </head>
    <body>
 <div id="wrapper">
		<?php include("header.php"); ?>
             <div id="page-wrapper">
            <div class="page-content">
                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Students
                                <small>Add Bulk Students</small>
                            </h1>
                            <ol class="breadcrumb">
                               <li><i class="fa fa-dashboard"></i>
							   <a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a></li>
                                <li class="active">Add Bulk Students</li>

                            </ol>
                        </div>		


                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->
				 <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
			 <!-- begin MAIN PAGE ROW -->
                <div class="row">
                    <div class="col-lg-12">
  <div class="portlet portlet-default">
  <div class="clearfix"></div> 
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Student Details</h4>
                                </div>
                               
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
								
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
										
                                            <tr>
											   <th><input id="selecctall" type="checkbox">&nbsp;Check All</th>
                                               <th>PRN</th>
                                               <th>Name</th>
                                              <th>Date of Birth </th>
                                              <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
							
                                           
                        <?php foreach ($info as $row): ?>
					        <?php $bcid=$row->batch_course_id;?>
                            <tr>
							 <td><input name="checkbox[]" class="checkbox1" type="checkbox" id="checkbox[]" value="<?php echo $row->student_id ?>"><?php echo $row->student_id ?></td>
                                <td><?php echo $row->PRN; ?></td>
                                <td><?php echo $row->first_name; ?>  <?php echo $row->middle_name; ?>  <?php echo $row->last_name;?></td>
                                <td><?php echo $row->DOB;?> </td>
                                <td><?php echo $row->status;?> </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                                    </table>
                                                                        <div class="row">
									 <div class="col-sm-2">
									 <button class="btn btn-default" id="del_all">Delete Selected</button>
									 </div>
									 <div class="col-sm-10">
									<button class="btn btn-default" id="all" onclick='viewBatch()'>Assign another Batch</button>
									  </div>
									  <input type="hidden" value="<?php echo $bcid;?>" id="1">
                                                                           <div>
                                                                            </br>
                                                                            </div>
									  <div id="batchlist" class="col-sm-12 table-responsive">
									  
									  </div>
									  <div id="button">
									  
									  </div>
                                </div>
						    </div>
                        </div>
						
						 </div>
						  
                    <!-- /.col-lg-12 -->

			<div id="editbatch">
			
			</div>
                </div>
                </div>
              </div>
			  <!-- /.row --> 
			 			

 <?php include('alljs.php');?>
 
    <script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	 <script>
            $(document).ready(function() {
                resetcheckbox();
                $('#selecctall').click(function(event) {  //on click
                    if (this.checked) { // check select status
                        $('.checkbox1').each(function() { //loop through each checkbox
                            this.checked = true;  //select all checkboxes with class "checkbox1"              
                        });
                    } else {
                        $('.checkbox1').each(function() { //loop through each checkbox
                            this.checked = false; //deselect all checkboxes with class "checkbox1"                      
                        });
                    }
                });


                $("#del_all").on('click', function(e) {
                    e.preventDefault();
                    var checkValues = $('.checkbox1:checked').map(function()
                    {
                        return $(this).val();
                    }).get();
                    console.log(checkValues);
                    
                    $.each( checkValues, function( i, val ) {
                        $("#"+val).remove();
                        });
//                    return  false;
                    $.ajax({
                        url: '<?php echo base_url(); ?>superadmin/deleteallstudents',
                        type: 'post',
                        data: 'ids=' + checkValues,
						    success: function(data){
                          $("#respose").html(data);
                       $('#selecctall').attr('checked', false);
					   swal({  title: "Students have been deleted successfully !! Do you want to delete Batch?",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes",   
          closeOnConfirm: false
     },  function(){ 
					 var bcid2=$("#1").val();
					 //alert(bcid2);
					   $.ajax({
                        url: '<?php echo base_url(); ?>superadmin/checkbatch/'+bcid2,
                        type: 'post',
						 async: true,					 
						    success: function(response){
								  if(response.indexOf("true")> -1)
                 {                  
					
					swal("Notice","This Batch contains students.Delete students first!","info");
					location.reload();
				 }
				 else
				 {
					
					  $.ajax({
                        url: '<?php echo base_url(); ?>superadmin/deletebatch',
                        type: 'POST',
                       data: {bcid2:bcid2},
					   	
					   async: true,
					     success: function(response){
							
							swal("Deleted","Batch has been deleted successfully","success");
					    setTimeout(function(){window.location.replace("<?php echo base_url();?>superadmin/managebatches");},2000);  
							
						}	 
				 });
					  
				 }
                
							}
							});
			 });	
							}
                   
                });
				});
                    
                
				

                function  resetcheckbox(){
                $('input:checkbox').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
                   });
                }
            });
        </script>
		
		<script type="text/javascript">
		function viewBatch()
		{
			
			var bcid=$("#1").val();
			$.ajax({
			url: "<?php echo base_url();?>superadmin/fetchbatch",
			type: "POST",
			async: true,
			dataType:'json',
		    data: { bcid:bcid},
         			
			//your form data to post goes here
			 success: function(response){
	
						var json = response;
						$('#batchlist').html('');
                                        
						$('#batchlist').append('<br/><label for="Student" class="col-sm-3 control-label">Change Batch</label><div class="col-sm-9"><select id="selectbatchid" name="bname1" onchange="deleteOrganization()" class="form-control"><option value="">Select</option>');
						
						var j = 1;
						for(var i = 0; i < json.length; i++) {
							var obj = json[i];
							
							$('#selectbatchid').append('<option value="'+obj.batch_course_id+'">'+obj.batch_name+'--'+obj.course_name+'</option>');
							j++;
						}
						
						$('#batchlist').append('</select>');
					
				}
			});
			
			
		}
	</script>

	<script type="text/javascript">
	function deleteOrganization()
{
 swal({  title: "Are you sure?",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes, Update it!",   
          closeOnConfirm: false 
     },

		function batchlist()
		{
			
			
			var bcid1=$("#1").val();
			//alert($("#selectbatchid").val());
		    //alert(bcid1);
			var studvalue = $("#selectbatchid").val();
			
			$.ajax({
        url: "<?php echo base_url();?>superadmin/changebatch",
        type: "POST",
        async: true, 
        data: { studvalue:studvalue,bcid1:bcid1}, //your form data to post goes here
         success: function(response){
			
					if(response.indexOf("true")> -1)
					{
					 swal("Updated", "Batch has been updated successfully!", "warning");				 
                 setTimeout(function(){window.location.replace("<?php echo base_url();?>superadmin/managebatches");},2000);  
				}
				else
				{
					 swal("Error", "Error in assigning batch!", "error"); 
					 setTimeout(function(){location.reload();},2000);
				}
					}
					
					
					
					
			
		});
		});
		 }
	</script>

<script>
	$("#pages").addClass("in");
	$("#managestudents").addClass("active");
	$("#addstudent").addClass("active");
	</script>
</body>
<!--

</html>
