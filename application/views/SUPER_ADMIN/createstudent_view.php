<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>



<body>
<style>
    .form-group .required .control-label:after {
  content:"*";color:red;
}
</style>
<div id="wrapper">

		<?php include("header.php"); ?>


        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Students
                                <small>Create Student</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> 
								<a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a></li>
                                <li class="active">Create Student</li>
                                    
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE AREA -->
			 <div class="row">	
<div class="col-lg-12" >
 <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Create Student</h4>
                                </div>
                              
                                <div class="clearfix"></div>
                            </div>
							
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="sky-form" role="form" method="post" action="<?php echo base_url();?>superadmin/insertstudent" novalidate="novalidate">
                                       
                                        
											<div class="form-group">
                                                    <label for="prn" class="col-sm-3 control-label">PRN Number <span style="color:#b81212">*</span></label>
                                                    <div class="col-sm-7">
                                                        <input type="text" class="form-control" id="prn" placeholder="Enter PRN No" name="prn" maxlength=20 >
														
                                                </div>
											</div>
											
<div class="form-group">
       <label for="rollno" class="col-sm-3 control-label"></label>
                    <div class="col-sm-7" id="rollnoexists" style="margin-left: -10px;
    width: 263px;color: #b81212; font-style:italic;"></div>
</div>
                                                <div class="form-group">
                                                    <label for="fname" class="col-sm-3 control-label">First Name <span style="color:#b81212">*</span></label>
													 <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="fname" placeholder="Enter First Name" name="fname">
                                                </div>
												</div>
												<div class="form-group">
                                                    <label for="mname" class="col-sm-3 control-label">Middle Name</label>
                                                     <div class="col-sm-7">
													<input type="text" class="form-control" id="mname" placeholder="Enter Middle Name" name="mname">
                                                </div>
												</div>
        										<div class="form-group">
												
                                                    <label for="lname" class="col-sm-3 control-label">Last Name <span style="color:#b81212">*</span></label>
													<div class="col-sm-7">
                                                    <input type="text" class="form-control" id="lname" placeholder="Enter Last Name" name="lname">
                                                </div>
												</div>
												
												<div class="form-group">	
		                                <div id="sandbox-container">
		                                  <label for="dob" class="col-sm-3 control-label">Date of Birth <span style="color:#b81212">*</span></label>
											  <div class="col-sm-7">
											 <input class="form-control dateWork" type="text"  id="dob" name="dob" placeholder="DD-MM-YYYY" readonly="readonly">					  
		                                      </div> 
												  </div>
												</div>
        
												<!--<div class="form-group">
												
                                                    <label for="contact" class="col-sm-3 control-label">Mobile Number</label>
													<div class="col-sm-7">
                                                    <input type="text" class="form-control" id="contact" placeholder="Enter Mobile Number" name="contact" maxlength=10>
                                                </div>
												</div>-->
												
												
												
												
				                                <div class="form-group">
												
                                                    <label for="gender" class="col-sm-3 control-label">Gender</label>
													
                                                    <div class="col-sm-6">
                                                       
														<label class="radio-inline">
                                                           <input type="radio" name="gender" value="M" checked> Male
                                                        </label>
														
                                                        
														<label class="radio-inline">
                                                           <input type="radio" name="gender" value="F"> Female 
                                                        </label>
                                                
												</div>
										</div>
										
                                               <div class="form-group">
                                                  <label for="bname" class="col-sm-3 control-label">Select Batch <span style="color:#b81212">*</span></label>
                                                      <div class="col-sm-6">
	                                           

														<select class="form-control" name="bname1">
                                                     <option selected="selected">
													 Select Batch
													 </option>
                                                      <?php		
                                                          foreach($bname as $b)
			                                              {
														    echo "<option  value=".$b->batch_course_id.">";
														     echo  $b->batch_name."--".$b->course_name;
															 
															 echo"</option>";
			                                              }
                                                       ?>
                                                      </select>
													  </div>
                                                    </div>
                                                                               
											
																
											
                                                <div class="form-group">
                                        <label for="email" class="col-sm-3 control-label" >Allocated Email ID <span style="color:#b81212">*</span></label>
										<div class="col-sm-7">
										 <input type="email" id="email" name="email" placeholder="Enter your Allocated Email Address" style="width:100%">				  
                                              </div> 
									</div>
                                    
										            <!--<div class="form-group">
                                        <label for="email1" class="col-sm-3 control-label" >Personal Email ID</label>
										<div class="col-sm-7">
										 <input type="text" id="email1" name="email1" placeholder="Enter your Personal Email Address" style="width:100%">				  
                                              </div> 
									</div>-->
                                    
								
										
										<div class="form-group">
												<label for="status" class="col-sm-3 control-label">Enabled</label>
												<div class="col-sm-7">
													<input type="checkbox" id="status" name="status" checked="checked">
												</div>
										</div>
                                    
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-2">
                                                <button type="submit" class="btn btn-default">Save</button>
												
                                            </div>
                                        </div>
										
                                    </form>
                             </div> </div> </div
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
</div>
</div>
    </div>
    <!-- /#wrapper -->
    </div>
<?php include("alljs.php"); ?>
	 <script src=" <?php echo base_url(); ?>assets/js/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo/advanced-form-demo.js"></script>
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
			$(function()
		{
		
		     $.validator.addMethod("email", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			
		      $.validator.addMethod("exp_no", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
		     $.validator.addMethod("regex", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			 $.validator.addMethod("regeNUM", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			  $.validator.addMethod("regnumeric", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });

				var ALPHA_REGEX = "[a-zA-Z ]*$";
				var alphaNumeric = "^[(a-z)(A-Z)(0-9)]+$";	
				var numeric = "^[(0-9)]{10}$";
				var email="^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$";
			
			$("#sky-form").validate(
			{	
                errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				},
				
               
				// Rules for form validation
				rules:
				{
					contact:
					{
				         required:false,
                                         minlength:10, 
                                        maxlength:10,
                                        digits:true
					},
					prn:
					{
					  regeNUM: alphaNumeric,
						required: true
					
					},
					fname:
					{
					   regex: ALPHA_REGEX,
					   required:true
					   
					},
					mname:
				    {
					regex: ALPHA_REGEX
					
				    },
					lname:
				    {
					   regex: ALPHA_REGEX,
						required:true
					}, 
					dob:
					{
					   required:true
					},
					
					
					bname1:
					{
					  required:true,
					  min:1
					},
					
					email:
					{
					    email:email, 
					  required: true
					},
					email1:
					{
                                    
					  email:email
					}
			
						
					
					
				},
									
				// Messages for form validation
				messages:
				{
					contact:
					{
                                           minlength: '<span style="color:#b81212; margin-left:265px;">Please Enter valid number</span>',
                                   maxlength: '<span style="color:#b81212; margin-left:265px;">Please Enter valid number</span>',
            digits: '<span style="color:#b81212; margin-left:265px;">Please Enter valid number</span>'
					
                      						
					},	
				    prn:
					{
					   regeNUM: '<span style="color:#b81212; margin-left:265px;">Please enter alphanumeric characters only</span>',
					   required: '<span style="color:#b81212; margin-left:265px;">Please enter prn number</span>'
						
					},
					fname:
					{
					    regex: '<span style="color:#b81212; margin-left:265px;">Please enter alphabets only</span>',
						required: '<span style="color:#b81212; margin-left:265px;">Please enter first name</span>'
						
					},
					mname:
					{
					   regex: '<span style="color:#b81212; margin-left:265px;">Please enter alphabets only</span>',
						required: '<span style="color:#b81212; margin-left:265px">Please enter middle name</span>'
						
			
					},
					
					lname:
					{
					   regex: '<span style="color:#b81212; margin-left:265px;">Please enter alphabets only</span>',
						required: '<span style="color:#b81212; margin-left:265px;">Please enter last name</span>'
						
					},
					
					dob:
                    {
					  required: '<span style="color:#b81212; margin-left:265px;" id="db">Please enter Date of birth</span>'
					},
				
					bname1:
					{
					    required:'<span style="color:#b81212; margin-left:265px;">Please select Batch</span>',
						min:'<span style="color:#b81212; margin-left:265px;">Please Select Batch</span>'
					},
					
					email:
					{
						required: '<span style="color:#b81212; margin-left:265px;">Please enter allocated email address</span>',
						email:'<span style="color:#b81212; margin-left:265px;">Please Enter a valid Email-id</span>'
						
						
					},
					email1:
					{
			
						email:'<span style="color:#b81212; margin-left:265px;">Please Enter a valid Email-id</span>'	
					}
					
				}				
				
				// Do not change code below
				
				
			});
			
			
				$.validator.addMethod('contact',function(value, element)
				{
				return $('#contact').val()<=10
				}, "contact less than 10");
			
		});

		$("#dob").change(function()
				{
					var dob =$(this).val();
					
					//alert(dob);	
					
					if(dob!='')
					{
						$('#db').html('');
					}
									
					var date = new Date();
					
var currentMonth = date.getMonth()+1;
var currentDate = date.getDate();
var currentYear = date.getFullYear();

var today = currentDate+'-'+currentMonth+'-'+currentYear;

					//alert(today);
					
 					if(dob==today)
					{
						alert("Today's date should not be allowed");
						$("#dob").val('');
					}
					
				});
	</script>
	
	<script>
	$("#pages").addClass("in");
	$("#managestudents").addClass("active");
	$("#createstudent").addClass("active");
	</script>
</body>
</html>

<script type="text/javascript">
	
	jQuery("#prn").change(function() {
		
    var rollno = $("#prn").val();
 	 //alert(rollno);
 	jQuery.ajax({
       url: "<?php echo base_url(); ?>superadmin/createprn",
       type: "POST",
       async: true,
       data: {rollno:rollno}, //your form data to post goes here
       success: function(response){    
 		 //alert(response);
	
		if(response == 1){
			
		//jQuery('#prn').val(response);
		
		jQuery('#rollnoexists').html("This is not a valid roll no.");
		
		//jQuery("#rollno").val("");
		//jQuery("#rollno").focus();
		
		jQuery("#prn").val("");
		
		jQuery("#prn").focus();
		
				
		jQuery('#rollnoexists').addClass("error");
		
		jQuery('#rollnoexists').removeClass("success_inline");
		
				
		} else {
		
		jQuery('#rollnoexists').html("<span style='color:green'>This roll no. is available.</span>");
		
		jQuery('#rollnoexists').removeClass("error");
		
		jQuery('#rollnoexists').addClass("success_inline");

			}
	
	}  
   });

});

</script>	
 




