<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>



<body>
<div id="wrapper">

		<?php include("header.php"); ?>

  
        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                    <div class="col-lg-12">
					
					
                        <div class="page-title">
						

                            <h1>Manage Courses
                                <small>Create Course</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a></li>
								 <li class="active">Create Course</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE AREA -->

				
				
                <!-- begin DASHBOARD CIRCLE TILES -->
				 
				
				 
                        <div class="row">

						

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">
							
                    <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
				
								
							   <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
							       <h4>Create Course</h4>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                     <div id="validationExamples" class="panel-collapse collapse in">
                                        <div class="portlet-body">
									
				
				
							
										
                           <form id="sky-form" class="form-horizontal" method="post" role="form" action="<?php echo base_url();?>superadmin/insertcourse">
                                                <div class="form-group">
                                                    <label for="createcourse" class="col-sm-4 control-label">Course Name<span style="color:#b81212">*</span></label>
                                                   <div class="col-sm-7">
												   <input type="text" class="form-control" id="cname" placeholder="Enter Course Name" name="cname" style="width:90%">
                                                </div>
												</div>
												<div class="form-group">
												<label for="enabled" class="col-sm-4 control-label">Enabled</label>
												<div class="col-sm-7">
													<input type="checkbox" value="1" id="status" name="status" checked="checked"/>
												</div>
										</div>
                                    </br>
                                               <div class="form-group">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-4">
												
                                                <button  type="submit" class="btn btn-default">Submit</button>
                                            </div>
											</div>
											</form>
                                        </div>
                                    </div>
                                </div>

				
                <!-- end DASHBOARD CIRCLE TILES -->
				
				
				
		
                
                <!-- /.row -->
             <!-- /.col-lg-12 -->

                        </div>
                    </div>
                </div>

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
		


    </div>
<?php include("alljs.php"); ?>
	
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
	$(function()
		{
		     $.validator.addMethod("regex", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			 	var ALPHA_REGEX = "^[a-zA-Z\ ^ ${ } [ ] ( ) . + ? | - &]$";

			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					cname:
					{
					    //regex: ALPHA_REGEX,
						required: true
					}
					
			
						
					
					
				},
									
				// Messages for form validation
				messages:
				{
					cname:
					{
					    //regex:'<span style="color:rgb(184, 18, 18); margin-left:350px;">Please Enter alphanumeric characheters only</span>',
						required: '<span style="color:rgb(184, 18, 18); margin-left:350px;">Please enter course name</span>'
					}
					
			
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
				
				
			});
		});
	
	</script>

<!--<script>
	function createBatch()
	{
	 
	  var bname = $("#bname").val();
	   var inyear = $("#inyear").val();
	    var outyear = $("#outyear").val();
		 var cname = $("#cname").val();
		  var terms = $("#terms").val();
		  
		  
	  $.ajax({
	  url:'<?php //echo base_url() ?>index.php/admin/batch_insert',
	  type:"POST",
	   data:{bname:bname,inyear:inyear,outyear:outyear,cname:cname,terms:terms},
	   success:function(data){
	  //alert(data);
	   
	   if(data==1)
	   {
	   $("#respon").html("Batch Successfully Create"); 
	   
	    $("#bname").val(' ');
	   $("#inyear").val(' ');
	  
	   }
	   
	  }
	    
	  
	  })
	
	}
	</script>-->
<script>
	$("#ui-elements").addClass("in");
	$("#managecourses").addClass("active");
	$("#createcourse").addClass("active");
	</script>
</body>

</html>
