<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("alllinks.php"); ?>


</head>
 
<body <?php if($this->session->userdata('studentid')){ echo "onload='show2($studid)'";}  //$this->session->unset_userdata('studentid'); ?> >
<div id="wrapper">

        <?php include("header.php"); ?>
        <link href="<?php echo base_url();?>assets/css/plugins/datatables/datatables.css" rel="stylesheet">        

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Students
                                <small>Manage students</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a>
                                </li>
                             <li class="active">Manage Students</li>
                            </ol>
                        </div>
                    </div><!-- /.col-lg-12 -->

								
                        <div class="col-lg-12">
                             <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                    {
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";

                    }
                        else if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Student List</h4>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="validate" role="form" novalidate="novalidate">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Select batch</label>
											<?php  $studid=$this->session->userdata('studentid'); ?>
											 <div class="col-sm-10">
											
                                            <select name="select" class="form-control" required="" id="batch_id" onchange="viewStudents()" name="bname1">
                                                    <option value="">Select Batch:</option>
                                                      <?php     
                                                          foreach($bname as $b)
                                                          {
														  
														  if(isset($studid))
                                                            { 
															   echo $studid;
                                                              if( $studid == $b->batch_course_id)
                                                              {
															     echo $studid;
                                                              echo "<option  value=".$b->batch_course_id." selected>";
                                                             echo  $b->batch_name."--".$b->course_name;
                                                             echo"</option>";
                                                              }
                                                            
															else
															{

                                                            echo "<option  value=".$b->batch_course_id.">";
                                                             echo  $b->batch_name."--".$b->course_name;
                                                             
                                                             echo"</option>";
															 }
															 }
                                                          }
                                                        ?>
													<?php echo	$this->session->unset_userdata('studentid');?>
                                          
                                                      </select>                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                            <div id="editspllist">
                                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<?php include("alljs.php"); ?>

    <script src="<?php echo base_url();?>assets/js/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/datatables/datatables-bs3.js"></script>

    <script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
    <script type="text/javascript">
    function viewStudents()
        {
            var bid = $("#batch_id").val();
		    
                       $.ajax({
            url: "<?php echo base_url();?>superadmin/view_students",
            type: "POST",
            async: true, 
            dataType:'json',
            data: {bid:bid}, //your form data to post goes here
             success: function(response){ 
			
                    var json = response;
                    var dataSet = [];
                    $('#editspllist').html('');
                    $('#editspllist').append('<div class="row"><div class="col-lg-12"><div class="portlet-body" style=><div class="table-responsive"><table id="example-table" class="table table-striped table-bordered table-hover table-green"><thead><tr><th>#</th><th>PRN </th><th>Name</th><th>Status</th> <th>Edit</th><th>Delete</th></tr></thead><tbody id="tbody">');
                    var j = 1;
                    for(var i = 0; i < json.length; i++) {
                        var obj = json[i];
                       if(obj.status==1)
                      {
                          data="<span class='badge green'>Enabled</span>"; 
                      }
                      else
                      {
                          data="<span class='badge red'>Block</span>"; 
                      }
                        
					
						
                        $('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td>'+obj.PRN+'</td><td>'+obj.first_name+' '+obj.middle_name+' '+obj.last_name+'</td><td>'+data+'</td><td> <a href="<?php echo base_url();?>superadmin/editstudent/'+obj.credentials_id+'" > <i class="fa fa-pencil"></i>&nbsp;EDIT </a></td><td> <a onclick="deleteOrganization('+obj.credentials_id+');" style="cursor: pointer;"> <div class=""><i class="fa fa-times"></i> DELETE </a></td></tr>');
                        j++;
                    }
                    $('#editspllist').append('</tbody></table></div></div></div></div>');
					$('#example-table').dataTable({
					"data": dataSet,
					"coumns":[]
					});
					
        }
    });
    
}


 function show2(id)
        {
            var bid =id;
		    //alert("onload");
            $.ajax({
            url: "<?php echo base_url();?>superadmin/view_students",
            type: "POST",
            async: true, 
            dataType:'json',
            data: {bid:bid}, //your form data to post goes here
             success: function(response){ 
			
                    var json = response;
                    var dataSet = [];
                    $('#editspllist').html('');
                    $('#editspllist').append('<div class="row"><div class="col-lg-12"><div class="portlet-body" style=><div class="table-responsive"><table id="example-table" class="table table-striped table-bordered table-hover table-green"><thead><tr><th>#</th><th>PRN </th><th>Name</th><th>Status</th> <th>Edit</th><th>Delete</th></tr></thead><tbody id="tbody">');
                    var j = 1;
                    for(var i = 0; i < json.length; i++) {
                        var obj = json[i];
                       if(obj.status==1)
                      {
                          data="Enabled"; 
                      }
                      else
                      {
                          data="Disabled"; 
                      }
                        
					
						
                        $('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td>'+obj.PRN+'</td><td>'+obj.first_name+' '+obj.middle_name+' '+obj.last_name+'</td><td>'+data+'</td><td> <a href="<?php echo base_url();?>superadmin/editstudent/'+obj.credentials_id+'" > <i class="fa fa-pencil"></i>&nbsp;EDIT </a></td><td> <a onclick="deleteOrganization('+obj.credentials_id+');" style="cursor: pointer;"> <div class=""><i class="fa fa-times"></i> DELETE </a></td></tr>');
                        j++;
                    }
                    $('#editspllist').append('</tbody></table></div></div></div></div>');
					$('#example-table').dataTable({
					"data": dataSet,
					"coumns":[]
					});
					
        }
    });
    
}


function deleteOrganization(id)
{

   var  batch_id=$("#batch_id").val();

 swal({  title: "Are you sure?",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes, delete it!",   
          closeOnConfirm: false 
     },


      function(){   
            $.ajax({
            url: "<?php echo base_url();?>superadmin/deletestudent",
            type: "POST",
			data:{id:id,batchid:batch_id},
            async: true, 
             success: function(response){ 
			    alert(response);
                 if(response.indexOf("true") > -1 )
                 {
                    swal("Deleted!", "Student has been deleted successfully.", "success");                    
                    setTimeout(function(){location.reload();},2000);          
                 }
                 else
                 {
                    swal("Opps...!", "Somthing going wrong. can't delete Student !!", "error"); 
                   location.reload();                                                 
                 }

        }
    });



    });
}


	$("#pages").addClass("in");
	$("#managestudents").addClass("active");
	$("#managestudent").addClass("active");
	</script>
	
	
</body>

</html>
