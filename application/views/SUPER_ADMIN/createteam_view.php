<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>

<body <?php  echo 'onload=usertypelist1(2)';?>>
<div id="wrapper">

		<?php include("header.php"); ?>
        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Placement Admin
                                <small>Manage Placement Admin</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="dashboard">Dashboard</a>
                                </li>
                                <li class="active">Manage Placement Admin</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

				<?php
				if($this->session->flashdata('successmessage'))
				{
					$successmessage = $this->session->flashdata('successmessage');
					echo "<script>swal('Ok', '$successmessage', 'success')</script>";
				}
				else if($this->session->flashdata('errormessage'))
				{
					$errormessage = $this->session->flashdata('errormessage');
					echo "<script>swal('Opps..!', '$errormessage', 'error')</script>";
				}
				?>

                   <div class="row">
                   	              
                    <!-- /.col-lg-12 -->
             

                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                    <div class="col-lg-12" style="margin-bottom:250px;">
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Placement Admin List</h4>
                                </div>
                              
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="sky-form" role="form" method="post" action="<?php echo base_url(); ?>superadmin/createteam_main">
<!-- 										<div class="form-group">
                                            <label for="usertype" class="col-sm-3 control-label">User Type <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-9">
                                                <select class="form-control" id="usertype" name="usertype" onchange="usertypelist()">
													<option value="0">Select</option>
													<option value="2">Faculty</option>
												</select>

												<input type="text" value="Faculty" readonly>
                                            </div>
                                        </div> -->
										
										<div class="form-group" id="forStudent">
										</div>
										
										<div class="form-group">
										<label for="StudentList" class="col-sm-3 control-label">&nbsp;</label>
										<div id="fetchusertypelist" class="col-sm-12 table-responsive">
										
										</div>
										<div id="fetchguestuserlist" class="col-sm-12 table-responsive">
										
										</div>										
										<div id="fetchstudlist" class="col-sm-12 table-responsive">
										
										</div>
										</div>
										
										<!--<div class="form-group">
												<label for="enabled" class="col-sm-3 control-label">Enabled</label>
												<div class="col-sm-9">
													<input type="checkbox" id="enabled" name="enabled" checked="checked" />
												</div>
										</div>-->
                                    
                                        <!--<div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <button type="submit" class="btn btn-default">Save</button>
												<input id="facultycount" type="hidden" />
                                            </div>
                                        </div>-->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->
					
					
					
					

                </div>
                <!-- /.row -->
				
				
				

                </div>
                <!-- /.row -->
				
            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
<!-- /#wrapper -->

    <?php include("alljs.php"); ?>
	
	<!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
	
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					"groupname":
					{
						required: true,
						min:1
					},
					"usertype":
					{
						required: true,
						min:1
					},
					"StudentBatch":
					{
						required: true,
						min:1
					},
					"Student[]":
					{
						required: true
					},
					"Faculty[]":
					{
						required: true
					},
					"GuestUser[]":
					{
						required: true
					}
				},
									
				// Messages for form validation
				messages:
				{
					"groupname":
					{
						required: '<span style="color:#b81212; margin-left:265px;">Please select placement group name</span>',
						min: '<span style="color:#b81212; margin-left:265px;">Please select placement group name</span>'
					},
					"usertype":
					{
						required: '<span style="color:#b81212; margin-left:265px;">Please select user type</span>',
						min: '<span style="color:#b81212; margin-left:265px;">Please select user type</span>'
					},
					"StudentBatch":
					{
						required: '<span style="color:#b81212; margin-left:265px;">Please select batch</span>',
						min: '<span style="color:#b81212; margin-left:265px;">Please select batch</span>'
					},
					"Student[]":
					{
						required: '<span style="color:#b81212;">Please select atleast one record</span>'
					},
					"Faculty[]":
					{
						required: '<span style="color:#b81212;">Please select atleast one record</span>'
					},
					"GuestUser[]":
					{
						required: '<span style="color:#b81212;">Please select atleast one record</span>'
					}
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					//error.insertAfter(element.parent());
					if ($(element).attr("type") === "checkbox") {
						$("#checkBoxErrorHolder").html(error);
					} else {
						error.insertAfter(element.parent());
					}
					
				}
				
				
			});
		});
	</script>
	<!-- for usertypelist -->
	<script type="text/javascript">
		function usertypelist()
		{
			
		var usertype = 2;
				alert(usertype);
					 if(usertype == 2)
					{
					var groupname = 1;
					$.ajax({
					url: "<?php echo base_url();?>superadmin/fetchusertypelist",
					type: "POST",
					async: true, 
					dataType:'json',
					data: { usertype:usertype, groupname:groupname}, //your form data to post goes here
					 success: function(response){
								//alert(response);
								var i = 0;
								var json = response;
								var obj = json[i];
								//alert(obj.numqry);
								$("#facultycount").val(obj.numqry);
								
								var dataSet = [];
								
								$('#forStudent').html('');
								$('#fetchguestuserlist').html('');
								$('#fetchusertypelist').html('');
								$('#fetchstudlist').html('');
								$('#fetchusertypelist').append('<div class="col-lg-12"><div id="checkBoxErrorHolder"></div><div class="portlet portlet-default"><table id="example-table1" class="table table-striped table-bordered table-hover table-green"><thead><tr><th width="100px;">Sr. No.</th><th width="100px;">Select <span style="color:#b81212">*</span></th><th>Faculty Name</th></tr></thead><tbody id="tbody">');
								
								var j = 1;
								for(var i = 0; i < json.length; i++) {
									var obj = json[i];
									//alert(obj.cred_id);
									
									//alert(obj.numfaculty);
									if(obj.numfaculty == 1)
									{
										//alert(obj.facultyy);
										$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value="'+obj.credentials_id+'_'+obj.first_name+'" id="Faculty" name="Faculty[]" onclick="funforappend()" ' + (obj.facultyy > 0 ? 'checked=checked' : '')+'></td><td>'+obj.first_name+' '+obj.last_name+'</td></tr>');
											j++;
									}
									else
									{
										$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value="'+obj.credentials_id+'_'+obj.first_name+'" id="Faculty" name="Faculty[]" onclick="funforappend()"></td><td>'+obj.first_name+' '+obj.last_name+'</td></tr>');
											j++;
									}
								}
								
								$('#fetchusertypelist').append('</tbody></table></div></div>');
								
								
								$('#example-table1').dataTable( {
									"data": dataSet,
									"columns": []
								} );
								
								
						}
					});
					
					}
					else if(usertype == 3)
					{
					var groupname = $("#groupname").val();
					$.ajax({
					url: "<?php echo base_url();?>superadmin/fetchusertypelist",
					type: "POST",
					async: true, 
					dataType:'json',
					data: { usertype:usertype, groupname:groupname}, //your form data to post goes here
					 success: function(response){
								//alert(response);
								var json = response;
								
								var dataSet = [];
								
								$('#forStudent').html('');
								$('#fetchguestuserlist').html('');
								$('#fetchusertypelist').html('');
								$('#fetchstudlist').html('');
								$('#fetchguestuserlist').append('<div class="col-lg-12"><div id="checkBoxErrorHolder"></div><div class="portlet portlet-default"><table id="example-table2" class="table table-striped table-bordered table-hover table-green"><thead><tr><th width="100px;">Sr. No.</th><th width="100px;">Select <span style="color:#b81212">*</span></th><th>Guest User Name</th></tr></thead><tbody id="tbody">');
								
								var j = 1;
								for(var i = 0; i < json.length; i++) {
									var obj = json[i];
									//alert(obj.first_name);
									//alert(obj.numguestuser);
									if(obj.numguestuser == 1)
									{
										$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value="'+obj.credentials_id+'_'+obj.first_name+'" id="GuestUser" name="GuestUser[]" onclick="funforappend()" ' + (obj.guestusery > 0 ? 'checked=checked' : '')+'></td><td>'+obj.first_name+'</td></tr>');
										j++;
									}
									else
									{
										$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value="'+obj.credentials_id+'_'+obj.first_name+'" id="GuestUser" name="GuestUser[]" onclick="funforappend()"></td><td>'+obj.first_name+'</td></tr>');
										j++;
									}
								}
								
								$('#fetchguestuserlist').append('</tbody></table></div></div>');
								
								$('#example-table2').dataTable( {
									"data": dataSet,
									"columns": []
								} );
						}
					});
				}
		}


// used

		function usertypelist1(id)
		{
			
		var usertype = id;
				//alert(usertype);
					 if(usertype == 2)
					{
					var groupname = 1;
					$.ajax({
					url: "<?php echo base_url();?>superadmin/fetchusertypelist",
					type: "POST",
					async: true, 
					dataType:'json',
					data: { usertype:usertype, groupname:groupname}, //your form data to post goes here
					 success: function(response){
								//alert(response);
								var i = 0;
								var json = response;
								var obj = json[i];
								//alert(obj.numqry);
								$("#facultycount").val(obj.numqry);
								
								var dataSet = [];
								
								$('#forStudent').html('');
								$('#fetchguestuserlist').html('');
								$('#fetchusertypelist').html('');
								$('#fetchstudlist').html('');
								$('#fetchusertypelist').append('<div class="col-lg-12"><div id="checkBoxErrorHolder"></div><div class="portlet portlet-default"><table id="example-table1" class="table table-striped table-bordered table-hover table-green"><thead><tr><th width="100px;">Sr. No.</th><th width="100px;">Select <span style="color:#b81212">*</span></th><th>Faculty Name</th></tr></thead><tbody id="tbody">');
								
								var j = 1;
								for(var i = 0; i < json.length; i++) {
									var obj = json[i];
									//alert(obj.cred_id);
									
									//alert(obj.numfaculty);
									if(obj.numfaculty == 1)
									{
										//alert(obj.facultyy);
										$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value="'+obj.credentials_id+'" id="Faculty" name="Faculty[]" onclick="funforappend()" ' + (obj.facultyy > 0 ? 'checked=checked' : '')+'></td><td>'+obj.first_name+' '+obj.last_name+'</td></tr>');
											j++;
									}
									else
									{
										$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value="'+obj.credentials_id+'" id="Faculty" name="Faculty[]" onclick="funforappend()"></td><td>'+obj.first_name+' '+obj.last_name+'</td></tr>');
											j++;
									}
								}
								
								$('#fetchusertypelist').append('</tbody></table></div></div><div class="form-group"><label class="col-sm-2 control-label"></label><div class="col-sm-10"><button type="submit" class="btn btn-default">Save</button><input id="facultycount" type="hidden" /></div></div>');
								
								
								$('#example-table1').dataTable( {
									"data": dataSet,
									"columns": []
								} );
								
								
						}
					});
					
					}
					else if(usertype == 3)
					{
					var groupname = $("#groupname").val();
					$.ajax({
					url: "<?php echo base_url();?>superadmin/fetchusertypelist",
					type: "POST",
					async: true, 
					dataType:'json',
					data: { usertype:usertype, groupname:groupname}, //your form data to post goes here
					 success: function(response){
								//alert(response);
								var json = response;
								
								var dataSet = [];
								
								$('#forStudent').html('');
								$('#fetchguestuserlist').html('');
								$('#fetchusertypelist').html('');
								$('#fetchstudlist').html('');
								$('#fetchguestuserlist').append('<div class="col-lg-12"><div id="checkBoxErrorHolder"></div><div class="portlet portlet-default"><table id="example-table2" class="table table-striped table-bordered table-hover table-green"><thead><tr><th width="100px;">Sr. No.</th><th width="100px;">Select <span style="color:#b81212">*</span></th><th>Guest User Name</th></tr></thead><tbody id="tbody">');
								
								var j = 1;
								for(var i = 0; i < json.length; i++) {
									var obj = json[i];
									//alert(obj.first_name);
									//alert(obj.numguestuser);
									if(obj.numguestuser == 1)
									{
										$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value="'+obj.credentials_id+'_'+obj.first_name+'" id="GuestUser" name="GuestUser[]" onclick="funforappend()" ' + (obj.guestusery > 0 ? 'checked=checked' : '')+'></td><td>'+obj.first_name+'</td></tr>');
										j++;
									}
									else
									{
										$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value="'+obj.credentials_id+'_'+obj.first_name+'" id="GuestUser" name="GuestUser[]" onclick="funforappend()"></td><td>'+obj.first_name+'</td></tr>');
										j++;
									}
								}
								
								$('#fetchguestuserlist').append('</tbody></table></div></div>');
								
								$('#example-table2').dataTable( {
									"data": dataSet,
									"columns": []
								} );
						}
					});
				}
		}

	</script>
	
	<!-- for groupnamelist -->
	<script type="text/javascript">
		function groupnamelist()
		{
			$("#usertype").val("0").attr('selected', true);
			$('#forStudent').html('');
		}
	</script>

	<script type="text/javascript">
		$("#managePTAdmin").addClass("active");
		// $("#pages3").addClass("in");
		// $("#managePTAdmin").addClass("active");
	</script>
	
</body>
</html>