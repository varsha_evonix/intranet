<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>



<body>
<div id="wrapper">

		<?php include("header.php"); ?>



        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Batches
                            <small>Manage Batches</small></h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>superadmin/dashboard/">  Dashboard</a>
                                </li>
                                <li class="active">Manage Batches</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
				
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">
				  <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>

                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Batch List</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
								  

							
	                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green" style="text-align:center;">
                                        <thead>
                                            <tr>
                                                <th>Batch Name</th>
                                                <th>In year</th>
                                                <th>Out year</th>
                                                <th>Course Name</th>
                                                <th> Status </th>
												<th>Edit</th>
												<th>Delete</th>
                                                 												
                                            </tr>
                                        </thead>
                                        <tbody>
										
                                     <?php


                                       foreach($info as $i) 
                                       {
									   $bcid= $i->batch_course_id;
									      ?>
                                             
                                                     <tr id='td<?php echo $bid; ?>'><td><?php echo  $i->batch_name;?></td><td>
														<?php echo $i->in_year ;?></td><td>
														 <?php echo  $i->out_year; ?></td><td>
														 <?php echo  $i->course_name ;?></td><td>

                                                   <?php if($i->enabled =="1") echo " <span class='badge green'>Enabled</span>"; else echo"<span class='badge red'>Disabled</span>"?></td><td>
										 <a href="<?php echo base_url();?>superadmin/editbatch/<?php echo $i->batch_id;?>/<?php echo $i->course_id;?>"><i class="fa fa-pencil"></i>&nbsp; EDIT</a></td><td>
					                             <a onclick='deleteOrganization(<?php echo $bcid; ?>)' title='Click Here To Delete' style="cursor: pointer;"><div class=''><i class='fa fa-times'></i> DELETE </a></td>
											   </tr>     

                                    <?php									  
									   } 
									   
									    
									?>


										
										
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
	    <?php include("alljs.php"); ?>

	<script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
	<script>
		function deleteOrganization(bcid)
{
 swal({  title: "Are you sure?",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes",   
          closeOnConfirm: false 
     },


      function(){ 
            $.ajax({
            url: "<?php echo base_url();?>superadmin/checkbatch/"+bcid,
            type: "POST",
            async: true, 
             success: function(response){ 
                 if(response.indexOf("true")> -1)
                 {
					swal({  title: "This Batch contains students.Delete students first!Do you want to delete students?",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes!",   
          closeOnConfirm: false 
     },
                  function(){
				 location.href = '<?php echo base_url();?>superadmin/deletestudents/'+bcid;                                                  
                 });
				 }
                 else
                 {  
			       $.ajax({
                   url: "<?php echo base_url();?>superadmin/deletebatches",
                   type: "POST",
				   data:{id:bcid},
				   async: true, 
                 success: function(response){
					    if(response.indexOf("true")> -1)
                 {
				  swal("Deleted", "Batch has been deleted successfully!", "success"); 
                   setTimeout(function(){location.reload();},2000);
				 }
                      else	
					  {
						  swal("oops","error in deleting batch","error");
						 location.reload();
					  }
				 
					 }
				 });					  
                 }
				
			 }

             });
        });
			
    }

   

	</script>
   <script>
	$("#pages2").addClass("in");
	$("#managebatches").addClass("active");
	$("#viewbatch").addClass("active");
	</script> 

</body>

</html>
