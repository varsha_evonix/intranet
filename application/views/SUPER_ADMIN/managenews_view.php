<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>



<body>
<div id="wrapper">

		<?php include("header.php"); ?>



        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage News
                           <small>Manage News </small></h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>superadmin/dashboard/"> Dashboard</a>
                                </li>
                                <li class="active">Manage News</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
				
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">
				    <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>


                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Course List</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
								
							
	                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green" style="text-align:center;">
                                        <thead>
                                            <tr>
                                                <th>News Name</th>
                                                <th> Created On </th>
												<th>Edit</th>
												<th>Delete</th>
                                                 												
                                            </tr>
                                        </thead>
                                        <tbody>
										
                                     <?php


                                       foreach($news as $n) 
                                       {



									   $nid= $n->ID;
									      ?>
                                                     <td><?php echo  $n->title;?></td>
                                                     <td>
									
														 <?php echo  $n->createdOn; ?></td>
														 
														 <td>
														 
										 <a href="<?php echo base_url();?>superadmin/editnews/<?php echo $n->ID; ?>" title='Click Here To Edit'><i class="fa fa-pencil"></i>&nbsp; EDIT</a></td><td>
										 
										 <a onclick='deleteOrganization(<?php echo $nid; ?>)' title='Click Here To Delete' style="cursor: pointer;"><div class=''><i class='fa fa-times'></i> DELETE </a></td>
										  
										  
										  </tr>
									
					                  

                                    <?php									  
									   } 
									   
									    
									?>


										
										
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
	    <?php include("alljs.php"); ?>
<script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
	
	<script>
	
	function deleteOrganization(cid)
{

  

 swal({  title: "Are you sure?",  
          type: "warning",    
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes, delete it!",   
          closeOnConfirm: false 
     },


      function(){ 
             	  
            $.ajax({
            url: "<?php echo base_url();?>superadmin/deletenews/"+cid,
            type: "POST",
            async: true, 
             success: function(response){ 
                 if(response.indexOf("true")> -1)
                 {
                    swal("Deleted!", "Course has been deleted successfully.", "success");                    
                   setTimeout(function(){location.reload();},2000);                   
                 }
                 else
                 {
                    swal("Oops..!", "Somthing going wrong. can't delete Course !!", "error"); 
                    location.reload();                                                  
                 }

        }
    });



    });
}

	</script>
	
	<script>
	$("#newsid").addClass("in");
	$("#managenews").addClass("active");
	$("#createnews").addClass("active");
	</script>
    

</body>

</html>
