<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>
<style>
.invalid{
  padding-left: 28%;
 <!-- color:rgb(184, 18, 18);-->
    
}
.form-control
{
  padding-left:2% !important;
}
</style>
<script type="text/javascript">

function show2(id)
{
      viewspl1(id);
}   

</script>
<body <?php if( isset($courseid)){ echo 'onload=show2('.$courseid.')';} ?>>
<div id="wrapper">

		<?php include("header.php"); ?>

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Batches
                                <small>Update Batch</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="#">Dashboard</a>
                                </li>
                                <li class="active">Update Batch</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->


                   <div class="row">
                   	              
                    <!-- /.col-lg-12 -->
               
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                    <div class="col-lg-8">
						 <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
					{
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";

					}
                        else if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Update Batch</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
									<?php
									//print_r($data);
									foreach($data as $r)
									{
										$bcid=$r->batch_course_id; 
									?>
                                <form class="form-horizontal" id="sky-form" role="form" method="post" action="<?php echo base_url();?>superadmin/updatebatch/">
                                 
                                        <div class="form-group">
                                            <label for="batchname" class="col-sm-3 control-label">Batch Name <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="bname" name="bname" style="width:90%" value="<?php echo $r->batch_name; ?>" placeholder="Enter Batch Name" required />
                                            </div>
                                        </div>
										
									   <div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">Batch In year <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" maxlength="4" id="inyear" name="inyear" style="width:90%" value="<?php echo $r->in_year; ?>" placeholder="Enter Batch in year" required />
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">Batch out year <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" maxlength="4" id="outyear"  style="width:90%" name="outyear" value="<?php echo $r->out_year; ?>" placeholder="Enter Batch out year" required />
                                            </div>
                                        </div>
										
									   <div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">No of Terms <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="terms" name="terms" value="<?php echo $r->no_of_terms; ?>" style="width:90%"  placeholder="Enter No of Terms" required />
                                            </div>
                                        </div>
										
							
									

										<div class="form-group">
                                            <label for="cname" class="col-sm-3 control-label">Change Course <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                             <select class="form-control" name="cname" onchange="editspl()" id="cname" style='width:90%'>
                                                      <?php		
                                                          foreach($cname as $c)
			                                              {?>
				                                                <option <?php if($r->course_id == $c->course_id) echo "selected=selected";?> value="<?php echo $c->course_id; ?>"><?php echo $c->course_name; ?></option><?php
			                                              }
                                                       ?>
                                                      </select>

                                                    </div>
											</div>	
											
										
													
											<div class="form-group">
											<label for="Student" class="col-sm-3 control-label" >&nbsp;</label>
											<div id="speclist">
											
											</div>
											</div>
											
								
										
										<div class="form-group">
												<label for="enabled" class="col-sm-3 control-label">Enabled</label>
												<div class="col-sm-7">
													<input type="checkbox" id="enabled" name="enabled" <?php if($r->enabled == 1) echo "checked='checked'"; ?> />
												</div>
										</div>
                                    
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-3">
                                                <button type="submit" class="btn btn-default">Update</button>
												<input type="hidden" class="form-control" id="batch_id" name="batch_id" value="<?php echo $r->batch_id; ?>" />
												<input type="hidden" class="form-control" id="batch_course_id" name="batch_course_id" value="<?php echo $r->batch_course_id; ?>" />
												
                                            </div>
                                        </div>
                                    </form><?php
									}?>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
<!-- /#wrapper -->

    <?php include("alljs.php"); ?>
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
		  $.validator.addMethod("regeNUM", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			var alphaNumeric = "^[(a-z)(A-Z)(0-9)\-]+$";
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					bname:
					{
					    
					   /*regeNUM: alphaNumeric,*/
						required: true
					},
					inyear:
				    {
					required: true,
					number: true,
					batchcheck: true
				    },
					outyear:
				    {
					required: true,
					number: true,
					batchcheck: true
					
					}, 
					cname:
					{
					 required:true,
					 min:1
					},
					terms:
				    {
					required: true,
					number: true
				    },
				 "spec[]":
					{
					  required:true
					}
			
						
					
					
				},
									
				// Messages for form validation
				messages:
				{
					bname:
					{
						 /*regeNUM: '<span style="color:red;margin-left:-3px;">Please enter alphanumeric charachters only</span>',*/
						
						required: '<span style="color:rgb(184, 18, 18); margin-left:-3px;">Please enter Batch name</span>',
						
					},
					inyear:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:-3px;">Please enter In Year</span>',
						number: '<span style="color:rgb(184, 18, 18);margin-left:-3px;">Please enter a valid year</span>'
			
					},
					
					outyear:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:-3px;">Please enter Out Year</span>',
						number: '<span style="color:red;margin-left:-3px;">Please enter a valid year</span>'
					},
					cname:
					{
					   required: '<span style="color:rgb(184, 18, 18); margin-left:-3px;">Please select course</span>',
						min: '<span style="color:rgb(184, 18, 18); margin-left:-3px;">Please select course</span>'
					},
					terms:
					{
						required: '<span style="color:rgb(184, 18, 18); margin-left:-3px;">Please enter total number of terms</span>',
						number: '<span style="color:red;margin-left:-3px;">Please enter a valid number</span>'
					},
					"spec[]":
					{
					   required:'<span style="color:#b81212; margin-left:-157px;">Please select specialization</span>'
					}
					
					
					
			
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					if($(element).attr("type")==="checkbox"){
				      $("#checkboxerror").html(error);
					  }
					  else
					  {
					 
					error.insertAfter(element.parent());
					}
				}



				
				
			});


      
				$.validator.addMethod('batchcheck',function(value, element)
				{
				return $('#inyear').val() < $('#outyear').val()
				}, "<span style='color:rgb(184, 18, 18);'>batch in Year should be less than batch out year</span>");
				 
				 
				

		});
	</script>

<script>
function editspl()
{
  //alert($("#cname").val());
			var cname = $("#cname").val();
			var bid = $("#batch_id").val();
			$.ajax({
        url: "<?php echo base_url();?>superadmin/fetchspeclist",
        type: "POST",
        async: true, 
		dataType:'json',
        data: { cname:cname, bid:bid }, //your form data to post goes here
         success: function(response){ 
					var json = response;
					$('#speclist').html('');
					$('#speclist').append('<br><br><br><div id="4" class="col-sm-12 table-responsive toggledDropDown" style="display: block;margin-left:20%";><div id="checkboxerror"></div><div class="col-lg-8"><div class="portlet portlet-default"><table id="example-table7" class="table table-striped table-bordered table-hover table-green" width="20%"><thead><tr><th width="190px;">Sr. No.</th><th width="190px;">Select</th><th width="290px">Specialization Name</th></tr></thead><tbody id="tbody">');
					
					var j = 1;
					for(var i = 0; i < json.length; i++) {
						var obj = json[i];
						
					//alert(obj.specialization_name);
						
						$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value='+obj.specialization_id+' id="spec" name="spec[]"></td><td>'+obj.specialization_name+'</td></tr>');
						j++;
						//$('#studlist').append('<div>'+obj.first_name+'</div>');
					}
					
					$('#speclist').append('</tbody></table></div></div></div>');
					
			}
    });
	
}


		$('#cname').change(function() {
		//alert();
		// Hide all drop downs sharing the CSS class "toggledDropDown".
		$('.toggledDropDown').hide();
		
		// Build a selector for the selected drop down
		var selector = ('#' + $(this).val());
		//alert(selector);
		// Show the selected drop down
		$(selector).show();

	});

function viewspl1(id)
{
 var cname = id;
 var bid = $("#batch_course_id").val();
			
// alert(bid);

  $.ajax({
        url: "<?php echo base_url();?>superadmin/fetchspeclist1",
        type: "POST",
        async: true, 
		dataType:'json',
        data: {cname:cname,bid:bid}, //your form data to post goes here
         success: function(response){ 
					var json = response;
					$('#speclist').html('');
					$('#speclist').append('<br><br><br><div id="4" class="col-sm-12 table-responsive toggledDropDown" style="display: block;margin-left:20%";><div id="checkboxerror"></div><div class="col-lg-8"><div class="portlet portlet-default"><table id="example-table7" class="table table-striped table-bordered table-hover table-green" width="20%"><thead><tr><th width="190px;">Sr. No.</th><th width="190px;">Select</th><th width="290px">Specialization Name</th></tr></thead><tbody id="tbody">');
					
					var j = 1;
					for(var i = 0; i < json.length; i++) {
						var obj = json[i];
						
						$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value='+obj.specialization_id+' id="spec" name="spec[]" ' + (obj.y > 0 ? 'checked=checked' : '')+'></td><td>'+obj.specialization_name+'</td></tr>');
						j++;
						//$('#studlist').append('<div>'+obj.first_name+'</div>');
					}
					
					$('#speclist').append('</tbody></table></div></div></div>');
					
			}
    });
	
}


	

  
  

</script>
	
</body>

</html>