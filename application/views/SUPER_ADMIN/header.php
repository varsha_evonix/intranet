            <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle pull-right" data-toggle="collapse" data-target=".sidebar-collapse">
                    <i class="fa fa-bars"></i> Menu
                </button>
                <div class="navbar-brand" style="color:white">
                    SITM
                </div>
            </div>
            <!-- end BRAND HEADING -->

            <div class="nav-top">

             
                <!-- begin MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->
                <ul class="nav navbar-right">

                    <!-- begin USER ACTIONS DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="#">
                                    <i class="fa fa-user"></i> My Profile
                                </a>
                            </li>
                         
                            <li class="divider"></li>
                            <li>
                                <a class="logout_open" href="#logout">
                                    <i class="fa fa-sign-out"></i> Logout
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- end USER ACTIONS DROPDOWN -->

                </ul>
                <!-- /.nav -->
                <!-- end MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->

            </div>
            <!-- /.nav-top -->
        </nav>
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->

        <!-- begin SIDE NAVIGATION -->
        <nav class="navbar-side" role="navigation">
            <div class="navbar-collapse sidebar-collapse collapse">
                <ul id="side" class="nav navbar-nav side-nav">
                    <!-- begin SIDE NAV USER PANEL -->
                    <li class="side-user hidden-xs">
                        <img class="img-circle" src="<?php echo base_url();?>assets/img/super_admin_image.jpg" alt="admin profile picture" style=" height: 160px; width: 155px;  margin-left: 15px;">
                        <p class="welcome">
                            <i class="fa fa-key"></i> Logged in as
                        </p>
                        <p class="name tooltip-sidebar-logout">
                            Super Admin <a style="color: inherit" class="logout_open" href="#logout" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></a>
                        </p>
                        <div class="clearfix"></div>
                    </li>
                    <!-- end SIDE NAV USER PANEL -->

                    <!-- begin DASHBOARD LINK -->
                    <li>
                        <a id="dashboard" href="<?php echo base_url();?>superadmin/dashboard/">
                            <i class="fa fa-dashboard"></i> Dashboard
                        </a>
                    </li>
                    
					 <li class="panel">
                        <a id="managecourses" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#ui-elements">
                           <i class="fa fa-edit"></i> Manage Courses <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="ui-elements">
                            <li>
                                <a id="createcourse" href="<?php echo base_url();?>superadmin/createcourse">
                                 <i class="fa fa-angle-double-right"></i> Create Course
                                </a>
                            </li>
                            <li>
                                <a id="managecourse" href="<?php echo base_url();?>superadmin/managecourses/">
                                    <i class="fa fa-angle-double-right"></i> Manage Courses
                                </a>
                            </li>
                        </ul>
						</li>
                
					
					   <li class="panel">
                        <a id="managespecializations" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#tables">
                           <i class="fa fa-thumb-tack"></i> Manage Specializations <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="tables">
                            <li>
                                <a id="createspecialization" href="<?php echo base_url();?>superadmin/createspecialization">
                                    <i class="fa fa-angle-double-right"></i> Create Specialization
                                </a>
                            </li>
                            <li>
                                <a id="managespecialization" href="<?php echo base_url();?>superadmin/managespecializations/">
                                    <i class="fa fa-angle-double-right"></i> Manage Specializations
                                </a>
                            </li>
                        </ul>
                    </li>
					
		    <li class="panel">
                        <a id="managebatches" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#pages2">
                            <i class="fa fa-sitemap"></i>  Manage Batches <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="pages2">
                            <li>
                                <a id="createbatch" href="<?php echo base_url();?>superadmin/createbatch">
                                   <i class="fa fa-angle-double-right"></i> Create Batch 
                                </a>
								
                            </li>
							  <li>
                                <a id="viewbatch" href="<?php echo base_url();?>superadmin/managebatches">
                                  <i class="fa fa-angle-double-right"></i> Manage Batches
                                </a>
                            </li>
                                                      
                          
                          
                          
                        </ul>
                    </li>
						
					
                     <li class="panel">
                        <a id="managestudents" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#pages">
                            <i class="fa fa-user"></i> Manage Students <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="pages">
						 <li>
                                <a id="createstudent" href="<?php echo base_url();?>superadmin/createstudent">
                                    <i class="fa fa-angle-double-right"></i> Create Student
                                </a>
                            </li>
                            <li>
                                <a id="addstudent" href="<?php echo base_url();?>superadmin/addbulkstudent">
                                    <i class="fa fa-angle-double-right"></i> Add Bulk Students
                                </a>
                            </li>
                            <li>
                                <a id="managestudent" href="<?php echo base_url();?>superadmin/managestudents">
                                  <i class="fa fa-angle-double-right"></i> Manage Students
                                </a>
                            </li>
							<li>
                                <a id="deletedstudent" href="<?php echo base_url();?>superadmin/deletedstudents">
                                  <i class="fa fa-angle-double-right"></i> Deleted Students
                                </a>
                            </li>

                        </ul>
                    </li>
					   <li class="panel">
                        <a id="managefaculties" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#pages1">
                            <i class="fa fa-suitcase"></i> Manage Faculty<i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="pages1">
						 <li>
                                <a id="createfaculty" href="<?php echo base_url();?>superadmin/createfaculty">
                                    <i class="fa fa-angle-double-right"></i> Create Faculty
                                </a>
                            </li>
                            <li>
                                <a id="addfaculty" href="<?php echo base_url();?>superadmin/addbulkfaculties">
                                    <i class="fa fa-angle-double-right"></i> Add Bulk Faculties
                                </a>
                            </li>
                            <li>
                                <a id="managefaculty" href="<?php echo base_url();?>superadmin/managefaculty">
                                  <i class="fa fa-angle-double-right"></i> Manage Faculty
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="panel">
                        <a id="managenews" href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#newsid">
                           <i class="fa fa-edit"></i> Manage News <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="newsid">
                            <li>
                                <a id="createnews" href="<?php echo base_url();?>superadmin/createnews">
                                 <i class="fa fa-angle-double-right"></i> Create News
                                </a>
                            </li>
                            <li>
                                <a id="managenews" href="<?php echo base_url();?>superadmin/managenews/">
                                    <i class="fa fa-angle-double-right"></i> Manage News
                                </a>
                            </li>
                        </ul>
						</li>
                       <!--<li class="panel">
                     
                        <a id="managePTAdmin" href="<?php echo base_url();?>superadmin/createPlacementAdmin">
                            <i class="fa fa-cogs"></i> Manage Placement Admin
                        </a>
                    </li>-->
					<li class="panel">
                        <a id="changepassAdmin" href="<?php echo base_url();?>superadmin/changepassword">
                            <i class="fa fa-key"></i> Change Password
                        </a>
                    </li>
				 
				 
                </ul>
				
				
				
				
				
                <!-- /.side-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </nav>
        <!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->
		
		