<html>
    <head>
	<?php include('alllinks.php');?>
        <meta charset="utf-8">
       <style>
td{
padding:6px !important;
font-size:14px !important;
}
</style>
    </head>
    <body>
 <div id="wrapper">
		<?php include("header.php"); ?>
             <div id="page-wrapper">
            <div class="page-content">
                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Faculty
                                <small>Add Bulk Faculties</small>
                            </h1>
                            <ol class="breadcrumb">
                               <li><i class="fa fa-dashboard"></i>
							   <a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a></li>
                                <li class="active">Add Bulk Facuties</li>

                            </ol>
                        </div>		
<!--new-->
<?php if(isset($error))
{ ?>
<h2>Unsaved Data </h2>
<div class="portlet portlet-red">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4><i class='fa fa-warning'></i> Please upload unique Email or valid fields!</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                     <table class="table table-striped table-bordered table-hover table-red">
                                        <thead>
                                            <tr>
                                               <th>Name</th>
                                              <th>Date of Birth </th>
                                              <th>Gender</th>
                                               <th>Email</th>
						<th>Contact</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                        <?php foreach ($error as $row): ?>
                            <tr>
                              <td><?php echo $row['first_name']; ?>  <?php echo $row['middle_name']; ?>  <?php echo $row['last_name'];?></td>
                                <td><?php echo $row['DOB'];?> </td>
	                        <td><?php echo $row['gender'];?></td>
                                <td><?php echo $row['allocated_email'];?> </td>
				<td><?php echo $row['contact_no_1'];?> </td>				
                            </tr>
                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
<?php } ?>
                                </div>
                            </div>
                        </div>
<!--new-->
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->
				 <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                     if(isset($message)) {
                     echo "
                        <script>
                        sweetAlert('Ok', '".$message."', 'success');
                        </script>";

                    }
                        
                     ?>
                </div>
			 <!-- begin MAIN PAGE ROW -->
                <div class="row">
                    <!-- begin LEFT COLUMN -->
                    <div class="col-lg-12">
                            <!-- Basic Form Example -->
                            <div class="col-lg-12">
                                <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Add Faculties</h4>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                   <div id="validationExamples" class="panel-collapse collapse in">
                                   <!--<div  class="panel-collapse collapse in">-->
                                        <div class="portlet-body">
			<form class="form-horizontal" role="form" id="sky-form" action="<?php echo base_url();?>superadmin/importcsv2" method="POST" enctype="multipart/form-data" >
		<p class="help-block" style="font-size:15px">
           <strong> Upload a CSV file with predefined format. You can download the format from the link given below. Fill in the necessary details and upload it using the upload control below. </strong>
        </p>
        <h4> Note </h4>

        <ul>
            <li style="font-size:15px"> Do not change header (first row in sample file). </li>
            <li style="font-size:15px"> Email address should be unique and valid.</li>
        </ul>            

		<div class="form-group">
		<label class="col-sm-6 control-label" style="text-align:left;"><a href="<?php echo base_url();?>samples/faculty_sample.csv" class="btn btn-default">Click Here to Download Sample</a>
		   </div>
             <div class="form-group"> 
			 <label class="col-sm-4 control-label">Select File to Upload:<span style="color:#b81212">*</span></label>
			   <div class="col-sm-8">
                <input type="file" name="userfile"  />
                <i><font color="#B81212">(Upload .csv file only)</font></i>
				</div> </div>
            
			 <div class="form-group">
                                                    <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-4">
           <button type="submit" name="submit" class="btn btn-default" value="Upload">
		   Upload</button>
		   </div>
		   </div>
             </form>
		 </div>
                                    </div>
                                </div>
                                <!-- /.portlet -->
                            </div>
							</div>
							
</div>
<!--new -->

			  <!-- /.row -->

 <?php include('alljs.php');?>
    <script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
		     
		    $.validator.addMethod("regex", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			 $.validator.addMethod("regeNUM", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
				var ALPHA_REGEX = "[a-zA-Z ]*$";
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
				
					bname1:
					{
					   required:true,
					   min:1
					}
					
					
					
				},
									
				// Messages for form validation
				messages:
				{
				
					bname1:
					{
					   required: '<span style="color:#B81212; margin-left:15px;">Please Select Batch</span>',
					   min:'<span style="color:#B81212; margin-left:15px;">Please Select Batch</span>'
						
					}
			
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
				
				
			});
		});
	</script>
<script>
	$("#pages1").addClass("in");
	$("#managefaculties").addClass("active");
	$("#addfaculty").addClass("active");
	</script>
</body>

</html>