<!DOCTYPE html>
<html lang="en">

<head>
    <?php include("alllinks.php"); ?>


</head>
 
<?php
    $studid=$this->session->userdata('studentid');
  
 
?>
<body <?php if(isset($studid)){ echo "onload='show2($studid)'";}  //$this->session->unset_userdata('studentid'); ?> >
<div id="wrapper">

        <?php include("header.php"); ?>
        <link href="<?php echo base_url();?>assets/css/plugins/datatables/datatables.css" rel="stylesheet">        

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Deleted Students
                                <small>Deleted Students</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a>
                                </li>
                             <li class="active">Deleted Students</li>
                            </ol>
                        </div>
                    </div><!-- /.col-lg-12 -->

								
                        <div class="col-lg-12">
                             <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                    {
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";

                    }
                        else if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Deleted Students List</h4>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
                                    <form class="form-horizontal" id="validate" role="form" novalidate="novalidate">
                                    	<!--<h5 style="color:#B81212;margin-left:166px;">*Select batch to show deleted student list</h5>-->
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><span style='color:#B81212;'>*</span>Select batch</label>
											<?php  $studid=$this->session->userdata('studentid'); ?>
											 <div class="col-sm-10">
											
                                            <select name="select" class="form-control" required="" id="batch_id" onchange="viewStudents()" name="bname1">
                                                    <option value="">Select Batch</option>
                                                      <?php     
                                                          foreach($bname as $b)
                                                          {
														  
														  if(isset($studid))
                                                            { 
															   echo $studid;
                                                              if( $studid == $b->batch_course_id)
                                                              {
															     echo $studid;
                                                              echo "<option  value=".$b->batch_course_id." selected>";
                                                             echo  $b->batch_name."--".$b->course_name;
                                                             echo"</option>";
                                                              }
                                                            
															else
															{

                                                            echo "<option  value=".$b->batch_course_id.">";
                                                             echo  $b->batch_name."--".$b->course_name;
                                                             
                                                             echo"</option>";
															 }
															 }
                                                          }
                                                        ?>
													<?php echo	$this->session->unset_userdata('studentid');?>
                                          
                                                      </select>                                                    
                                                </select>
                                            </div>
                                        </div>
                                    </form>
											 <div id="editspllist">
                                          
                            </div>
                           
                            <div class="row">
									 <!-- <div class="col-sm-2" id="addbtn">
										
									 </div> -->
									 <div class="col-sm-10" id="batchbtn">
												
									  </div>
									 
                                                                           <div>
                                                                            </br>
                                                                            </div>
									  <div id="batchlist" class="col-sm-12 table-responsive">
									  
									  </div>
									  <div id="button">
									  
									  </div>
                                </div>
                                </div>
                            </div>
                             
                            
                            <input type="hidden" name="checkstud_id[]" id="checkstud_id"> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<?php include("alljs.php"); ?>

    <script src="<?php echo base_url();?>assets/js/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url();?>assets/js/plugins/datatables/datatables-bs3.js"></script>

    <script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
    <script type="text/javascript">
    function viewStudents()
        {
            var bid = $("#batch_id").val();
		    //alert(bid);
                       $.ajax({
            url: "<?php echo base_url();?>superadmin/view_deletedstudents",
            type: "POST",
            async: true, 
            dataType:'json',
            data: {bid:bid}, //your form data to post goes here
             success: function(response){ 
			
                    var json = response;
                    var dataSet = [];
                    $('#editspllist').html('');
                    $('#editspllist').append('<div class="row"><div class="col-lg-12"><div class="portlet-body" style=><div class="table-responsive"><table id="example-table" class="table table-striped table-bordered table-hover table-green"><thead><tr><th>#</th><th><input type="checkbox" id="selectall" onclick="checkall();"></th><th>PRN </th><th>Name</th><th>Status</th> </tr></thead><tbody id="tbody">');
                    var j = 1;
                    for(var i = 0; i < json.length; i++) {
                        var obj = json[i];
                       if(obj.status==1)
                      {
                          data="<span class='badge green'>Active</span>"; 
                      }
                      else
                      {
                          data="<span class='badge red'>Block</span>";
                      }
                        
					
						
                        $('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" class="case" value="'+obj.credentials_id+'" onclick="checkindv();"></td><td>'+obj.PRN+'</td><td>'+obj.first_name+' '+obj.middle_name+' '+obj.last_name+'</td><td>'+data+'</td></tr>');
                        j++;
                    }
                    $('#editspllist').append('</tbody></table></div></div></div></div>');
					$('#example-table').dataTable({
					"data": dataSet,
					"coumns":[]
					});
					$('#addbtn').html('');
				$('#addbtn').append('<label><button type="submit" class="btn btn-default" onclick="multistudentadd()">Add  Selected</button></label>');
				$('#batchbtn').html('');
				$('#batchbtn').append('<label><button class="btn btn-default" id="all" onclick="viewBatch()">Assign to Batch</button></label>');	
        }
    });
    
}


 function show2(id)
        {
            var bid =id;
		    //alert("onload");
            $.ajax({
            url: "<?php echo base_url();?>superadmin/view_deletedstudents",
            type: "POST",
            async: true, 
            dataType:'json',
            data: {bid:bid}, //your form data to post goes here
             success: function(response){ 
			
                    var json = response;
                    var dataSet = [];
                    $('#editspllist').html('');
                    $('#editspllist').append('<div class="row"><div class="col-lg-12"><div class="portlet-body" style=><div class="table-responsive"><table id="example-table" class="table table-striped table-bordered table-hover table-green"><thead><tr><th>#</th><th><input type="checkbox" id="selectall" onclick="checkall();"></th><th>PRN </th><th>Name</th><th>Status</th></tr></thead><tbody id="tbody">');
                    var j = 1;
                    for(var i = 0; i < json.length; i++) {
                        var obj = json[i];
                       if(obj.status==1)
                      {
                          data="<span class='badge green'>Active</span>"; 
                      }
                      else
                      {
                          data="<span class='badge red'>Block</span>"; 
                      }
                        
					
						
                        $('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" class="case" value="'+obj.credentials_id+'" onclick="checkindv();"></td><td>'+obj.PRN+'</td><td>'+obj.first_name+' '+obj.middle_name+' '+obj.last_name+'</td><td>'+data+'</td></tr>');
                        j++;
                    }
                    $('#editspllist').append('</tbody></table></div></div></div></div>');
					$('#example-table').dataTable({
					"data": dataSet,
					"coumns":[]
					});
					$('#addbtn').html('');
				$('#addbtn').append('<label><button type="submit" class="btn btn-green" onclick="multistudentadd()">Add  Selected</button></label>');	
					
        }
    });
    
}




	function checkall()
	{
		//alert('ddd');	
		var checkAll = $("#selectall").prop('checked');
                    if (checkAll) {
                        $(".case").prop("checked", true);
              				$('input:checkbox[class=case]').each(function() 
								{    
   			 					if($(this).is(':checked'))
   			 					var checkVall = $(this).val()+",";
   			 					$("#checkstud_id").append(checkVall);
   			 					//alert(checkVall);
     			 					
								});
                    } else {
                        $(".case").prop("checked", false);
                        $("#checkstud_id").text('')
                    }
	}


            function checkindv(){
            	//alert('dd');
                if($(".case").length == $(".case:checked").length) {
                    $("#selectall").prop("checked", true);
                     $('input:checkbox[class=case]').each(function() 
								{    
   			 					if($(this).is(':checked'))
   			 					var checkVall = $(this).val()+",";
   			 					$("#checkstud_id").append(checkVall);
   			 					//alert(checkVall);
     			 					
								});
                    
                } else {
                	$("#checkstud_id").text('')
                    $("#selectall").prop("checked", false);
                    	$('input:checkbox[class=case]').each(function() 
								{    
   			 					if($(this).is(':checked'))
   			 					var checkVall = $(this).val()+",";
   			 					$("#checkstud_id").append(checkVall);
   			 					//alert(checkVall);
   			 					
								});
                }

            }
    		
        
        function assigntobatch() 
        {
        		var sid = $("#checkstud_id").text();
        		//alert(sid);
        		
        			var  asgnbatch_id=$("#selectbatchid").val();
        			var  batch_id=$("#batch_id").val();

 			swal({  title: "Are you sure?",   
          	type: "warning",   
          	showCancelButton: true,   
          	confirmButtonColor: "#DD6B55",   
          	confirmButtonText: "Yes, add it!",   
          	closeOnConfirm: false 
     		},
        		function(){   
            $.ajax({
            url: "<?php echo base_url();?>superadmin/addmultistudent",
            type: "POST",
			data:{id:sid,abatchid:asgnbatch_id},
            async: true, 
             success: function(response){ 
			    //alert(response);
                 if(response.indexOf("true") > -1 )
                 {
                    swal("Added!", "Students has been assigned successfully.", "success");                    
                    setTimeout(function(){location.reload(true);},2000);          
                 }
                 else
                 {
                    swal("Opps...!", "Somthing going wrong. can't add Student !!", "error"); 
                   location.reload();                                                 
                 }

        	}
    	});
    });
        		
        }
       
	function viewBatch()
		{
			
			//var bcid=$("#batch_id").val();
			var bcid='';
			var sid = $("#checkstud_id").text();
        		//alert(sid);
        		if(sid=="")
        		{
        			//alert("Please select record(s)");
        			swal("Oops..!", "Please select atlease one record", "error");
        			return false;
        		}
        		else {
        				$.ajax({
			url: "<?php echo base_url();?>superadmin/fetchbatch",
			type: "POST",
			async: true,
			dataType:'json',
		    data: { bcid:bcid},
         			
			//your form data to post goes here
			 success: function(response){
	
						var json = response;
						
						//alert(json);						
						$('#batchlist').html('');
                                        
						$('#batchlist').append('<br/><label for="Student" class="col-sm-3 control-label">Select Batch</label><div class="col-sm-9"><select id="selectbatchid" name="bname1" onchange="assigntobatch()" class="form-control"><option value="">Select</option>');
						
						var j = 1;
						for(var i = 0; i < json.length; i++) {
							var obj = json[i];
							
							$('#selectbatchid').append('<option value="'+obj.batch_course_id+'">'+obj.batch_name+'--'+obj.course_name+'</option>');
							j++;
						}
						
						$('#batchlist').append('</select>');
					
				}
			});
       }
			
		}
		
			
		
	$("#pages").addClass("in");
	//$("#managestudents").addClass("active");
	$("#deletedstudent").addClass("active");
	</script>
	
	
</body>

</html>