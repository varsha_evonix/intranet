<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>

<body>
<div id="wrapper">

		<?php include("header.php"); ?>

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Specializations
                                <small>Update Specialization</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a>
                                </li>
                                <li class="active">Update Specialization</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->

                   <div class="col-lg-12">
                   <div class="row">
                   	              
                    <!-- /.col-lg-12 -->
               
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                    <div class="col-lg-12">
						 <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
					{
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";

					}
                        else if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Update Specialization</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
									<?php
									//print_r($data);
									foreach($data as $r)
									{
									?>
                                <form class="form-horizontal" id="sky-form" role="form" method="post" action="<?php echo base_url();?>superadmin/updatespecialization/">
                                       
                                        <div class="form-group">
                                            <label for="cname" class="col-sm-4 control-label">Change Course <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                             <select class="form-control" name="cname" id="cname">
                                                       
													  <?php		
                                                          foreach($cname as $c)
			                                              {?>
				                                                <option <?php if($r->course_id == $c->course_id) echo "selected=selected";?> value="<?php echo $c->course_id; ?>"><?php echo $c->course_name; ?></option><?php
			                                              }
                                                       ?>
                                                      </select>

                                                    </div>
											</div>
                                        <div class="form-group">
                                            <label for="batchname" class="col-sm-4 control-label">Specialization Name<span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="sname" name="sname" value="<?php echo $r->specialization_name; ?>" placeholder="Enter Specialization Name" required />
                                            </div>
											</div>
                                  
											
								
										
										<div class="form-group">
												<label for="enabled" class="col-sm-4 control-label">Enabled</label>
												<div class="col-sm-7">
													<input type="checkbox" id="enabled" name="enabled" <?php if($r->status == 1) echo "checked='checked'"; ?> />
												</div>
										</div>
                                    
                                        <div class="form-group">
                                            <label class="col-sm-5 control-label"></label>
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn btn-default">Update</button>
												<input type="hidden" class="form-control" id="specialization_id" name="specialization_id" value="<?php echo $r->specialization_id; ?>" />
                                            </div>
                                        </div>
                                    </form><?php
									}?>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
<!-- /#wrapper -->

    <?php include("alljs.php"); ?>
	
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
		     
		    $.validator.addMethod("regex", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			 $.validator.addMethod("regeNUM", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
				var ALPHA_REGEX = "[a-zA-Z ]*$";
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					sname:
					{
					   //regex: ALPHA_REGEX,
					   required: true
					}
					
					
				},
									
				// Messages for form validation
				messages:
				{
					sname:
					{
					    //regex: '<span style="color:rgb(184, 18, 18); margin-left:340px;">Please enter alphabets only</span>',
						required: '<span style="color:rgb(184, 18, 18); margin-left:340px;">Please enter specialization name</span>'
						
					}
						
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
				
				
			});
		});
	</script>
</body>

</html>
