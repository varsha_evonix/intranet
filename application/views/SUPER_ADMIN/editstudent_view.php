<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>

<body>
<div id="wrapper">

		<?php include("header.php"); ?>

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Students
                                <small>Update Student Details</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="#">Dashboard</a>
                                </li>
                                <li class="active">Update Student</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->


                   <div class="row">
                   	              
                    <!-- /.col-lg-12 -->
               
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                    <div class="col-lg-12" >
					 <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
					{
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";

					}
                        else if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
                          
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Update Student</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
									<?php
									//print_r($stud);
									foreach($stud as $r)
									{
									?>
                                <form class="form-horizontal" id="sky-form" role="form" method="post" action="<?php echo base_url();?>superadmin/updatestudent/">
                                       
          						   <div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">PRN Number <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="prnno" name="prnno" style="width:90%"value="<?php echo $r->PRN; ?>" placeholder="Enter PRN No" maxlength="20" />
                                            </div>
                                     </div>
									   <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">First Name <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="fname" name="fname" style="width:90%"value="<?php echo $r->first_name; ?>" placeholder="Enter First Name" required />
                                            </div>
                                        </div>
										<div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">Middle Name</label>
                                            <div class="col-sm-7">
                                                <input type="text" style="width:90%" class="form-control" id="mname" name="mname" value="<?php echo $r->middle_name; ?>" placeholder="Enter Middle Name"/>
                                            </div>
                                        </div>
										
									   <div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">Last Name <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="lname" name="lname" style="width:90%" value="<?php echo $r->last_name; ?>" placeholder="Enter Last Name"/>
                                            </div>
                                        </div>
                                 <div class="form-group">	
                                        <div id="sandbox-container">
                                          <label for="dob" class="col-sm-3 control-label">Date of Birth <span style="color:#b81212">*</span></label>
										  <div class="col-sm-7">
										 <input class="form-control dateWork" type="text"  name="dob" placeholder="DD-MM-YYYY" readonly="readonly" style="width:90%" value="<?php echo date('d-m-Y',strtotime($r->DOB));?>">					  
                                              </div> 
											  </div>
											</div>
											<div class="form-group">
												
                                                    <label for="gender" class="col-sm-3 control-label">Gender</label>
													
                                                    <div class="col-sm-6">
                                                       
														<label class="radio-inline">
                                                           <input type="radio" name="gender" value="M" <?php if($r->gender=="Male"){ ?>checked <?php }?>> Male
                                                        </label>
														
                                                        
														<label class="radio-inline">
                                                           <input type="radio" name="gender" value="F" <?php if($r->gender=="Female"){ ?>checked <?php }?> > Female 
                                                        </label>
                                                
												</div>
										</div>
										  <div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">Allocated Email ID <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" style="width:90%" class="form-control" id="uname" name="uname" value="<?php echo $r->username; ?>" placeholder="Enter Username"/>
                                            </div>
                                        </div>
												 <div class="form-group">
                                            <label for="inyear" class="col-sm-3 control-label">Change Batch <span style="color:#b81212">*</span></label>
                                          
							               <div class="col-sm-7">
														<select class="form-control" name="bname1" style="width:90%">
                                                    
                                                      <?php		
                                                          foreach($bname as $b)
			                                              {?>
		                                                <option <?php if($r->batch_course_id == $b->batch_course_id) echo "selected=selected"; ?> value=" <?php echo $b->batch_course_id ;?>">
														 <?php    echo  $b->batch_name."--".$b->course_name ?>
															 
															 </option>
			                                             <?php }
                                                       ?>
                                                      </select>
                                               </div>
</div>											   
                                                                               
						
							         
								
										<div class="form-group">
												<label for="enabled" class="col-sm-3 control-label">Enabled</label>
												<div class="col-sm-9">
													<input type="checkbox" id="status" name="status" <?php if($r->status == 1) echo "checked='checked'"; ?> />
												</div>
										</div>
                                    
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-6">
                                                <button type="submit" class="btn btn-default">Update</button>
												<input type="hidden" class="form-control" id="credentials_id" name="credentials_id" value="<?php echo $r->credentials_id; ?>" />
												<input type="hidden" class="form-control" id="student_id" name="student_id" value="<?php echo $r->student_id; ?>" />
                                            </div>
                                        </div>
                                    </form><?php
									}?>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
<!-- /#wrapper -->

    <?php include("alljs.php"); ?>
	<script src=" <?php echo base_url(); ?>assets/js/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo/advanced-form-demo.js"></script>
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
			$(function()
		{
		
		
		     $.validator.addMethod("regex", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			 $.validator.addMethod("email", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			 $.validator.addMethod("regeNUM", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
				var ALPHA_REGEX = "[a-zA-Z ]*$";
				var alphaNumeric = "^[(a-z)(A-Z)(0-9)]+$";	
				var email="^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$";
			$("#sky-form").validate(
			{	

               
				// Rules for form validation
				rules:
				{
					
					prnno:
					{
					   regeNUM: alphaNumeric,
						required: true
					
					},
					fname:
					{
					   regex: ALPHA_REGEX,
					   required:true
					   
					},
					mname:
				    {
					regex: ALPHA_REGEX
					//required: true
					
				    },
					lname:
				    {
					   regex: ALPHA_REGEX,
					   required: true
					 
					}, 
					contact:
				    {
					required: true,
					number: true
				
				
				    },
					
					bname1:
					{
					  required:true,
					  min:1
					},
					
					uname:
					{
					  email:email, 
					  required: true
					}
			
						
					
					
				},
									
				// Messages for form validation
				messages:
				{
				
				    prnno:
					{
					   regeNUM: '<span style="color:#b81212; margin-left:265px;">Please enter alphanumeric charachters only</span>',
					   required: '<span style="color:#b81212; margin-left:265px;">Please enter prn number</span>'
						
					},
					fname:
					{
					    regex: '<span style="color:#b81212; margin-left:265px;">Please enter alphabets only</span>',
						required: '<span style="color:#b81212; margin-left:265px;">Please enter first name</span>'
						
					},
					mname:
					{
					   regex: '<span style="color:#b81212; margin-left:265px;">Please enter alphabets only</span>'
						//required: '<span style="color:#b81212; margin-left:265px;">Please enter middle name</span>'
						
			
					},
					
					lname:
					{
					   regex: '<span style="color:#b81212; margin-left:265px;">Please enter alphabets only</span>',
						required: '<span style="color:#b81212; margin-left:265px;">Please enter last name</span>'
						
					},
					
					contact:
					{
						required: '<span style="color:#b81212; margin-left:265px;">Please enter contact number</span>',
						number: '<span style="color:#b81212; margin-left:265px;">Please enter a valid number</span>'
						
						
					},
					bname1:
					{
					    required:'<span style="color:#b81212; margin-left:265px;">Please enter password</span>',
						min:'<span style="color:#b81212; margin-left:265px;">Please select Batch</span>'
					},
					
					uname:
					{
						required: '<span style="color:#b81212; margin-left:265px;">Please enter username</span>',
						email:'<span style="color:#b81212; margin-left:265px;">Please Enter a valid Email-id</span>'
						
					}
					
					
					
			
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
				
				
			});
		});
	</script>
</body>

</html>
