<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>

<body>
<div id="wrapper">

		<?php include("header.php"); ?>

        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Update course Details
                                <small></small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i>  <a href="#">Dashboard</a>
                                </li>
                                <li class="active">Update course</li>
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->


                   <div class="row">
                   	              
                    <!-- /.col-lg-12 -->
               
                    <!-- /.col-lg-12 -->
                    <!-- End Validation States -->

                    <!-- Validation Examples -->
                    <div class="col-lg-12">
						 <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
					{
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";

					}
                        else if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Update course</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div id="validationExamples" class="panel-collapse collapse in">
                                <div class="portlet-body">
									<?php
									//print_r($data);
									foreach($data as $r)
									{
									?>
                                <form class="form-horizontal" id="sky-form" role="form" method="post" action="<?php echo base_url();?>superadmin/updatecourse">
                                       
                                        <div class="form-group">
                                            <label for="batchname" class="col-sm-4 control-label">Course Name <span style="color:#b81212">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control" id="cname" name="cname" style="width:90%" value="<?php echo $r->course_name; ?>" placeholder="Enter Course Name" required />
                                            </div>
                                        </div>
									   
										
										<div class="form-group">
												<label for="enabled" class="col-sm-4 control-label">Enabled</label>
												<div class="col-sm-7">
													<input type="checkbox" id="status" name="status" <?php if($r->status == 1) echo "checked='checked'"; ?> />
												</div>
										</div>
                                  
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-4">
                                                <button type="submit" class="btn btn-default">Update</button>
												<input type="hidden" class="form-control" id="course_id" name="course_id" value="<?php echo $r->course_id; ?>" />
                                            </div>
                                        </div>
                                    </form><?php
									}?>
                                </div>
                            </div>
                        </div>
                        <!-- /.portlet -->
                    </div>
                    <!-- /.col-lg-12 -->
                    <!-- End Validation Examples -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
<!-- /#wrapper -->

    <?php include("alljs.php"); ?>
	
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
		     $.validator.addMethod("regex", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			 	var ALPHA_REGEX = "[a-zA-Z\-]*$";

			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					cname:
					{
					    //regex: ALPHA_REGEX,
						required: true
					}
					
				},
									
				// Messages for form validation
				messages:
				{
					cname:
					{
					    //regex:'<span style="color:rgb(184, 18, 18); margin-left:350px;">Please Enter alphanumeric characters only</span>',
						required: '<span style="color:rgb(184, 18, 18); margin-left:350px;">Please enter Course name</span>'
					}
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
				
				
			});
		});
	
	</script>
</body>

</html>
