<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>



<body>
<div id="wrapper">

		<?php include("header.php"); ?>



        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE ROW -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="page-title">
                            <h1>Manage Specializations
                              <small>Manage Specialization</small></h1>
                            <ol class="breadcrumb">
							 <li><i class="fa fa-dashboard"></i> 
								<a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a></li>
                                <li class="active">Manage Specialization</li>
                                    
                             
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
				
                <!-- /.row -->
                <!-- end PAGE TITLE ROW -->

                <!-- begin ADVANCED TABLES ROW -->
                <div class="row">

                    <div class="col-lg-12">
		 <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
                        <div class="portlet portlet-default">
                            <div class="portlet-heading">
                                <div class="portlet-title">
                                    <h4>Specializations List</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="portlet-body">
							

							
	                                <div class="table-responsive">
                                    <table id="example-table" class="table table-striped table-bordered table-hover table-green">
                                        <thead>
                                            <tr>
                                                <th>Specialization Name</th>
                                               
                                                 <th>Course Name</th>
                                                <th> Status </th>
												<th>Edit</th>
												<th>Delete</th>
                                                 												
                                            </tr>
                                        </thead>
                                        <tbody>
										
                                     <?php


                                       foreach($info as $i) 
                                       {
									   $sid= $i->specialization_id;
									      ?>
                                             
                                                     <tr id='td<?php echo $bid; ?>'><td><?php echo  $i->specialization_name;?></td><td>
														<?php echo  $i->course_name ;?></td><td>
<!-- 														 <?php if($i->status =="1") echo "ENABLED"; else echo"DISABLED"?></td><td> -->
                             <?php if($i->status =="1") echo " <span class='badge green'>Enabled</span>"; else echo"<span class='badge red'>Disabled</span>"?></td><td>                             
					<a href="<?php echo base_url();?>superadmin/editspecialization/<?php echo $i->specialization_id; ?>"><i class="fa fa-pencil"></i>&nbsp; EDIT</a></td><td>
					   <a onclick='deleteOrganization(<?php echo $sid; ?>)' title='Click Here To Delete' style="cursor: pointer;"><div class=''><i class='fa fa-times'></i> DELETE </a></td>
					</tr>
				

                                    <?php									  
									   } 
									   
									    
									?>


										
										
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <!-- /.portlet-body -->
                        </div>
                        <!-- /.portlet -->

                    </div>
                    <!-- /.col-lg-12 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->

    </div>
	    <?php include("alljs.php"); ?>
	
<script src="<?php echo base_url();?>assets/js/demo/advanced-tables-demo.js"></script>
	
	<script>
	function deleteOrganization(sid)
{

  

 swal({  title: "Are you sure?",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes, delete it!",   
          closeOnConfirm: false 
     },


      function(){ 
             	  
            $.ajax({
            url: "<?php echo base_url();?>superadmin/deletespecialization/"+sid,
            type: "POST",
            async: true, 
             success: function(response){ 
                if(response.indexOf("true")> -1)
                 {
                    swal("Deleted!", "Specialization has been deleted successfully.", "success");                    
                 setTimeout(function(){location.reload();},2000);                 
                 }
                 else
                 {
                    swal("Oops..!", "Somthing going wrong. can't delete Specialization !!", "error"); 
                    location.reload();                                                  
                 }

        }
    });


    });
}

	</script>
	
	<script>
	$("#tables").addClass("in");
	$("#managespecializations").addClass("active");
	$("#managespecialization").addClass("active");
	</script>
    

</body>

</html>
