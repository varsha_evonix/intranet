<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
	
	<style>
	.toggledDropDown
	{
		display: none; /* Hiding the optional drop down lists */
	}
	
	
	</style>
	<style>
.invalid{
  padding-left: 28%;
 <!-- color:rgb(184, 18, 18);-->
    
}
.form-control
{
  padding-left:2% !important;
}
</style>

</head>



<body>
<div id="wrapper">

		<?php include("header.php"); ?>

  
        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                    <div class="col-lg-12">
					
					
                        <div class="page-title">
						

                            <h1>Manage Batches
                                <small>Create Batch</small>
                            </h1>
                            <ol class="breadcrumb">
                                <li><i class="fa fa-dashboard"></i> 
								<a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a></li>
                                <li class="active">Create Batch</li>
                                    
                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE AREA -->

				
				
                <!-- begin DASHBOARD CIRCLE TILES -->
				 
				

			
                        <div class="row">

						 <div class="col-lg-12">

                            <!-- Basic Form Example -->
                             <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
							   <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
							       <h4>Create Batch</h4>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                     <div id="validationExamples" class="panel-collapse collapse in">
                                        <div class="portlet-body">
								
				
							
										
                           <form id="sky-form" class="form-horizontal" role="form" method="post" action="<?php echo base_url();?>superadmin/insertbatch">
                                                <div class="form-group">
                                                    <label for="bname" class="col-sm-3 control-label">Batch Name <span style="color:#b81212">*</span></label>
                                                    <div class="col-sm-7">
													<input type="text" class="form-control" id="bname" placeholder="Enter Batch Name" name="bname">
                                                </div>
												</div>
                                                <div class="form-group">
                                                    <label for="inyear" class="col-sm-3 control-label">Batch In year <span style="color:#b81212">*</span></label>
                                                     <div class="col-sm-7">
													<input type="text" class="form-control" id="inyear" placeholder="Enter Batch In Year" name="inyear" maxlength="4" >
                                                </div>
												</div>
												
			                                    <div class="form-group">
                                                    <label for="outyear" class="col-sm-3 control-label">Batch out year <span style="color:#b81212">*</span></label></label>
                                                     <div class="col-sm-7">
													<input type="text" class="form-control"  id="outyear" placeholder="Enter Batch Out Year" name="outyear" maxlength="4">
                                                </div>
												</div>
			                                    <div class="form-group">
												<label  for="cname" class="col-sm-3 control-label">Select Course <span style="color:#b81212">*</span></label>
    												   <div class="col-sm-7">
													  <select class="form-control" name="cname" id="1" onchange="speclist()">
													  <option value="0">Select Course</option>
                                                      <?php		
                                                          foreach($cname as $c)
			                                              {
				                                                echo  "<option value=".$c->course_id.">".$c->course_name."</option>";
			                                              }
                                                       ?>
                                                      </select>
                                                     </div>
                                                    </div>
													
											<div class="form-group">
											<label for="Student" class="col-sm-3 control-label"></label>
											<div id="speclist">
											</div>
											</div>

			                                    <div class="form-group">
                                                    <label for="terms" class="col-sm-3 control-label" >No of Terms <span style="color:#b81212;">*</span></label>
                                                      <div class="col-sm-7">
													<input type="text"  class="form-control" id="terms" placeholder="Enter total number of Terms" name="terms" >
                                                </div>
												</div>
												
												 <div class="form-group">
                                                    <label for="enabled" class="col-sm-3 control-label">Enabled</label>
                                               <div class="col-sm-7">
											   <input type="checkbox"   class="form-control" id="enable" value="1" name="enabled" checked="checked">
                                                </div>
												</div>
                                                                                                                                            <div class="form-group">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-4">
                                                <button  type="submit" class="btn btn-default">Submit</button>
												 </div>
												</div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

				
                <!-- end DASHBOARD CIRCLE TILES -->
				
                
                <!-- /.row -->
             <!-- /.col-lg-12 -->

                        </div>
                    </div>
                </div>

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
		


    </div>
<?php include("alljs.php"); ?>
	
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	
	<script type="text/javascript">
	
		function speclist()
		{
			//alert($("#1").val());
			var cname = $("#1").val();
			$.ajax({
        url: "<?php echo base_url();?>superadmin/fetchspeclist",
        type: "POST",
        async: true, 
		dataType:'json',
        data: {cname:cname}, //your form data to post goes here
         success: function(response){ 
		          // alert(response);
					var json = response;
					$('#speclist').html('');
					$('#speclist').append('<br><br><br><div id="4" class="col-sm-12 table-responsive toggledDropDown" style="display: block;margin-left: 23%;"><div id="checkboxerror"></div><div class="col-lg-8"><div class="portlet portlet-default" style="margin-left=154px;"><table id="example-table7" class="table table-striped table-bordered table-hover table-green" width="20%"><thead><tr><th width="190px;">Sr. No.</th><th width="190px;">Select</th><th width="290px">Specialization Name</th></tr></thead><tbody id="tbody">');
					
					var j = 1;
					for(var i = 0; i < json.length; i++) {
						var obj = json[i];
						
					//alert(obj.specialization_name);
						
						$('#tbody').append('<tr class="odd gradeX"><td>'+j+'</td><td><input type="checkbox" value='+obj.specialization_id+' id="spec" name="spec[]"></td><td>'+obj.specialization_name+'</td></tr>');
						j++;
						//$('#studlist').append('<div>'+obj.first_name+'</div>');
					}
					
					$('#speclist').append('</tbody></table></div></div></div>');
					
			}
    });
	
}


		$('#cname').change(function() {
		//alert();
		// Hide all drop downs sharing the CSS class "toggledDropDown".
		$('.toggledDropDown').hide();
		
		// Build a selector for the selected drop down
		var selector = ('#' + $(this).val());
		//alert(selector);
		// Show the selected drop down
		$(selector).show();

	});



		
	</script>
	<script type="text/javascript">
			$(function()
		{
		  $.validator.addMethod("regeNUM", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			var alphaNumeric = "/^[ A-Za-z0-9_@./#&+-]*$/";
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					"bname":
					{
					    
					   //regeNUM: alphaNumeric,
						required: true
					},
					"inyear":
				    {
					required: true,
					number: true,
					batchcheck: true
				    },
					"outyear":
				    {
					required: true,
					number: true,
					batchcheck: true
					
					}, 
					"cname":
					{
					 required:true,
					 min:1
					},
					"terms":
				    {
					required: true,
					number: true
				    },
					"spec[]":
					{
					  required:true
					}
					
				},
									
				// Messages for form validation
				messages:
				{
					"bname":
					{
						 //regeNUM: '<span style="color:#b81212; margin-left:-15px;">Please enter alphanumeric charachters only</span>',
						
						required: '<span style="color:#b81212; margin-left:-15px;">Please enter Batch name</span>'
						
					},
					"inyear":
					{
						required: '<span style="color:#b81212; margin-left:-15px;">Please enter In Year</span>',
						number: '<span style="color:#b81212; margin-left:-15px;">Please enter a valid number</span>'
					
			
					},
					
					"outyear":
					{
						required: '<span style="color:#b81212; margin-left:-15px;">Please enter out Year</span>',
						number: '<span style="color:#b81212; margin-left:-15px;">Please enter a valid number</span>'
					},
					"cname":
					{
					   required: '<span style="color:#b81212; margin-left:-15px;">Please select course</span>',
						min: '<span style="color:#b81212; margin-left:-15px">Please select course</span>'
					},
					"terms":
					{
						required: '<span style="color:#b81212; margin-left:-15px;">Please enter total number of terms</span>',
						number: '<span style="color:#b81212; margin-left:-15px;">Please enter a valid number</span>'
					},
					"spec[]":
					{
					   required:'<span style="color:#b81212; margin-left:-255px;">Please select specialization</span>'
					}
					
					
			
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
				   if($(element).attr("type")==="checkbox"){
				      $("#checkboxerror").html(error);
					  }
					  else
					  {
					 
					error.insertAfter(element.parent());
					}
				}
				
				
			});
			
			
			
$("#inyear").change(function()
			{
					
					var inyear = $("#inyear").val();
					
					var inyear1=inyear.length;
					
					if(inyear1!=4)
					{
						alert('Year should be 4 digit');
						$("#inyear").val('');
						
					}else{
						
						$("#inyear").val();
					}						
				
					
			});
			
					$("#outyear").change(function()
				{
					
					var inyear = $("#outyear").val();
					
					var inyear1=inyear.length;
					
					if(inyear1!=4)
					{
						alert('Year should be 4 digit');
						$("#outyear").val('');
						
					}else{
						
						$("#outyear").val();
					}						
				
					
			});			
			
				$.validator.addMethod('batchcheck',function(value, element)
				{
				return $('#inyear').val() < $('#outyear').val()
				
				}, "<span style='color:#b81212;'>Batch in Year should be less than Batch out year</span>");
				
				
		});
		

		
	</script>
	
<script>
	$("#pages2").addClass("in");
	$("#managebatches").addClass("active");
	$("#createbatch").addClass("active");
	</script>

	
</body>

</html>
