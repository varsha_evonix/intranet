<!DOCTYPE html>
<html lang="en">

<head>
	<?php include("alllinks.php"); ?>
</head>



<body>
<div id="wrapper">

		<?php include("header.php"); ?>

  
        <!-- begin MAIN PAGE CONTENT -->
        <div id="page-wrapper">

            <div class="page-content">

                <!-- begin PAGE TITLE AREA -->
                <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
                <div class="row">
                    <div class="col-lg-12">
					
					
                        <div class="page-title">
						

                            <h1>Manage Specializations
                                <small>Create Specialization</small>
                            </h1>
                            <ol class="breadcrumb">
							
                               <li><i class="fa fa-dashboard"></i>
							   <a href="<?php echo base_url() ?>superadmin/dashboard/">Dashboard</a></li>
                                <li class="active">Create Specialization</li>

                            </ol>
                        </div>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- end PAGE TITLE AREA -->

				
				
                <!-- begin DASHBOARD CIRCLE TILES -->
				 
				
				  <div class="col-lg-12">
			
                        <div class="row">

						

                            <!-- Basic Form Example -->
                            <div class="col-lg-12">
							 <div class='feedback'>
                    <?php if($this->session->flashdata('message'))
                     echo "
                        <script>
                        sweetAlert('Ok', '".$this->session->flashdata('message')."', 'success');
                        </script>";


                        if($this->session->flashdata('errormsg')) {
                     echo "
                        <script>
                        sweetAlert('Oops..!', '".$this->session->flashdata('errormsg')."', 'error');
                        </script>";

                    }
                        
                     ?>
                </div>
							   <div class="portlet portlet-default">
                                    <div class="portlet-heading">
                                        <div class="portlet-title">
							       <h4>Create Specialization</h4>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                     <div id="validationExamples" class="panel-collapse collapse in">
                                        <div class="portlet-body">
										
				
							
										
                           <form class="form-horizontal" id="sky-form" role="form" method="post" action="<?php echo base_url();?>superadmin/insertspecialization">
                                                
                                                <div class="form-group">
												<label for="cname" class="col-sm-4 control-label">Select Course <span style="color:#b81212">*</span></label>
    												  <div class="col-sm-7">
													 <select class="form-control" name="cname"  style="width:90%" id="cname">
													 <option selected="selected">
	Select Course
	</option>
                                                      <?php		
                                                          foreach($cname as $c)
			                                              {   echo  "<option value=".$c->course_id.">".$c->course_name."</option>";
			                                              }
                                                       ?>
                                                      </select>

                                                    </div>
													</div>
                                                <div class="form-group">
                                                    <label for="sname" class="col-sm-4 control-label">Specialization Name <span style="color:#b81212">*</span></label>
                                                    <div class="col-sm-7">
													<input type="text" class="form-control" id="sname" placeholder="Enter Specialization Name"  style="width:90%" name="sname">
                                                </div>
												</div>
                                              
			                                    
			                                    
												
												 <div class="form-group">
                                                    <label for="status" class="col-sm-4 control-label">Enabled</label>
                                               <div class="col-sm-7">
											   <input type="checkbox"   class="form-control" id="status" value="1" name="status" checked="checked">
                                                </div>
												</div>
												
												<div class="form-group">
                                                    <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-4">                  
                                                <button  type="submit" class="btn btn-default">Submit</button>
                                               </div>
                                        </div>
											</form>
											
                                        </div>
                                    </div>
                                </div>

				
                <!-- end DASHBOARD CIRCLE TILES -->
				
				
				
		
                
                <!-- /.row -->
             <!-- /.col-lg-12 -->

                        </div>
                    </div>
                </div>

            </div>
            <!-- /.page-content -->

        </div>
        <!-- /#page-wrapper -->
        <!-- end MAIN PAGE CONTENT -->
		


    </div>
<?php include("alljs.php"); ?>
	
	<!-- for validation -->
	<script src="<?php echo base_url();?>assets/js/demo/jquery.validate.min.js"></script>
	<script type="text/javascript">
		$(function()
		{
		     
		    $.validator.addMethod("regex", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
			 $.validator.addMethod("regeNUM", function(value, element, param) { return value.match(new RegExp("^" + param + "$")); });
				var ALPHA_REGEX = "[a-zA-Z ]*$";
			$("#sky-form").validate(
			{					
				// Rules for form validation
				rules:
				{
					
					sname:
					{
					   //regex: ALPHA_REGEX,
					   required: true
					},
					cname:
					{
					   required:true,
					   min:1
					}
					
					
					
				},
									
				// Messages for form validation
				messages:
				{
					sname:
					{
					    //regex: '<span style="color:rgb(184, 18, 18); margin-left: 340px;">Please enter alphabets only</span>',
						required: '<span style="color:rgb(184, 18, 18); margin-left:340px">Please enter specialization name</span>'
						
					},
					cname:
					{
					   required: '<span style="color:rgb(184, 18, 18); margin-left:340px;">Please select course</span>',
					   min:'<span style="color:rgb(184, 18, 18); margin-left:340px;">Please select course</span>'
						
					}
			
					
				},					
				
				// Do not change code below
				errorPlacement: function(error, element)
				{
					error.insertAfter(element.parent());
				}
				
				
			});
		});
	</script>


<script>
	$("#tables").addClass("in");
	$("#managespecializations").addClass("active");
	$("#createspecialization").addClass("active");
	</script>

</body>

</html>
